import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bloom/pages/user/account_login.dart';
import 'package:bloom/pages/plan_page.dart';
import 'package:bloom/pages/task_page.dart';
import 'package:bloom/pages/plan/plan_save.dart';
import 'package:bloom/pages/time_page.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/utilities/global_router.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));

    return MaterialApp(
      title: 'flower',
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(
            bodyColor: GlobalColors.lightBlack,
            displayColor: GlobalColors.lightBlack),
      ),
      navigatorKey: GlobalRouter.navigatorKey,
      routes: <String, WidgetBuilder>{
        '/plan': (BuildContext context) => const PlanPage(),
        '/task': (BuildContext context) => const TaskPage(),
        '/time': (BuildContext context) => const TimePage(),
        '/login': (BuildContext context) => const AccountLogin(),
        '/plan/save': (BuildContext context) => const PlanSave(),
      },
      //initialRoute: '/home',
      home: const PlanPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
