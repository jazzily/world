import 'dart:convert';
import 'author_label.dart';

class AuthorInfo {
  late String name;
  late String email;
  late String avatar;
  late String status;
  late List<AuthorLabel> labels;

  // 是否登录
  bool logged = false;

  // 本地读取
  bool loading = false;

  // 头像文件
  String profile = "https://z1.ax1x.com/2023/08/09/pPeJtsA.png";

  static AuthorInfo empty() {
    AuthorInfo result = AuthorInfo();
    result.name = "";
    result.email = "";
    result.status = "";
    result.avatar = "";
    result.profile = "";
    result.labels = [];

    result.logged = false;
    result.loading = false;
    return result;
  }

  static AuthorInfo fromJson(Map<String, dynamic> json) {
    AuthorInfo result = AuthorInfo();
    result.name = json["name"];
    result.email = json["email"];
    result.avatar = json["avatar"];
    result.status = json["status"];
    result.profile = json["profile"];
    result.logged = true;
    result.loading = true;

    // 标签
    if (json["labels"] != null) {
      List<AuthorLabel> labels = (json["labels"] as List)
          .map((e) => AuthorLabel.fromJson(e as Map<String, dynamic>))
          .toList();

      result.labels = labels;
    } else {
      result.labels = [];
    }
    return result;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["name"] = name;
    data["email"] = email;
    data["avatar"] = avatar;
    data["status"] = status;
    data["profile"] = profile;

    List list = [];
    for (AuthorLabel label in labels) {
      list.add(label.toJson());
    }
    data["labels"] = list;
    return data;
  }
}
