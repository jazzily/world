class AuthorLabel {
  late String id;
  late String name;
  late int count;

  static AuthorLabel fromJson(Map<String, dynamic> json) {
    AuthorLabel result = AuthorLabel();
    result.id = json["id"];
    result.name = json["name"];
    result.count = json["count"];
    return result;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    data["name"] = name;
    data["count"] = count;
    return data;
  }
}
