import 'package:bloom/model/schedule_event.dart';

/// 日历数据
class Calendar {
  String? day;

  // 名称：日期或节气
  String? name;

  // 全称：阴历月日
  String? fullName;

  // 周几
  String? week;

  // 标志：休 班 气
  String? sign;

  // 备注：高速免费
  String? remark;

  List<ScheduleEvent> events = [];

  Calendar();

  String toDate() {
    return "农历 $fullName 星期$week";
  }

  String toRemark() {
    return remark ?? "";
  }

  bool hasEvent() {
    return events.isNotEmpty;
  }

  addEvent(ScheduleEvent event) {
    events.add(event);
  }

  static final Calendar empty = ofItem("day", "", "", "", "", "");

  /// 从项目获得
  static Calendar ofItem(String? day, String? name, String? fullName,
      String? week, String? sign, String? remark) {
    Calendar ca = Calendar();
    ca.day = day;
    ca.name = name;
    ca.fullName = fullName;
    ca.week = week;
    ca.sign = sign;
    ca.remark = remark;
    return ca;
  }

  /// 从字符串获得
  /// 格式：[日期:2000-01-17],[阴历日:十一],[阴历月日:腊],[星期:一],[标志:节气休],[备注:高速免费]
  static Calendar ofString(String origin) {
    List<String> items = origin.split(",");
    return ofItem(items[0], items[1], items[2], items[3], items[4], items[5]);
  }
}
