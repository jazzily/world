import 'package:flutter/material.dart';
import 'package:bloom/model/schedule_elect.dart';
import 'package:bloom/model/schedule_one.dart';
import 'package:bloom/model/schedule_query.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/utilities/utility_storage.dart';
import 'package:bloom/model/global_calendar.dart';
import 'package:bloom/model/author_info.dart';

/// 用于提供全局数据支持
class GlobalData {
  static const String userToken = "USER_TOKEN";
  static const String userInfo = "USER_INFO";

  static const String eventScheduleDay = "schedule_day";
  static const String eventScheduleOne = "schedule_one";
  static const String eventScheduleInfo = "schedule_info";
  static const String eventMenuOpen = "menu_open";

  final List<Calendar> _calendars = [];
  final Map<String, String> _scheduleTypes = {
    "ONE_TIME": "一次性",
    "EVERY_DAY": "每天",
    "EVERY_WEEK": "每周",
    "EVERY_MONTH": "每月",
    "EVERY_YEAR": "每年"
  };
  final Map<String, String> _scheduleStates = {
    "PLANNING": "计划中",
    "FINISHED": "已完成",
  };

  late AuthorInfo _author = AuthorInfo.empty();
  late ScheduleElect _selectDay = ScheduleElect.today();
  final ScheduleOne _selectOne = ScheduleOne.empty();
  final ScheduleQuery _selectQuery = ScheduleQuery.empty();

  static final GlobalData instance = GlobalData._();

  GlobalData._();

  String getScheduleState(String code) {
    if (_scheduleStates.containsKey(code)) {
      return _scheduleStates[code]!;
    }
    return "";
  }

  Map<String, String> getScheduleTypes() {
    return _scheduleTypes;
  }

  String getScheduleType(String code) {
    if (_scheduleTypes.containsKey(code)) {
      return _scheduleTypes[code]!;
    }
    return "";
  }

  // 读取文件获取数据
  Future<List<Calendar>> readCalendar(BuildContext context) async {
    // 日历为空，则读取文件
    if (_calendars.isEmpty) {
      await DefaultAssetBundle.of(context)
          .loadString("assets/calendar.data")
          .then((value) {
        _calendars.clear();
        List<String> lines = value.split('\n');
        for (var element in lines) {
          Calendar ca = Calendar.ofString(element);
          _calendars.add(ca);
        }
      });
    }

    return _calendars;
  }

  // 读取账号数据
  Future<AuthorInfo> readAccount() async {
    var json = await UtilityStorage.get(GlobalData.userInfo);
    if (json.isEmpty) {
      return AuthorInfo.empty();
    }

    _author = AuthorInfo.fromJson(json);
    _author.loading = true;
    return _author;
  }

  // 写入账号
  Future<AuthorInfo> writeAccount(dynamic account) async {
    _author = AuthorInfo.fromJson(account);
    // 写入缓存
    await UtilityHttp.image(_author.avatar)
        .then((image) => _author.profile = image);

    UtilityStorage.set(GlobalData.userInfo, _author.toJson());
    return _author;
  }

  // 清空账号
  AuthorInfo clearAccount() {
    // 删除token
    UtilityStorage.remove(GlobalData.userToken);
    // 修改登录状态-下次登录使用email作为默认值
    _author.logged = false;
    UtilityStorage.set(GlobalData.userInfo, _author.toJson());
    return _author;
  }

  // 读取日程日期
  ScheduleElect readTaskDay() {
    return _selectDay;
  }

  // 写入日程日期
  writeTaskDay(DateTime selectDay) {
    _selectDay = ScheduleElect.fromDay(selectDay);
  }

  // 读取日程信息
  ScheduleOne readTaskInfo() {
    return _selectOne;
  }

  // 写入日程日期
  writeTaskInfo(String id) {
    _selectOne.id = id;
  }

  // 对于日程查询
  readTaskQuery() {
    return _selectQuery;
  }

  // 写入日程查询
  writeTaskQuery(String label) {
    _selectQuery.label = label;
  }
}
