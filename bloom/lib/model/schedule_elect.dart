import 'package:bloom/utilities/utility_date.dart';

class ScheduleElect {
  late DateTime currentDate;

  // 日历月份-显示用
  late String selectMonth;

  // 日历日期-新建用
  late String selectDate;

  // 当前月份
  late bool current;

  static ScheduleElect today() {
    return fromDay(DateTime.now());
  }

  static ScheduleElect fromDay(DateTime focusedDay) {
    ScheduleElect model = ScheduleElect();

    model.currentDate = focusedDay;
    model.selectMonth = "${focusedDay.year}年${focusedDay.month}月";
    model.selectDate = UtilityDate.toDateString(focusedDay);

    DateTime today = DateTime.now();
    model.current =
        (focusedDay.year == today.year && focusedDay.month == today.month);
    return model;
  }
}
