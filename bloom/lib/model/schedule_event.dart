import 'package:bloom/utilities/utility_date.dart';

class ScheduleEvent {
  late String id;
  late String name;
  late String title;
  late String state;
  late String startTime;
  late String finishTime;

  late String scheduleTitle;

  // 日期：年月日，用于比较
  String day = "";

  static String drawDay(Map<String, dynamic> json) {
    return UtilityDate.getDateString(json["startTime"]);
  }

  static ScheduleEvent fromJson(Map<String, dynamic> json) {
    ScheduleEvent result = ScheduleEvent();
    result.id = json["id"];
    result.name = json["title"];
    result.state = json["state"];

    String startTime = UtilityDate.getTimeString(json["startTime"]);
    String finishTime = UtilityDate.getTimeString(json["finishTime"]);
    if (startTime == "00:00" && finishTime == "00:00") {
      result.title = "全天";
    } else {
      result.startTime = startTime;
      result.finishTime = finishTime;
      result.title = "$startTime - $finishTime";
    }
    result.day = UtilityDate.getDateString(json["startTime"]);
    result.scheduleTitle = json["scheduleTitle"];
    return result;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    return data;
  }
}
