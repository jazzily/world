class ScheduleEventOne {
  late String id;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    return data;
  }

  static ScheduleEventOne fromId(String id) {
    ScheduleEventOne one = ScheduleEventOne();
    one.id = id;
    return one;
  }
}
