import 'package:bloom/pages/page/about_issue.dart';
import 'package:bloom/pages/page/about_logout.dart';
import 'package:bloom/pages/page/about_privacy.dart';
import 'package:bloom/pages/page/about_usage.dart';
import 'package:flutter/material.dart';
import 'package:bloom/themes/global_colors.dart';

class AboutInfo extends StatefulWidget {
  const AboutInfo({key}) : super(key: key);

  @override
  State<AboutInfo> createState() => _AboutInfoState();
}

class _AboutInfoState extends State<AboutInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text(
          "关于",
          style: TextStyle(fontSize: 18),
        ),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                size: 24,
              ),
              onPressed: () {
                Navigator.pop(context);
              });
        }),
      ),
      body: Container(
        padding: const EdgeInsets.only(top: 50, bottom: 10),
        child: Column(children: [
          Container(
            width: 80,
            height: 80,
            margin: const EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: const DecorationImage(
                    image: AssetImage("assets/icon_logo.png"),
                    fit: BoxFit.cover)),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(bottom: 10),
            child: const Text("版本 1.0.0"),
          ),
          Container(
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height - 600),
            padding: const EdgeInsets.only(top: 100, left: 30, right: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
            ),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AboutUsage()));
                  },
                  child: Container(
                    height: 42,
                    color: Colors.white,
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Row(
                      children: const <Widget>[
                        Icon(
                          Icons.local_library,
                          weight: 28,
                          color: Colors.blue,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Text("用户协议"),
                        Expanded(child: SizedBox()),
                        Icon(
                          Icons.keyboard_arrow_right,
                          weight: 28,
                        ),
                      ],
                    ),
                  ),
                ),
                const Divider(
                  height: 2,
                  indent: 0,
                  color: GlobalColors.lightGrep,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AboutPrivacy()));
                  },
                  child: Container(
                    height: 42,
                    color: Colors.white,
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Row(
                      children: const <Widget>[
                        Icon(
                          Icons.new_releases,
                          weight: 28,
                          color: Colors.blue,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Text("隐私政策"),
                        Expanded(child: SizedBox()),
                        Icon(
                          Icons.keyboard_arrow_right,
                          weight: 28,
                        ),
                      ],
                    ),
                  ),
                ),
                const Divider(
                  height: 2,
                  indent: 0,
                  color: GlobalColors.lightGrep,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AboutIssue()));
                  },
                  child: Container(
                    height: 42,
                    color: Colors.white,
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Row(
                      children: const <Widget>[
                        Icon(
                          Icons.draw,
                          weight: 28,
                          color: Colors.blue,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Text("意见反馈"),
                        Expanded(child: SizedBox()),
                        Icon(
                          Icons.keyboard_arrow_right,
                          weight: 28,
                        ),
                      ],
                    ),
                  ),
                ),
                const Divider(
                  height: 2,
                  indent: 0,
                  color: GlobalColors.lightGrep,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AboutLogout()));
                  },
                  child: Container(
                    height: 42,
                    color: Colors.white,
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Row(
                      children: const <Widget>[
                        Icon(
                          Icons.power_settings_new,
                          weight: 28,
                          color: Colors.blue,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Text("注销账户"),
                        Expanded(child: SizedBox()),
                        Icon(
                          Icons.keyboard_arrow_right,
                          weight: 28,
                        ),
                      ],
                    ),
                  ),
                ),
                const Divider(
                  height: 2,
                  indent: 0,
                  color: GlobalColors.lightGrep,
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.only(top: 40),
            child: const Text("Copyright @2023 Jimmy.Zhang"),
          ),
        ]),
      ),
    );
  }
}
