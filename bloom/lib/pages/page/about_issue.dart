import 'package:bloom/pages/page/about_issue_model.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:flutter/material.dart';

class AboutIssue extends StatefulWidget {
  const AboutIssue({key}) : super(key: key);

  @override
  State<AboutIssue> createState() => _AboutIssueState();
}

class _AboutIssueState extends State<AboutIssue> {
  AboutIssueModel model = AboutIssueModel.empty();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: const Text(
            "意见反馈",
            style: TextStyle(fontSize: 18),
          ),
          centerTitle: true,
          leading: Builder(builder: (context) {
            return IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  size: 24,
                ),
                onPressed: () {
                  Navigator.pop(context);
                });
          }),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 50, left: 10, right: 10),
            child: Column(children: [
              const Text("TI.ME.FLOW",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  )),
              const Text("一切都是最好的安排",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                  )),
              const Padding(padding: EdgeInsets.only(top: 20)),
              TextField(
                maxLength: 32,
                decoration: const InputDecoration(
                    icon: Icon(Icons.draw), hintText: "反馈标题"),
                controller: TextEditingController(text: model.title),
                onChanged: (text) {
                  model.title = text;
                },
              ),
              TextField(
                maxLength: 32,
                decoration: const InputDecoration(
                    icon: Icon(Icons.account_circle), hintText: "联系方式"),
                onChanged: (text) {
                  model.contact = text;
                },
              ),
              TextField(
                maxLines: 5,
                maxLength: 200,
                decoration: const InputDecoration(
                  hintText: "反馈内容",
                ),
                onChanged: (text) {
                  model.content = text;
                },
              ),
              SizedBox(
                width: 300,
                child: TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.resolveWith((states) {
                      return GlobalColors.submitForeground;
                    }),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                    ),
                    backgroundColor:
                        MaterialStateProperty.resolveWith((states) {
                      return GlobalColors.submitBackground;
                    }),
                  ),
                  child: const Text(
                    "提交",
                  ),
                  onPressed: () {
                    submitIssue();
                  },
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 10),
                child: const Text("https://gitee.com/jazzily/world/issues"),
              ),
            ]),
          ),
        ));
  }

  void submitIssue() {
    UtilityHttp.post("/api/issue", model).then((data) {
      if (data.isSuccess()) {
        var snackBar = const SnackBar(content: Text("虚心竹有低头叶 傲骨梅无仰面花"));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.of(context).pop(context);
      }
    });
  }
}
