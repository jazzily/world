class AboutIssueModel {
  String title = "";
  String contact = "";
  String content = "";

  AboutIssueModel();

  static AboutIssueModel empty() {
    AboutIssueModel model = AboutIssueModel();
    model.title = "";
    model.contact = "";
    model.content = "";
    return model;
  }

  AboutIssueModel.fromJson(Map<String, dynamic> json) {
    title = json["title"];
    contact = json["contact"];
    content = json["authCode"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["title"] = title;
    data["contact"] = contact;
    data["content"] = content;
    return data;
  }

  valid(){
    if(title.isEmpty){
      title="(无)";
    }
    if(contact.isEmpty){
      contact = "(无)";
    }
  }
}
