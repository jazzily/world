import 'dart:async';
import 'package:bloom/pages/page/about_logout_model.dart';
import 'package:bloom/pages/user/account_login.dart';
import 'package:bloom/themes/global_dialog.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:flutter/material.dart';
import 'package:bloom/themes/global_colors.dart';

class AboutLogout extends StatefulWidget {
  const AboutLogout({key}) : super(key: key);

  @override
  State<AboutLogout> createState() => _AboutLogoutState();
}

class _AboutLogoutState extends State<AboutLogout> {
  AboutLogoutModel model = AboutLogoutModel.empty();

  // 计时器
  Timer? _countTimer;
  var _countDown = 0;

  _AboutLogoutState() {
    GlobalData.instance.readAccount().then((info) {
      setState(() {
        model.userName = info.email;
      });
    });
  }

  void startCountTimer() {
    _countDown = 60;
    _countTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (_countDown < 1) {
          timer.cancel();
        } else {
          _countDown--;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: const Text(
            "注销账户",
            style: TextStyle(fontSize: 18),
          ),
          centerTitle: true,
          leading: Builder(builder: (context) {
            return IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  size: 24,
                ),
                onPressed: () {
                  Navigator.pop(context);
                });
          }),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 100, left: 10, right: 10),
            child: Column(children: [
              const Text("L.OG.OUT",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  )),
              const Text("注销后 所有数据均会被删除且无法恢复",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                  )),
              const Padding(padding: EdgeInsets.only(top: 50)),
              TextField(
                maxLength: 32,
                readOnly: true,
                decoration: const InputDecoration(
                    icon: Icon(Icons.account_circle), hintText: "邮箱号"),
                controller: TextEditingController(text: model.userName),
                onChanged: null,
              ),
              TextField(
                maxLength: 32,
                decoration: InputDecoration(
                    icon: const Icon(Icons.verified_user_rounded),
                    hintText: "验证码",
                    suffixIcon: IgnorePointer(
                      ignoring: _countDown > 0,
                      child: TextButton(
                          child:
                              Text(_countDown == 0 ? "发送" : "${_countDown}s"),
                          onPressed: () {
                            // 发送验证码
                            startCountTimer();
                            UtilityHttp.get("/api/message/email", query: {
                              "target": model.userName.trim(),
                              "category": "LOGOUT_EMAIL"
                            }).then((data) {
                              var message =
                                  data.isSuccess() ? "发送成功" : data.message;
                              var snackBar = SnackBar(content: Text(message));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                            });
                          }),
                    )),
                onChanged: (text) {
                  model.authCode = text;
                },
              ),
              Container(
                padding: const EdgeInsets.only(top: 10),
                width: 300,
                child: TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.resolveWith((states) {
                      return GlobalColors.lightOrange;
                    }),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                    ),
                    backgroundColor:
                        MaterialStateProperty.resolveWith((states) {
                      return GlobalColors.submitBackground;
                    }),
                  ),
                  child: const Text(
                    "确认注销",
                  ),
                  onPressed: () {
                    globalDialog
                        .showConfirmDialog(context, "注销账号", "您确认要注销账号吗")
                        .then((confirm) {
                      if (confirm != null && confirm) {
                        logout();
                      }
                    });
                  },
                ),
              )
            ]),
          ),
        ));
  }

  void logout() {
    UtilityHttp.post("/api/mine/logout", model).then((data) {
      if (data.isSuccess()) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const AccountLogin()),
            ModalRoute.withName("login"));
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (_countTimer != null) {
      _countTimer!.cancel();
    }
  }
}
