class AboutLogoutModel {
  String userName = "";
  String authCode = "";

  AboutLogoutModel();

  static AboutLogoutModel empty() {
    AboutLogoutModel model = AboutLogoutModel();
    model.userName = "";
    model.authCode = "";
    return model;
  }

  AboutLogoutModel.fromJson(Map<String, dynamic> json) {
    userName = json["userName"];
    authCode = json["authCode"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["userName"] = userName;
    data["authCode"] = authCode;
    return data;
  }
}
