import 'package:flutter/material.dart';

class AboutPrivacy extends StatelessWidget {
  const AboutPrivacy({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text(
          "隐私协议",
          style: TextStyle(fontSize: 18),
        ),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                size: 24,
              ),
              onPressed: () {
                Navigator.pop(context);
              });
        }),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding:
              const EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child:
                        const Text("用户隐私协议", style: TextStyle(fontSize: 20))),
                const Text(
                  "您的隐私对我们（“我们”或“我们的”）很重要。并且我们致力于保护您的隐私。因此，我们制定了隐私政策。本隐私政策（“隐私政策”）解释了当您使用软件和服务时，我们如何处理您的信息并保护您的隐私，以及与您的信息有关的可用权利和选项。我们认为，您有权了解我们在使用软件和服务（“软件和服务”）时可能收集和使用的信息的做法。如果您不同意此处设定的条款和条件，请不要使用该软件和服务。",
                ),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child: const Text("一、我们处理的信息以及处理您的信息的目的",
                        style: TextStyle(fontSize: 16))),
                const Text(
                  "我们将处理两大类信息。我们的使命是不断改进我们的软件和服务，并为您提供新的或更好的体验。作为此任务的一部分，我们将您的信息用于以下目的。如果出现用于处理您的个人数据的任何新目的，我们将通过对本隐私政策进行相应的更改，在开始处理有关该新目的的信息之前通知您。",
                ),
                const Text(
                  "1.1您提交的信息",
                ),
                const Text(
                  "（1）邮箱地址。通常，为了向您提供账号相关功能（例如安全登录，账号变更），我们将向您的邮箱地址发送邮件。对于此类邮件，我们不会与任何第三方共享。此信息对于您与我们之间的合同的适当履行是必要的。",
                ),
                const Text(
                  "（2）个人数据。您的任何个人数据，包括所有日程，我们不会与任何第三方共享。",
                ),
                const Text(
                  "（3）支持相关信息。如果您通过反馈与我们联系，我们可能会收集您的联系信息，您的反馈以及您自愿提供的与此支持有关的任何信息。此类信息仅用于为您提供支持服务，不会与通过软件和服务从您那里收集的任何其他信息相关联。",
                ),
                const Text(
                  "1.2自动处理的信息",
                ),
                const Text(
                  "（1）当您使用软件和服务时，我们将收集以下信息：通用唯一标识符（UUID），移动设备的类型以及安装在移动设备上的应用程序和操作系统版本；国家，语言；",
                ),
                const Text(
                  "（2）元数据。元数据可以描述照片或其他文件中包含的位置信息和文件创建时间，或者照片中字符的粗略特征以及照片中包含的其他内容。我们使用照片元数据为您提供某些功能，包括精确的“位置信息”，以及其他基于这些基本功能的自定义功能。",
                ),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child:
                        const Text("二、保护个人信息", style: TextStyle(fontSize: 16))),
                const Text(
                  "我们采取预防措施，包括行政，技术和物理措施，以保护您的个人信息免遭丢失，盗窃和滥用以及未经授权的访问，披露，更改和破坏。",
                ),
                const Text(
                  "确保您的个人信息安全；我们会向所有员工传达我们的隐私和安全准则，并严格执行公司内部的隐私保护措施。",
                ),
                const Text(
                  "不幸的是，互联网上的传输方法或电子存储方法都不是100％安全的。我们尽力保护您的个人信息，但是，我们不能保证其绝对安全。如果您的个人信息因安全受到破坏而被盗用，我们将立即按照适用法律通知您。",
                ),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child: const Text("三、与第三方共享信息",
                        style: TextStyle(fontSize: 16))),
                const Text(
                  "我们不会共享我们从您那里收集的任何个人信息",
                ),
                const Text(
                  "3.1如果法律要求我们披露您的信息，我们可能会视需要与执法机构或其他主管当局和任何第三方共享您的信息（例如，检测，预防或以其他方式解决欺诈，安全或技术问题） ；回应要求或满足任何法律程序，传票或政府要求；或保护小组用户，合作伙伴或公众的权利，财产或人身安全）；",
                ),
                const Text(
                  "3.2如果团队经历了业务过渡，例如另一家公司的合并或收购，合并，控制权变更，重组或出售其全部或部分资产，则您的信息将包含在转让的资产中。",
                ),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child: const Text("四、我们将如何保留您的个人数据",
                        style: TextStyle(fontSize: 16))),
                const Text(
                  "我们通常会保留您的个人信息，直到您与我们之间履行合同并遵守我们的法律义务。如果您不再希望我们使用我们实际访问和存储的信息，则可以通过注销账号要求我们擦除您的个人信息并关闭您的帐户。",
                ),
                const Text(
                  "但是，如果为了遵守法律义务（征税，会计，审计）或为了维护安全和数据备份设置，防止欺诈或其他恶意行为而需要信息，某些数据可能仍会存储一段时间。",
                ),
                Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child:
                        const Text("五、更改隐私政策", style: TextStyle(fontSize: 16))),
                const Text(
                  "本隐私政策可能会不时更改。任何更改都将在软件界面上发布。您持续使用软件和服务将被视为您接受此类更新。",
                ),
                const Text(
                  "在本协议中未声明的其他一切权利，仍归本公司所有。本公司保留对本协议的最终解释权利。",
                ),
              ]),
        ),
      ),
    );
  }
}
