import 'dart:async';
import 'package:bloom/utilities/global_json.dart';
import 'package:flutter/material.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/model/author_info.dart';
import 'mine_account_model.dart';

class MineAccount extends StatefulWidget {
  final AuthorInfo author;

  const MineAccount({key, required this.author}) : super(key: key);

  @override
  State<MineAccount> createState() => _MineAccountState();
}

class _MineAccountState extends State<MineAccount> {
  MineAccountModel model = MineAccountModel.empty();

  // 控制器
  var hidePassword = true;
  var authMessage = false;

  // 计时器
  Timer? _countTimer;
  var _countDown = 0;

  @override
  void initState() {
    super.initState();
    AuthorInfo author = widget.author;
    setState(() {
      model.userName = author.email;
    });
  }

  void startCountTimer() {
    _countDown = 60;
    _countTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (_countDown < 1) {
          timer.cancel();
        } else {
          _countDown--;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text(
          "设置密码",
          style: TextStyle(fontSize: 18),
        ),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                size: 24,
              ),
              onPressed: () {
                Navigator.pop(context);
              });
        }),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 20),
          child: Column(children: [
            Row(
              children: const [
                SizedBox(width: 20),
                Icon(Icons.circle, color: Colors.blue, size: 12),
                SizedBox(width: 5),
                Text("邮箱",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: GlobalColors.lightBlack))
              ],
            ),
            Row(children: [
              const SizedBox(width: 20),
              Expanded(
                child: TextField(
                  style: const TextStyle(fontSize: 14),
                  readOnly: true,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                  ),
                  controller: TextEditingController(text: model.userName),
                ),
              ),
              const SizedBox(width: 10),
            ]),
            Container(height: 4, color: GlobalColors.lightGrep),
            Row(children: const [
              SizedBox(width: 20),
              Icon(Icons.circle, color: Colors.blue, size: 12),
              SizedBox(width: 5),
              Text("密码",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ))
            ]),
            Row(children: [
              const SizedBox(width: 20),
              Expanded(
                child: TextField(
                  maxLength: 24,
                  obscureText: hidePassword,
                  style: const TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                    hintText: "必填，至少6位字符",
                    border: InputBorder.none,
                    suffixIcon: IconButton(
                      icon: Icon(hidePassword
                          ? Icons.visibility
                          : Icons.visibility_off),
                      onPressed: () {
                        setState(() {
                          hidePassword = !hidePassword;
                        });
                      },
                    ),
                  ),
                  onChanged: (text) {
                    model.password = text;
                  },
                ),
              ),
              const SizedBox(width: 10),
            ]),
            Container(height: 4, color: GlobalColors.lightGrep),
            Row(children: const [
              SizedBox(width: 20),
              Icon(Icons.circle, color: Colors.blue, size: 12),
              SizedBox(width: 5),
              Text("验证码",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ))
            ]),
            Row(children: [
              const SizedBox(width: 20),
              Expanded(
                child: TextField(
                  maxLength: 24,
                  style: const TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                      hintText: "必填，邮箱验证码",
                      border: InputBorder.none,
                      suffixIcon: IgnorePointer(
                        ignoring: _countDown > 0,
                        child: TextButton(
                            child:
                                Text(_countDown == 0 ? "发送" : "${_countDown}s"),
                            onPressed: () {
                              // 发送验证码
                              startCountTimer();
                              UtilityHttp.get("/api/message/email", query: {
                                "target": model.userName.trim(),
                                "category": "CHANGE_EMAIL"
                              }).then((data) {
                                var message =
                                    data.isSuccess() ? "发送成功" : data.message;
                                var snackBar = SnackBar(content: Text(message));
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              });
                            }),
                      )),
                  onChanged: (text) {
                    model.authCode = text;
                  },
                ),
              ),
              const SizedBox(width: 10),
            ]),
            SizedBox(
                width: MediaQuery.of(context).size.width,
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        GlobalColors.submitBackground),
                    shape: MaterialStateProperty.all(
                        const RoundedRectangleBorder(
                            borderRadius: BorderRadius.zero)),
                  ),
                  child: const Text(
                    "提交",
                    style: TextStyle(color: GlobalColors.submitForeground),
                  ),
                  onPressed: () {
                    GlobalJson valid = model.validate();
                    if (valid.noSuccess()) {
                      var snackBar = SnackBar(content: Text(valid.message));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      return;
                    }

                    UtilityHttp.put("/api/mine/account", model).then((data) {
                      if (data.isSuccess()) {
                        Navigator.pop(context);
                      }
                    });
                  },
                )),
          ]),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (_countTimer != null) {
      _countTimer!.cancel();
    }
  }
}
