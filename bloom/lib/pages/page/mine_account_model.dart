import 'package:bloom/utilities/global_json.dart';

class MineAccountModel {
  String userName = "";
  String password = "";
  String authCode = "";

  MineAccountModel();

  static MineAccountModel empty() {
    MineAccountModel model = MineAccountModel();
    model.userName = "";
    model.password = "";
    model.authCode = "";
    return model;
  }

  MineAccountModel.fromJson(Map<String, dynamic> json) {
    userName = json["userName"];
    password = json["password"];
    authCode = json["authCode"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["userName"] = userName;
    data["password"] = password;
    data["authCode"] = authCode;
    return data;
  }

  GlobalJson validate() {
    GlobalJson result = GlobalJson.result();
    if (password == "") {
      result.code = "password";
      result.message = "密码不能为空";
    }
    if (authCode == "") {
      result.code = "authCode";
      result.message = "验证码不能为空";
    }

    return result;
  }
}
