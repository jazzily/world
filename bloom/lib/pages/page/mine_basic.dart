import 'dart:io';
import 'package:flutter/material.dart';

import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/model/author_info.dart';

class MineBasic extends StatefulWidget {
  final AuthorInfo author;

  const MineBasic({key, required this.author}) : super(key: key);

  @override
  State<MineBasic> createState() => _MineBasicState();
}

class _MineBasicState extends State<MineBasic> {
  @override
  Widget build(BuildContext context) {
    AuthorInfo author = widget.author;

    DecorationImage image = author.profile.isNotEmpty
        ? DecorationImage(
        image: FileImage(File(author.profile)), fit: BoxFit.cover)
        : const DecorationImage(
        image: AssetImage("assets/header_none.png"), fit: BoxFit.cover);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text(
          "个人信息",
          style: TextStyle(fontSize: 18),
        ),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                size: 24,
              ),
              onPressed: () {
                Navigator.pop(context);
              });
        }),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 20),
          child: Column(children: [
            Container(
              width: 80,
              height: 80,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: image),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 10),
              child: TextField(
                style: const TextStyle(fontSize: 14),
                decoration: const InputDecoration(
                    hintText: "仅支持网络地址",
                    border: InputBorder.none,
                    suffixIcon: Icon(Icons.edit_note)),
                controller: TextEditingController(text: author.avatar),
                onChanged: (text) {
                  author.avatar = text;
                },
              ),
            ),
            Container(height: 4, color: GlobalColors.lightGrep),
            Row(
              children: const [
                SizedBox(width: 20),
                Icon(Icons.circle, color: Colors.blue, size: 12),
                SizedBox(width: 5),
                Text("姓名",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: GlobalColors.lightBlack))
              ],
            ),
            Row(children: [
              const SizedBox(width: 20),
              Expanded(
                child: TextField(
                  maxLength: 12,
                  style: const TextStyle(fontSize: 14),
                  decoration: const InputDecoration(
                    hintText: "如其人，人如其名",
                    border: InputBorder.none,
                    suffixIcon: Icon(Icons.edit_note),
                  ),
                  controller: TextEditingController(text: author.name),
                  onChanged: (text) {
                    author.name = text;
                  },
                ),
              ),
              const SizedBox(width: 10),
            ]),
            Container(height: 4, color: GlobalColors.lightGrep),
            Row(children: const [
              SizedBox(width: 20),
              Icon(Icons.circle, color: Colors.blue, size: 12),
              SizedBox(width: 5),
              Text("状态",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ))
            ]),
            Row(children: [
              const SizedBox(width: 20),
              Expanded(
                child: TextField(
                  maxLength: 24,
                  style: const TextStyle(fontSize: 14),
                  decoration: const InputDecoration(
                      hintText: "始吾于人也，听其言而信其行",
                      border: InputBorder.none,
                      suffixIcon: Icon(Icons.edit_note)),
                  controller: TextEditingController(text: author.status),
                  onChanged: (text) {
                    author.status = text;
                  },
                ),
              ),
              const SizedBox(width: 10),
            ]),
            SizedBox(
                width: MediaQuery.of(context).size.width,
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        GlobalColors.submitBackground),
                    shape: MaterialStateProperty.all(
                        const RoundedRectangleBorder(
                            borderRadius: BorderRadius.zero)),
                  ),
                  child: const Text(
                    "提交",
                    style: TextStyle(color: GlobalColors.submitForeground),
                  ),
                  onPressed: () {
                    UtilityHttp.put("/api/mine/info", author).then((data) {
                      if (data.isSuccess()) {
                        // 保存数据
                        GlobalData.instance.writeAccount(data.data);
                        var snackBar = const SnackBar(content: Text("正在生成头像..."));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        Future.delayed(const Duration(seconds: 3),
                            () => {Navigator.pop(context, true)});
                      }
                    });
                  },
                )),
          ]),
        ),
      ),
    );
  }
}
