import 'dart:io';
import 'package:flutter/material.dart';

import 'package:bloom/model/author_label.dart';
import 'package:bloom/pages/page/mine_account.dart';
import 'package:bloom/pages/page/mine_basic.dart';
import 'package:bloom/pages/page/mine_info_label.dart';
import 'package:bloom/pages/user/account_login.dart';
import 'package:bloom/themes/global_dialog.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/model/author_info.dart';

class MineInfo extends StatefulWidget {
  const MineInfo({key}) : super(key: key);

  @override
  State<MineInfo> createState() => _MineInfoState();
}

class _MineInfoState extends State<MineInfo> {
  AuthorInfo author = AuthorInfo.empty();

  // 标签组件
  BuildContext? _labelContext;

  @override
  void initState() {
    super.initState();
    readAccount();
  }

  Container buildLabel(AuthorLabel label) {
    return Container(
      padding: const EdgeInsets.only(left: 8, top: 12, bottom: 6, right: 18),
      child: Row(
        children: <Widget>[
          const Icon(
            Icons.bookmarks,
            weight: 5,
            size: 20,
            color: GlobalColors.lightAccent,
          ),
          const SizedBox(
            width: 10,
          ),
          Text(label.name),
          const SizedBox(
            width: 5,
          ),
          Container(
            width: 20,
            alignment: Alignment.center,
            decoration: const BoxDecoration(
              color: GlobalColors.labelBackground,
              borderRadius: BorderRadius.horizontal(
                  left: Radius.circular(5), right: Radius.circular(5)),
            ),
            child: Text(
              label.count.toString(),
              style: const TextStyle(color: GlobalColors.labelForeground),
            ),
          ),
          const Expanded(child: SizedBox()),
          InkWell(
            onTap: () {
              openLabelDialog(label);
            },
            child: const Icon(
              Icons.edit_note,
              weight: 24,
            ),
          ),
          InkWell(
            onTap: () {
              globalDialog
                  .showConfirmDialog(context, "删除标签", "您确认要删除该标签吗")
                  .then((confirm) {
                if (confirm != null && confirm) {
                  deleteLabel(label);
                }
              });
            },
            child: const Icon(
              Icons.clear,
              weight: 24,
            ),
          )
        ],
      ),
    );
  }

  // 打开标签
  Future openLabelDialog(AuthorLabel label) {
    return showDialog(
        context: context,
        builder: (context) {
          _labelContext = context;
          return AlertDialog(
            title: const Text('修改标签'),
            content: TextField(
              decoration: const InputDecoration(hintText: "标签名称"),
              controller: TextEditingController(text: label.name),
              onChanged: (text) {
                label.name = text;
              },
            ),
            actions: <Widget>[
              MaterialButton(
                color: GlobalColors.submitBackground,
                child: const Text('提交'),
                onPressed: () {
                  updateLabel(label);
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  // 关闭标签
  void closeLabelDialog() {
    if (_labelContext != null) {
      Navigator.pop(_labelContext!);
    }
  }

  @override
  Widget build(BuildContext context) {
    // 图片
    DecorationImage avatar = author.profile.isNotEmpty
        ? DecorationImage(
            image: FileImage(File(author.profile)), fit: BoxFit.cover)
        : const DecorationImage(
            image: AssetImage("assets/header_none.png"), fit: BoxFit.cover);

    // 标签
    List<Widget> labels = [];
    if (author.labels.isEmpty) {
      // 没有标签
      Container empty = Container(
        padding: const EdgeInsets.only(left: 8, top: 12, bottom: 6, right: 18),
        child: const Text("没有标签"),
      );
      labels.add(empty);
    } else {
      // 有标签
      for (var label in author.labels) {
        labels.add(buildLabel(label));
      }
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text(
          "个人信息",
          style: TextStyle(fontSize: 18),
        ),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                size: 24,
              ),
              onPressed: () {
                Navigator.pop(context);
              });
        }),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(top: 12),
          child: Column(children: [
            Container(
                height: 48,
                color: Colors.white,
                padding: const EdgeInsets.only(left: 14, right: 14),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                            builder: (context) => MineBasic(author: author)))
                        .then((value) => readAccount());
                  },
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 42,
                        height: 42,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            image: avatar),
                      ),
                      const Expanded(child: SizedBox()),
                      Text(author.name),
                      const Icon(
                        Icons.keyboard_arrow_right,
                        weight: 28,
                      ),
                    ],
                  ),
                )),
            const Divider(
              height: 2,
              indent: 12,
              color: GlobalColors.lightGrep,
            ),
            Container(
                height: 42,
                color: Colors.white,
                padding: const EdgeInsets.only(left: 14, right: 14),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => MineAccount(author: author)));
                  },
                  child: Row(
                    children: const <Widget>[
                      Text("设置密码"),
                      Expanded(child: SizedBox()),
                      Icon(
                        Icons.keyboard_arrow_right,
                        weight: 28,
                      ),
                    ],
                  ),
                )),
            Container(
                padding: const EdgeInsets.only(
                    top: 6, left: 14, right: 14, bottom: 6),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage("assets/icon_label.png"),
                  fit: BoxFit.cover,
                  alignment: Alignment.topRight,
                )),
                child: Row(
                  children: const [
                    Text(
                      "我的标签",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                )),
            Container(
              margin: const EdgeInsets.only(bottom: 6),
              child: Column(
                children: labels,
              ),
            ),
            Container(
              height: 8,
              color: GlobalColors.lightGrep,
            ),
            Container(
              alignment: Alignment.center,
              height: 40,
              color: Colors.white,
              padding: const EdgeInsets.only(left: 14, right: 14),
              child: TextButton(
                  child: const Text("退出"),
                  onPressed: () {
                    GlobalData.instance.clearAccount();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => const AccountLogin()),
                        ModalRoute.withName("plan"));
                  }),
            ),
          ]),
        ),
      ),
    );
  }

  readAccount() {
    UtilityHttp.get("/api/mine").then((data) {
      if (data.isSuccess()) {
        GlobalData.instance.writeAccount(data.data).then((info) {
          info.loading = true;
          setState(() {
            author = info;
          });
        });
      }
    });
  }

  updateLabel(AuthorLabel label) {
    MineInfoLabel one = MineInfoLabel.from(label.id);
    one.name = label.name;

    UtilityHttp.put("/api/label", one).then((result) {
      if (result.isSuccess()) {
        readAccount();
      }
    });
  }

  deleteLabel(AuthorLabel label) {
    MineInfoLabel one = MineInfoLabel.from(label.id);
    UtilityHttp.delete("/api/label", one).then((result) {
      if (result.isSuccess()) {
        readAccount();
      }
    });
  }
}
