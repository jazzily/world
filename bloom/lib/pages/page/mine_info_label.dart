import 'package:bloom/utilities/global_json.dart';

class MineInfoLabel {
  String id = "";
  String name = "";

  MineInfoLabel();

  static MineInfoLabel from(String id) {
    MineInfoLabel model = MineInfoLabel();
    model.id = id;
    return model;
  }

  MineInfoLabel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    data["name"] = name;
    return data;
  }

  GlobalJson validate() {
    GlobalJson result = GlobalJson.result();
    if (name == "") {
      result.code = "name";
      result.message = "名称不能为空";
    }

    return result;
  }
}
