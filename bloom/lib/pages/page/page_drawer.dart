import 'dart:io';

import 'package:bloom/event/global_event.dart';
import 'package:bloom/model/author_info.dart';
import 'package:bloom/model/author_label.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/pages/page/about_info.dart';
import 'package:bloom/pages/page/mine_info.dart';
import 'package:bloom/pages/plan_page.dart';
import 'package:bloom/pages/task_page.dart';
import 'package:bloom/pages/time_page.dart';
import 'package:bloom/pages/user/account_login.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:flutter/material.dart';

class PageDrawer extends StatefulWidget {
  const PageDrawer({super.key});

  @override
  State<PageDrawer> createState() => _PageDrawerState();
}

class _PageDrawerState extends State<PageDrawer> {
  AuthorInfo _account = AuthorInfo.empty();

  @override
  void initState() {
    super.initState();
    readAccount();
    // 触发drawer事件
    globalEvent.on(GlobalData.eventMenuOpen, (arg) {
      loadAccount();
    });
  }

  void readAccount() {
    GlobalData.instance.readAccount().then((account) {
      setState(() {
        _account = account;
      });
    });
  }

  void loadAccount() {
    UtilityHttp.get("/api/mine").then((data) {
      if (data.isSuccess()) {
        GlobalData.instance.writeAccount(data.data).then((info) {
          info.loading = true;
          setState(() {
            _account = info;
          });
        });
      }
    });
  }

  Container _buildUserCard(BuildContext context) {
    DecorationImage image = _account.profile.isNotEmpty
        ? DecorationImage(
            image: FileImage(File(_account.profile)), fit: BoxFit.cover)
        : const DecorationImage(
            image: AssetImage("assets/header_none.png"), fit: BoxFit.cover);
    Widget status = _account.status.isNotEmpty
        ? Text(_account.status,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontSize: 12))
        : const Text("没有状态",
            style: TextStyle(fontSize: 12, fontStyle: FontStyle.italic));

    return Container(
      height: 160,
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(left: 12, right: 6.0),
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/header_202301.jpg"),
              fit: BoxFit.cover)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 60,
            height: 60,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10), image: image),
          ),
          Expanded(
            child: Container(
              height: 80,
              padding: const EdgeInsets.only(left: 10, top: 14),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 32,
                    child: Text(
                      _account.name,
                      style: const TextStyle(fontSize: 20),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                    child: status,
                  )
                ],
              ),
            ),
          ),
          IconButton(
            icon: const Icon(
              Icons.chevron_right,
              size: 30,
              weight: 30,
            ),
            onPressed: () {
              Navigator.of(context)
                  .push(
                      MaterialPageRoute(builder: (context) => const MineInfo()))
                  .then((refresh) => readAccount());
            },
          ),
        ],
      ),
    );
  }

  Container _buildNoneCard(BuildContext context) {
    return Container(
      height: 160,
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(16.0),
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/header_202302.jpg"),
              fit: BoxFit.cover)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 80,
            height: 80,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: const DecorationImage(
                    image: AssetImage("assets/header_none.png"),
                    fit: BoxFit.cover)),
          ),
          Container(
            height: 80,
            padding: const EdgeInsets.only(left: 10, top: 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 42,
                  child: TextButton(
                    style: const ButtonStyle(alignment: Alignment.centerLeft),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const AccountLogin()));
                    },
                    child: const Text(
                      "登录",
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                  child: Text("一切都是最好的安排",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 14, color: Colors.black)),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _buildDivide() {
    return Container(height: 8, color: GlobalColors.lightGrep);
  }

  Container _buildLabel(AuthorLabel label) {
    return Container(
      height: 36,
      padding: const EdgeInsets.only(left: 8, top: 12, bottom: 6, right: 18),
      child: InkWell(
        onTap: () {
          GlobalData.instance.writeTaskQuery(label.name);
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => const TaskPage()),
              (route) => false);
        },
        child: Row(
          children: <Widget>[
            const Icon(
              Icons.bookmarks,
              weight: 5,
              size: 20,
              color: GlobalColors.lightAccent,
            ),
            const SizedBox(
              width: 15,
            ),
            Text(label.name),
            const Expanded(child: SizedBox()),
            Container(
              width: 20,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: GlobalColors.labelBackground,
                borderRadius: BorderRadius.horizontal(
                    left: Radius.circular(5), right: Radius.circular(5)),
              ),
              child: Text(
                label.count.toString(),
                style: const TextStyle(color: GlobalColors.labelForeground),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildDrawerMine() {
    return _account.logged ? _buildUserCard(context) : _buildNoneCard(context);
  }

  Widget buildDrawerMenu() {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Container(
            height: 8,
            color: GlobalColors.lightGrep,
          ),
          Container(
              height: 42,
              color: Colors.white,
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => const PlanPage()),
                      (router) => false);
                },
                child: Row(
                  children: const <Widget>[
                    Icon(
                      Icons.calendar_month,
                      weight: 28,
                      color: Colors.blue,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text("日历"),
                    Expanded(child: SizedBox()),
                    Icon(
                      Icons.keyboard_arrow_right,
                      weight: 28,
                    ),
                  ],
                ),
              )),
          const Divider(
            height: 2,
            indent: 45,
            color: GlobalColors.lightGrep,
          ),
          Container(
            height: 42,
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: InkWell(
              onTap: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => const TaskPage()),
                    (router) => false);
              },
              child: Row(
                children: const <Widget>[
                  Icon(
                    Icons.format_list_bulleted,
                    weight: 28,
                    color: Colors.blue,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text("日程"),
                  Expanded(child: SizedBox()),
                  Icon(
                    Icons.keyboard_arrow_right,
                    weight: 28,
                  ),
                ],
              ),
            ),
          ),
          const Divider(
            height: 2,
            indent: 45,
            color: GlobalColors.lightGrep,
          ),
          Container(
            height: 42,
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: InkWell(
              onTap: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => const TimePage()),
                    (router) => false);
              },
              child: Row(
                children: const <Widget>[
                  Icon(
                    Icons.more_time,
                    weight: 28,
                    color: Colors.blue,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text("时间线"),
                  Expanded(child: SizedBox()),
                  Icon(
                    Icons.keyboard_arrow_right,
                    weight: 28,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildDrawerLabel() {
    List<Widget> labels = [];
    if (_account.labels.isEmpty) {
      return SizedBox(width: MediaQuery.of(context).size.width, height: 0);
    }

    labels.add(_buildDivide());

    // 取前5条
    int max = 5;
    for (var label in _account.labels) {
      if (label.count > 0) {
        labels.add(_buildLabel(label));
        max--;
      }
      if (max == 0) {
        break;
      }
    }

    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(bottom: 6),
      child: Column(
        children: labels,
      ),
    );
  }

  Widget buildDrawerOther() {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Container(
            height: 8,
            color: GlobalColors.lightGrep,
          ),
          Container(
              height: 42,
              color: Colors.white,
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: InkWell(
                onTap: () {},
                child: Row(
                  children: const <Widget>[
                    Icon(
                      Icons.assistant,
                      weight: 28,
                      color: GlobalColors.lightApple,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text("统计"),
                    Expanded(child: SizedBox()),
                    Icon(
                      Icons.keyboard_arrow_right,
                      weight: 28,
                    ),
                  ],
                ),
              )),
          const Divider(
            height: 2,
            indent: 45,
            color: GlobalColors.lightGrep,
          ),
          Container(
            height: 42,
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => const AboutInfo()));
              },
              child: Row(
                children: const <Widget>[
                  Icon(
                    Icons.pending,
                    weight: 28,
                    color: GlobalColors.lightGreen,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text("关于"),
                  Expanded(child: SizedBox()),
                  Icon(
                    Icons.keyboard_arrow_right,
                    weight: 28,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            buildDrawerMine(),
            buildDrawerMenu(),
            buildDrawerLabel(),
            buildDrawerOther(),
          ],
        ),
      ),
    );
  }
}
