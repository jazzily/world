import 'package:flutter/material.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/model/schedule_elect.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/event/global_event.dart';

class PageHeader extends StatefulWidget implements PreferredSizeWidget {
  const PageHeader({super.key});

  @override
  State<PageHeader> createState() => _PageHeaderState();

  @override
  Size get preferredSize => const Size.fromHeight(40);
}

class _PageHeaderState extends State<PageHeader> {
  ScheduleElect _elect = ScheduleElect.today();

  @override
  void initState() {
    super.initState();
    globalEvent.on(GlobalData.eventScheduleDay, (day) {
      ScheduleElect elect = ScheduleElect.fromDay(day);
      setState(() {
        _elect = elect;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: GlobalColors.lightBlue,
      leading: Builder(builder: (context) {
        return IconButton(
            padding: const EdgeInsets.only(bottom: 0),
            icon: const Icon(
              Icons.menu,
              size: 24,
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            });
      }),
      leadingWidth: 40,
      title: Text(_elect.selectMonth, style: const TextStyle(fontSize: 16)),
      centerTitle: false,
      titleSpacing: 2,
      actions: <Widget>[
        Visibility(
          maintainSize: true,
          maintainAnimation: true,
          maintainState: true,
          visible: !_elect.current,
          child: IconButton(
              icon: const Icon(Icons.today, size: 24),
              padding: const EdgeInsets.only(bottom: 0),
              onPressed: () {
                GlobalData.instance.writeTaskDay(DateTime.now());
                Navigator.pushNamedAndRemoveUntil(
                    context, "/plan", (router) => false);
              }),
        ),
        IconButton(
            icon: const Icon(Icons.add_box, size: 24),
            padding: const EdgeInsets.only(bottom: 2),
            onPressed: () {
              Navigator.pushNamed(context, "/plan/save");
            }),
      ],
    );
  }
}
