import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:bloom/model/schedule_event.dart';
import 'package:bloom/pages/plan/plan_info.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/utilities/utility_date.dart';
import 'package:bloom/model/global_calendar.dart';
import 'package:bloom/model/schedule_elect.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/event/global_event.dart';

class PlanCalendar extends StatefulWidget {
  const PlanCalendar({super.key});

  @override
  State<PlanCalendar> createState() => _PlanCalendarState();
}

class _PlanCalendarState extends State<PlanCalendar> {
  // 日历数据
  List<Calendar> calendarDays = [];

  // 选中的那天，表示正在被选中的那一天
  Calendar _electDay = Calendar.empty;

  // 起始的那天，表示可见的范围的第一天
  int _startDay = 0;

  // 日历相关参数
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime _selectedDay = DateTime.now();
  final DateTime _firstDay = DateTime(1970, 1, 1);
  final DateTime _lastDay = DateTime(2099, 12, 31);

  @override
  void initState() {
    super.initState();

    // 日历值
    GlobalData.instance.readCalendar(context).then((value) {
      calendarDays = value;

      // 选中日期
      ScheduleElect elect = GlobalData.instance.readTaskDay();
      setState(() {
        _startDay = indexCalendar(elect.currentDate);
        _electDay = findCalendar(elect.currentDate);
      });

      // 加载数据
      loadCalendar(elect.currentDate);
    });
  }

  @override
  Widget build(BuildContext context) {
    // 创建任务控件
    List<Widget> widgets = [];
    // 分割线
    widgets.add(builderDivider());

    // 日历
    widgets.add(buildMonthCalendar());

    // 今日
    widgets.add(buildTodayContainer());

    // 任务
    if (_electDay.hasEvent()) {
      List<Widget> taskWidgets = [];
      for (ScheduleEvent event in _electDay.events) {
        taskWidgets.add(buildTaskContainer(event));
        taskWidgets.add(builderDivider());
      }
      // 可滚动
      Expanded taskView = Expanded(
        child: SingleChildScrollView(
          child: Column(
            children: taskWidgets,
          ),
        ),
      );
      widgets.add(taskView);
    } else {
      widgets.add(buildVacant());
    }

    return Column(children: widgets);
  }

  int indexCalendar(DateTime focusedDay) {
    DateTime origin = UtilityDate.copy(focusedDay, day: 1);
    return origin.difference(_firstDay).inDays - 7;
  }

  void loadCalendar(DateTime focusedDay) {
    // 获取计划日程
    DateTime start =
        UtilityDate.copy(focusedDay, day: 1).subtract(const Duration(days: 7));
    DateTime finish =
        UtilityDate.copy(focusedDay, month: focusedDay.month + 1, day: 1)
            .add(const Duration(days: 7));

    var startDay = start.difference(_firstDay).inDays;
    var finishDay = finish.difference(_firstDay).inDays;

    var query = {
      'start': UtilityDate.toDateString(start),
      'finish': UtilityDate.toDateString(finish)
    };
    UtilityHttp.get("/api/schedule/event", query: query).then((result) {
      if (result.isSuccess()) {
        // 数据
        List data = result.data as List;

        // 初始化数据
        for (int i = startDay; i < finishDay; i++) {
          calendarDays[i].events = [];
          for (var one in data) {
            if (ScheduleEvent.drawDay(one) == calendarDays[i].day) {
              ScheduleEvent event = ScheduleEvent.fromJson(one);
              calendarDays[i].addEvent(event);
            }
          }
        }

        setState(() {
          _startDay = startDay;
          _selectedDay = focusedDay;
          _focusedDay = focusedDay;
        });
      }
    });
  }

  Calendar findCalendar(DateTime day) {
    Calendar today = Calendar.empty;
    for (int i = _startDay; i < calendarDays.length; i++) {
      if (calendarDays[i].day == UtilityDate.toDateString(day)) {
        today = calendarDays[i];
        break;
      }
    }
    return today;
  }

  TableCalendar buildMonthCalendar() {
    return TableCalendar(
      firstDay: _firstDay,
      lastDay: _lastDay,
      focusedDay: _focusedDay,
      calendarFormat: _calendarFormat,
      startingDayOfWeek: StartingDayOfWeek.monday,
      headerVisible: false,
      daysOfWeekHeight: 20,
      rowHeight: 54,
      selectedDayPredicate: (day) {
        return isSameDay(_selectedDay, day);
      },
      onDaySelected: (selectedDay, focusedDay) {
        if (!isSameDay(_selectedDay, selectedDay)) {
          GlobalData.instance.writeTaskDay(selectedDay);

          setState(() {
            _selectedDay = selectedDay;
            _focusedDay = focusedDay;
            _electDay = findCalendar(_selectedDay);
          });
        }
      },
      onFormatChanged: (format) {
        if (_calendarFormat != format) {
          setState(() {
            _calendarFormat = format;
          });
        }
      },
      onPageChanged: (focusedDay) {
        GlobalData.instance.writeTaskDay(focusedDay);
        globalEvent.emit(GlobalData.eventScheduleDay, focusedDay);
        loadCalendar(focusedDay);
      },
      calendarBuilders:
          CalendarBuilders(defaultBuilder: (context, day, focusedDay) {
        Calendar today = findCalendar(day);
        return buildDateContainer(day, today);
      }, outsideBuilder: (context, day, focusedDay) {
        Calendar today = findCalendar(day);
        return buildDateContainer(day, today, outside: true);
      }, selectedBuilder: (context, day, focusedDay) {
        Calendar today = findCalendar(day);
        return buildDateContainer(day, today, outside: false, selected: true);
      }, todayBuilder: (context, day, focusedDay) {
        Calendar today = findCalendar(day);
        return buildDateContainer(day, today,
            outside: false, selected: false, present: true);
      }, dowBuilder: (context, day) {
        return buildWeekContainer(day);
      }),
    );
  }

  Container buildWeekContainer(DateTime day) {
    return Container(
      margin: const EdgeInsets.only(bottom: 0),
      child: Text(
        UtilityDate.toWeekString(day),
        textAlign: TextAlign.center,
      ),
    );
  }

  Container buildDateContainer(DateTime day, Calendar today,
      {bool outside = false, bool selected = false, bool present = false}) {
    // 圆圈
    BoxDecoration dayDecoration = const BoxDecoration();
    if (selected) {
      dayDecoration = const BoxDecoration(
          shape: BoxShape.circle, color: GlobalColors.lightOrange);
    }
    if (present) {
      dayDecoration = BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(width: 2.0, color: GlobalColors.lightOrange),
      );
    }

    // 样式
    TextStyle signText =
        const TextStyle(fontSize: 11, color: GlobalColors.lightGreen);
    TextStyle dayText = const TextStyle(fontSize: 18, color: Colors.black);
    if (outside) {
      dayText = const TextStyle(fontSize: 18, color: Colors.grey);
    }
    TextStyle emptyText = const TextStyle(fontSize: 10, color: Colors.white);
    TextStyle nameText = const TextStyle(fontSize: 12, color: Colors.grey);
    if (selected) {
      emptyText =
          const TextStyle(fontSize: 10, color: GlobalColors.lightOrange);
      nameText = const TextStyle(fontSize: 12, color: Colors.white);
      dayText = const TextStyle(fontSize: 18, color: Colors.white);
    }

    return Container(
      margin: const EdgeInsets.only(top: 4),
      padding: const EdgeInsets.only(top: 2),
      decoration: dayDecoration,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                today.sign!,
                style: emptyText,
              ),
              Text(
                day.day.toString(),
                style: dayText,
              ),
              Text(
                today.sign!,
                style: signText,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                today.name!,
                style: nameText,
              ),
            ],
          ),
          today.events.isNotEmpty
              ? const Icon(Icons.lens, size: 5, color: Colors.blueAccent)
              : const SizedBox.shrink(),
        ],
      ),
    );
  }

  Container buildTodayContainer() {
    return Container(
      margin: const EdgeInsets.only(top: 4.0),
      padding: const EdgeInsets.only(left: 14.0),
      alignment: Alignment.centerLeft,
      color: GlobalColors.lightGrep,
      height: 30,
      child: Row(
        children: <Widget>[
          Icon(Icons.access_time, size: _electDay.sign == "" ? 0 : 20),
          const SizedBox(
            width: 5,
          ),
          Text(_electDay.toDate()),
          const Expanded(child: SizedBox()),
          Text(_electDay.toRemark()),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }

  Container buildTaskContainer(ScheduleEvent event) {
    return Container(
      margin: const EdgeInsets.only(top: 2.0, left: 8.0, bottom: 2.0),
      child: Row(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => PlanInfo(id: event.id)));
                    },
                    child: Text(
                      event.name,
                      style: const TextStyle(fontSize: 16),
                    )),
                Wrap(
                  spacing: 4,
                  children: [
                    Text(
                      event.title,
                      style: const TextStyle(
                          fontSize: 14, color: GlobalColors.lightAccent),
                      strutStyle: const StrutStyle(
                          forceStrutHeight: true, leading: 0.5),
                    ),
                    Text(
                      "#${event.scheduleTitle}",
                      style: const TextStyle(
                          fontSize: 14, color: GlobalColors.lightAccent),
                      strutStyle: const StrutStyle(
                          forceStrutHeight: true, leading: 0.5),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const Expanded(child: SizedBox()),
          Container(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            width: 50,
            height: 50,
            child: IconButton(
              icon: Icon(
                (event.state == "FINISHED"
                    ? Icons.check_circle
                    : Icons.check_box_outline_blank),
                size: 20,
              ),
              onPressed: () {
                if (event.state == "FINISHED") {
                  return;
                }
                completeEvent(event);
              },
            ),
          ),
        ],
      ),
    );
  }

  Container builderDivider() {
    return Container(
      color: GlobalColors.lightGrep,
      height: 4.0,
    );
  }

  Container buildVacant() {
    return Container(
      height: 40,
      padding: const EdgeInsets.only(top: 10.0),
      child: const Text("没有日程"),
    );
  }

  void completeEvent(ScheduleEvent event) {
    UtilityHttp.put("/api/schedule/event/complete", event).then((result) {
      if (result.isSuccess()) {
        var snackBar = const SnackBar(content: Text("不积小流，无以成江海"));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);

        setState(() {
          for (ScheduleEvent eve in _electDay.events) {
            if (eve.id == event.id) {
              eve.state = "FINISHED";
            }
          }
        });
      }
    });
  }
}
