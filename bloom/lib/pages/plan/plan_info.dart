import 'package:bloom/themes/global_dialog.dart';
import 'package:flutter/material.dart';
import 'package:bloom/pages/plan_page.dart';
import 'package:bloom/pages/plan/plan_info_model.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/utilities/utility_date.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/model/schedule_event_one.dart';

class PlanInfo extends StatefulWidget {
  final String id;

  const PlanInfo({super.key, required this.id});

  @override
  State<PlanInfo> createState() => _PlanInfoState();
}

class _PlanInfoState extends State<PlanInfo> {
  // 实体
  PlanInfoModel model = PlanInfoModel.empty();

  // 显示部分
  var _startTime = "";
  var _finishTime = "";

  @override
  void initState() {
    super.initState();
    UtilityHttp.get("/api/schedule/event/one", query: {"id": widget.id}).then((data) {
      if (data.isSuccess()) {
        var event = PlanInfoModel.fromJson(data.data);
        setState(() {
          model = event;
          _startTime = UtilityDate.getShortString(event.startTime);
          _finishTime = UtilityDate.getShortString(event.finishTime);
        });
      }
    });
  }

  void updateEvent() {
    UtilityHttp.put("/api/schedule/event/one", model).then((data) {
      if (data.isSuccess()) {
        var snackBar = const SnackBar(content: Text("长风破浪会有时，直挂云帆济沧海"));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const PlanPage()),
            (router) => false);
      }
    });
  }

  void deleteEvent() {
    ScheduleEventOne one = ScheduleEventOne.fromId(model.id);
    UtilityHttp.delete("/api/schedule/event/one", one).then((data) {
      if (data.isSuccess()) {
        var snackBar = const SnackBar(content: Text("青山不改水长流，明月依旧星渐稀"));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const PlanPage()),
            (router) => false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text(
          "日程信息",
          style: TextStyle(fontSize: 16),
        ),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                size: 24,
              ),
              onPressed: () {
                Navigator.pop(context);
              });
        }),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            color: GlobalColors.lightGrep,
            padding: const EdgeInsets.only(left: 10, top: 2, right: 4),
            width: MediaQuery.of(context).size.width,
            height: 28,
            child: Row(children: <Widget>[
              Text(
                model.creator,
                style: const TextStyle(color: Colors.grey, fontSize: 14),
              ),
              const Expanded(child: SizedBox()),
              IconButton(
                onPressed: () {
                  globalDialog
                      .showConfirmDialog(context, "删除日程", "您确认要删除该日程吗")
                      .then((confirm) {
                    if (confirm != null && confirm) {
                      deleteEvent();
                    }
                  });
                },
                padding: const EdgeInsets.only(top: 0),
                icon: const Icon(
                  Icons.remove_circle,
                  size: 20,
                  color: Colors.white,
                ),
              ),
            ]),
          ),
          TextField(
            maxLines: 1,
            maxLength: 20,
            decoration: const InputDecoration(
                hintText: "标题", prefix: SizedBox(width: 10)),
            controller: TextEditingController(text: model.title),
            onChanged: (text) {
              model.title = text;
            },
          ),
          Container(
            width: MediaQuery.of(context).size.width - 20,
            padding: const EdgeInsets.only(top: 5, bottom: 5),
            child: Row(
              children: <Widget>[
                const Icon(
                  Icons.access_time,
                  weight: 20,
                ),
                const SizedBox(
                  width: 15,
                ),
                const Text("开始"),
                const Expanded(child: SizedBox()),
                Text(
                  _startTime,
                  textAlign: TextAlign.right,
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(top: 2, bottom: 8),
            child: const Divider(
              height: 2,
              indent: 40,
              color: GlobalColors.lightDark,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width - 20,
            padding: const EdgeInsets.only(top: 5, bottom: 5),
            child: Row(
              children: <Widget>[
                const Icon(
                  Icons.lock_clock,
                  weight: 20,
                ),
                const SizedBox(
                  width: 15,
                ),
                const Text("结束"),
                const Expanded(child: SizedBox()),
                Text(
                  _finishTime,
                  textAlign: TextAlign.right,
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(top: 2, bottom: 8),
            child: const Divider(
              height: 2,
              indent: 40,
              color: GlobalColors.lightDark,
            ),
          ),
          TextField(
            maxLines: 4,
            maxLength: 64,
            decoration: const InputDecoration(
                hintText: "备注", prefix: SizedBox(width: 10)),
            controller: TextEditingController(text: model.remark),
            onChanged: (text) {
              model.remark = text;
            },
          ),
          Row(
            children: [
              Expanded(
                child: IgnorePointer(
                  ignoring: model.finished,
                  child: TextButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          GlobalColors.submitBackground),
                      shape: MaterialStateProperty.all(
                          const RoundedRectangleBorder(
                              borderRadius: BorderRadius.zero)),
                    ),
                    child: Text(
                      model.finished ? "已完成" : "提交",
                      style:
                          const TextStyle(color: GlobalColors.submitForeground),
                    ),
                    onPressed: () {
                      updateEvent();
                    },
                  ),
                ),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
