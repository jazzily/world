import 'package:bloom/utilities/global_json.dart';
import 'package:bloom/utilities/utility_date.dart';

class PlanInfoModel {
  late String id;
  late String state;
  late String title;
  late String remark;
  late String startTime;
  late String finishTime;

  late String scheduleId;

  // 创建信息，包括创建时间和所属日程
  String creator = "";

  // 是否完成
  bool finished = false;

  PlanInfoModel();

  static PlanInfoModel empty() {
    PlanInfoModel model = PlanInfoModel();
    model.id = "0";
    model.state = "";
    model.title = "";
    model.remark = "";
    model.startTime = "";
    model.finishTime = "";
    model.scheduleId = "";
    return model;
  }

  PlanInfoModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    title = json["title"];
    state = json["state"];
    remark = json["remark"];
    startTime = json["startTime"];
    finishTime = json["finishTime"];
    scheduleId = json["scheduleId"];

    // 创建于createTime #scheduleTitle
    var createTime = json["createTime"];
    var create = UtilityDate.fromTimeString(createTime);
    var weekTime = UtilityDate.toWeekString(create);
    var noonTime = UtilityDate.toNoonString(create);
    var scheduleTitle = json["scheduleTitle"];

    creator =
        "创建于${create.year}年${create.month}月${create.day}日 星期$weekTime $noonTime #$scheduleTitle";
    finished = (state == "FINISHED");
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    data["title"] = title;
    data["remark"] = remark;
    return data;
  }

  GlobalJson validate() {
    GlobalJson result = GlobalJson.result();
    if (title == "") {
      result.code = "TITLE";
      result.message = "标题不能为空";
    }

    return result;
  }
}
