import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pickers/pickers.dart';
import 'package:flutter_pickers/time_picker/model/date_mode.dart';
import 'package:flutter_pickers/time_picker/model/pduration.dart';
import 'package:bloom/pages/plan_page.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:bloom/utilities/utility_date.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/utilities/utility_pickers.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/utilities/global_json.dart';
import 'package:bloom/pages/plan/plan_save_model.dart';

class PlanSave extends StatefulWidget {
  const PlanSave({super.key});

  @override
  State<PlanSave> createState() => _PlanSaveState();
}

class _PlanSaveState extends State<PlanSave> {
  final List<String> _weeks = UtilityDate.weeks;
  final List<String> _months = UtilityDate.months;
  final List<String> _years = UtilityDate.years;
  final _repeatNames = GlobalData.instance.getScheduleTypes().values.toList();
  final _repeatEnums = GlobalData.instance.getScheduleTypes().keys.toList();

  // 显示部分
  var _startTime = "";
  var _finishTime = "";
  var _repeatType = "一次性 (共1次)";
  var _repeatHeight = 0.0;
  var _repeatCount = false;
  final Map<String, List<String>> _repeatItems = {
    "EVERY_WEEK": [],
    "EVERY_MONTH": [],
    "EVERY_YEAR": []
  };

  // 实体部分
  PlanSaveModel model = PlanSaveModel.empty();

  _PlanSaveState() {
    String today = GlobalData.instance.readTaskDay().selectDate;

    model.startTime = "${today}T00:00:00";
    model.finishTime = "${today}T00:00:00";
    _startTime = UtilityDate.getShortString(model.startTime);
    _finishTime = UtilityDate.getShortString(model.finishTime);
  }

  calculateRepeat() {
    // 计算总次数
    var repeatType = "";
    var repeatCount = 0;
    var repeatHeight = 0.0;

    // 计算总天数
    int days = DateTime.parse(model.finishTime)
        .difference(DateTime.parse(model.startTime))
        .inDays;

    switch (model.repeatType) {
      case "ONE_TIME":
        repeatHeight = 0.0;
        repeatCount = days + 1;
        repeatType = "一次性 (共$repeatCount次)";
        break;
      case "EVERY_DAY":
        repeatHeight = 0.0;
        repeatCount = days + 1;
        repeatType = "每天 (共$repeatCount次)";
        break;
      case "EVERY_WEEK":
        repeatHeight = 0.0;
        model.repeatItem = _repeatItems["EVERY_WEEK"]!.join(",");

        // 计算个数
        DateTime start = DateTime.parse(model.startTime);
        for (int i = 0; i < days; i++) {
          DateTime item = start.add(Duration(days: i));
          if (_repeatItems["EVERY_WEEK"]!.contains(item.weekday.toString())) {
            repeatCount++;
          }
        }
        repeatType = "每周 (共$repeatCount次)";
        break;
      case "EVERY_MONTH":
        repeatHeight = 200.0;
        model.repeatItem = _repeatItems["EVERY_MONTH"]!.join(",");

        // 计算个数
        DateTime start = DateTime.parse(model.startTime);
        DateFormat format = DateFormat("dd");
        for (int i = 0; i < days; i++) {
          DateTime item = start.add(Duration(days: i));
          String value = format.format(item);
          if (_repeatItems["EVERY_MONTH"]!.contains(value)) {
            repeatCount++;
          }
        }
        repeatType = "每月 (共$repeatCount次)";

        break;
      case "EVERY_YEAR":
        repeatHeight = 200.0;
        model.repeatItem = _repeatItems["EVERY_YEAR"]!.join(",");

        // 计算个数
        DateTime start = DateTime.parse(model.startTime);
        DateFormat format = DateFormat("MM.dd");
        for (int i = 0; i < days; i++) {
          DateTime item = start.add(Duration(days: i));
          String value = format.format(item);
          if (_repeatItems["EVERY_YEAR"]!.contains(value)) {
            repeatCount++;
          }
        }
        repeatType = "每年 (共$repeatCount次)";
        break;
    }
    model.repeatCount = repeatCount;

    setState(() {
      _repeatType = repeatType;
      _repeatHeight = repeatHeight;
      _repeatCount = repeatCount > 520;

      _startTime = UtilityDate.getShortString(model.startTime);
      _finishTime = UtilityDate.getShortString(model.finishTime);
    });
  }

  List<Widget> buildWeekChip() {
    List<String> repeatItem = _repeatItems["EVERY_WEEK"]!;

    List<Widget> chips = [];
    for (int i = 1; i < _weeks.length; i++) {
      String item = i.toString();
      bool has = repeatItem.contains(item);
      Color textFore =
          has ? GlobalColors.labelForeground : GlobalColors.lightDark;
      Color chipBack =
          has ? GlobalColors.labelBackground : GlobalColors.lightGrep;

      InkWell week = InkWell(
        child: Chip(
            backgroundColor: chipBack,
            padding: const EdgeInsets.only(top: 0, bottom: 0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            label: Text(
              _weeks[i],
              style: TextStyle(color: textFore),
            )),
        onTap: () {
          if (repeatItem.contains(item)) {
            repeatItem.remove(item);
          } else {
            repeatItem.add(item);
          }
          calculateRepeat();
        },
      );

      chips.add(week);
    }
    return chips;
  }

  List<Widget> buildMonthChip() {
    List<String> repeatItem = _repeatItems["EVERY_MONTH"]!;

    List<Widget> chips = [];
    for (int i = 1; i < _months.length; i++) {
      String item = _months[i];
      bool has = repeatItem.contains(item);
      Color textFore =
          has ? GlobalColors.labelForeground : GlobalColors.lightDark;
      Color chipBack =
          has ? GlobalColors.labelBackground : GlobalColors.lightGrep;

      InkWell month = InkWell(
          child: Chip(
              backgroundColor: chipBack,
              padding: const EdgeInsets.only(top: 0, bottom: 0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              label: Text(
                _months[i],
                style: TextStyle(color: textFore),
              )),
          onTap: () {
            if (repeatItem.contains(item)) {
              repeatItem.remove(item);
            } else {
              repeatItem.add(item);
            }
            calculateRepeat();
          });
      chips.add(month);
    }
    return chips;
  }

  List<Widget> buildYearChip() {
    List<String> repeatItem = _repeatItems["EVERY_YEAR"]!;

    List<Widget> chips = [];
    for (int i = 1; i < _years.length; i++) {
      String item = _years[i];
      bool has = repeatItem.contains(item);
      Color textFore =
          has ? GlobalColors.labelForeground : GlobalColors.lightDark;
      Color chipBack =
          has ? GlobalColors.labelBackground : GlobalColors.lightGrep;

      InkWell month = InkWell(
          child: Chip(
              backgroundColor: chipBack,
              padding: const EdgeInsets.only(top: 0, bottom: 0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              label: Text(
                _years[i],
                style: TextStyle(color: textFore),
              )),
          onTap: () {
            if (repeatItem.contains(item)) {
              repeatItem.remove(item);
            } else {
              repeatItem.add(item);
            }
            calculateRepeat();
          });
      chips.add(month);
    }
    return chips;
  }

  // 保存
  saveTask() {
    // 验证
    GlobalJson valid = model.validate();
    if (valid.noSuccess()) {
      var snackBar = SnackBar(content: Text(valid.message));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return;
    }

    UtilityHttp.post("/api/schedule", model).then((data) {
      if (data.isSuccess()) {
        var snackBar = const SnackBar(content: Text("路漫漫其修远兮，吾将上下而求索"));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const PlanPage()),
            (router) => false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> repeatChips = [];
    switch (model.repeatType) {
      case "EVERY_WEEK":
        repeatChips = buildWeekChip();
        break;
      case "EVERY_MONTH":
        repeatChips = buildMonthChip();
        break;
      case "EVERY_YEAR":
        repeatChips = buildYearChip();
        break;
      default:
        break;
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: const Text(
          "新建日程",
          style: TextStyle(fontSize: 20),
        ),
        centerTitle: true,
        leading: Builder(builder: (context) {
          return IconButton(
              icon: const Icon(
                Icons.arrow_back,
                size: 24,
              ),
              onPressed: () {
                Navigator.pop(context);
              });
        }),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(top: 5),
            child: TextField(
              decoration: const InputDecoration(
                  hintText: "标题", prefix: SizedBox(width: 10)),
              onChanged: (text) {
                model.title = text;
              },
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 10, top: 5),
            child: TextField(
              decoration: const InputDecoration(
                hintText: "位置",
                icon: Icon(Icons.location_searching),
              ),
              onChanged: (text) {
                model.place = text;
              },
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 10, top: 5),
            child: Row(
              children: <Widget>[
                const Icon(
                  Icons.event_repeat,
                  weight: 20,
                ),
                const SizedBox(
                  width: 15,
                ),
                const Text("重复"),
                const Expanded(child: SizedBox()),
                Text(
                  _repeatType,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                      color: _repeatCount
                          ? GlobalColors.lightApple
                          : GlobalColors.lightDark),
                ),
                IconButton(
                  icon: const Icon(
                    Icons.keyboard_arrow_right,
                    weight: 28,
                  ),
                  onPressed: () {
                    Pickers.showSinglePicker(context, data: _repeatNames,
                        onConfirm: (dat, pos) {
                      model.repeatType = _repeatEnums.elementAt(pos);
                      calculateRepeat();
                    });
                  },
                ),
              ],
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              height: _repeatHeight == 0.0 ? null : _repeatHeight,
              padding: const EdgeInsets.only(left: 30),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Wrap(spacing: 4, runSpacing: 0, children: repeatChips),
              )),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 10),
            child: const Divider(
              height: 2,
              indent: 40,
              color: GlobalColors.lightDark,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 10, top: 5),
            child: Row(
              children: <Widget>[
                const Icon(
                  Icons.access_time,
                  weight: 20,
                ),
                const SizedBox(
                  width: 15,
                ),
                const Text("开始"),
                const Expanded(child: SizedBox()),
                Text(
                  _startTime,
                  textAlign: TextAlign.right,
                ),
                IconButton(
                  icon: const Icon(
                    Icons.keyboard_arrow_right,
                    weight: 30,
                  ),
                  onPressed: () {
                    PDuration day = UtilityPickers.moment();
                    Pickers.showDatePicker(context,
                        mode: DateMode.YMDHM, selectDate: day, onConfirm: (d) {
                      model.startTime = UtilityDate.of(
                          d.year, d.month, d.day, d.hour, d.minute);
                      calculateRepeat();
                    });
                  },
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 10),
            child: const Divider(
              height: 2,
              indent: 40,
              color: GlobalColors.lightDark,
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 10, top: 5),
            child: Row(
              children: <Widget>[
                const Icon(
                  Icons.lock_clock,
                  weight: 20,
                ),
                const SizedBox(
                  width: 15,
                ),
                const Text("截至"),
                const Expanded(child: SizedBox()),
                Text(
                  _finishTime,
                  textAlign: TextAlign.right,
                ),
                IconButton(
                  icon: const Icon(
                    Icons.keyboard_arrow_right,
                    weight: 28,
                  ),
                  onPressed: () {
                    PDuration day = UtilityPickers.moment();
                    Pickers.showDatePicker(context,
                        mode: DateMode.YMDHM, selectDate: day, onConfirm: (d) {
                      model.finishTime = UtilityDate.of(
                          d.year, d.month, d.day, d.hour, d.minute);
                      calculateRepeat();
                    });
                  },
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 10),
            child: const Divider(
              height: 2,
              indent: 40,
              color: GlobalColors.lightDark,
            ),
          ),
          TextField(
            maxLines: 4,
            maxLength: 64,
            decoration: const InputDecoration(
                hintText: "备注", prefix: SizedBox(width: 10)),
            onChanged: (text) {
              model.remark = text;
            },
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              height: 40,
              margin: const EdgeInsets.only(top: 5),
              decoration:
                  const BoxDecoration(color: GlobalColors.submitBackground),
              child: TextButton(
                child: const Text("提交",
                    style: TextStyle(color: GlobalColors.submitForeground)),
                onPressed: () {
                  saveTask();
                },
              ))
        ]),
      ),
    );
  }
}
