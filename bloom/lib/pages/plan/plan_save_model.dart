import 'package:bloom/utilities/global_json.dart';
import 'package:bloom/utilities/utility_date.dart';

class PlanSaveModel {
  late String title;
  late String place;
  late String remark;
  late String startTime;
  late String finishTime;
  late String repeatType;
  late String repeatItem;

  // 重复次数
  int repeatCount = 0;

  PlanSaveModel();

  PlanSaveModel.fromJson(Map<String, dynamic> json) {
    title = json["title"];
    place = json["place"];
    startTime = json["startTime"];
    finishTime = json["finishTime"];
    repeatType = json["repeatType"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["title"] = title;
    data["place"] = place;
    data["remark"] = remark;
    data["startTime"] = startTime;
    data["finishTime"] = finishTime;
    data["repeatType"] = repeatType;
    data["repeatItem"] = repeatItem;
    return data;
  }

  // 验证结果
  GlobalJson validate() {
    GlobalJson result = GlobalJson.result();
    if (title == "") {
      result.code = "title";
      result.message = "标题不能为空";
    }
    if (repeatType == "") {
      result.code = "repeatType";
      result.message = "重复类型不能为空";
    }

    // 项目
    if (repeatType == "EVERY_WEEK" ||
        repeatType == "EVERY_MONTH" ||
        repeatType == "EVERY_YEAR") {
      if (repeatItem == "" || repeatCount == 0) {
        result.code = "repeatItem";
        result.message = "重复项目不能为空";
      }

      repeatItem = repeatItem.replaceAll(".", "");
    }

    // 项目数
    if (repeatCount > 520) {
      result.code = "repeatCount";
      result.message = "重复项太多了";
    }

    return result;
  }

  static PlanSaveModel empty() {
    PlanSaveModel model = PlanSaveModel();
    model.title = "";
    model.place = "";
    model.remark = "";
    model.startTime = "";
    model.finishTime = "";
    model.repeatType = "ONE_TIME";
    model.repeatItem = "";
    return model;
  }
}
