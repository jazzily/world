import 'package:flutter/material.dart';
import 'package:bloom/event/global_event.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/pages/plan/plan_calendar.dart';
import 'package:bloom/pages/page/page_drawer.dart';
import 'package:bloom/pages/page/page_header.dart';

class PlanPage extends StatelessWidget {
  const PlanPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const PageHeader(),
      drawer: const PageDrawer(),
      onDrawerChanged: (isOpen) {
        if (isOpen) {
          globalEvent.emit(GlobalData.eventMenuOpen);
        }
      },
      body: const PlanCalendar(),
    );
  }
}
