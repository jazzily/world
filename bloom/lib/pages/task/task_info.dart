import 'package:bloom/event/global_event.dart';
import 'package:bloom/pages/task/task_next.dart';
import 'package:bloom/pages/task/task_info_event.dart';
import 'package:bloom/pages/task/task_info_label.dart';
import 'package:bloom/pages/task/task_info_model.dart';
import 'package:bloom/themes/global_dialog.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:flutter/material.dart';
import 'package:bloom/themes/global_colors.dart';

class TaskInfo extends StatefulWidget {
  const TaskInfo({super.key});

  @override
  State<TaskInfo> createState() => _TaskInfoState();
}

class _TaskInfoState extends State<TaskInfo> {
  // 重复类型
  final _repeatTypes = GlobalData.instance.getScheduleTypes();

  // 标签组件
  BuildContext? _labelContext;

  // 任务信息
  TaskInfoModel task = TaskInfoModel.empty();

  // 标签信息
  TaskInfoLabel label = TaskInfoLabel.empty();

  @override
  void initState() {
    super.initState();
    querySchedule();
  }

  @override
  void dispose() {
    globalEvent.emit(GlobalData.eventScheduleInfo, false);
    super.dispose();
  }

  Container buildTitle(TaskInfoModel task) {
    return Container(
      padding: const EdgeInsets.only(top: 30),
      width: MediaQuery.of(context).size.width,
      height: 60,
      color: GlobalColors.lightBlue,
      alignment: Alignment.center,
      child: Text(task.title, style: const TextStyle(fontSize: 18)),
    );
  }

  Container buildCreator(TaskInfoModel task) {
    return Container(
      padding: const EdgeInsets.only(left: 14, top: 4, right: 4),
      width: MediaQuery.of(context).size.width,
      height: 24,
      child: Row(children: <Widget>[
        Text(
          task.creator,
          style: const TextStyle(color: Colors.grey, fontSize: 14),
        ),
        const Expanded(child: SizedBox()),
        IconButton(
          onPressed: () {
            globalDialog
                .showConfirmDialog(context, "删除日程", "您确认要删除该日程吗")
                .then((confirm) {
              if (confirm != null && confirm) {
                deleteSchedule(task);
              }
            });
          },
          padding: const EdgeInsets.only(top: 0),
          icon: const Icon(
            Icons.remove_circle,
            size: 20,
            color: Colors.white,
          ),
        ),
      ]),
    );
  }

  Container buildLabel(TaskInfoModel task) {
    List<Widget> labels = [];
    for (TaskInfoLabel lab in task.labels) {
      // label item
      Chip item = Chip(
        backgroundColor: GlobalColors.labelBackground,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.horizontal(
                left: Radius.zero, right: Radius.circular(20.0))),
        label: Text(
          lab.labelName,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
              fontSize: 12, color: GlobalColors.labelForeground),
        ),
        labelPadding:
            const EdgeInsets.only(left: 4, right: 0, top: -2, bottom: -2),
        deleteIcon: const Icon(Icons.dangerous,
            size: 16, color: GlobalColors.lightGrep),
        onDeleted: () {
          globalDialog
              .showConfirmDialog(context, "删除日程标签", "您确认要删除该日程标签吗")
              .then((confirm) {
            if (confirm != null && confirm) {
              delScheduleLabel(lab);
            }
          });
        },
      );
      labels.add(item);
    }

    // add button
    SizedBox addBtn = SizedBox(
        width: 40,
        child: TextButton(
          child:
              const Icon(Icons.add, size: 18, color: GlobalColors.lightAccent),
          onPressed: () {
            openLabelDialog();
          },
        ));
    labels.add(addBtn);

    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(left: 5, right: 5),
      decoration: const BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            image: AssetImage("assets/icon_label.png"),
            fit: BoxFit.cover,
            alignment: Alignment.topRight,
          )),
      child: Wrap(spacing: 4, children: labels),
    );
  }

  Container buildBasic(TaskInfoModel task) {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(bottom: 10),
        decoration: const BoxDecoration(color: Colors.white),
        child: Column(children: [
          TextField(
            style:
                const TextStyle(fontSize: 14, color: GlobalColors.lightGreen),
            decoration: const InputDecoration(
                hintText: "标题",
                border: InputBorder.none,
                prefix: SizedBox(
                  width: 10,
                ),
                suffixIcon: Icon(Icons.edit_note)),
            controller: TextEditingController(text: task.title),
            onChanged: (text) {
              task.title = text;
            },
          ),
          TextField(
            style:
                const TextStyle(fontSize: 14, color: GlobalColors.lightGreen),
            decoration: const InputDecoration(
                hintText: "位置",
                border: InputBorder.none,
                prefixIcon: Icon(
                  Icons.my_location,
                  size: 18,
                  weight: 40,
                ),
                suffixIcon: Icon(Icons.edit_note),
                prefixIconConstraints:
                    BoxConstraints.tightFor(width: 40, height: 20)),
            controller: TextEditingController(text: task.place),
            onChanged: (text) {
              task.place = text;
            },
          ),
          TextField(
              enabled: false,
              style: const TextStyle(fontSize: 14),
              decoration: const InputDecoration(
                  hintText: "开始时间",
                  border: InputBorder.none,
                  isDense: true,
                  contentPadding: EdgeInsets.zero,
                  prefixIcon: Icon(Icons.access_time, size: 18),
                  prefixIconConstraints:
                      BoxConstraints.tightFor(width: 40, height: 20)),
              controller: TextEditingController(text: task.startTime),
              onChanged: null),
          TextField(
            enabled: false,
            style: const TextStyle(fontSize: 14),
            decoration: const InputDecoration(
                hintText: "结束时间",
                border: InputBorder.none,
                prefixIcon: Icon(Icons.lock_clock, size: 18),
                prefixIconConstraints:
                    BoxConstraints.tightFor(width: 40, height: 20)),
            controller: TextEditingController(text: task.finishTime),
            onChanged: null,
          ),
          TextField(
            enabled: false,
            style: const TextStyle(fontSize: 14),
            decoration: const InputDecoration(
                hintText: "重复",
                border: InputBorder.none,
                isDense: true,
                contentPadding: EdgeInsets.zero,
                prefixIcon: Icon(Icons.event_repeat, size: 18),
                prefixIconConstraints:
                    BoxConstraints.tightFor(width: 40, height: 20)),
            controller: TextEditingController(text: task.repeatType),
            onChanged: null,
          ),
        ]));
  }

  Container buildEvent(TaskInfoModel task) {
    List<Widget> events = [];
    for (TaskInfoEvent event in task.events) {
      Icon evIcon = event.completed
          ? const Icon(Icons.check_circle,
              color: GlobalColors.lightGrep, size: 18)
          : const Icon(Icons.radio_button_off,
              color: GlobalColors.lightGreen, size: 18);
      Color ttColor =
          (event.completed ? GlobalColors.lightGrep : GlobalColors.lightGreen);
      Color wkColor =
          (event.completed ? GlobalColors.lightGrep : GlobalColors.lightDark);

      // 内容
      Container e = Container(
        padding: const EdgeInsets.only(left: 0, right: 2),
        height: 38,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 30,
              child: IconButton(
                  onPressed: () {
                    completeEvent(event);
                  },
                  icon: evIcon),
            ),
            Expanded(
              child: TextField(
                readOnly: event.completed,
                style: TextStyle(fontSize: 14, color: ttColor),
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  isCollapsed: true,
                ),
                controller: TextEditingController(text: event.title),
                onChanged: (text) {
                  event.title = text;
                  updateEvent(event);
                },
              ),
            ),
            Text(
              event.workTime,
              style: TextStyle(fontSize: 14, color: wkColor),
            ),
            SizedBox(
                width: 30,
                child: IconButton(
                  padding: const EdgeInsets.only(top: 4),
                  onPressed: () {
                    globalDialog
                        .showConfirmDialog(context, "删除日程项", "您确认要删除该日程项吗")
                        .then((confirm) {
                      if (confirm != null && confirm) {
                        deleteEvent(event);
                      }
                    });
                  },
                  icon: const Icon(
                    Icons.clear,
                    size: 14,
                  ),
                ))
          ],
        ),
      );
      events.add(e);

      // 分割线
      Divider d = const Divider(
        height: 2,
        indent: 30,
        color: GlobalColors.lightGrep,
      );
      events.add(d);
    }

    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(top: 5, bottom: 0),
      decoration: const BoxDecoration(color: Colors.white),
      child: Column(children: events),
    );
  }

  Container buildEventNext(TaskInfoModel task) {
    Container e = Container(
      height: 40,
      padding: const EdgeInsets.only(left: 8, right: 0),
      decoration: const BoxDecoration(color: Colors.white),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const Icon(Icons.add, color: GlobalColors.lightGreen, size: 20),
          TextButton(
            child: const Text("下一步",
                style: TextStyle(fontSize: 14, color: GlobalColors.lightGreen)),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(
                      builder: (context) =>
                          TaskNext(scheduleId: task.id, title: task.title)))
                  .then((refresh) => querySchedule());
            },
          ),
        ],
      ),
    );

    return e;
  }

  Container buildRemark(TaskInfoModel task) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(top: 5, bottom: 5),
      decoration: const BoxDecoration(color: Colors.white),
      child: TextField(
        maxLines: 4,
        maxLength: 64,
        style: const TextStyle(fontSize: 14, color: GlobalColors.lightGreen),
        controller: TextEditingController(text: task.remark),
        decoration: const InputDecoration(
            hintText: "备注",
            border: InputBorder.none,
            prefix: SizedBox(
              width: 8,
            ),
            suffixIcon: Icon(Icons.edit_note)),
        onChanged: (text) {
          task.remark = text;
        },
      ),
    );
  }

  Container buildUpdate(TaskInfoModel task) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 40,
        decoration: const BoxDecoration(color: GlobalColors.submitBackground),
        child: TextButton(
          child: const Text("提交",
              style: TextStyle(color: GlobalColors.submitForeground)),
          onPressed: () {
            updateSchedule(task);
            Navigator.pop(context, true);
          },
        ));
  }

  SizedBox buildLadder() {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).viewInsets.bottom,
        child: const SizedBox());
  }

  // 打开标签
  Future openLabelDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          _labelContext = context;
          return AlertDialog(
            title: const Text('新增标签'),
            content: TextField(
              decoration: const InputDecoration(hintText: "标签名称"),
              onChanged: (text) {
                label.labelName = text;
              },
            ),
            actions: <Widget>[
              MaterialButton(
                color: GlobalColors.submitBackground,
                child: const Text('提交'),
                onPressed: () {
                  addScheduleLabel(label);
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  // 关闭标签
  void closeLabelDialog() {
    if (_labelContext != null) {
      Navigator.pop(_labelContext!);
    }
  }

  // 查询日程
  void querySchedule() {
    // 获取数据
    var info = GlobalData.instance.readTaskInfo();
    label.scheduleId = info.id;
    UtilityHttp.get("/api/schedule/one", query: {"id": info.id}).then((data) {
      if (data.isSuccess()) {
        var one = TaskInfoModel.fromJson(data.data);
        one.repeatType = _repeatTypes[one.repeatType]!;
        setState(() {
          task = one;
        });
      }
    });
  }

  // 更新日程
  void updateSchedule(TaskInfoModel task) {
    // 验证
    if (!task.validate()) {
      return;
    }

    UtilityHttp.put("/api/schedule/one", task).then((result) {});
  }

  // 删除日程
  void deleteSchedule(TaskInfoModel one) {
    UtilityHttp.delete("/api/schedule/one", one).then((result) {
      if (result.isSuccess()) {}
    });
  }

  // 增加标签
  void addScheduleLabel(TaskInfoLabel label) {
    UtilityHttp.post("/api/schedule/label", label).then((result) {
      if (result.isSuccess()) {
        querySchedule();
      }
    });
  }

  // 删除标签
  void delScheduleLabel(TaskInfoLabel label) {
    UtilityHttp.delete("/api/schedule/label", label).then((result) {
      if (result.isSuccess()) {
        querySchedule();
      }
    });
  }

  // 完成事件
  void completeEvent(TaskInfoEvent event) {
    if (event.completed) {
      return;
    }

    UtilityHttp.put("/api/schedule/event/complete", event).then((result) {
      if (result.isSuccess()) {
        event.state = "FINISHED";
        event.process();
        setState(() {});
      }
    });
  }

  // 删除事件
  void deleteEvent(TaskInfoEvent event) {
    UtilityHttp.delete("/api/schedule/event/one", event).then((result) {
      if (result.isSuccess()) {
        querySchedule();
      }
    });
  }

  // 更新事件
  void updateEvent(TaskInfoEvent event) {
    if (!event.validate()) {
      return;
    }

    UtilityHttp.put("/api/schedule/event/one", event).then((result) {});
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [];
    // title
    widgets.add(buildTitle(task));
    // create
    widgets.add(buildCreator(task));

    // events
    widgets.add(buildEvent(task));
    // next
    widgets.add(buildEventNext(task));

    // labels
    widgets.add(buildLabel(task));

    // basic
    widgets.add(buildBasic(task));
    // remark
    widgets.add(buildRemark(task));
    // update
    widgets.add(buildUpdate(task));
    // ladder
    widgets.add(buildLadder());

    return Drawer(
      backgroundColor: GlobalColors.lightGrep,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: widgets,
        ),
      ),
    );
  }
}
