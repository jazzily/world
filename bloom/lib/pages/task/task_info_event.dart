import 'package:bloom/utilities/utility_date.dart';

class TaskInfoEvent {
  late String id;
  late String state;
  late String title;
  late String remark;
  late String startTime;
  late String finishTime;
  late String completeTime;

  // 是否完成
  bool completed = false;

  // 执行时间
  String workTime = "";

  process() {
    completed = (state == "FINISHED");
  }

  TaskInfoEvent();

  static TaskInfoEvent empty() {
    TaskInfoEvent model = TaskInfoEvent();
    model.id = "0";
    model.state = "";
    model.title = "";
    model.remark = "";
    model.startTime = "";
    model.finishTime = "";
    return model;
  }

  TaskInfoEvent.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    state = json["state"];
    title = json["title"];
    remark = json["remark"];
    startTime = json["startTime"];
    finishTime = json["finishTime"];
    completeTime = json["completeTime"];

    completed = (state == "FINISHED");

    // 工作时间
    var dayDate = UtilityDate.getDateString(startTime).substring(5);
    var dayStart = UtilityDate.getTimeString(startTime);
    var dayFinish = UtilityDate.getTimeString(finishTime);

    workTime = (dayStart == "00:00" && dayFinish == "00:00")
        ? "$dayDate (全天)"
        : "$dayDate ($dayStart-$dayFinish)";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    data["state"] = state;
    data["title"] = title;
    data["remark"] = remark;
    return data;
  }

  bool validate() {
    return title.isNotEmpty;
  }
}
