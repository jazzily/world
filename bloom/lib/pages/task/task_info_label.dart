class TaskInfoLabel {
  late String id;
  late String labelId;
  late String labelName;
  late String scheduleId;

  TaskInfoLabel();

  static TaskInfoLabel empty() {
    TaskInfoLabel model = TaskInfoLabel();
    model.id = "0";
    model.labelId = "";
    model.labelName = "";
    model.scheduleId = "0";
    return model;
  }

  TaskInfoLabel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    labelId = json["labelId"];
    labelName = json["labelName"];
    scheduleId = json["scheduleId"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    data["labelId"] = labelId;
    data["labelName"] = labelName;
    data["scheduleId"] = scheduleId;
    return data;
  }
}
