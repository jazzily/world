import 'package:bloom/pages/task/task_info_event.dart';
import 'package:bloom/pages/task/task_info_label.dart';
import 'package:bloom/utilities/utility_date.dart';

class TaskInfoModel {
  late String id;
  late String state;
  late String title;
  late String place;
  late String remark;
  late String startTime;
  late String finishTime;
  late int completeRate;
  late String completeTime;

  late List<TaskInfoEvent> events;
  late List<TaskInfoLabel> labels;

  String repeatType = "";
  String repeatItem = "";
  String createTime = "";

  // 是否完成
  bool completed = false;

  // 创造者
  String creator = "";

  TaskInfoModel();

  static TaskInfoModel empty() {
    TaskInfoModel model = TaskInfoModel();
    model.id = "0";
    model.state = "";
    model.title = "";
    model.place = "";
    model.remark = "";
    model.startTime = "";
    model.finishTime = "";
    model.repeatType = "";
    model.repeatItem = "";
    model.completeRate = 0;
    model.completeTime = "";
    model.createTime = "2021-02-03T05:06:07";
    model.events = [];
    model.labels = [];
    return model;
  }

  TaskInfoModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    state = json["state"];
    title = json["title"];
    place = json["place"];
    remark = json["remark"];
    startTime = json["startTime"];
    finishTime = json["finishTime"];
    repeatType = json["repeatType"];
    repeatItem = json["repeatItem"];
    completeRate = json["completeRate"];
    completeTime = json["completeTime"];

    // 开始时间
    startTime = UtilityDate.getDateString(startTime);
    finishTime = UtilityDate.getDateString(finishTime);

    // 创建时间
    createTime = json["createTime"];
    var create = UtilityDate.fromTimeString(createTime);
    var weekTime = UtilityDate.toWeekString(create);
    var noonTime = UtilityDate.toNoonString(create);
    creator =
        "创建于${create.year}年${create.month}月${create.day}日 星期$weekTime $noonTime";

    // 是否完成
    completed = (completeRate == 100);

    // 事件
    if (json["events"] != null) {
      events = (json["events"] as List)
          .map((e) => TaskInfoEvent.fromJson(e as Map<String, dynamic>))
          .toList();
    }

    // 标签
    if (json["labels"] != null) {
      labels = (json["labels"] as List)
          .map((e) => TaskInfoLabel.fromJson(e as Map<String, dynamic>))
          .toList();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    data["title"] = title;
    data["place"] = place;
    data["remark"] = remark;
    return data;
  }

  bool validate() {
    return title.isNotEmpty;
  }
}
