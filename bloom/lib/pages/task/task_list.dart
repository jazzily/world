import 'package:bloom/event/global_event.dart';
import 'package:bloom/model/schedule_query.dart';
import 'package:bloom/pages/task/task_list_query.dart';
import 'package:bloom/model/global_data.dart';
import 'package:flutter/material.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/pages/task/task_one_model.dart';
import 'package:bloom/themes/global_colors.dart';

class TaskList extends StatefulWidget {
  const TaskList({super.key});

  @override
  State<TaskList> createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  TaskListQuery query = TaskListQuery.init();
  List<TaskOneModel> models = [];

  // 是否有任务
  bool _hasTask = false;

  // 搜索组件
  BuildContext? _search;

  @override
  void initState() {
    super.initState();
    queryTask();
    // 触发drawer事件
    globalEvent.on(GlobalData.eventScheduleOne, (arg) {
      queryTask();
    });
  }

  Container buildNavigator() {
    return Container(
      color: GlobalColors.lightGrep,
      padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
      child: Row(
        children: <Widget>[
          Container(
            width: 60,
            height: 24,
            alignment: Alignment.center,
            decoration: const BoxDecoration(
              color: GlobalColors.labelBackground,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(4), topRight: Radius.circular(4)),
            ),
            child: Text(
              query.stateView,
              style: const TextStyle(color: GlobalColors.labelForeground),
            ),
          ),
          const SizedBox(
            width: 5,
          ),
          Container(
            width: 70,
            height: 24,
            alignment: Alignment.center,
            decoration: const BoxDecoration(
              color: GlobalColors.labelBackground,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(4)),
            ),
            child: Text(query.orderView,
                style: const TextStyle(color: GlobalColors.labelForeground)),
          ),
          Container(
              width: 24,
              height: 24,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: GlobalColors.labelBackground,
                borderRadius: BorderRadius.only(topRight: Radius.circular(4)),
              ),
              child: IconButton(
                padding: const EdgeInsets.all(2),
                icon: Icon(
                  query.getRank() ? Icons.arrow_upward : Icons.arrow_downward,
                  color: GlobalColors.lightOrange,
                ),
                iconSize: 18,
                onPressed: () {
                  setState(() {
                    query.setRank();
                    queryTask();
                  });
                },
              )),
          const Expanded(child: SizedBox()),
          Container(
            width: 40,
            height: 24,
            alignment: Alignment.center,
            decoration: const BoxDecoration(
              color: GlobalColors.lightOrange,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(4)),
            ),
            child: InkWell(
              onTap: () {
                openSearchDialog();
              },
              child: const Text(
                "筛选",
              ),
            ),
          ),
          Container(
              width: 24,
              height: 24,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: GlobalColors.lightOrange,
                borderRadius: BorderRadius.only(topRight: Radius.circular(4)),
              ),
              child: IconButton(
                icon: const Icon(
                  Icons.filter_alt,
                  size: 16,
                ),
                padding: const EdgeInsets.all(0),
                onPressed: () {
                  openSearchDialog();
                },
              )),
        ],
      ),
    );
  }

  Container buildDivider() {
    return Container(
      height: 3,
      color: GlobalColors.lightGrep,
    );
  }

  Container buildVacant() {
    return Container(
      height: 40,
      padding: const EdgeInsets.only(top: 10.0),
      child: const Text("没有日程"),
    );
  }

  Container buildTask(TaskOneModel one) {
    Icon tkIcon = one.completed
        ? const Icon(Icons.check_circle)
        : const Icon(Icons.radio_button_unchecked);
    Color tkColor =
        one.completed ? GlobalColors.lightGrep : GlobalColors.lightGreen;
    Color otColor =
        one.overtime ? GlobalColors.lightApple : GlobalColors.lightAccent;
    Icon srIcon =
        one.starred ? const Icon(Icons.star) : const Icon(Icons.star_border);

    return Container(
      height: 56,
      padding: const EdgeInsets.only(left: 3.0, right: 3.0),
      child: Row(
        children: <Widget>[
          IconButton(
            icon: tkIcon,
            color: tkColor,
            onPressed: () {
              completeTask(one);
            },
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    InkWell(
                      child: Text(
                        one.title,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontSize: 14),
                      ),
                      onTap: () {
                        GlobalData.instance.writeTaskInfo(one.id);
                        Scaffold.of(context).openEndDrawer();
                      },
                    )
                  ],
                ),
                Row(
                  children: [
                    Container(
                      width: 50,
                      height: 20,
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        color: GlobalColors.lightGrep,
                        borderRadius: BorderRadius.horizontal(
                            left: Radius.circular(4),
                            right: Radius.circular(4)),
                      ),
                      child: Text(
                        one.repeatView,
                        strutStyle: const StrutStyle(
                            forceStrutHeight: true, leading: 0.4),
                        style: const TextStyle(
                            fontSize: 12,
                            color: GlobalColors.lightGreen,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    const SizedBox(width: 6),
                    Visibility(
                      visible: one.labels.isNotEmpty,
                      child: Container(
                        width: 8,
                        height: 20,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          color: GlobalColors.labelBackground,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(4),
                              bottomLeft: Radius.circular(4)),
                        ),
                        child: const SizedBox(width: 8),
                      ),
                    ),
                    Visibility(
                      visible: one.labels.isNotEmpty,
                      child: Container(
                        height: 20,
                        margin: const EdgeInsets.only(right: 6),
                        padding: const EdgeInsets.only(left: 4, right: 4),
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          color: GlobalColors.lightGrep,
                          borderRadius: BorderRadius.horizontal(
                              right: Radius.circular(4)),
                        ),
                        child: Text(
                          one.labelView,
                          overflow: TextOverflow.ellipsis,
                          strutStyle: const StrutStyle(
                              forceStrutHeight: true, leading: 0.3),
                          style: const TextStyle(
                              fontSize: 12, color: GlobalColors.lightDark),
                        ),
                      ),
                    ),
                    Text(
                      one.finishView,
                      style: TextStyle(fontSize: 12, color: otColor),
                    ),
                  ],
                )
              ],
            ),
          ),
          Text(
            "${one.completeRate}%",
            style: TextStyle(
                fontSize: 16, color: one.completed ? Colors.grey : Colors.blue),
          ),
          IconButton(
            icon: srIcon,
            onPressed: () {
              starTask(one);
            },
          ),
        ],
      ),
    );
  }

  // 打开搜索框
  Future openSearchDialog() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          _search = context;
          return Material(
              color: Colors.transparent,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: Stack(children: <Widget>[
                    Positioned(
                      top: 72,
                      right: 5,
                      width: 200,
                      height: 300,
                      child: Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5),
                          ),
                          color: Colors.white,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              color: GlobalColors.lightGreen,
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 8, right: 8),
                              child: Row(
                                children: const [
                                  Icon(Icons.manage_search),
                                  Text("搜索条件", style: TextStyle(fontSize: 14))
                                ],
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 12, right: 2),
                              child: InkWell(
                                onTap: () {
                                  closeSearchDialog();
                                  setState(() {
                                    query.setState("A");
                                    GlobalData.instance.writeTaskQuery("");
                                    queryTask();
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("A 全部的"),
                                    Expanded(child: SizedBox()),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      weight: 18,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 12, right: 2),
                              child: InkWell(
                                onTap: () {
                                  closeSearchDialog();
                                  setState(() {
                                    query.setState("P");
                                    GlobalData.instance.writeTaskQuery("");
                                    queryTask();
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("P 计划中"),
                                    Expanded(child: SizedBox()),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      weight: 18,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 10, left: 12, right: 2),
                              child: InkWell(
                                onTap: () {
                                  closeSearchDialog();
                                  setState(() {
                                    query.setState("C");
                                    GlobalData.instance.writeTaskQuery("");
                                    queryTask();
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("C 已完成"),
                                    Expanded(child: SizedBox()),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      weight: 18,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              color: GlobalColors.lightGreen,
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 8, right: 8),
                              child: Row(
                                children: const [
                                  Icon(Icons.import_export),
                                  Text("排序依据", style: TextStyle(fontSize: 14))
                                ],
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 12, right: 2),
                              child: InkWell(
                                onTap: () {
                                  closeSearchDialog();
                                  setState(() {
                                    query.setOrder("F");
                                    queryTask();
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("F 结束时间"),
                                    Expanded(child: SizedBox()),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      weight: 18,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 12, right: 2),
                              child: InkWell(
                                onTap: () {
                                  closeSearchDialog();
                                  setState(() {
                                    query.setOrder("S");
                                    queryTask();
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("S 开始时间"),
                                    Expanded(child: SizedBox()),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      weight: 18,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 12, right: 2),
                              child: InkWell(
                                onTap: () {
                                  closeSearchDialog();
                                  setState(() {
                                    query.setOrder("T");
                                    queryTask();
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("T 字母顺序"),
                                    Expanded(child: SizedBox()),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      weight: 18,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 12, right: 2),
                              child: InkWell(
                                onTap: () {
                                  closeSearchDialog();
                                  setState(() {
                                    query.setOrder("R");
                                    queryTask();
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("R 完成率"),
                                    Expanded(child: SizedBox()),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      weight: 18,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  top: 2, bottom: 2, left: 12, right: 2),
                              child: InkWell(
                                onTap: () {
                                  closeSearchDialog();
                                  setState(() {
                                    query.setOrder("V");
                                    queryTask();
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("V 重要度"),
                                    Expanded(child: SizedBox()),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      weight: 18,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
                ),
                onTap: () {
                  closeSearchDialog();
                },
              ));
        });
  }

  // 关闭搜索框
  void closeSearchDialog() {
    if (_search != null) {
      Navigator.pop(_search!);
    }
  }

  // 搜索任务
  void queryTask() {
    // 设置标签，如有
    ScheduleQuery tq = GlobalData.instance.readTaskQuery();
    UtilityHttp.get("/api/schedule", query: {
      "state": query.state,
      "label": tq.label,
      "sort_by": "${query.order}.${query.orderRank}"
    }).then((result) {
      if (result.isSuccess()) {
        List data = result.data as List;

        List<TaskOneModel> tasks = [];
        for (var one in data) {
          TaskOneModel task = TaskOneModel.fromJson(one);
          task.processView();
          tasks.add(task);
        }

        setState(() {
          models = tasks;
          _hasTask = models.isNotEmpty;
        });
      }
    });
  }

  // 完成任务
  void completeTask(TaskOneModel one) {
    if (one.completed) {
      return;
    }

    UtilityHttp.put("/api/schedule/complete", one).then((result) {
      if (result.isSuccess()) {
        one.state = "FINISHED";
        one.processView();
        setState(() {});
      }
    });
  }

  // 星级任务
  void starTask(TaskOneModel one) {
    one.importance = (one.importance > 0 ? 0 : 1);
    UtilityHttp.put("/api/schedule/star", one).then((result) {
      if (result.isSuccess()) {
        one.processView();
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [];
    // 导航
    widgets.add(buildNavigator());

    if (_hasTask) {
      ListView tasks = ListView.separated(
        itemBuilder: (context, index) {
          Container row = buildTask(models[index]);
          return row;
        },
        separatorBuilder: (context, index) => buildDivider(),
        itemCount: models.length,
        shrinkWrap: true,
      );

      // 防止OVERFLOW
      widgets.add(Expanded(
          child: RefreshIndicator(
        onRefresh: () {
          queryTask();
          var snackBar = const SnackBar(content: Text("轻舟已过万重山"));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          return Future<void>.value(null);
        },
        child: tasks,
      )));
    } else {
      widgets.add(buildVacant());
    }

    return Column(
      children: widgets,
    );
  }
}
