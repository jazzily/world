class TaskListQuery {
  late String state;
  late String order;
  late String label;

  late String stateView;
  late String orderView;
  String orderRank = "asc";

  TaskListQuery();

  void setState(String value) {
    switch (value) {
      case 'A':
        state = "";
        label = "";
        stateView = "全部的";
        break;
      case 'P':
        state = "PLANNING";
        label = "";
        stateView = "计划中";
        break;
      case 'C':
        state = "FINISHED";
        label = "";
        stateView = "已完成";
        break;
    }
  }

  // start.asc,finish.desc,title,complete
  void setOrder(String value) {
    switch (value) {
      case 'F':
        order = "finish";
        orderView = "结束时间";
        break;
      case 'S':
        order = "start";
        orderView = "开始时间";
        break;
      case 'T':
        order = "title";
        orderView = "字母顺序";
        break;
      case 'R':
        order = "complete";
        orderView = "完成率";
        break;
      case 'V':
        order = "star";
        orderRank = "desc";
        orderView = "重要度";
        break;
    }
  }

  // 设置排序
  void setRank() {
    orderRank = (orderRank == "asc" ? "desc" : "asc");
  }

  bool getRank() {
    return orderRank == "asc";
  }

  static TaskListQuery init() {
    TaskListQuery model = TaskListQuery();
    model.setState("P");
    model.setOrder("F");
    return model;
  }
}
