import 'package:bloom/pages/task/task_next_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pickers/pickers.dart';
import 'package:flutter_pickers/time_picker/model/date_mode.dart';
import 'package:flutter_pickers/time_picker/model/pduration.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/utilities/utility_date.dart';
import 'package:bloom/utilities/utility_pickers.dart';
import 'package:bloom/themes/global_colors.dart';

class TaskNext extends StatefulWidget {
  final String scheduleId;
  final String title;

  const TaskNext({super.key, required this.scheduleId, required this.title});

  @override
  State<TaskNext> createState() => _TaskNextState();
}

class _TaskNextState extends State<TaskNext> {
  // 实体
  TaskNextModel model = TaskNextModel.empty();

  // 显示部分
  var _startTime = "";
  var _finishTime = "";

  @override
  void initState() {
    super.initState();
    var electDate = DateTime.now();

    model.scheduleId = widget.scheduleId;
    model.title = widget.title;
    model.startTime = "${UtilityDate.toDateString(electDate)}T00:00:00";
    model.finishTime = "${UtilityDate.toDateString(electDate)}T00:00:00";

    setState(() {
      _startTime = UtilityDate.getShortString(model.startTime);
      _finishTime = UtilityDate.getShortString(model.finishTime);
    });
  }

  // 计算时间，日期与StartTime一致，时间按finishTime计算
  String calculateDate(String startTime, String finishTime) {
    return "${startTime.substring(0, 10)} ${finishTime.substring(11, 16)}:00";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: const Text(
            "新建日程",
            style: TextStyle(fontSize: 18),
          ),
          centerTitle: true,
          leading: Builder(builder: (context) {
            return IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  size: 24,
                ),
                onPressed: () {
                  Navigator.pop(context);
                });
          }),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 20, left: 0, right: 0),
            child: Column(children: [
              TextField(
                maxLines: 1,
                maxLength: 20,
                decoration: const InputDecoration(
                    hintText: "标题", prefix: SizedBox(width: 10)),
                controller: TextEditingController(text: model.title),
                onChanged: (text) {
                  model.title = text;
                },
              ),
              Container(
                width: MediaQuery.of(context).size.width - 20,
                padding: const EdgeInsets.only(top: 2, bottom: 2),
                child: Row(
                  children: <Widget>[
                    const Icon(
                      Icons.access_time,
                      weight: 20,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    const Text("开始"),
                    const Expanded(child: SizedBox()),
                    Text(
                      _startTime,
                      textAlign: TextAlign.right,
                    ),
                    IconButton(
                      icon: const Icon(
                        Icons.keyboard_arrow_right,
                        weight: 30,
                      ),
                      onPressed: () {
                        PDuration day = UtilityPickers.moment();
                        Pickers.showDatePicker(context,
                            mode: DateMode.YMDHM,
                            selectDate: day, onConfirm: (d) {
                          var startTime = UtilityDate.of(
                              d.year, d.month, d.day, d.hour, d.minute);
                          var finishTime =
                              calculateDate(startTime, model.finishTime);
                          setState(() {
                            _startTime = UtilityDate.getShortString(startTime);
                            _finishTime =
                                UtilityDate.getShortString(finishTime);
                            model.startTime = startTime;
                            model.finishTime = finishTime;
                          });
                        });
                      },
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width - 20,
                padding: const EdgeInsets.only(top: 0, bottom: 8),
                child: const Divider(
                  height: 2,
                  indent: 40,
                  color: GlobalColors.lightDark,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width - 20,
                padding: const EdgeInsets.only(top: 2, bottom: 2),
                child: Row(
                  children: <Widget>[
                    const Icon(
                      Icons.lock_clock,
                      weight: 20,
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    const Text("结束"),
                    const Expanded(child: SizedBox()),
                    Text(
                      _finishTime,
                      textAlign: TextAlign.right,
                    ),
                    IconButton(
                      icon: const Icon(
                        Icons.keyboard_arrow_right,
                        weight: 28,
                      ),
                      onPressed: () {
                        PDuration day = UtilityPickers.moment();
                        Pickers.showDatePicker(context,
                            mode: DateMode.HM, selectDate: day, onConfirm: (d) {
                          var finishTime =
                              UtilityDate.of(2023, 4, 5, d.hour, d.minute);
                          finishTime = calculateDate(model.startTime, finishTime);
                          setState(() {
                            _finishTime = UtilityDate.getShortString(finishTime);
                            model.finishTime = finishTime;
                          });
                        });
                      },
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width - 20,
                padding: const EdgeInsets.only(top: 0, bottom: 8),
                child: const Divider(
                  height: 2,
                  indent: 40,
                  color: GlobalColors.lightDark,
                ),
              ),
              TextField(
                maxLines: 4,
                maxLength: 64,
                decoration: const InputDecoration(
                    hintText: "备注", prefix: SizedBox(width: 10)),
                controller: TextEditingController(text: model.remark),
                onChanged: (text) {
                  model.remark = text;
                },
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 40,
                decoration:
                    const BoxDecoration(color: GlobalColors.submitBackground),
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        GlobalColors.submitBackground),
                    shape: MaterialStateProperty.all(
                        const RoundedRectangleBorder(
                            borderRadius: BorderRadius.zero)),
                  ),
                  child: const Text(
                    "提交",
                    style: TextStyle(color: GlobalColors.submitForeground),
                  ),
                  onPressed: () {
                    UtilityHttp.post("/api/schedule/event/append", model)
                        .then((data) {
                      if (data.isSuccess()) {
                        var snackBar =
                            const SnackBar(content: Text("不积跬步，无以至千里"));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        Navigator.of(context).pop(true);
                      }
                    });
                  },
                ),
              )
            ]),
          ),
        ));
  }
}
