import 'package:bloom/utilities/global_json.dart';

class TaskNextModel {
  late String title;
  late String remark;
  late String startTime;
  late String finishTime;
  late String scheduleId;

  TaskNextModel();

  static TaskNextModel empty() {
    TaskNextModel model = TaskNextModel();
    model.title = "";
    model.remark = "";
    model.startTime = "";
    model.finishTime = "";
    model.scheduleId = "";
    return model;
  }

  TaskNextModel.fromJson(Map<String, dynamic> json) {
    title = json["title"];
    remark = json["remark"];
    startTime = json["startTime"];
    finishTime = json["finishTime"];
    scheduleId = json["scheduleId"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["title"] = title;
    data["remark"] = remark;
    data["startTime"] = startTime;
    data["finishTime"] = finishTime;
    data["scheduleId"] = scheduleId;
    return data;
  }

  GlobalJson validate() {
    GlobalJson result = GlobalJson.result();
    if (title == "") {
      result.code = "TITLE";
      result.message = "标题不能为空";
    }

    return result;
  }
}
