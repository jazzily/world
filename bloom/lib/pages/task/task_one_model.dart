import 'package:bloom/utilities/utility_date.dart';
import 'package:bloom/model/global_data.dart';

class TaskOneModel {
  late String id;
  late String state;
  late String title;
  late String repeatType;
  late String finishTime;
  late String completeRate;
  late int importance;
  late String labels;

  String stateView = "";
  String repeatView = "";
  String finishView = "";
  String labelView = "";

  // 是否星标
  bool starred = false;

  // 是否超期
  bool overtime = false;

  // 是否完成
  bool completed = false;

  TaskOneModel();

  processView() {
    stateView = GlobalData.instance.getScheduleState(state);
    repeatView = GlobalData.instance.getScheduleType(repeatType);
    finishView = UtilityDate.fromNow(finishTime);
    if (labels.isNotEmpty) {
      labelView = labels.replaceAll(",", " ").trim();
    }

    completed = (state == "FINISHED");
    if (completed) {
      completeRate = "100";
    }
    starred = importance > 0;
    overtime = !completed &&
        UtilityDate.fromTimeString(finishTime).isBefore(DateTime.now());
  }

  static TaskOneModel empty() {
    TaskOneModel model = TaskOneModel();
    model.id = "";
    model.state = "";
    model.title = "";
    model.repeatType = "";
    model.finishTime = "";
    model.completeRate = "";
    model.labels = "";

    model.stateView = "";
    model.repeatView = "";
    model.finishView = "";
    return model;
  }

  TaskOneModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    state = json["state"];
    title = json["title"];
    repeatType = json["repeatType"];
    finishTime = json["finishTime"];
    completeRate = json["completeRate"].toString();
    importance = json["importance"];
    if (json["labels"] == null) {
      labels = "";
    } else {
      labels = json["labels"];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["id"] = id;
    data["state"] = state;
    data["star"] = importance;
    return data;
  }
}
