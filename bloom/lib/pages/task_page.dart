import 'package:flutter/material.dart';
import 'package:bloom/event/global_event.dart';
import 'package:bloom/pages/task/task_info.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/pages/page/page_drawer.dart';
import 'package:bloom/pages/page/page_header.dart';
import 'package:bloom/pages/task/task_list.dart';

class TaskPage extends StatelessWidget {
  const TaskPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const PageHeader(),
        drawer: const PageDrawer(),
        onDrawerChanged: (isOpen) {
          if (isOpen) {
            globalEvent.emit(GlobalData.eventMenuOpen);
          }
        },
        endDrawer: const TaskInfo(),
        endDrawerEnableOpenDragGesture: false,
        onEndDrawerChanged: (isOpen) {
          // 关闭后刷新页面
          if (!isOpen) {
            globalEvent.emit(GlobalData.eventScheduleOne);
          }
        },
        body: const TaskList());
  }
}
