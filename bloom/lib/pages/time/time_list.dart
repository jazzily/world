import 'package:bloom/pages/time/time_list_day.dart';
import 'package:bloom/pages/time/time_list_model.dart';
import 'package:bloom/pages/time/time_list_query.dart';
import 'package:flutter/material.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/themes/global_colors.dart';

class TimeList extends StatefulWidget {
  const TimeList({super.key});

  @override
  State<TimeList> createState() => _TimeListState();
}

class _TimeListState extends State<TimeList> {
  TimeListQuery query = TimeListQuery.moment();
  List<TimeListDay> models = [];

  // 是否有数据
  var _hasTask = false;

  // 是否加载数据
  var _loadTask = false;

  // 是否结束数据
  var _noneTask = false;

  // 时间线控制器
  final ScrollController _timeScroll = ScrollController();

  @override
  void initState() {
    super.initState();
    scrollTime();
    queryTime();
  }

  Container buildNavigator() {
    return Container(
        color: GlobalColors.lightGrep,
        padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
        child: Row(children: <Widget>[
          Container(
            width: 100,
            height: 24,
            alignment: Alignment.center,
            decoration: const BoxDecoration(
              color: GlobalColors.labelBackground,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(4), topRight: Radius.circular(4)),
            ),
            child: Text(
              "${query.year} - ${query.last}",
              style: const TextStyle(color: GlobalColors.labelForeground),
            ),
          )
        ]));
  }

  Container buildDivider() {
    return Container(
      height: 2,
      color: GlobalColors.lightGrep,
    );
  }

  Container buildVacant() {
    return Container(
      height: 40,
      padding: const EdgeInsets.only(top: 10.0),
      child: const Text("没有日程"),
    );
  }

  Container buildItem(TimeListDay one) {
    List<Widget> times = [];
    for (TimeListModel day in one.items) {
      Widget avatar;
      switch (day.phase) {
        case "finish":
          avatar = const Icon(Icons.check_box_rounded, size: 13, color: GlobalColors.lightOrange,);
          break;
        case "start":
          avatar = const Icon(Icons.add_box_rounded, size: 13, color: GlobalColors.lightAccent,);
          break;
        default:
          avatar = const Icon(Icons.data_saver_off, size: 13);
          break;
      }

      Chip item = Chip(
          padding: const EdgeInsets.only(top: 0, bottom: 0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          label: Text(
            day.title,
            overflow: TextOverflow.ellipsis,
            style:
                const TextStyle(fontSize: 13, color: GlobalColors.lightGreen),
          ),
          labelPadding: const EdgeInsets.only(left: -6, right: 6),
          avatar: avatar);

      times.add(item);
    }

    // 是否计数
    bool countDisable = one.category == "annual_report";
    // 每天的颜色
    Color dayColor = (one.category == "annual_report"
        ? GlobalColors.lightApple
        : GlobalColors.lightBlack);
    // 圆圈的颜色
    Color circleColor = (one.category == "annual_report"
        ? GlobalColors.lightApple
        : GlobalColors.lightGreen);
    double circleHeight = (one.category == "annual_report" ? 22 : 16);
    double circlePrefix = (one.category == "annual_report" ? 9 : 13);

    return Container(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        child: Column(
          children: [
            // 第1行，天数长度
            Row(
              children: <Widget>[
                SizedBox(
                  width: 20,
                  height: one.upHeight,
                ),
                SizedBox(
                  width: 2,
                  height: one.upHeight,
                  child: const DecoratedBox(
                    decoration: BoxDecoration(
                      color: GlobalColors.lightGreen,
                    ),
                  ),
                ),
              ],
            ),
            // 第2行，创建日期+任务数
            Row(
              children: <Widget>[
                SizedBox(
                  width: circlePrefix,
                  height: 16,
                ),
                Icon(
                  Icons.circle_rounded,
                  color: circleColor,
                  size: circleHeight,
                ),
                const SizedBox(
                  width: 10,
                  height: 16,
                ),
                Text(one.title, style: TextStyle(color: dayColor)),
                const SizedBox(
                  width: 5,
                  height: 16,
                ),
                Visibility(
                  visible: !countDisable,
                  child: Container(
                    width: 20,
                    height: 16,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      color: Colors.blueAccent,
                      borderRadius: BorderRadius.horizontal(
                          left: Radius.circular(5), right: Radius.circular(5)),
                    ),
                    child: Text(
                      one.count.toString(),
                      style: const TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ),
                )
              ],
            ),
            // 第3行，项目列表
            Row(
              children: <Widget>[
                SizedBox(
                  width: 20,
                  height: one.downHeight,
                ),
                SizedBox(
                  width: 2,
                  height: one.downHeight,
                  child: const DecoratedBox(
                    decoration: BoxDecoration(color: GlobalColors.lightGreen),
                  ),
                ),
                SizedBox(
                  width: 12,
                  height: one.downHeight,
                ),
                Expanded(
                  child: Wrap(
                    spacing: 4,
                    direction: Axis.horizontal,
                    children: times,
                  ),
                ),
              ],
            ),
          ],
        ));
  }

  queryTime() {
    // 获取数据
    UtilityHttp.get("/api/schedule/timeline",
        query: {"year": query.year, "plan": query.plan}).then((result) {
      if (result.isSuccess()) {
        List data = result.data as List;

        if (data.isEmpty) {
          _noneTask = true;
        }

        for (var one in data) {
          TimeListModel model = TimeListModel.fromJson(one);
          TimeListDay.append(models, model);
        }
        setState(() {
          _hasTask = models.isNotEmpty;
        });
      }
    });
  }

  scrollTime() {
    _timeScroll.addListener(() {
      if (!_loadTask &&
          !_noneTask &&
          _timeScroll.position.pixels == _timeScroll.position.maxScrollExtent) {
        _loadTask = true;
        query.next();
        queryTime();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [];
    // 导航
    widgets.add(buildNavigator());

    // 时间线数据
    if (_hasTask) {
      ListView tasks = ListView.builder(
          itemBuilder: (context, index) {
            Container row = buildItem(models[index]);
            return row;
          },
          itemCount: models.length,
          shrinkWrap: true,
          controller: _timeScroll);

      // 防止OVERFLOW
      widgets.add(Expanded(
        child: tasks,
      ));
    } else {
      widgets.add(buildVacant());
    }

    return Column(
      children: widgets,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _timeScroll.dispose();
  }
}
