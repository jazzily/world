import 'dart:math';
import 'package:bloom/pages/time/time_list_model.dart';
import 'package:bloom/utilities/utility_date.dart';

class TimeListDay {
  // 那天
  String day = "";
  // 标题，显示那天或那年
  String title = "";

  // 类别
  String category = "";

  // 个数
  int count = 0;

  // 上宽度，由天数计算
  double upHeight = 50;

  // 下宽度，由个数计算
  double downHeight = 50;

  // 字符长度，计算下宽度
  int _charWidth = 0;

  late List<TimeListModel> items;

  TimeListDay();

  void addDay(TimeListModel model) {
    items.add(model);
    count++;

    _charWidth = _charWidth + model.title.length;
    downHeight = max(_charWidth * 5, 50);
  }

  static TimeListDay create(TimeListModel model) {
    TimeListDay day = TimeListDay();
    day.day = model.createDate;
    day.count = 1;
    day.items = [];
    day.items.add(model);

    // annual_report年度总结
    if (model.category == "annual_report") {
      day.category = "annual_report";
      day.title = "${model.phase}年度报告";
    } else {
      day.category = "daily_schedule";
      day.title = model.createDate;
    }

    return day;
  }

  static void append(List<TimeListDay> days, TimeListModel model) {
    bool has = false;
    String last = model.createDate;
    for (TimeListDay day in days) {
      if (day.day == model.createDate) {
        has = true;
        day.addDay(model);
        break;
      } else {
        last = day.day;
      }
    }

    // 是否新增
    if (!has) {
      TimeListDay now = TimeListDay.create(model);
      // 计算天数
      DateTime lastDay = UtilityDate.fromDateString(last);
      DateTime nextDay = UtilityDate.fromDateString(now.day);
      int difDays = lastDay.difference(nextDay).inDays;
      // 第1个
      if (difDays == 0) {
        difDays = 1;
      }
      // 大于10个
      if (difDays > 10) {
        difDays = 10;
      }

      now.upHeight = difDays * 30;
      days.add(now);
    }
  }
}
