import 'package:bloom/utilities/utility_date.dart';

class TimeListModel {
  late String id;
  late String title;
  late String phase;
  late String category;
  late String scheduleTitle;
  // 创建时间，日期+时间
  late String createTime;
  // 创建日期
  late String createDate;

  TimeListModel();

  static TimeListModel empty() {
    TimeListModel model = TimeListModel();
    model.id = "0";
    model.title = "";
    model.phase = "";
    model.category = "";
    model.scheduleTitle = "";
    model.createTime = "";
    return model;
  }

  TimeListModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    title = json["title"];
    phase = json["phase"];
    category = json["category"];
    scheduleTitle = json["scheduleTitle"];
    createTime = json["createTime"];
    createDate = UtilityDate.getDateString(createTime);
  }
}
