class TimeListQuery {
  late String last;
  late String year;
  late String plan;

  TimeListQuery();

  next() {
    year = (int.parse(year) - 1).toString();
  }

  static TimeListQuery moment() {
    DateTime now = DateTime.now();

    TimeListQuery model = TimeListQuery();
    model.last = now.year.toString();
    model.year = now.year.toString();
    model.plan = "false";
    return model;
  }

  TimeListQuery.fromJson(Map<String, dynamic> json) {
    year = json["year"];
    plan = json["plan"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["year"] = year;
    data["plan"] = plan;
    return data;
  }
}
