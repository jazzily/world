import 'package:flutter/material.dart';
import 'package:bloom/event/global_event.dart';
import 'package:bloom/model/global_data.dart';
import 'package:bloom/pages/time/time_list.dart';
import 'package:bloom/pages/page/page_drawer.dart';
import 'package:bloom/pages/page/page_header.dart';

class TimePage extends StatelessWidget {
  const TimePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const PageHeader(),
      drawer: const PageDrawer(),
      onDrawerChanged: (isOpen) {
        if (isOpen) {
          globalEvent.emit(GlobalData.eventMenuOpen);
        }
      },
      body: const TimeList(),
    );
  }
}
