import 'dart:async';
import 'package:bloom/model/author_info.dart';
import 'package:bloom/pages/page/about_privacy.dart';
import 'package:bloom/pages/page/about_usage.dart';
import 'package:bloom/themes/global_colors.dart';
import 'package:flutter/material.dart';
import 'package:bloom/pages/plan_page.dart';
import 'package:bloom/pages/user/account_login_model.dart';
import 'package:bloom/utilities/utility_http.dart';
import 'package:bloom/utilities/utility_storage.dart';
import 'package:bloom/model/global_data.dart';

class AccountLogin extends StatefulWidget {
  const AccountLogin({super.key});

  @override
  State<AccountLogin> createState() => _AccountLoginState();
}

class _AccountLoginState extends State<AccountLogin> {
  // 实体
  AccountLoginModel model = AccountLoginModel();

  // 前用户
  String lastName = "";

  // 控制器
  var hidePassword = true;
  var authMessage = false;

  // 计时器
  Timer? _countTimer;
  var _countDown = 0;

  _AccountLoginState() {
    AuthorInfo info = GlobalData.instance.clearAccount();
    lastName = info.email;
    model.userName = info.email;
  }

  void startCountTimer() {
    _countDown = 60;
    _countTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (_countDown < 1) {
          timer.cancel();
        } else {
          _countDown--;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 100, left: 30, right: 30),
            child: Column(children: [
              const Text("WE.L.COME",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  )),
              const Text("一切都是最好的安排",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                  )),
              const Padding(padding: EdgeInsets.only(top: 80)),
              TextField(
                decoration: const InputDecoration(
                    icon: Icon(Icons.person),
                    hintText: "用户邮箱",
                    border: InputBorder.none),
                controller: TextEditingController(text: lastName),
                onChanged: (text) {
                  lastName = text;
                  model.userName = text.trim();
                },
              ),
              const Padding(padding: EdgeInsets.only(top: 10)),
              TextField(
                obscureText: hidePassword,
                decoration: InputDecoration(
                  icon: const Icon(Icons.pattern),
                  hintText: "登录密码",
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                    icon: Icon(
                        hidePassword ? Icons.visibility : Icons.visibility_off),
                    onPressed: () {
                      setState(() {
                        hidePassword = !hidePassword;
                      });
                    },
                  ),
                ),
                onChanged: (text) {
                  model.password = text;
                },
              ),
              const Padding(padding: EdgeInsets.only(top: 10)),
              Visibility(
                visible: authMessage,
                child: TextField(
                  decoration: InputDecoration(
                      icon: const Icon(Icons.verified_user_rounded),
                      hintText: "验证码",
                      border: InputBorder.none,
                      suffixIcon: IgnorePointer(
                        ignoring: _countDown > 0,
                        child: TextButton(
                            child:
                                Text(_countDown == 0 ? "发送" : "${_countDown}s"),
                            onPressed: () {
                              // 发送验证码
                              startCountTimer();
                              UtilityHttp.get("/api/message/email", query: {
                                "target": model.userName.trim(),
                                "category": "LOGIN_EMAIL"
                              }).then((data) {
                                var message =
                                    data.isSuccess() ? "发送成功" : data.message;
                                var snackBar = SnackBar(content: Text(message));
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              });
                            }),
                      )),
                  onChanged: (text) {
                    model.authCode = text;
                  },
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 50),
                width: 300,
                child: TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.resolveWith((states) {
                      return GlobalColors.submitForeground;
                    }),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                    ),
                    backgroundColor:
                        MaterialStateProperty.resolveWith((states) {
                      return GlobalColors.submitBackground;
                    }),
                  ),
                  child: const Text(
                    "登录/注册",
                  ),
                  onPressed: () {
                    // 请求接口
                    UtilityHttp.post("/api/account/signin", model).then((data) {
                      if (data.isSuccess()) {
                        // 本地保存
                        UtilityStorage.set(GlobalData.userToken, data.data);
                      } else {
                        // 多次失败会锁定-返回A1400
                        // 需要使用验证码
                        if (data.code == "A1400") {
                          setState(() {
                            authMessage = true;
                          });
                        }
                      }
                      return data;
                    }).then((data) {
                      if (data.isSuccess()) {
                        UtilityHttp.get("/api/mine").then((data) {
                          if (data.isSuccess()) {
                            GlobalData.instance.writeAccount(data.data);
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => const PlanPage()),
                                (router) => false);
                          }
                        });
                      }
                    });
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("登录表示同意"),
                    const SizedBox(width: 2),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const AboutUsage()));
                      },
                      child: const Text("<用户协议>",
                          style: TextStyle(color: GlobalColors.lightBlue)),
                    ),
                    const SizedBox(width: 2),
                    InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => const AboutPrivacy()));
                        },
                        child: const Text("<隐私协议>",
                            style: TextStyle(color: GlobalColors.lightBlue)))
                  ],
                ),
              )
            ]),
          ),
        ));
  }

  @override
  void dispose() {
    super.dispose();
    if (_countTimer != null) {
      _countTimer!.cancel();
    }
  }
}
