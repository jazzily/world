class AccountLoginModel {
  String userName = "";
  String password = "";
  String authCode = "";

  AccountLoginModel();

  static AccountLoginModel of(
      String userName, String password, String authCode) {
    AccountLoginModel model = AccountLoginModel();
    model.userName = userName;
    model.password = password;
    model.authCode = authCode;
    return model;
  }

  AccountLoginModel.fromJson(Map<String, dynamic> json) {
    userName = json["userName"];
    password = json["password"];
    authCode = json["authCode"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["userName"] = userName;
    data["password"] = password;
    data["authCode"] = authCode;
    return data;
  }
}
