class AccountLoginToken {
  String token = "";
  DateTime expire = DateTime(1970, 1, 1);

  AccountLoginToken();

  bool isValid() {
    return expire.isAfter(DateTime.now());
  }

  AccountLoginToken.fromJson(Map<String, dynamic> json) {
    token = json["token"];
    expire = DateTime.now().add(Duration(seconds: json["expire"]));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["token"] = token;
    data["expire"] = expire;
    return data;
  }

  static AccountLoginToken empty() {
    AccountLoginToken token = AccountLoginToken();
    return token;
  }
}
