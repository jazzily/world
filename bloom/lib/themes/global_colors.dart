import 'package:flutter/material.dart';

class GlobalColors {
  static const Color lightDark = Color(0xFF333333);
  static const Color lightGrep = Color(0xFFD3D3D3);
  static const Color lightBlack = Color(0xFF0D253F);
  static const Color lightBlue = Color(0xFF009688);
  static const Color lightGreen = Color(0xFF2196F3);
  static const Color lightOrange = Color(0xFFFF9800);
  static const Color lightAccent = Color(0xFF9E9E9E);
  static const Color lightApple = Color(0xFFFF5252);

  static const Color labelBackground = Color(0xFF2196F3);
  static const Color labelForeground = Color(0xFFFFFFFF);
  static const Color submitBackground = Color(0xFF2196F3);
  static const Color submitForeground = Color(0xFFFFFFFF);
  static const Color deleteBackground = Color(0xFF9E9E9E);
  static const Color deleteForeground = Color(0xFFFF5252);
}