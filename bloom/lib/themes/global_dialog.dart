import 'package:flutter/material.dart';

class GlobalDialog {
  // 私有构造函数
  GlobalDialog._internal();

  // 保存单例
  static final GlobalDialog _instance = GlobalDialog._internal();

  // 工厂构造
  factory GlobalDialog() => _instance;

  Future<dynamic> showConfirmDialog(
      BuildContext context, String title, String content) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            MaterialButton(
              color: Colors.teal,
              child: const Text(
                "取消",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            MaterialButton(
              color: Colors.red,
              child: const Text(
                "确定",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            )
          ],
        );
      },
    );
  }
}

// 定义一个top-level（全局）变量，页面引入该文件后可以直接使用bus
var globalDialog = GlobalDialog();
