/// 统一返回结果
class GlobalJson {
  static const String successCode = "0";

  late String code;
  late String message;
  late dynamic data;

  bool isSuccess() {
    return successCode == code;
  }

  bool noSuccess() {
    return !isSuccess();
  }

  static GlobalJson result() {
    GlobalJson result = GlobalJson();
    result.code = "0";
    result.message = "ok";
    return result;
  }

  static GlobalJson error() {
    GlobalJson result = GlobalJson();
    result.code = "E500";
    result.message = "内部错误";
    return result;
  }

  static GlobalJson fromJson(Map<String, dynamic> json) {
    GlobalJson result = GlobalJson();
    result.code = json["code"];
    result.message = json["message"];
    return result;
  }
}
