import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:bloom/utilities/global_json.dart';
import 'package:bloom/utilities/global_router.dart';
import 'package:bloom/pages/user/account_login.dart';
import 'package:bloom/pages/user/account_login_token.dart';
import 'package:bloom/resources/application.dart';

import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart' as path;

/// 请求工具类
class UtilityHttp {
  static const _baseUri = Application.apiUrl;
  static const String userToken = "USER_TOKEN";

  // 日志
  static Logger logger = Logger();

  static Future<AccountLoginToken> _userToken() async {
    final storage = await SharedPreferences.getInstance();
    if (storage.containsKey(userToken)) {
      final jsonStr = storage.getString(userToken);
      final jsonMap = jsonDecode(jsonStr!);
      return AccountLoginToken.fromJson(jsonMap);
    }

    return AccountLoginToken.empty();
  }

  static Uri _requestUri(String path, {Map<String, String>? query}) {
    if (query != null) {
      bool has = false;
      StringBuffer sbQuery = StringBuffer();
      query.forEach((key, value) {
        if (has) {
          sbQuery.write("&");
        } else {
          sbQuery.write("?");
          has = true;
        }
        sbQuery.write(key);
        sbQuery.write("=");
        sbQuery.write(value);
      });
      return Uri.parse("$_baseUri$path${sbQuery.toString()}");
    }

    return Uri.parse("$_baseUri$path");
  }

  static Map<String, String> _requestHeader(AccountLoginToken token) {
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=UTF-8",
    };

    // authorization
    if (token.isValid()) {
      headers["Authorization"] = "Bearer ${token.token}";
    }

    return headers;
  }

  static GlobalJson _responseResult(http.Response response) {
    // response status
    var code = response.statusCode;
    var body = response.body;

    final context = GlobalRouter.navigatorContext;
    // auth error
    if (code == 401) {
      Navigator.of(context!)
          .push(MaterialPageRoute(builder: (context) => const AccountLogin()));
      return GlobalJson.error();
    }

    // request success
    if (body.isNotEmpty) {
      String decoder = const Utf8Decoder().convert(response.bodyBytes);
      Map<String, dynamic> json = jsonDecode(decoder);
      GlobalJson result = GlobalJson.fromJson(json);
      // no permission
      if (result.code == "401") {
        final context = GlobalRouter.navigatorContext;
        Navigator.of(context!).push(
            MaterialPageRoute(builder: (context) => const AccountLogin()));
        return GlobalJson.error();
      }

      // response success
      if (result.isSuccess()) {
        result.data = json["data"];
      } else {
        var snackBar = SnackBar(content: Text(json["message"]));
        ScaffoldMessenger.of(context!).showSnackBar(snackBar);
      }

      return result;
    }

    // other error
    return GlobalJson.error();
  }

  static Future<GlobalJson> get(String path,
      {Map<String, String>? query}) async {
    AccountLoginToken token = await _userToken();
    final response = await http
        .get(_requestUri(path, query: query), headers: _requestHeader(token))
        .timeout(const Duration(seconds: 60));
    return _responseResult(response);
  }

  static Future<GlobalJson> post(String path, Object data) async {
    AccountLoginToken token = await _userToken();
    final response = await http
        .post(
          _requestUri(path),
          headers: _requestHeader(token),
          body: jsonEncode(data),
        )
        .timeout(const Duration(seconds: 60));

    return _responseResult(response);
  }

  static Future<GlobalJson> put(String path, Object data) async {
    AccountLoginToken token = await _userToken();
    final response = await http
        .put(
          _requestUri(path),
          headers: _requestHeader(token),
          body: jsonEncode(data),
        )
        .timeout(const Duration(seconds: 60));

    return _responseResult(response);
  }

  static Future<GlobalJson> delete(String path, Object data) async {
    AccountLoginToken token = await _userToken();
    final response = await http
        .delete(
          _requestUri(path),
          headers: _requestHeader(token),
          body: jsonEncode(data),
        )
        .timeout(const Duration(seconds: 60));

    return _responseResult(response);
  }

  static Future<String> image(String url) async {
    if (url.isEmpty) {
      return "";
    }

    // 查看文件是否存在 base64(url)
    var code = base64.encode(url.codeUnits);
    var ext = url.substring(url.lastIndexOf("."));

    bool exist = false;
    String name = "";
    File? file;
    try {
      var dir = await path.getApplicationDocumentsDirectory();
      name = "${dir.path}/$code$ext";
      logger.i(name);
      file = File(name);
      exist = await file.exists();
      if (!exist) {
        await file.create();
      }
    } catch (e) {
      logger.e('utility-http.image error', error: e);
    }

    // 存在则返回
    if (exist) {
      return name;
    }

    // 请求
    final response = await http.get(Uri.parse(url), headers: {
      "Content-Type": "application/octet-stream",
    }).timeout(const Duration(seconds: 60));

    // 请求成功
    if (response.statusCode == 200 && response.body.isNotEmpty) {
      file!.writeAsBytes(response.bodyBytes, flush: true);
      return name;
    }
    return "";
  }
}
