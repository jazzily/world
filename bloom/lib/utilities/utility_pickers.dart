import 'package:flutter_pickers/time_picker/model/date_type.dart';
import 'package:flutter_pickers/time_picker/model/pduration.dart';

/// 选择器公共类
class UtilityPickers {
  static PDuration moment() {
    var now = DateTime.now();
    PDuration moment = PDuration.parse(now);
    moment.setSingle(DateType.Hour, 0);
    moment.setSingle(DateType.Minute, 0);
    moment.setSingle(DateType.Second, 0);
    return moment;
  }
}
