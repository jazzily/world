import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

/// 存储公共类
class UtilityStorage {
  static set(String key, Map<String, dynamic> data) async {
    final storage = await SharedPreferences.getInstance();
    final json = jsonEncode(data);
    await storage.setString(key, json);
  }

  static Future<Map<String, dynamic>> get(String key) async {
    final storage = await SharedPreferences.getInstance();
    if (storage.containsKey(key)) {
      final json = storage.getString(key);
      return jsonDecode(json!);
    }

    return {};
  }

  static remove(String key) async {
    final storage = await SharedPreferences.getInstance();
    storage.remove(key);
  }
}
