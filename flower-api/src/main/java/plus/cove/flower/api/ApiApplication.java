package plus.cove.flower.api;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;

/**
 * 启动应用
 *
 * @author jimmy.zhang
 * @since  1.0
 */
@QuarkusMain
public class ApiApplication {
    public static void main(String[] args) {
        Quarkus.run(ApiCommand.class, args);
    }
}
