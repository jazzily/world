package plus.cove.flower.api;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;

/**
 * 启动应用
 *
 * @author jimmy.zhang
 * @since  1.0
 */
public class ApiCommand implements QuarkusApplication {
    @Override
    public int run(String... args) throws Exception {
        Quarkus.waitForExit();
        return 0;
    }
}
