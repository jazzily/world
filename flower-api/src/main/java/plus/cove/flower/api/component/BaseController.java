package plus.cove.flower.api.component;

import io.quarkus.security.identity.SecurityIdentity;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import jakarta.inject.Inject;
import plus.cove.flower.application.PrincipalApplication;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.principal.PrincipalError;
import plus.cove.flower.domain.principal.PrincipalUser;
import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.BusinessException;
import plus.cove.infrastructure.extension.authorizer.JwtAuthorizer;
import plus.cove.infrastructure.extension.authorizer.JwtClaim;

/**
 * 控制器基类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class BaseController {
    @Inject
    protected HttpServerRequest request;
    @Inject
    protected HttpServerResponse response;

    @Inject
    SecurityIdentity securityIdentity;
    @Inject
    PrincipalApplication principalApp;

    /**
     * 成功
     */
    protected ActionResult success() {
        return ActionResult.success();
    }

    /**
     * 成功
     */
    protected ActionResult success(Object data) {
        return ActionResult.result().succeed(data);
    }

    // 获取当前用户
    private PrincipalUser getUser() {
        if (securityIdentity.getPrincipal() instanceof JwtAuthorizer jwt) {
            JwtClaim claim = jwt.getClaim();
            return principalApp.findPrincipal(Long.parseLong(claim.getClaim()));
        }

        return null;
    }

    /**
     * 获取当前用户
     */
    protected PrincipalUser currentUser() {
        PrincipalUser user = this.getUser();
        if (user == null) {
            throw new BusinessException(PrincipalError.PRINCIPAL_NO_EXIST);
        }

        return user;
    }

    /**
     * 填充请求信息
     * 用户信息存在则获取，不存在则不获取
     */
    protected void withRequest(RequestInput input) {
        // 跟踪
        String trace = response.headers().get("X-Product-Trace");
        // 路由 POST/api/account/login
        String route = response.headers().get("X-Product-Router");
        // 版本 [WEB|APP][100]
        String version = request.getHeader("X-Product-Version");
        input.withRequest(trace, route, version);

        // 用户
        PrincipalUser user = this.getUser();
        if (user != null) {
            input.withPrincipal(user);
        }
    }

    /**
     * 填充请求信息，
     * 必须包括用户信息，无信息则异常
     *
     * @param input 当前请求
     */
    protected void withPrincipal(RequestInput input) {
        // 跟踪
        String trace = response.headers().get("X-Product-Trace");
        // 路由 POST/api/account/login
        String route = response.headers().get("X-Product-Router");
        // 版本 [WEB|APP][100]
        String version = request.getHeader("X-Product-Version");
        input.withRequest(trace, route, version);

        // 用户
        input.withPrincipal(this.currentUser());
    }
}
