package plus.cove.flower.api.config;

import org.jboss.logging.Logger;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * api配置
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class WebApiConfig {
    private static final Logger LOG = Logger.getLogger(WebApiConfig.class);

    public WebApiConfig() {
    }
}
