package plus.cove.flower.api.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.jackson.ObjectMapperCustomizer;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Singleton;
import org.jboss.logging.Logger;
import plus.cove.infrastructure.extension.serializer.JsonRegister;

/**
 * api配置
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class WebJsonConfig {
    private static final Logger LOG = Logger.getLogger(WebJsonConfig.class);

    final JsonRegister jsonRegister;

    public WebJsonConfig(final JsonRegister jsonRegister) {
        this.jsonRegister = jsonRegister;
    }

    @Singleton
    ObjectMapper objectMapper(Instance<ObjectMapperCustomizer> customizers) {
        ObjectMapper mapper = jsonRegister.jsonMapper();
        for (ObjectMapperCustomizer customizer : customizers) {
            customizer.customize(mapper);
        }
        return mapper;
    }
}
