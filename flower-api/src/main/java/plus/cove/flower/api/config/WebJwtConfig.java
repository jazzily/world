package plus.cove.flower.api.config;

import io.quarkus.security.AuthenticationFailedException;
import io.smallrye.jwt.auth.principal.JWTAuthContextInfo;
import io.smallrye.jwt.auth.principal.JWTCallerPrincipal;
import io.smallrye.jwt.auth.principal.JWTCallerPrincipalFactory;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import org.jboss.logging.Logger;
import plus.cove.infrastructure.extension.authorizer.JwtAuthorizer;
import plus.cove.infrastructure.extension.authorizer.JwtClaim;
import plus.cove.infrastructure.extension.authorizer.JwtUtils;

/**
 * jwt配置
 * <p>
 * 使用自定义jwt配置和生成
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Priority(1)
@Alternative
@ApplicationScoped
public class WebJwtConfig extends JWTCallerPrincipalFactory {
    private static final Logger LOG = Logger.getLogger(WebJwtConfig.class);

    final JwtUtils jwtUtils;

    public WebJwtConfig(final JwtUtils jwtUtils) {
        super();
        this.jwtUtils = jwtUtils;
    }

    @Override
    public JWTCallerPrincipal parse(String token, JWTAuthContextInfo authContextInfo) {
        try {
            JwtClaim jwt = jwtUtils.decode(token);
            return new JwtAuthorizer(token, jwt);
        } catch (Exception ex) {
            throw new AuthenticationFailedException(ex.getMessage());
        }
    }
}
