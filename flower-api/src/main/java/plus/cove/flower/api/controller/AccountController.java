package plus.cove.flower.api.controller;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import plus.cove.flower.api.component.BaseController;
import plus.cove.flower.application.AccountApplication;
import plus.cove.flower.application.view.account.AccountLoginInput;
import plus.cove.flower.application.view.account.AccountLoginOutput;
import plus.cove.flower.application.view.account.AccountResetInput;
import plus.cove.flower.application.view.account.AccountSignupInput;
import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.extension.security.SecurityUtils;

import jakarta.validation.Valid;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * 用户控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Tag(name = "用户管理")
@Path(value = "/api/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountController extends BaseController {
    private static final Logger LOG = Logger.getLogger(AccountController.class);

    final AccountApplication accountApp;
    final SecurityUtils securityUtils;

    AccountController(
            final AccountApplication accountApp,
            final SecurityUtils securityUtils) {
        this.accountApp = accountApp;
        this.securityUtils = securityUtils;
    }

    @Operation(summary = "用户注册")
    @POST
    @Path("signup")
    public ActionResult userSignup(@Valid AccountSignupInput input) {
        AccountLoginOutput output = accountApp.signup(input);
        return success(output);
    }

    @Operation(summary = "用户登录")
    @POST
    @Path("login")
    public ActionResult userLogin(@Valid AccountLoginInput input) {
        securityUtils.apiIdempotent(this.request);
        // withRequest(input);

        AccountLoginOutput output = accountApp.login(input);
        return success(output);
    }

    @Operation(summary = "用户登录或注册")
    @POST
    @Path("signin")
    public ActionResult userSign(@Valid AccountLoginInput input) {
        AccountLoginOutput output = accountApp.signin(input);
        return success(output);
    }

    @Operation(summary = "重置密码")
    @POST
    @Path("password")
    public ActionResult resetPassword(@Valid AccountResetInput input) {
        return accountApp.resetPassword(input);
    }
}
