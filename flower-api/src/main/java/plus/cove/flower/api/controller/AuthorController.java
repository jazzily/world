package plus.cove.flower.api.controller;

import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import plus.cove.flower.api.component.BaseController;
import plus.cove.flower.application.AccountApplication;
import plus.cove.flower.application.AuthorApplication;
import plus.cove.flower.application.view.author.AuthorChangeInput;
import plus.cove.flower.application.view.author.AuthorLogoutInput;
import plus.cove.flower.application.view.author.AuthorMineInput;
import plus.cove.flower.application.view.author.AuthorMineOutput;
import plus.cove.infrastructure.component.ActionResult;

/**
 * 作者控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Tag(name = "我的")
@Path(value = "/api/mine")
@Produces(MediaType.APPLICATION_JSON)
public class AuthorController extends BaseController {
    private static final Logger LOG = Logger.getLogger(AuthorController.class);

    final AccountApplication accountApp;
    final AuthorApplication authorApp;

    AuthorController(
            final AccountApplication accountApp,
            final AuthorApplication authorApp) {
        this.accountApp = accountApp;
        this.authorApp = authorApp;
    }

    @Operation(summary = "获取个人信息")
    @GET
    @Path("")
    public ActionResult getAuthor() {
        Long authorId = this.currentUser().getUserId();
        AuthorMineOutput author = authorApp.loadAuthor(authorId);
        return success(author);
    }

    @Operation(summary = "更新个人信息")
    @PUT
    @Path("info")
    public ActionResult putAuthor(@Valid AuthorMineInput input) {
        withPrincipal(input);

        authorApp.updateAuthor(input);
        AuthorMineOutput author = authorApp.loadAuthor(input.getRequestUser());
        return success(author);
    }

    @Operation(summary = "修改账号")
    @PUT
    @Path("account")
    public ActionResult changeAccount(@Valid AuthorChangeInput input) {
        withPrincipal(input);
        authorApp.changeAuthor(input);
        return success();
    }

    @Operation(summary = "注销账号")
    @POST
    @Path("logout")
    public ActionResult logoutAccount(@Valid AuthorLogoutInput input) {
        withPrincipal(input);
        authorApp.logoutAuthor(input);
        return success();
    }
}
