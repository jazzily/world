package plus.cove.flower.api.controller;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import plus.cove.flower.application.GlobalApplication;
import plus.cove.flower.domain.entity.global.CalendarData;
import plus.cove.flower.api.component.BaseController;
import plus.cove.infrastructure.component.ActionResult;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

/**
 * 日历控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Tag(name = "日历")
@Path(value = "/api/calendar")
@Produces(MediaType.APPLICATION_JSON)
public class CalendarController extends BaseController {
    private static final Logger LOG = Logger.getLogger(CalendarController.class);

    final GlobalApplication globalApp;

    CalendarController(
            final GlobalApplication globalApp) {
        this.globalApp = globalApp;
    }

    @Operation(summary = "按月获取日历信息", description = "获取某月的日历，格式yyyy-MM")
    @GET
    @Path("/{month}")
    public ActionResult month(@PathParam("month") String month) {
        List<CalendarData> months = globalApp.findCalendar(month);
        return success(months);
    }
}
