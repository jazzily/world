package plus.cove.flower.api.controller;

import io.quarkus.cache.CacheResult;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import plus.cove.flower.application.DistrictApplication;
import plus.cove.flower.application.view.district.DistrictQueryInput;
import plus.cove.flower.application.view.district.DistrictQueryOutput;
import plus.cove.flower.domain.entity.global.DistrictType;
import plus.cove.flower.api.component.BaseController;
import plus.cove.infrastructure.component.ActionResult;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

/**
 * 区域控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Tag(name = "城市")
@Path(value = "/api/district")
@Produces(MediaType.APPLICATION_JSON)
public class DistrictController extends BaseController {
    private static final Logger LOG = Logger.getLogger(DistrictController.class);

    final DistrictApplication districtApp;

    DistrictController(final DistrictApplication districtApp) {
        this.districtApp = districtApp;
    }

    @GET
    @Path("city")
    @CacheResult(cacheName = "district-city")
    public ActionResult getCity() {
        DistrictQueryInput input = DistrictQueryInput.from(DistrictType.CITY);
        List<DistrictQueryOutput> list = districtApp.loadDistrict(input);
        return success(list);
    }
}
