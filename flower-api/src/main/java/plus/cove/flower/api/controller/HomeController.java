package plus.cove.flower.api.controller;

import plus.cove.flower.api.component.BaseController;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * 首页控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Path(value = "/")
public class HomeController extends BaseController {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String homeIndex() {
        return "ok";
    }
}
