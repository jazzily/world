package plus.cove.flower.api.controller;

import jakarta.validation.Valid;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import plus.cove.flower.api.component.BaseController;
import plus.cove.flower.application.IssueApplication;
import plus.cove.flower.application.view.issue.IssueSaveInput;
import plus.cove.infrastructure.component.ActionResult;

/**
 * 需求控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Tag(name = "需求")
@Path(value = "/api/issue")
@Produces(MediaType.APPLICATION_JSON)
public class IssueController extends BaseController {
    private static final Logger LOG = Logger.getLogger(IssueController.class);

    final IssueApplication issueApp;

    IssueController(final IssueApplication issueApp) {
        this.issueApp = issueApp;
    }

    @Operation(summary = "反馈建议")
    @POST
    @Path("")
    public ActionResult save(@Valid IssueSaveInput input) {
        withRequest(input);
        issueApp.saveIssue(input);
        return success();
    }
}
