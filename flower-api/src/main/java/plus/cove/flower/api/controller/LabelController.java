package plus.cove.flower.api.controller;

import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import plus.cove.flower.api.component.BaseController;
import plus.cove.flower.application.LabelApplication;
import plus.cove.flower.application.view.label.LabelDeleteInput;
import plus.cove.flower.application.view.label.LabelListOutput;
import plus.cove.flower.application.view.label.LabelUpdateInput;
import plus.cove.infrastructure.component.ActionResult;

import java.util.List;

/**
 * 标签控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Tag(name = "标签")
@Path(value = "/api/label")
@Produces(MediaType.APPLICATION_JSON)
public class LabelController extends BaseController {
    private static final Logger LOG = Logger.getLogger(LabelController.class);

    final LabelApplication labelApp;

    LabelController(
            final LabelApplication labelApp) {
        this.labelApp = labelApp;
    }

    @Operation(summary = "获取标签")
    @GET
    @Path("")
    public ActionResult getLabel() {
        Long userId = this.currentUser().getUserId();
        List<LabelListOutput> list = labelApp.loadLabel(userId);
        return success(list);
    }

    @Operation(summary = "修改标签")
    @PUT
    @Path("")
    public ActionResult updateLabel(@Valid LabelUpdateInput input) {
        withPrincipal(input);
        labelApp.updateLabel(input);
        return success();
    }

    @Operation(summary = "删除标签")
    @DELETE
    @Path("")
    public ActionResult deleteLabel(@Valid LabelDeleteInput input) {
        withPrincipal(input);
        labelApp.deleteLabel(input);
        return success();
    }
}
