package plus.cove.flower.api.controller;

import org.dromara.hutool.core.text.CharSequenceUtil;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.MockMailbox;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import plus.cove.flower.application.GlobalApplication;
import plus.cove.flower.application.constant.CommonConstant;
import plus.cove.flower.application.view.global.ValidationInput;
import plus.cove.flower.api.component.BaseController;
import plus.cove.infrastructure.component.ActionResult;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

/**
 * 消息控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Tag(name = "消息管理")
@Path(value = "/api/message")
@Produces(MediaType.APPLICATION_JSON)
public class MessageController extends BaseController {
    private static final Logger LOG = Logger.getLogger(MessageController.class);

    final GlobalApplication globalApp;
    final MockMailbox mailBox;

    MessageController(
            final GlobalApplication globalApp,
            final MockMailbox mailBox) {
        this.globalApp = globalApp;
        this.mailBox = mailBox;
    }

    @Operation(summary = "邮箱验证码")
    @GET
    @Path("email")
    public ActionResult validation(@BeanParam ValidationInput input) {
        globalApp.saveValidation(input);
        return success();
    }

    @Operation(summary = "邮箱验证码-测试模拟")
    @GET
    @Path("email/mock")
    public ActionResult validationMock(@QueryParam("target") String target,
                                       @QueryParam("clear") String clear) {
        List<Mail> mails = this.mailBox.getMailsSentTo(target);

        // 邮件内容
        List<String> mailContent = mails.stream()
                .map(Mail::getHtml)
                .toList();

        // 清空
        if (CharSequenceUtil.equals(clear, CommonConstant.SUCCESS)) {
            mails.clear();
        }

        return success(mailContent);
    }
}
