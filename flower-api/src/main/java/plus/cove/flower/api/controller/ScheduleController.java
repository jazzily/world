package plus.cove.flower.api.controller;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import plus.cove.flower.api.component.BaseController;
import plus.cove.flower.application.ScheduleApplication;
import plus.cove.flower.application.view.schedule.EventListInput;
import plus.cove.flower.application.view.schedule.EventListOutput;
import plus.cove.flower.application.view.schedule.EventUpdateInput;
import plus.cove.flower.application.view.schedule.*;
import plus.cove.flower.domain.entity.schedule.validator.ScheduleValidator;
import plus.cove.infrastructure.component.ActionResult;

import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

/**
 * 日程控制器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Tag(name = "日程")
@Path(value = "/api/schedule")
@Produces(MediaType.APPLICATION_JSON)
public class ScheduleController extends BaseController {
    private static final Logger LOG = Logger.getLogger(ScheduleController.class);

    final ScheduleApplication scheduleApp;

    ScheduleController(
            final ScheduleApplication scheduleApp) {
        this.scheduleApp = scheduleApp;
    }

    @Operation(summary = "创建日程")
    @POST
    @Path("/")
    public ActionResult saveSchedule(@Valid ScheduleSaveInput input) {
        // 验证
        ScheduleValidator validator = ScheduleValidator.of(
                input.getStartTime(), input.getFinishTime(), input.getRepeatType(), input.getRepeatItem());
        validator.valid();
        withPrincipal(input);

        scheduleApp.saveSchedule(input);
        return success();
    }

    @Operation(summary = "查看列表日程，根据起始，标签或状态查看")
    @GET
    @Path("/")
    public ActionResult listSchedule(@BeanParam @Valid ScheduleListInput input) {
        withPrincipal(input);

        List<ScheduleListOutput> list = scheduleApp.loadSchedule(input);
        return success(list);
    }

    @Operation(summary = "查看单个日程")
    @GET
    @Path("/one")
    public ActionResult oneSchedule(@BeanParam @Valid ScheduleOneInput input) {
        withPrincipal(input);

        ScheduleOneOutput output = scheduleApp.loadSchedule(input);
        return success(output);
    }

    @Operation(summary = "删除日程，删除某个日程及相关事件")
    @DELETE
    @Path("/one")
    public ActionResult deleteSchedule(@Valid ScheduleDeleteInput input) {
        withPrincipal(input);

        scheduleApp.deleteSchedule(input);
        return success();
    }

    @Operation(summary = "更新日程")
    @PUT
    @Path("/one")
    public ActionResult updateSchedule(@Valid ScheduleUpdateInput input) {
        withPrincipal(input);

        scheduleApp.updateSchedule(input);
        return success();
    }

    @Operation(summary = "星标日程，星标或取消星标")
    @PUT
    @Path("/star")
    public ActionResult starSchedule(@Valid ScheduleStarInput input) {
        withPrincipal(input);

        scheduleApp.starSchedule(input);
        return success();
    }

    @Operation(summary = "完成日程，完成日程和相关事件")
    @PUT
    @Path("/complete")
    public ActionResult closeSchedule(@Valid ScheduleCompleteInput input) {
        withPrincipal(input);

        scheduleApp.completeSchedule(input);
        return success();
    }

    @Operation(summary = "查看日程事件列表，根据起始，标签或状态查看")
    @GET
    @Path("/event")
    public ActionResult getEvent(@BeanParam @Valid EventListInput input) {
        withPrincipal(input);

        List<EventListOutput> list = scheduleApp.loadScheduleEvent(input);
        return success(list);
    }

    @Operation(summary = "获取某个日程事件")
    @GET
    @Path("/event/one")
    public ActionResult getEvent(@BeanParam @Valid EventOneInput input) {
        withPrincipal(input);
        EventOneOutput one = scheduleApp.loadScheduleEvent(input);
        return success(one);
    }

    @Operation(summary = "更新某个日程事件")
    @PUT
    @Path("/event/one")
    public ActionResult updateEvent(@Valid EventUpdateInput input) {
        withPrincipal(input);

        scheduleApp.updateScheduleEvent(input);
        return success();
    }

    @Operation(summary = "删除某个日程事件")
    @DELETE
    @Path("/event/one")
    public ActionResult deleteEvent(@Valid EventDeleteInput input) {
        withPrincipal(input);

        scheduleApp.deleteScheduleEvent(input);
        return success();
    }

    @Operation(summary = "新增日程事件")
    @POST
    @Path("/event/append")
    public ActionResult appendEvent(@Valid EventAppendInput input) {
        withPrincipal(input);

        scheduleApp.appendScheduleEvent(input);
        return success();
    }

    @Operation(summary = "完成日程事件")
    @PUT
    @Path("/event/complete")
    public ActionResult completeEvent(@Valid EventCompleteInput input) {
        withPrincipal(input);

        scheduleApp.completeScheduleEvent(input);
        return success();
    }

    @Operation(summary = "添加日程标签")
    @POST
    @Path("/label")
    public ActionResult appendLabel(@Valid ScheduleLabelInput input) {
        withPrincipal(input);

        scheduleApp.appendScheduleLabel(input);
        return success();
    }

    @Operation(summary = "删除日程标签")
    @DELETE
    @Path("/label")
    public ActionResult deleteLabel(@Valid ScheduleLabelInput input) {
        withPrincipal(input);

        scheduleApp.deleteScheduleLabel(input);
        return success();
    }

    @Operation(summary = "查看时间线")
    @GET
    @Path("/timeline")
    public ActionResult listTimeline(@BeanParam @Valid TimelineListInput input) {
        withPrincipal(input);

        List<TimelineListOutput> list = scheduleApp.loadScheduleTimeline(input);
        return success(list);
    }
}
