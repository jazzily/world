package plus.cove.flower.api.test.schedule;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.builder.ScheduleBuilder;
import plus.cove.flower.domain.entity.schedule.ScheduleRepeatType;

import java.time.LocalDateTime;

@QuarkusTest
public class ScheduleBuilderTest {
    @Test
    public void buildOneTime() {
        LocalDateTime startTime = LocalDateTime.of(2023, 5, 1, 9, 0, 0);
        LocalDateTime finishTime = LocalDateTime.of(2023, 5, 1, 12, 0, 0);

        ScheduleBuilder builder = ScheduleBuilder.init(ScheduleRepeatType.ONE_TIME, null);
        builder.title("一次性日程")
                .place("活动地点")
                .remark("活动备注")
                .userId(10000L)
                .plan(startTime, finishTime);
        Schedule entity = builder.build();

        Assertions.assertNotNull(entity);
        Assertions.assertEquals(entity.getEvents().size(), 1);
    }

    @Test
    public void buildEveryDay() {
        LocalDateTime startTime = LocalDateTime.of(2023, 5, 1, 9, 0, 0);
        LocalDateTime finishTime = LocalDateTime.of(2023, 5, 2, 12, 0, 0);

        ScheduleBuilder builder = ScheduleBuilder.init(ScheduleRepeatType.EVERY_DAY, null);
        builder.title("每天日程")
                .place("活动地点")
                .remark("活动备注")
                .userId(10000L)
                .plan(startTime, finishTime);
        Schedule entity = builder.build();

        Assertions.assertNotNull(entity);
        Assertions.assertEquals(entity.getEvents().size(), 2);
    }

    @Test
    public void buildEveryWeek() {
        LocalDateTime startTime = LocalDateTime.of(2023, 5, 1, 9, 0, 0);
        LocalDateTime finishTime = LocalDateTime.of(2023, 6, 1, 12, 0, 0);

        ScheduleBuilder builder = ScheduleBuilder.init(ScheduleRepeatType.EVERY_WEEK, "2,3,");
        builder.title("每周日程")
                .place("活动地点")
                .remark("活动备注")
                .userId(10000L)
                .plan(startTime, finishTime);
        Schedule entity = builder.build();

        Assertions.assertNotNull(entity);
        Assertions.assertEquals(entity.getEvents().size(), 10);
    }

    @Test
    public void buildEveryMonth() {
        LocalDateTime startTime = LocalDateTime.of(2023, 5, 1, 9, 0, 0);
        LocalDateTime finishTime = LocalDateTime.of(2023, 6, 1, 12, 0, 0);

        ScheduleBuilder builder = ScheduleBuilder.init(ScheduleRepeatType.EVERY_MONTH, "02,12,22,");
        builder.title("每月日程")
                .place("活动地点")
                .remark("活动备注")
                .userId(10000L)
                .plan(startTime, finishTime);
        Schedule entity = builder.build();

        Assertions.assertNotNull(entity);
        Assertions.assertEquals(entity.getEvents().size(), 3);
    }

    @Test
    public void buildEveryYear() {
        LocalDateTime startTime = LocalDateTime.of(2023, 1, 1, 9, 0, 0);
        LocalDateTime finishTime = LocalDateTime.of(2023, 12, 31, 12, 0, 0);

        ScheduleBuilder builder = ScheduleBuilder.init(ScheduleRepeatType.EVERY_YEAR, "0201,1220,");
        builder.title("每年日程")
                .place("活动地点")
                .remark("活动备注")
                .userId(10000L)
                .plan(startTime, finishTime);
        Schedule entity = builder.build();

        Assertions.assertNotNull(entity);
        Assertions.assertEquals(entity.getEvents().size(), 2);
    }
}
