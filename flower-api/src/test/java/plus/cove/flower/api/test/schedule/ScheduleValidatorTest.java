package plus.cove.flower.api.test.schedule;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import plus.cove.flower.domain.entity.schedule.ScheduleRepeatType;
import plus.cove.flower.domain.entity.schedule.validator.ScheduleValidator;
import plus.cove.infrastructure.exception.BusinessException;

import java.time.LocalDateTime;

@QuarkusTest
public class ScheduleValidatorTest {
    @Test
    public void validOneTime() {
        LocalDateTime startTime = LocalDateTime.of(2023, 5, 1, 9, 0, 0);
        LocalDateTime finishTime = LocalDateTime.of(2123, 5, 1, 12, 0, 0);

        ScheduleValidator validator = ScheduleValidator.of(
                startTime, finishTime, ScheduleRepeatType.EVERY_DAY, null);

        Assertions.assertThrows(BusinessException.class, validator::valid, "超过日程最大支持数");
    }

    @Test
    public void validEveryWeek() {
        LocalDateTime startTime = LocalDateTime.of(2023, 5, 1, 9, 0, 0);
        LocalDateTime finishTime = LocalDateTime.of(2023, 5, 1, 9, 0, 0);

        ScheduleValidator validator = ScheduleValidator.of(
                startTime, finishTime, ScheduleRepeatType.EVERY_DAY, "1");
        long count = validator.valid();
        Assertions.assertTrue(count < 521);

    }
}
