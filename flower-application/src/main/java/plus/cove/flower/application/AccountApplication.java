package plus.cove.flower.application;

import plus.cove.flower.application.view.account.*;
import plus.cove.infrastructure.component.ActionResult;

/**
 * 账号应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface AccountApplication {
    /**
     * 登录即注册
     * signin = signup+login
     */
    AccountLoginOutput signin(AccountLoginInput input);

    /**
     * 注册
     * <p>
     * 注册后返回登录token，可直接登录
     * <p>
     *
     * @param input 注册输入
     * @return 访问token和过期时间
     */
    AccountLoginOutput signup(AccountSignupInput input);

    /**
     * 登录
     * <p>
     * 使用用户名和密码登录
     * 会对用户名登录状态进行校验和判断
     * 连续登录失败，会要求使用验证码或临时锁定账号
     *
     * @param input 登录输入
     * @return 访问token和过期时间
     */
    AccountLoginOutput login(AccountLoginInput input);

    /**
     * 重置密码
     * <p>
     * 同时修改账号状态
     */
    ActionResult resetPassword(AccountResetInput input);

    /**
     * 修改密码
     */
    ActionResult changePassword(AccountChangeInput input);
}
