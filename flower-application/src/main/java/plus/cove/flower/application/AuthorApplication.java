package plus.cove.flower.application;

import plus.cove.flower.application.view.author.AuthorChangeInput;
import plus.cove.flower.application.view.author.AuthorLogoutInput;
import plus.cove.flower.application.view.author.AuthorMineInput;
import plus.cove.flower.application.view.author.AuthorMineOutput;

/**
 * 作者应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface AuthorApplication {
    /**
     * 获取作者
     *
     * @return 作者基本信息
     */
    AuthorMineOutput loadAuthor(Long id);

    /**
     * 修改作者
     */
    void updateAuthor(AuthorMineInput input);

    /**
     * 更新作者
     */
    void changeAuthor(AuthorChangeInput input);

    /**
     * 注销作者
     */
    void logoutAuthor(AuthorLogoutInput input);
}
