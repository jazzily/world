package plus.cove.flower.application;

import plus.cove.flower.application.view.district.DistrictQueryInput;
import plus.cove.flower.application.view.district.DistrictQueryOutput;

import java.util.List;

/**
 * 城市应用
 * <p>
 * 中华人民共和国民政部-行政区划代码
 * <a href="http://www.mca.gov.cn/article/sj/xzqh/2020/">行政区划</a>
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface DistrictApplication {
    /**
     * 获取区域
     * <p>
     * 从存储中获取
     *
     * @param input 查询输入
     * @return 区域列表
     */
    List<DistrictQueryOutput> loadDistrict(DistrictQueryInput input);

    /**
     * 获取区域
     * <p>
     * 从缓存中获取
     *
     * @param input 查询输入
     * @return 区域列表
     */
    List<DistrictQueryOutput> findDistrict(DistrictQueryInput input);
}
