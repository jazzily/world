package plus.cove.flower.application;

import plus.cove.flower.application.view.global.ValidationInput;
import plus.cove.flower.domain.entity.global.CalendarData;
import plus.cove.flower.domain.entity.global.GeneralData;

import java.util.List;

/**
 * 通用应用
 * <p>
 * 包括一般数据，通用验证等
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface GlobalApplication {
    /**
     * 获取数据
     * <p>
     * 从本地缓存获取
     *
     * @param code 编码
     */
    GeneralData findGeneral(String code);

    /**
     * 获取日历
     * <p>
     * 从本地缓存获取
     */
    List<CalendarData> findCalendar(String date);

    /**
     * 创建验证
     *
     * @param input 输入
     */
    void saveValidation(ValidationInput input);
}
