package plus.cove.flower.application;

import plus.cove.flower.application.view.issue.IssueSaveInput;

/**
 * 需求应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface IssueApplication {
    /**
     * 保存
     */
    void saveIssue(IssueSaveInput input);
}
