package plus.cove.flower.application;

import plus.cove.flower.application.view.label.LabelDeleteInput;
import plus.cove.flower.application.view.label.LabelListOutput;
import plus.cove.flower.application.view.label.LabelSaveInput;
import plus.cove.flower.application.view.label.LabelUpdateInput;
import plus.cove.flower.domain.entity.label.Label;

import java.util.List;

/**
 * 标签应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface LabelApplication {
    /**
     * 获取标签
     */
    List<LabelListOutput> loadLabel(Long userId);

    /**
     * 获取标签
     */
    List<Label> findLabel(Long userId);

    /**
     * 保存标签
     */
    void saveLabel(LabelSaveInput input);

    /**
     * 更新标签
     */
    void updateLabel(LabelUpdateInput input);

    /**
     * 删除标签
     */
    void deleteLabel(LabelDeleteInput input);
}
