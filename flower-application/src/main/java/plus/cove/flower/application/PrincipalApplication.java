package plus.cove.flower.application;

import plus.cove.flower.domain.principal.PrincipalUser;

/**
 * 身份应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface PrincipalApplication {
    /**
     * 获取用户
     * <p>
     * 获取用户信息，并保存至缓存中
     *
     * @param id 用户id
     * @return 用户基本信息
     */
    PrincipalUser findPrincipal(Long id);
}
