package plus.cove.flower.application;

import plus.cove.flower.application.view.schedule.EventListInput;
import plus.cove.flower.application.view.schedule.EventListOutput;
import plus.cove.flower.application.view.schedule.EventUpdateInput;
import plus.cove.flower.application.view.schedule.*;

import java.util.List;

/**
 * 日程应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface ScheduleApplication {
    /**
     * 创建日程
     * <p>
     * 创建完整日程
     *
     * @param input 日程输入
     */
    void saveSchedule(ScheduleSaveInput input);

    /**
     * 更新日程
     * <p>
     * 创建完整日程
     *
     * @param input 日程输入
     */
    void updateSchedule(ScheduleUpdateInput input);

    /**
     * 星标日程
     */
    void starSchedule(ScheduleStarInput input);

    /**
     * 获取日程
     */
    List<ScheduleListOutput> loadSchedule(ScheduleListInput input);

    /**
     * 删除日程
     */
    void deleteSchedule(ScheduleDeleteInput input);

    /**
     * 获取一个日程
     */
    ScheduleOneOutput loadSchedule(ScheduleOneInput input);

    /**
     * 完成日程
     */
    void completeSchedule(ScheduleCompleteInput input);

    /**
     * 获取日程事件
     */
    List<EventListOutput> loadScheduleEvent(EventListInput input);

    /**
     * 添加日程事件
     */
    void appendScheduleEvent(EventAppendInput input);

    /**
     * 完成日程事件
     * <p>
     * 需要计算完成率
     */
    void completeScheduleEvent(EventCompleteInput input);

    /**
     * 删除日程事件
     */
    EventOneOutput loadScheduleEvent(EventOneInput input);

    /**
     * 删除日程事件
     */
    void updateScheduleEvent(EventUpdateInput input);

    /**
     * 删除日程事件
     */
    void deleteScheduleEvent(EventDeleteInput input);

    /**
     * 添加日程标签
     */
    void appendScheduleLabel(ScheduleLabelInput input);

    /**
     * 修改日程事件
     */
    void deleteScheduleLabel(ScheduleLabelInput input);

    /**
     * 获取时间线
     */
    List<TimelineListOutput> loadScheduleTimeline(TimelineListInput input);
}
