package plus.cove.flower.application.constant;

/**
 * 缓存常量
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public final class CachingConstant {
    private CachingConstant() {
    }

    // 用户凭证
    public static final String USER_PRINCIPAL = "USER";

    // 全局配置
    public static final String GLOBAL_GENERAL = "GLOB_GENE_";
    public static final String GLOBAL_CALENDAR = "GLOB_CALE_";
}
