package plus.cove.flower.application.constant;

/**
 * 常用常量
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public final class CommonConstant {
    private CommonConstant() {
    }

    // 成功
    public static final String SUCCESS = "Y";
    // 失败
    public static final String FAILURE = "N";
}
