package plus.cove.flower.application.impl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import org.jboss.logging.Logger;
import plus.cove.flower.application.AccountApplication;
import plus.cove.flower.application.constant.CommonConstant;
import plus.cove.flower.application.view.account.*;
import plus.cove.flower.domain.entity.account.Account;
import plus.cove.flower.domain.entity.account.AccountError;
import plus.cove.flower.domain.entity.account.AccountRuler;
import plus.cove.flower.domain.entity.account.AccountState;
import plus.cove.flower.domain.entity.author.Author;
import plus.cove.flower.domain.entity.global.Operation;
import plus.cove.flower.domain.entity.global.OperationAction;
import plus.cove.flower.domain.entity.global.Validation;
import plus.cove.flower.domain.entity.global.ValidationCategory;
import plus.cove.flower.domain.entity.global.builder.OperationBuilder;
import plus.cove.flower.domain.repository.AccountRepository;
import plus.cove.flower.domain.repository.AuthorRepository;
import plus.cove.flower.domain.repository.OperationRepository;
import plus.cove.flower.domain.repository.ValidationRepository;
import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.BusinessException;
import plus.cove.infrastructure.extension.authorizer.JwtResult;
import plus.cove.infrastructure.extension.authorizer.JwtUtils;
import plus.cove.infrastructure.helper.AssertHelper;

/**
 * 账号应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class AccountApplicationImpl implements AccountApplication {
    private static final Logger LOG = Logger.getLogger(AccountApplicationImpl.class);

    private final AccountRepository accountRep;
    private final AuthorRepository authorRep;
    private final OperationRepository operationRep;
    private final ValidationRepository validationRep;

    private final JwtUtils jwtUtil;

    public AccountApplicationImpl(
            final AccountRepository accountRep,
            final AuthorRepository authorRep,
            final OperationRepository operationRep,
            final ValidationRepository validationRep,
            final JwtUtils jwtUtil) {
        this.accountRep = accountRep;
        this.authorRep = authorRep;
        this.operationRep = operationRep;
        this.validationRep = validationRep;
        this.jwtUtil = jwtUtil;
    }

    @Override
    @Transactional(rollbackOn = Exception.class, dontRollbackOn = BusinessException.class)
    public AccountLoginOutput signin(AccountLoginInput input) {
        // 查找account是否存在，存在则报错
        Account account = accountRep.selectByName(input.getUserName());
        // 注册流程
        if (account == null) {
            AccountSignupInput sign = AccountSignupInput.create(input.getUserName(), input.getPassword());
            return this.signup(sign);
        }

        return login(input);
    }

    @Override
    @Transactional(rollbackOn = Exception.class, dontRollbackOn = BusinessException.class)
    public AccountLoginOutput signup(AccountSignupInput input) {
        // 查找account是否存在，存在则报错
        Account account = accountRep.selectByName(input.getUserName());
        AssertHelper.assertNull(account, AccountError.ACCOUNT_HAD_EXISTED);

        // 生成实体并保存
        Author author = input.toAuthor();
        account = input.toAccount(author.getId());

        authorRep.insertOne(author);
        accountRep.insertOne(account);

        // 生成Token
        JwtResult token = jwtUtil.encode(account.getUserId().toString());
        return AccountLoginOutput.from(token.getToken(), token.getExpire());
    }

    @Override
    @Transactional(rollbackOn = Exception.class, dontRollbackOn = BusinessException.class)
    public AccountLoginOutput login(AccountLoginInput input) {
        // 获取账号
        Account account = accountRep.selectByName(input.getUserName());
        AssertHelper.assertNotNull(account, AccountError.ACCOUNT_NAME_ABSENT);

        // 验证状态
        boolean verifyState = account.verifyState();
        AssertHelper.assertTrue(verifyState, AccountError.ACCOUNT_LOGIN_LOCKED);

        // 验证规则
        // 验证失败数
        Operation ruleOperation = OperationBuilder.init(OperationAction.USER_LOGIN)
                .action(account.getUserId())
                .failure()
                .build();
        Integer countOperation = operationRep.selectCount(ruleOperation);

        // 账号规则
        AccountRuler ruler = AccountRuler.init(countOperation);

        // 验证密码
        boolean verify = account.verifyPassword(input.getPassword());
        ruler.validPassword(verify);

        // 需要验证码
        boolean validate = account.getState().equals(AccountState.SUSPECT);
        if (validate) {
            // 校验验证码
            Validation validation = validationRep.selectOne(input.getUserName(), ValidationCategory.LOGIN_EMAIL.name());
            validate = Validation.valid(validation, input.getAuthCode());
            ruler.validValidation(validate);
        } else {
            // 验证通过
            validate = true;
        }

        // 验证规则
        if (!verify || !validate) {
            Operation operation = Operation.init(account.getUserId(), OperationAction.USER_LOGIN, CommonConstant.FAILURE);
            operationRep.insertOne(operation);
            ruler.validAccount(account);
            accountRep.updateState(account);
            ruler.validException();
        } else {
            Operation operation = Operation.init(account.getUserId(), OperationAction.USER_LOGIN, CommonConstant.SUCCESS);
            operationRep.updateOne(operation);
            ruler.validAccount(account);
            accountRep.updateState(account);
        }

        // 生成Token
        JwtResult token = jwtUtil.encode(account.getUserId().toString());
        return AccountLoginOutput.from(token.getToken(), token.getExpire());
    }

    @Override
    public ActionResult resetPassword(AccountResetInput input) {
        // 获取账号
        Account account = accountRep.selectByName(input.getUserName());
        AssertHelper.assertNotNull(account, AccountError.ACCOUNT_NAME_ABSENT);

        // 校验验证码
        Validation validation = validationRep.selectOne(input.getUserName(), ValidationCategory.RESET_EMAIL.name());
        boolean validate = Validation.valid(validation, input.getAuthCode());
        AssertHelper.assertTrue(validate, AccountError.ACCOUNT_VALIDATE_FAILURE);

        // 更新密码
        account.createPassword(input.getPassword());
        accountRep.updateOne(account);

        return ActionResult.success();
    }

    @Override
    public ActionResult changePassword(AccountChangeInput input) {
        Account account = this.accountRep.selectByUser(input.getRequestUser());
        AssertHelper.assertNotNull(account, AccountError.ACCOUNT_NAME_ABSENT);

        // 校验验证码
        Validation validation = validationRep.selectOne(account.getName(), ValidationCategory.CHANGE_EMAIL.name());
        boolean validate = Validation.valid(validation, input.getAuthCode());
        AssertHelper.assertTrue(validate, AccountError.ACCOUNT_VALIDATE_FAILURE);

        // 更新密码
        account.createPassword(input.getPassword());
        accountRep.updateOne(account);

        return ActionResult.success();
    }
}
