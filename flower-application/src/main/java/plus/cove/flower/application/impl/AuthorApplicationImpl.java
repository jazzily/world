package plus.cove.flower.application.impl;

import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;
import plus.cove.flower.application.AuthorApplication;
import plus.cove.flower.application.constant.CommonConstant;
import plus.cove.flower.application.view.author.AuthorChangeInput;
import plus.cove.flower.application.view.author.AuthorLogoutInput;
import plus.cove.flower.application.view.author.AuthorMineInput;
import plus.cove.flower.application.view.author.AuthorMineOutput;
import plus.cove.flower.domain.entity.account.Account;
import plus.cove.flower.domain.entity.account.AccountError;
import plus.cove.flower.domain.entity.author.Author;
import plus.cove.flower.domain.entity.global.Operation;
import plus.cove.flower.domain.entity.global.OperationAction;
import plus.cove.flower.domain.entity.global.Validation;
import plus.cove.flower.domain.entity.global.ValidationCategory;
import plus.cove.flower.domain.entity.label.Label;
import plus.cove.flower.domain.repository.*;
import plus.cove.infrastructure.helper.AssertHelper;
import plus.cove.infrastructure.helper.LongHelper;

import java.util.List;

/**
 * 作者应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class AuthorApplicationImpl implements AuthorApplication {
    private static final Logger LOG = Logger.getLogger(AuthorApplicationImpl.class);

    private final AuthorRepository authorRep;
    private final LabelRepository labelRep;
    private final AccountRepository accountRep;
    private final ValidationRepository validationRep;
    private final OperationRepository operationRep;

    public AuthorApplicationImpl(
            final AuthorRepository authorRep,
            final LabelRepository labelRep,
            final AccountRepository accountRep,
            final OperationRepository operationRep,
            final ValidationRepository validationRep) {
        this.authorRep = authorRep;
        this.labelRep = labelRep;
        this.accountRep = accountRep;
        this.operationRep = operationRep;
        this.validationRep = validationRep;
    }

    @Override
    public AuthorMineOutput loadAuthor(Long id) {
        Author entity = this.authorRep.selectById(id);
        List<Label> labels = this.labelRep.selectByUser(id);

        return AuthorMineOutput.from(entity, labels);
    }

    @Override
    public void updateAuthor(AuthorMineInput input) {
        Author entity = input.toAuthor();
        this.authorRep.updateOne(entity);
    }

    @Override
    public void changeAuthor(AuthorChangeInput input) {
        // 获取账号
        Account account = accountRep.selectByName(input.getUserName());
        // 存在但不是本人账号
        boolean exist = (account != null && LongHelper.notEquals(account.getUserId(), input.getRequestUser()));
        AssertHelper.assertFalse(exist, AccountError.ACCOUNT_HAD_EXISTED);

        // 校验验证码
        Validation validation = validationRep.selectOne(input.getUserName(), ValidationCategory.CHANGE_EMAIL.name());
        boolean validate = Validation.valid(validation, input.getAuthCode());
        AssertHelper.assertTrue(validate, AccountError.ACCOUNT_VALIDATE_FAILURE);

        // 更新作者
        Author author = input.toAuthor();
        authorRep.updateOne(author);

        // 更新账号
        account = input.toAccount();
        accountRep.updateUser(account);
    }

    @Override
    public void logoutAuthor(AuthorLogoutInput input) {
        // 获取账号
        Author entity = this.authorRep.selectById(input.getRequestUser());
        // 校验邮箱
        boolean validate = entity.getEmail().equals(input.getUserName());
        AssertHelper.assertTrue(validate, AccountError.ACCOUNT_VALIDATE_FAILURE);

        // 校验验证码
        Validation validation = validationRep.selectOne(input.getUserName(), ValidationCategory.LOGOUT_EMAIL.name());
        validate = Validation.valid(validation, input.getAuthCode());
        AssertHelper.assertTrue(validate, AccountError.ACCOUNT_VALIDATE_FAILURE);

        // 锁定账号
        Account account = input.toAccount();
        accountRep.updateUser(account);

        // 重要操作
        Operation operation = Operation.init(account.getUserId(), OperationAction.USER_LOGOUT, CommonConstant.SUCCESS);
        operationRep.insertOne(operation);
    }
}
