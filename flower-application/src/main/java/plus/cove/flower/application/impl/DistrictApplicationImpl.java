package plus.cove.flower.application.impl;

import org.jboss.logging.Logger;
import plus.cove.flower.application.DistrictApplication;
import plus.cove.flower.application.view.district.DistrictQueryInput;
import plus.cove.flower.application.view.district.DistrictQueryOutput;
import plus.cove.flower.domain.entity.global.District;
import plus.cove.flower.domain.repository.DistrictRepository;
import plus.cove.infrastructure.extension.redis.RedisUtils;

import jakarta.enterprise.context.ApplicationScoped;

import java.util.ArrayList;
import java.util.List;

/**
 * 地区应用
 * <p>
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class DistrictApplicationImpl implements DistrictApplication {
    private static final Logger LOG = Logger.getLogger(DistrictApplicationImpl.class);

    private final DistrictRepository districtRep;
    private final RedisUtils redisUtils;

    public DistrictApplicationImpl(
            final DistrictRepository districtRep,
            final RedisUtils redisUtils) {
        this.districtRep = districtRep;
        this.redisUtils = redisUtils;
    }

    @Override
    public List<DistrictQueryOutput> loadDistrict(DistrictQueryInput input) {
        // 获取列表
        District entity = District.from(input.getType());
        List<District> list = districtRep.selectList(entity);

        // 最终数据
        List<DistrictQueryOutput> districts = new ArrayList<>();

        // 层级数据-支持三级
        DistrictQueryOutput[] levels = new DistrictQueryOutput[3];
        for (District district : list) {
            DistrictQueryOutput one = DistrictQueryOutput.from(district);

            // 保存层级
            int level = district.getType().getValue() - 1;
            levels[level] = one;

            // 0级特殊处理
            if (level == 0) {
                districts.add(one);
            } else {
                levels[level - 1].addDistrict(one);
            }
        }

        return districts;
    }

    @Override
    public List<DistrictQueryOutput> findDistrict(DistrictQueryInput input) {
        List<DistrictQueryOutput> list = redisUtils.getList("global", "city", DistrictQueryOutput.class);
        if (list == null) {
            list = loadDistrict(input);
            redisUtils.set("global", "city", list);
        }
        return list;
    }
}
