package plus.cove.flower.application.impl;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import org.jboss.logging.Logger;
import plus.cove.flower.application.GlobalApplication;
import plus.cove.flower.application.constant.CachingConstant;
import plus.cove.flower.application.view.global.ValidationInput;
import plus.cove.flower.domain.entity.global.*;
import plus.cove.flower.domain.repository.CalendarRepository;
import plus.cove.flower.domain.repository.GeneralRepository;
import plus.cove.flower.domain.repository.ValidationRepository;
import plus.cove.infrastructure.helper.AssertHelper;
import plus.cove.infrastructure.extension.cache.LocalCache;
import plus.cove.infrastructure.extension.security.SecurityUtils;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;

/**
 * 全局应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class GlobalApplicationImpl implements GlobalApplication {
    private static final Logger LOG = Logger.getLogger(GlobalApplicationImpl.class);

    private final GeneralRepository generalRep;
    private final CalendarRepository calendarRep;
    private final ValidationRepository validationRep;

    private final SecurityUtils securityUtils;
    private final LocalCache localCache;

    private final Mailer mailer;

    public GlobalApplicationImpl(
            final GeneralRepository generalRep,
            final CalendarRepository calendarRep,
            final ValidationRepository validationRep,
            final SecurityUtils securityUtils,
            final LocalCache localCache,
            final Mailer mailer) {
        this.generalRep = generalRep;
        this.calendarRep = calendarRep;
        this.validationRep = validationRep;
        this.securityUtils = securityUtils;
        this.localCache = localCache;

        this.mailer = mailer;
    }

    @Override
    public GeneralData findGeneral(String code) {
        return localCache.get(CachingConstant.GLOBAL_GENERAL, code,
                key -> this.generalRep.selectByCode(code)
        );
    }

    @Override
    public List<CalendarData> findCalendar(String date) {
        return localCache.get(CachingConstant.GLOBAL_CALENDAR, date,
                key -> this.calendarRep.selectByDate(date)
        );
    }

    @Override
    public void saveValidation(ValidationInput input) {
        ValidationBuilder builder = ValidationBuilder.init();
        builder.target(input.getTarget());
        builder.authCode(input.getCategory());
        Validation entity = builder.build();

        // 特殊处理
        String authCode = securityUtils.validationCode(entity.getAuthCode());
        entity.setAuthCode(authCode);

        // 保存
        this.validationRep.insertOne(entity);

        // 获取配置
        GeneralData data = this.generalRep.selectByCode(input.getCategory().getConfig());
        AssertHelper.assertNotNull(data, GlobalError.GENERAL_NO_EXIST);

        // 发送邮件
        String content = data.format(input.getTarget(), builder.authCode());
        Mail mail = Mail.withHtml(entity.getTarget(), data.getName(), content);
        mailer.send(mail);
    }
}
