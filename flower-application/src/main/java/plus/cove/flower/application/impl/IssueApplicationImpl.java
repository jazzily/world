package plus.cove.flower.application.impl;

import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;
import plus.cove.flower.application.IssueApplication;
import plus.cove.flower.application.view.issue.IssueSaveInput;
import plus.cove.flower.domain.entity.issue.Issue;
import plus.cove.flower.domain.repository.IssueRepository;

/**
 * 需求应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class IssueApplicationImpl implements IssueApplication {
    private static final Logger LOG = Logger.getLogger(IssueApplicationImpl.class);

    private final IssueRepository issueRep;

    public IssueApplicationImpl(
            final IssueRepository issueRep) {
        this.issueRep = issueRep;
    }

    @Override
    public void saveIssue(IssueSaveInput input) {
        Issue entity = input.toIssue();
        this.issueRep.insertOne(entity);
    }
}
