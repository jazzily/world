package plus.cove.flower.application.impl;

import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;
import plus.cove.flower.application.LabelApplication;
import plus.cove.flower.application.view.label.LabelDeleteInput;
import plus.cove.flower.application.view.label.LabelListOutput;
import plus.cove.flower.application.view.label.LabelSaveInput;
import plus.cove.flower.application.view.label.LabelUpdateInput;
import plus.cove.flower.domain.entity.label.Label;
import plus.cove.flower.domain.entity.label.LabelError;
import plus.cove.flower.domain.entity.schedule.ScheduleLabel;
import plus.cove.flower.domain.repository.LabelRepository;
import plus.cove.flower.domain.repository.ScheduleRepository;
import plus.cove.infrastructure.exception.BusinessException;
import plus.cove.infrastructure.helper.LongHelper;
import plus.cove.infrastructure.helper.StringHelper;

import java.util.List;

/**
 * 标签应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class LabelApplicationImpl implements LabelApplication {
    private static final Logger LOG = Logger.getLogger(LabelApplicationImpl.class);

    private final LabelRepository labelRep;
    private final ScheduleRepository scheduleRep;

    public LabelApplicationImpl(
            final LabelRepository labelRep,
            final ScheduleRepository scheduleRep) {
        this.labelRep = labelRep;
        this.scheduleRep = scheduleRep;
    }

    @Override
    public List<Label> findLabel(Long userId) {
        return this.labelRep.selectByUser(userId);
    }

    @Override
    public List<LabelListOutput> loadLabel(Long userId) {
        List<Label> list = this.labelRep.selectByUser(userId);

        return list.stream()
                .map(LabelListOutput::from)
                .toList();
    }

    @Override
    public void saveLabel(LabelSaveInput input) {
        Label entity = input.toLabel();
        // 查找是否存在相同的
        List<Label> labels = this.findLabel(entity.getUserId());
        boolean match = labels.stream()
                .anyMatch(lb -> lb.getName().equals(entity.getName()));
        if (match) {
            throw BusinessException.from(LabelError.LABEL_HAD_EXIST);
        }

        this.labelRep.insertOne(entity);
    }

    @Override
    public void updateLabel(LabelUpdateInput input) {
        Label entity = input.toLabel();

        // 查找是否存在相同的
        List<Label> labels = this.findLabel(entity.getUserId());
        boolean match = labels.stream()
                .filter(lb -> LongHelper.notEquals(lb.getId(), entity.getId()))
                .anyMatch(lb -> StringHelper.equals(lb.getName(), entity.getName()));
        if (match) {
            throw BusinessException.from(LabelError.LABEL_HAD_EXIST);
        }
        this.labelRep.updateOne(entity);

        // 更新标签名称
        ScheduleLabel relation = new ScheduleLabel();
        relation.setLabelId(entity.getId());
        relation.setLabelName(entity.getName());
        scheduleRep.updateLabelName(relation);
    }

    @Override
    public void deleteLabel(LabelDeleteInput input) {
        Label entity = input.toLabel();
        this.labelRep.deleteOne(entity);

        ScheduleLabel relation = input.toScheduleLabel();
        this.scheduleRep.deleteLabel(relation);
    }
}
