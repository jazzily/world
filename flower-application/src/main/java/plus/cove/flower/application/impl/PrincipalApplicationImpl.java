package plus.cove.flower.application.impl;

import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;
import plus.cove.flower.application.PrincipalApplication;
import plus.cove.flower.application.constant.CachingConstant;
import plus.cove.flower.domain.entity.author.Author;
import plus.cove.flower.domain.principal.PrincipalUser;
import plus.cove.flower.domain.repository.AuthorRepository;
import plus.cove.infrastructure.extension.redis.RedisUtils;

/**
 * 身份应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class PrincipalApplicationImpl implements PrincipalApplication {
    private static final Logger LOG = Logger.getLogger(PrincipalApplicationImpl.class);

    private final AuthorRepository authorRep;
    private final RedisUtils redisUtils;

    public PrincipalApplicationImpl(
            final AuthorRepository author,
            final RedisUtils redis) {
        this.authorRep = author;
        this.redisUtils = redis;
    }

    @Override
    public PrincipalUser findPrincipal(Long id) {
        // 查找缓存
        PrincipalUser user = redisUtils.getObject(CachingConstant.USER_PRINCIPAL, id.toString(), PrincipalUser.class);
        if (user == null) {
            Author author = authorRep.selectById(id);
            user = PrincipalUser.from(author.getId(), author.getName());

            redisUtils.set(CachingConstant.USER_PRINCIPAL, id.toString(), user);
        }

        return user;
    }
}