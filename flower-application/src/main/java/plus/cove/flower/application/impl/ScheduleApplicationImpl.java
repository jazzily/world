package plus.cove.flower.application.impl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import org.jboss.logging.Logger;
import plus.cove.flower.application.ScheduleApplication;
import plus.cove.flower.application.view.schedule.*;
import plus.cove.flower.domain.entity.global.Operation;
import plus.cove.flower.domain.entity.global.OperationAction;
import plus.cove.flower.domain.entity.global.builder.OperationBuilder;
import plus.cove.flower.domain.entity.label.Label;
import plus.cove.flower.domain.entity.schedule.*;
import plus.cove.flower.domain.entity.schedule.builder.ScheduleBuilder;
import plus.cove.flower.domain.entity.schedule.counter.ScheduleCounter;
import plus.cove.flower.domain.repository.LabelRepository;
import plus.cove.flower.domain.repository.OperationRepository;
import plus.cove.flower.domain.repository.ScheduleRepository;
import plus.cove.flower.domain.repository.schedule.EventListExport;
import plus.cove.flower.domain.repository.schedule.EventListImport;
import plus.cove.flower.domain.repository.schedule.ScheduleListExport;
import plus.cove.flower.domain.repository.schedule.ScheduleListImport;
import plus.cove.infrastructure.helper.AssertHelper;

import java.util.List;

/**
 * 日程应用
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class ScheduleApplicationImpl implements ScheduleApplication {
    private static final Logger LOG = Logger.getLogger(ScheduleApplicationImpl.class);

    private final ScheduleRepository scheduleRep;
    private final OperationRepository operationRep;
    private final LabelRepository labelRep;

    public ScheduleApplicationImpl(final ScheduleRepository scheduleRep, final OperationRepository operationRep, final LabelRepository labelRep) {
        this.scheduleRep = scheduleRep;
        this.operationRep = operationRep;
        this.labelRep = labelRep;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void saveSchedule(ScheduleSaveInput input) {
        // 创建日程
        ScheduleBuilder builder = ScheduleBuilder.init(input.getRepeatType(), input.getRepeatItem());
        Schedule entity = builder.title(input.getTitle())
                .place(input.getPlace())
                .remark(input.getRemark())
                .userId(input.getRequestUser())
                .plan(input.getStartTime(), input.getFinishTime())
                .build();

        scheduleRep.insertSchedule(entity);
        scheduleRep.insertEvent(entity);

        ScheduleTimeline timeline = ScheduleTimeline.schedule(entity).start();
        scheduleRep.insertTimeline(timeline);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateSchedule(ScheduleUpdateInput input){
        Schedule origin = scheduleRep.selectOneSchedule(input.getId());
        input.checkPermit(origin);

        Schedule entity = input.toSchedule();
        scheduleRep.updateSchedule(entity);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void starSchedule(ScheduleStarInput input){
        Schedule origin = scheduleRep.selectOneSchedule(input.getId());
        input.checkPermit(origin);

        Schedule entity = input.toSchedule();
        scheduleRep.updateSchedule(entity);
    }

    @Override
    public List<ScheduleListOutput> loadSchedule(ScheduleListInput input) {
        ScheduleListImport im = input.toImport();
        List<ScheduleListExport> ex = scheduleRep.selectListSchedule(im);
        return ex.stream().map(ScheduleListOutput::fromExport).toList();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteSchedule(ScheduleDeleteInput input) {
        Schedule origin = this.scheduleRep.selectOneSchedule(input.getId());
        input.checkPermit(origin);

        this.scheduleRep.deleteSchedule(input.getId());

        ScheduleTimeline timeline = ScheduleTimeline.schedule(input.getId());
        this.scheduleRep.deleteTimeline(timeline);
    }

    @Override
    public ScheduleOneOutput loadSchedule(ScheduleOneInput input) {
        // 实体
        Schedule entity = this.scheduleRep.selectOneSchedule(input.getId());
        input.checkPermit(entity);

        // 对应标签
        List<ScheduleLabel> labels = this.scheduleRep.selectScheduleLabel(input.getId());

        // 对应事件
        List<ScheduleEvent> events = this.scheduleRep.selectScheduleEvent(input.getId());

        return ScheduleOneOutput.from(entity, labels, events);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void completeSchedule(ScheduleCompleteInput input) {
        // 确保有权限
        Schedule origin = this.scheduleRep.selectOneSchedule(input.getId());
        input.checkPermit(origin);

        // 更新日程
        Schedule schedule = input.toSchedule();
        this.scheduleRep.updateSchedule(schedule);

        // 更新所有事件
        ScheduleEvent event = input.toEvent();
        this.scheduleRep.updateEventBySchedule(event);

        // 时间线
        ScheduleTimeline timeline = ScheduleTimeline.schedule(origin).finish();
        this.scheduleRep.insertTimeline(timeline);
    }

    @Override
    public List<EventListOutput> loadScheduleEvent(EventListInput input) {
        EventListImport im = input.toImport();
        List<EventListExport> ex = scheduleRep.selectListEvent(im);
        return ex.stream().map(EventListOutput::fromExport).toList();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void appendScheduleEvent(EventAppendInput input) {
        // 确保有权限
        Schedule origin = this.scheduleRep.selectOneSchedule(input.getScheduleId());
        input.checkPermit(origin);

        // 插入日程事件
        Schedule entity = input.toSchedule();
        this.scheduleRep.insertEvent(entity);

        // 重新设置状态
        this.scheduleRep.updateSchedule(entity);

        // 删除完成
        ScheduleTimeline timeline = ScheduleTimeline.schedule(input.getScheduleId()).finish();
        this.scheduleRep.deleteTimeline(timeline);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void completeScheduleEvent(EventCompleteInput input) {
        ScheduleEvent event = this.scheduleRep.selectOneEvent(input.getId());
        input.checkPermit(event);

        // 保存状态
        event.complete();
        this.scheduleRep.updateEvent(event);

        // 计算完成率
        List<ScheduleCounter> counters = this.scheduleRep.selectCounter(event.getScheduleId());
        Schedule entity = ScheduleCounter.calculate(counters, event.getScheduleId());
        this.scheduleRep.updateSchedule(entity);

        // 日程事件完成
        ScheduleTimeline timeline = ScheduleTimeline.event(event).finish();
        this.scheduleRep.insertTimeline(timeline);

        // 日程是否完成
        if (entity.completed()) {
            Schedule origin = this.scheduleRep.selectOneSchedule(event.getScheduleId());
            timeline = ScheduleTimeline.schedule(origin).finish();
            this.scheduleRep.insertTimeline(timeline);
        }
    }

    @Override
    public EventOneOutput loadScheduleEvent(EventOneInput input) {
        ScheduleEvent event = this.scheduleRep.selectOneEvent(input.getId());
        input.checkPermit(event);

        return EventOneOutput.fromEntity(event);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateScheduleEvent(EventUpdateInput input) {
        ScheduleEvent origin = this.scheduleRep.selectOneEvent(input.getId());
        input.checkPermit(origin);

        // 更新日程事件
        ScheduleEvent event = input.toEvent();
        this.scheduleRep.updateEvent(event);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteScheduleEvent(EventDeleteInput input) {
        ScheduleEvent event = this.scheduleRep.selectOneEvent(input.getId());
        input.checkPermit(event);

        Schedule origin = this.scheduleRep.selectOneSchedule(event.getScheduleId());

        // 计算完成率
        List<ScheduleCounter> counters = this.scheduleRep.selectCounter(event.getScheduleId());
        Schedule entity = ScheduleCounter.calculate(counters, event.getScheduleId());
        this.scheduleRep.updateSchedule(entity);

        // 记录重要时刻
        Operation operation = OperationBuilder.init(OperationAction.EVENT_DELETE).action(input.getRequestUser()).target(input.getId().toString()).success().build();
        this.operationRep.insertOne(operation);
        this.scheduleRep.deleteEvent(input.getId());

        // 删除日程事件
        ScheduleTimeline timeline = ScheduleTimeline.event(event.getId());
        this.scheduleRep.deleteTimeline(timeline);

        // 是否完成日程
        if (!origin.completed() && entity.completed()) {
            timeline = ScheduleTimeline.schedule(entity);
            this.scheduleRep.insertTimeline(timeline);
        }
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void appendScheduleLabel(ScheduleLabelInput input) {
        // 确保有权限
        Schedule origin = this.scheduleRep.selectOneSchedule(input.getScheduleId());
        input.checkPermit(origin);

        // 查找标签是否存在
        Label query = Label.from(input.getRequestUser(), input.getLabelName());
        Label label = this.labelRep.selectOne(query);
        // 不存在则创建
        if (label == null) {
            label = input.toLabel();
            this.labelRep.insertOne(label);
        }

        // 查询是否存在相同标签
        ScheduleLabel search = ScheduleLabel.from(input.getScheduleId(), label.getId());
        ScheduleLabel relation = this.scheduleRep.selectOneLabel(search);
        AssertHelper.assertNull(relation, ScheduleError.LABEL_HAD_EXIST);

        // 更新日程标签值
        relation = input.toScheduleLabel();
        relation.setLabelId(label.getId());
        this.scheduleRep.insertLabel(relation);
        // 增加标签数
        this.labelRep.increaseOne(label.getId());
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteScheduleLabel(ScheduleLabelInput input) {
        // 确保有权限
        Schedule origin = this.scheduleRep.selectOneSchedule(input.getScheduleId());
        input.checkPermit(origin);

        // 减少标签数
        ScheduleLabel search = ScheduleLabel.from(input.getScheduleId(), input.getLabelId());
        ScheduleLabel relation = this.scheduleRep.selectOneLabel(search);
        this.labelRep.decreaseOne(relation.getLabelId());

        // 删除标签值
        relation = input.toScheduleLabel();
        this.scheduleRep.deleteLabel(relation);
    }

    @Override
    public List<TimelineListOutput> loadScheduleTimeline(TimelineListInput input) {
        List<ScheduleTimeline> list = this.scheduleRep.selectTimeline(input.toImport());

        return ScheduleMapper.INSTANCE.fromTimelineList(list);
    }
}