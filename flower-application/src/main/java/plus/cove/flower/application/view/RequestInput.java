package plus.cove.flower.application.view;

import lombok.Getter;
import plus.cove.flower.domain.principal.PrincipalError;
import plus.cove.flower.domain.principal.PrincipalPermit;
import plus.cove.flower.domain.principal.PrincipalUser;
import plus.cove.infrastructure.exception.BusinessException;
import plus.cove.infrastructure.helper.LongHelper;

/**
 * 用户输入
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Getter
public class RequestInput {
    private Long requestUser;

    private String requestTracer;

    private String requestRouter;

    private String requestVersion;

    /**
     * 用当前用户填充
     */
    public RequestInput withPrincipal(PrincipalUser user) {
        this.requestUser = user.getUserId();
        return this;
    }

    /**
     * 用请求填充
     */
    public RequestInput withRequest(String tracer, String router, String version) {
        this.requestTracer = tracer;
        this.requestRouter = router;
        this.requestVersion = version;
        return this;
    }

    /**
     * 检查权限
     */
    public void checkPermit(PrincipalPermit user) {
        if (LongHelper.notEquals(this.requestUser, user.getPermitUser())) {
            throw new BusinessException(PrincipalError.PRINCIPAL_NO_PERMIT);
        }
    }
}
