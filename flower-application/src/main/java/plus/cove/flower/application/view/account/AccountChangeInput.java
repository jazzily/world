package plus.cove.flower.application.view.account;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

/**
 * 用户登录
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AccountChangeInput extends RequestInput {
    @Schema(title = "密码", description = "必填，长度5-64")
    @NotEmpty(message = "密码不能为空")
    @Size(min = 5, max = 64, message = "密码长度5-64位")
    private String password;

    @Schema(title = "验证码", description = "必填，长度6-16")
    @NotEmpty(message = "验证码不能为空")
    @Size(min = 6, max = 16, message = "验证码长度6-16位")
    private String authCode;
}
