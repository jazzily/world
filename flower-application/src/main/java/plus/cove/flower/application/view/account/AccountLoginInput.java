package plus.cove.flower.application.view.account;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

/**
 * 用户登录
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AccountLoginInput {
    @Schema(title = "用户名", description = "必填，用户邮箱，最大64字符")
    @NotEmpty(message = "用户名不能为空")
    @Email(message = "用户名必须使用邮箱")
    @Size(max = 64, message = "用户名最大长度64位")
    private String userName;

    @Schema(title = "密码", description = "必填，长度5-64")
    @NotEmpty(message = "密码不能为空")
    @Size(min = 5, max = 64, message = "密码长度5-64位")
    private String password;

    @Schema(title = "验证码", description = "登陆失败后，返回特定code指明需要验证码")
    private String authCode;
}
