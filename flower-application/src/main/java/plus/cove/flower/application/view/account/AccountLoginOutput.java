package plus.cove.flower.application.view.account;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;

/**
 * 用户登录
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AccountLoginOutput {
    @Parameter(name = "访问token")
    private String token;

    @Parameter(name = "过期时间", description = "过期时间，单位秒")
    private Integer expire;

    public static AccountLoginOutput from(String token, Integer expire) {
        AccountLoginOutput output = new AccountLoginOutput();
        output.token = token;
        output.expire = expire;
        return output;
    }
}
