package plus.cove.flower.application.view.account;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.domain.entity.account.Account;
import plus.cove.flower.domain.entity.account.AccountBuilder;
import plus.cove.flower.domain.entity.author.Author;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

/**
 * 用户注册
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AccountSignupInput {
    @Schema(title = "用户名", description = "必填，用户邮箱，最大64字符")
    @NotEmpty(message = "用户名不能为空")
    @Email(message = "用户名必须使用邮箱")
    @Size(max = 64, message = "用户名最大长度64位")
    private String userName;

    @Schema(title = "密码", description = "必填，长度5-64")
    @NotEmpty(message = "密码不能为空")
    @Size(min = 5, max = 64, message = "密码长度5-64位")
    private String password;

    @Schema(title = "昵称", description = "必填，最大20个字符")
    @NotEmpty(message = "昵称不能为空")
    @Size(max = 20, message = "昵称最多20个字符")
    private String nickName;

    public static AccountSignupInput create(String userName, String password) {
        AccountSignupInput input = new AccountSignupInput();
        input.userName = userName;
        input.password = password;

        int at = userName.indexOf("@");
        input.nickName = userName.substring(0, at);
        return input;
    }


    public Account toAccount(Long userId) {
        return AccountBuilder.init()
                .user(userId)
                .name(this.userName)
                .password(this.password)
                .build();
    }

    public Author toAuthor() {
        return Author.create(this.nickName, this.userName);
    }
}
