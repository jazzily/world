package plus.cove.flower.application.view.author;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import plus.cove.flower.domain.entity.label.Label;

/**
 * 作者
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AuthorLabelOutput {
    @Parameter(name = "id")
    private Long id;

    @Parameter(name = "名称")
    private String name;

    @Parameter(name = "个数")
    private Integer count;

    public static AuthorLabelOutput from(Label entity) {
        AuthorLabelOutput output = new AuthorLabelOutput();
        output.id = entity.getId();
        output.name = entity.getName();
        output.count = entity.getUsingCount();
        return output;
    }
}
