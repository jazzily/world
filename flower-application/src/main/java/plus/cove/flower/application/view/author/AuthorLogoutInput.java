package plus.cove.flower.application.view.author;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.account.Account;
import plus.cove.flower.domain.entity.account.AccountState;

/**
 * 用户注销
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AuthorLogoutInput extends RequestInput {
    @Schema(title = "用户名", description = "必填，用户邮箱，最大64字符")
    @NotEmpty(message = "用户名不能为空")
    @Email(message = "用户名必须使用邮箱")
    @Size(max = 64, message = "用户名最大长度64位")
    private String userName;

    @Schema(title = "验证码", description = "必填，长度6-16")
    @NotEmpty(message = "验证码不能为空")
    @Size(min = 6, max = 16, message = "验证码长度6-16位")
    private String authCode;

    public Account toAccount(){
        Account entity = Account.init();
        entity.setUserId(this.getRequestUser());
        entity.setState(AccountState.DISABLED);
        return entity;
    }
}
