package plus.cove.flower.application.view.author;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.author.Author;

import java.time.LocalDateTime;

/**
 * 作者
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AuthorMineInput extends RequestInput {
    @Parameter(name = "昵称")
    private String name;

    @Parameter(name = "头像")
    private String avatar;

    @Parameter(name = "状态")
    private String status;

    public Author toAuthor() {
        Author entity = Author.init(this.getRequestUser());
        entity.setName(this.name);
        entity.setAvatar(this.avatar);
        entity.setStatus(this.status);
        entity.setUpdateTime(LocalDateTime.now());
        return entity;
    }
}
