package plus.cove.flower.application.view.author;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import plus.cove.flower.domain.entity.author.Author;
import plus.cove.flower.domain.entity.label.Label;

import java.util.Comparator;
import java.util.List;

/**
 * 作者
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class AuthorMineOutput {
    @Parameter(name = "昵称")
    private String name;

    @Parameter(name = "邮箱")
    private String email;

    @Parameter(name = "头像")
    private String avatar;

    @Parameter(name = "状态")
    private String status;

    @Parameter(name = "标签，最多的前5个")
    private List<AuthorLabelOutput> labels;

    public static AuthorMineOutput from(Author entity, List<Label> labels) {
        AuthorMineOutput output = new AuthorMineOutput();
        output.setName(entity.getName());
        output.setEmail(entity.getEmail());
        output.setAvatar(entity.getAvatar());
        output.setStatus(entity.getStatus());

        // 按使用数倒序排列
        output.labels = labels.stream()
                .sorted(Comparator.comparing(Label::getUsingCount).reversed())
                .map(AuthorLabelOutput::from)
                .toList();

        return output;
    }
}
