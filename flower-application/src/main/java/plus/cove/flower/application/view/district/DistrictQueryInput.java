package plus.cove.flower.application.view.district;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.domain.entity.global.DistrictType;

import jakarta.ws.rs.QueryParam;


/**
 * 地区
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class DistrictQueryInput {
    /**
     * 获取类型
     * 默认全部
     */
    @Schema(title = "类型")
    @QueryParam(value = "type")
    private DistrictType type = DistrictType.DISTRICT;

    public static DistrictQueryInput from(DistrictType type) {
        DistrictQueryInput input = new DistrictQueryInput();
        if (type != null) {
            input.type = type;
        }
        return input;
    }
}
