package plus.cove.flower.application.view.district;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.domain.entity.global.District;

import java.util.ArrayList;
import java.util.List;

@Data
public class DistrictQueryOutput {
    /**
     * 编号
     */
    @Schema(title = "编号")
    private Long id;

    /**
     * 编码
     */
    @Schema(title = "编码")
    private String code;

    /**
     * 名称
     */
    @Schema(title = "名称")
    private String name;

    /**
     * 下级区域
     */
    @Schema(title = "下级区域")
    private List<DistrictQueryOutput> districts;

    public void addDistrict(DistrictQueryOutput output) {
        if (this.districts == null) {
            this.districts = new ArrayList<>();
        }
        this.districts.add(output);
    }

    public static DistrictQueryOutput from(District entity) {
        DistrictQueryOutput output = new DistrictQueryOutput();
        output.id = entity.getId();
        output.code = entity.getCode();
        output.name = entity.getName();
        return output;
    }
}
