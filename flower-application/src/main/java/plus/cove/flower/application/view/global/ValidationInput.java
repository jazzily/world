package plus.cove.flower.application.view.global;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.domain.entity.global.ValidationCategory;

import jakarta.ws.rs.QueryParam;

@Data
public class ValidationInput {
    @Schema(title = "发送对象")
    @QueryParam(value = "target")
    private String target;

    @Schema(title = "类型，LOGIN_EMAIL: 使用邮箱收取验证码进行登录")
    @QueryParam(value = "category")
    private ValidationCategory category;
}

