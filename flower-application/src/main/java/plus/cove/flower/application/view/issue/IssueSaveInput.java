package plus.cove.flower.application.view.issue;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.issue.Issue;

/**
 * 需求
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class IssueSaveInput extends RequestInput {
    @Parameter(name = "标题")
    @NotEmpty(message = "标题不能为空")
    @Size(min = 2, max = 32, message = "标题长度2-32位")
    private String title;

    @Parameter(name = "联系方式")
    @Size(max = 32, message = "联系方式最大32位")
    private String contact;

    @Parameter(name = "内容")
    @Size(max = 200, message = "内容最大200位")
    private String content;

    public Issue toIssue() {
        Issue entity = Issue.create();
        entity.setTitle(this.title);
        entity.setContact(this.contact);
        entity.setContent(this.content);
        entity.setUserId(this.getRequestUser());

        // 默认0L
        if (entity.getUserId() == null) {
            entity.setUserId(0L);
        }
        return entity;
    }
}
