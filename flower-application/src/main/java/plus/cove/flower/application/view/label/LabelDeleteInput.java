package plus.cove.flower.application.view.label;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.label.Label;
import plus.cove.flower.domain.entity.schedule.ScheduleLabel;

/**
 * 标签
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LabelDeleteInput extends RequestInput {
    @Parameter(name = "id")
    @NotNull(message = "id不能为空")
    private Long id;

    public Label toLabel(){
        Label entity = Label.init(id);
        entity.setUserId(this.getRequestUser());
        return entity;
    }

    public ScheduleLabel toScheduleLabel(){
        ScheduleLabel entity = new ScheduleLabel();
        entity.setLabelId(this.id);
        return entity;
    }
}
