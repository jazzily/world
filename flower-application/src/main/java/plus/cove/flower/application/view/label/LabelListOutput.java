package plus.cove.flower.application.view.label;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import plus.cove.flower.domain.entity.label.Label;

/**
 * 标签
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class LabelListOutput {
    @Parameter(name = "id")
    private Long id;

    @Parameter(name = "名称")
    private String name;

    @Parameter(name = "使用数")
    private Integer usingCount;

    public static LabelListOutput from(Label label){
        LabelListOutput output = new LabelListOutput();
        output.id = label.getId();
        output.name = label.getName();
        output.usingCount = label.getUsingCount();

        return output;
    }
}
