package plus.cove.flower.application.view.label;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.label.Label;

/**
 * 标签
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LabelUpdateInput extends RequestInput {
    @Parameter(name = "id")
    @NotNull(message = "id不能为空")
    private Long id;

    @Parameter(name = "名称")
    @NotEmpty(message = "名称不能为空")
    @Size(min = 2, max = 10, message = "名称长度2-10位")
    private String name;

    public Label toLabel() {
        Label entity = Label.init(this.id);
        entity.setName(name);
        entity.setUserId(this.getRequestUser());
        entity.setUsingCount(0);
        return entity;
    }
}
