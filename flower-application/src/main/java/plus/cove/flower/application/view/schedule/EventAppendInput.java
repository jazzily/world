package plus.cove.flower.application.view.schedule;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;
import plus.cove.flower.domain.entity.schedule.ScheduleEventState;
import plus.cove.flower.domain.entity.schedule.ScheduleState;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.time.LocalDateTime;

/**
 * 日程添加
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class EventAppendInput extends RequestInput {
    @Schema(title = "标题", description = "必填，最多32个字符")
    @NotEmpty(message = "标题不能为空")
    @Size(max = 32, message = "标题最多32个字符")
    private String title;

    @Schema(title = "备注", description = "选填，最多1024个字符")
    @Size(max = 1024, message = "备注最多1024个字符")
    private String remark;

    @Schema(title = "开始时间", description = "必填，开始时间，格式：yyyy-MM-ddTHH:mm:ss")
    @NotNull(message = "开始时间不能为空")
    private LocalDateTime startTime;

    @Schema(title = "结束时间", description = "必填，结束时间，格式：yyyy-MM-ddTHH:mm:ss")
    @NotNull(message = "结束时间不能为空")
    private LocalDateTime finishTime;

    @Schema(title = "日程id", description = "必填，对应的日程")
    @NotNull(message = "日程id不能为空")
    private Long scheduleId;

    public Schedule toSchedule() {
        Schedule schedule = Schedule.init(this.scheduleId);
        schedule.setState(ScheduleState.PLANNING);

        ScheduleEvent event = ScheduleEvent.create(this.scheduleId, this.title, this.remark, this.getRequestUser());
        event.setState(ScheduleEventState.RUNNING);
        event.setStartTime(this.startTime);
        event.setFinishTime(this.finishTime);

        schedule.addEvent(event);
        return schedule;
    }
}
