package plus.cove.flower.application.view.schedule;

import lombok.Data;
import plus.cove.flower.domain.entity.schedule.ScheduleEventState;
import plus.cove.flower.domain.repository.schedule.EventListExport;

import java.time.LocalDateTime;

/**
 * 日程事件列表
 * <p>
 * 所有日程事件信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class EventListOutput {
    /**
     * id
     */
    private Long id;

    /**
     * 事件名称
     */
    private String title;

    /**
     * 状态
     */
    private ScheduleEventState state;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    private LocalDateTime finishTime;

    /**
     * 日程id
     */
    private Long scheduleId;

    /**
     * 日程名称
     */
    private String scheduleTitle;

    public static EventListOutput fromExport(EventListExport ep) {
        EventListOutput op = new EventListOutput();
        op.id = ep.getId();
        op.state = ep.getState();
        op.title = ep.getTitle();
        op.startTime = ep.getStartTime();
        op.finishTime = ep.getFinishTime();
        op.scheduleId = ep.getScheduleId();
        op.scheduleTitle = ep.getScheduleTitle();
        return op;
    }
}
