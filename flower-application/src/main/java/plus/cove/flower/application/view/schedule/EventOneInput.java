package plus.cove.flower.application.view.schedule;

import jakarta.ws.rs.QueryParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;

import jakarta.validation.constraints.NotNull;

/**
 * 事件
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class EventOneInput extends RequestInput {
    @Schema(title = "事件id", description = "必填")
    @QueryParam(value = "id")
    @NotNull(message = "id不能为空")
    private Long id;
}
