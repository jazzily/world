package plus.cove.flower.application.view.schedule;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;
import plus.cove.flower.domain.entity.schedule.ScheduleEventState;

import java.time.LocalDateTime;

/**
 * 日程更新
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class EventOneOutput {
    @Schema(title = "日程id", description = "必填，对应的日程")
    private Long id;

    @Schema(title = "标题", description = "必填，最多32个字符")
    private String title;

    @Schema(title = "备注", description = "选填，最多1024个字符")
    private String remark;

    @Schema(title = "状态", description = "状态")
    private ScheduleEventState state;

    @Schema(title = "开始时间", description = "必填，开始时间，格式：yyyy-MM-ddTHH:mm:ss")
    private LocalDateTime startTime;

    @Schema(title = "结束时间", description = "必填，结束时间，格式：yyyy-MM-ddTHH:mm:ss")
    private LocalDateTime finishTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 日程id
     */
    private Long scheduleId;
    /**
     * 日程名称
     */
    private String scheduleTitle;

    public static EventOneOutput fromEntity(ScheduleEvent entity) {
        EventOneOutput event = new EventOneOutput();
        event.id = entity.getId();
        event.title = entity.getTitle();
        event.remark = entity.getRemark();
        event.state = entity.getState();
        event.startTime = entity.getStartTime();
        event.finishTime = entity.getFinishTime();
        event.createTime = entity.getCreateTime();
        event.scheduleId = entity.getScheduleId();
        event.scheduleTitle = entity.getSchedule().getTitle();
        return event;
    }
}
