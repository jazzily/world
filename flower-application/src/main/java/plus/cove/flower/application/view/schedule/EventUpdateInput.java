package plus.cove.flower.application.view.schedule;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 日程更新
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class EventUpdateInput extends RequestInput {
    @Schema(title = "日程id", description = "必填，对应的日程")
    @NotNull(message = "日程id不能为空")
    private Long id;

    @Schema(title = "标题", description = "必填，最多32个字符")
    @NotEmpty(message = "标题不能为空")
    @Size(max = 32, message = "标题最多32个字符")
    private String title;

    @Schema(title = "备注", description = "选填，最多1024个字符")
    @Size(max = 1024, message = "备注最多1024个字符")
    private String remark;

    @Schema(title = "开始时间", description = "必填，开始时间，格式：yyyy-MM-ddTHH:mm:ss")
    private LocalDateTime startTime;

    @Schema(title = "结束时间", description = "必填，结束时间，格式：yyyy-MM-ddTHH:mm:ss")
    private LocalDateTime finishTime;

    public ScheduleEvent toEvent() {
        ScheduleEvent event = ScheduleEvent.init(this.id);
        event.setTitle(this.title);
        event.setRemark(this.remark);
        event.setStartTime(this.startTime);
        event.setFinishTime(this.finishTime);
        return event;
    }
}
