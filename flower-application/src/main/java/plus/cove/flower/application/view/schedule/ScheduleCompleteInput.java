package plus.cove.flower.application.view.schedule;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;

/**
 * 日程
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ScheduleCompleteInput extends RequestInput {
    @Schema(title = "日程id", description = "必填")
    @NotNull(message = "id不能为空")
    private Long id;

    public Schedule toSchedule() {
        Schedule entity = Schedule.init(this.id);
        entity.complete();
        return entity;
    }

    public ScheduleEvent toEvent() {
        ScheduleEvent entity = new ScheduleEvent();
        entity.setScheduleId(this.id);
        entity.complete();

        return entity;
    }

}
