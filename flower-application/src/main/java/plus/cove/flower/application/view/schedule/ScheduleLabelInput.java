package plus.cove.flower.application.view.schedule;

import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.label.Label;
import plus.cove.flower.domain.entity.schedule.ScheduleLabel;

/**
 * 日程标签
 * <p>
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ScheduleLabelInput extends RequestInput {
    @Schema(title = "id", description = "标签id")
    private Long id;

    @Schema(title = "日程id", description = "日程id")
    private Long scheduleId;

    @Schema(title = "标签id", description = "标签id")
    private Long labelId;

    @Schema(title = "标签名称", description = "标签名称，为空则为删除")
    @Size(max = 20, message = "标签长度最大为20个字符")
    private String labelName;

    public Label toLabel() {
        Label entity = Label.create();
        entity.setName(this.labelName);
        entity.setUserId(this.getRequestUser());
        return entity;
    }

    public ScheduleLabel toScheduleLabel() {
        ScheduleLabel entity = ScheduleLabel.init(this.id);
        entity.setScheduleId(this.scheduleId);
        entity.setLabelId(this.labelId);
        entity.setLabelName(this.labelName);
        return entity;
    }
}
