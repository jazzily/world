package plus.cove.flower.application.view.schedule;

import jakarta.validation.constraints.Size;
import jakarta.ws.rs.QueryParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.dromara.hutool.core.text.CharSequenceUtil;
import org.dromara.hutool.core.text.split.SplitUtil;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.schedule.ScheduleState;
import plus.cove.flower.domain.repository.schedule.ScheduleListImport;
import plus.cove.infrastructure.helper.EnumHelper;
import plus.cove.infrastructure.helper.SqlHelper;

import java.time.LocalDate;
import java.util.List;

/**
 * 日程列表
 * <p>
 * 支持
 * 按时间查找
 * 按标签查找
 * 按状态查找
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ScheduleListInput extends RequestInput {
    @Schema(title = "开始时间", description = "按时间查找，开始时间，格式：yyyy-MM-dd")
    @QueryParam(value = "start")
    private LocalDate start;

    @Schema(title = "结束时间", description = "按时间查找，结束时间，格式：yyyy-MM-dd")
    @QueryParam(value = "finish")
    private LocalDate finish;

    @Schema(title = "标签名称", description = "按标签查找，支持最多10个标签，逗号分隔")
    @Size(max = 200, message = "标签最长200个字符")
    @QueryParam(value = "label")
    private String label;

    @Schema(title = "状态名称", description = "按状态查找，支持最多10个状态，逗号分隔")
    @Size(max = 200, message = "状态最长200个字符")
    @QueryParam(value = "state")
    private String state;

    @Schema(title = "排序规则", description = "排序规则，排序字段.排序顺序，比如：start.asc,finish.desc,title,complete")
    @Size(max = 200, message = "排序规则最长200个字符")
    @QueryParam(value = "sort_by")
    private String sortBy;

    public ScheduleListImport toImport() {
        ScheduleListImport im = new ScheduleListImport();
        im.setUserId(this.getRequestUser());

        // 区间处理
        if (this.start != null && this.finish != null) {
            im.setStart(start);
            im.setFinish(finish.plusDays(1L));
        }

        // 标签处理
        if (CharSequenceUtil.isNotEmpty(label)) {
            List<String> labels = SplitUtil.split(label, ",");
            im.setLabels(labels);
        }

        // 状态处理ScheduleState
        if (CharSequenceUtil.isNotEmpty(state)) {
            List<Integer> values = EnumHelper.toValues(ScheduleState.class, state);
            im.setStates(values);
        }

        // 排序规则
        if (CharSequenceUtil.isNotEmpty(sortBy)) {
            String ob = SqlHelper.orderBy(sortBy,
                    "start.start_time,finish.finish_time,complete.complete_rate,star.importance");
            im.setSortBy(ob);
        } else {
            im.setSortBy("sd.id");
        }

        return im;
    }
}
