package plus.cove.flower.application.view.schedule;

import lombok.Data;
import plus.cove.flower.domain.entity.schedule.ScheduleRepeatType;
import plus.cove.flower.domain.entity.schedule.ScheduleState;
import plus.cove.flower.domain.repository.schedule.ScheduleListExport;

import java.time.LocalDateTime;

/**
 * 日程列表
 * <p>
 * 简单日程信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class ScheduleListOutput {
    /**
     * id
     */
    private Long id;

    /**
     * 执行状态
     * 表示计划，完成，超期
     */
    private ScheduleState state;

    /**
     * 标题
     */
    private String title;

    /**
     * 重复类型
     */
    private ScheduleRepeatType repeatType;

    /**
     * 结束时间
     */
    private LocalDateTime finishTime;

    /**
     * 完成率
     */
    private Integer completeRate;

    /**
     * 重要度
     */
    private Byte importance;

    /**
     * 标签列表
     */
    private String labels;

    public static ScheduleListOutput fromExport(ScheduleListExport ep) {
        return ScheduleMapper.INSTANCE.fromScheduleList(ep);
    }
}
