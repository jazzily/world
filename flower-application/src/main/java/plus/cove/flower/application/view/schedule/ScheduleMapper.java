package plus.cove.flower.application.view.schedule;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleTimeline;
import plus.cove.flower.domain.repository.schedule.ScheduleListExport;

import java.util.List;

@Mapper(componentModel = "jakarta")
public interface ScheduleMapper {
    ScheduleMapper INSTANCE = Mappers.getMapper(ScheduleMapper.class);

    ScheduleListOutput fromScheduleList(ScheduleListExport export);

    @Mapping(target = "labels", ignore = true)
    ScheduleOneOutput fromScheduleOne(Schedule entity);

    TimelineListOutput fromTimelineOne(ScheduleTimeline entity);

    List<TimelineListOutput> fromTimelineList(List<ScheduleTimeline> entities);
}
