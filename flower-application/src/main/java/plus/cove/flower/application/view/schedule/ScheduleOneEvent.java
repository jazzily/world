package plus.cove.flower.application.view.schedule;

import lombok.Data;
import plus.cove.flower.domain.entity.schedule.ScheduleEventState;

import java.time.LocalDateTime;

/**
 * 日程列表
 * <p>
 * 简单日程信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class ScheduleOneEvent {
    /**
     * 编码
     */
    private Long id;

    /**
     * 状态
     */
    private ScheduleEventState state;

    /**
     * 事件名称
     */
    private String title;

    /**
     * 备注
     */
    private String remark;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    private LocalDateTime finishTime;

    /**
     * 完成时间
     */
    private LocalDateTime completeTime;
}
