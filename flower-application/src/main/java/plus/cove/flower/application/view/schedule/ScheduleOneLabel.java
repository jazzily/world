package plus.cove.flower.application.view.schedule;

import lombok.Data;

/**
 * 日程列表
 * <p>
 * 简单日程信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class ScheduleOneLabel {
    /**
     * 编号
     */
    private Long id;

    /**
     * 标签id
     */
    private Long labelId;

    /**
     * 标签名称
     */
    private String labelName;

    /**
     * 日程id
     */
    private Long scheduleId;
}
