package plus.cove.flower.application.view.schedule;

import lombok.Data;
import plus.cove.flower.domain.entity.schedule.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 日程列表
 * <p>
 * 简单日程信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class ScheduleOneOutput {
    /**
     * 编号
     */
    private Long id;

    /**
     * 执行状态
     * 表示计划，完成，超期
     */
    private ScheduleState state;

    /**
     * 标题
     */
    private String title;

    /**
     * 地点
     */
    private String place;

    /**
     * 备注
     */
    private String remark;

    /**
     * 重复类型
     */
    private ScheduleRepeatType repeatType;

    /**
     * 重复项目
     */
    private String repeatItem;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    private LocalDateTime finishTime;

    /**
     * 完成率
     */
    private Integer completeRate;

    /**
     * 完成时间
     */
    private LocalDateTime completeTime;

    /**
     * 重要度
     */
    private Byte importance;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 标签
     */
    private List<ScheduleOneLabel> labels;

    /**
     * 日程事件
     */
    private List<ScheduleOneEvent> events;

    public static ScheduleOneOutput from(Schedule entity, List<ScheduleLabel> labels, List<ScheduleEvent> events) {
        ScheduleOneOutput one = ScheduleMapper.INSTANCE.fromScheduleOne(entity);

        // label
        List<ScheduleOneLabel> oneLabels = new ArrayList<>(labels.size());
        for (ScheduleLabel label : labels) {
            ScheduleOneLabel ol = new ScheduleOneLabel();
            ol.setId(label.getId());
            ol.setLabelId(label.getLabelId());
            ol.setLabelName(label.getLabelName());
            ol.setScheduleId(label.getScheduleId());
            oneLabels.add(ol);
        }
        one.setLabels(oneLabels);

        // event
        List<ScheduleOneEvent> oneEvents = new ArrayList<>(events.size());
        for (ScheduleEvent event : events) {
            ScheduleOneEvent oe = new ScheduleOneEvent();
            oe.setId(event.getId());
            oe.setTitle(event.getTitle());
            oe.setState(event.getState());
            oe.setStartTime(event.getStartTime());
            oe.setFinishTime(event.getFinishTime());
            oe.setRemark(event.getRemark());
            oe.setCompleteTime(event.getCompleteTime());
            oneEvents.add(oe);
        }
        one.setEvents(oneEvents);
        return one;
    }
}
