package plus.cove.flower.application.view.schedule;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.schedule.ScheduleRepeatType;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 日程创建
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ScheduleSaveInput extends RequestInput {
    @Schema(title = "标题", description = "必填，最多32个字符")
    @NotEmpty(message = "标题不能为空")
    @Size(max = 32, message = "标题最多32个字符")
    private String title;

    @Schema(title = "位置", description = "选填，最多64个字符")
    @Size(max = 64, message = "位置最多64个字符")
    private String place;

    @Schema(title = "备注", description = "选填，最多1024个字符")
    @Size(max = 1024, message = "备注最多1024个字符")
    private String remark;

    @Schema(title = "重复类型", description = "必填，影响重复项")
    @NotNull(message = "重复类型不能为空")
    private ScheduleRepeatType repeatType;

    @Schema(title = "重复项目", description = "选填，按周使用周几：[1-7]；按月使用日：[01-31]；按年使用月日：[0101-1231]")
    private String repeatItem;

    @Schema(title = "开始时间", description = "必填，开始时间，格式：yyyy-MM-ddTHH:mm:ss")
    @NotNull(message = "开始时间不能为空")
    private LocalDateTime startTime;

    @Schema(title = "结束时间", description = "必填，结束时间，格式：yyyy-MM-ddTHH:mm:ss")
    @NotNull(message = "结束时间不能为空")
    private LocalDateTime finishTime;
}
