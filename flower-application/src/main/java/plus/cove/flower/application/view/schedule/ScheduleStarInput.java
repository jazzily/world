package plus.cove.flower.application.view.schedule;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.schedule.Schedule;

/**
 * 日程创建
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ScheduleStarInput extends RequestInput {
    @Schema(title = "编号", description = "必填")
    @NotNull(message = "编号不能为空")
    private Long id;

    @Schema(title = "重要度", description = "必填，1-5")
    @NotNull(message = "重要度不能为空")
    private Byte star;

    public Schedule toSchedule(){
        Schedule entity = Schedule.init(this.id);
        entity.setImportance(star);
        return entity;
    }
}
