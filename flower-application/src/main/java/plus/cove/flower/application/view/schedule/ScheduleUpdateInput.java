package plus.cove.flower.application.view.schedule;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.entity.schedule.Schedule;

import java.time.LocalDateTime;

/**
 * 日程创建
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ScheduleUpdateInput extends RequestInput {
    @Schema(title = "编号", description = "必填")
    @NotNull(message = "编号不能为空")
    private Long id;

    @Schema(title = "标题", description = "必填，最多32个字符")
    @NotEmpty(message = "标题不能为空")
    @Size(max = 32, message = "标题最多32个字符")
    private String title;

    @Schema(title = "位置", description = "选填，最多64个字符")
    @Size(max = 64, message = "位置最多64个字符")
    private String place;

    @Schema(title = "备注", description = "选填，最多1024个字符")
    @Size(max = 1024, message = "备注最多1024个字符")
    private String remark;

    @Schema(title = "开始时间", description = "开始时间，格式：yyyy-MM-ddTHH:mm:ss")
    private LocalDateTime startTime;

    @Schema(title = "结束时间", description = "结束时间，格式：yyyy-MM-ddTHH:mm:ss")
    private LocalDateTime finishTime;

    public Schedule toSchedule(){
        Schedule entity = Schedule.init(this.id);
        entity.setTitle(this.title);
        entity.setPlace(this.place);
        entity.setRemark(this.remark);
        return entity;
    }
}
