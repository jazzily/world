package plus.cove.flower.application.view.schedule;

import jakarta.ws.rs.QueryParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import plus.cove.flower.application.view.RequestInput;
import plus.cove.flower.domain.repository.schedule.TimelineListImport;

import java.util.List;

/**
 * 时间线
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TimelineListInput extends RequestInput {
    @Schema(title = "年份", description = "年份")
    @QueryParam(value = "year")
    private String year;

    @Schema(title = "是否计划", description = "重要日程")
    @QueryParam(value = "plan")
    private Boolean plan = false;

    public TimelineListImport toImport() {
        TimelineListImport input = new TimelineListImport();
        input.setUserId(this.getRequestUser());
        input.setYear(year);

        // 对应日程和年度报告
        if (plan) {
            input.setCategory(List.of("schedule", "annual_report"));
        }

        return input;
    }
}
