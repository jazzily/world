package plus.cove.flower.application.view.schedule;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 时间线
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class TimelineListOutput {
    /**
     * 编号
     */
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 阶段
     */
    private String phase;

    /**
     * 类型
     * schedule         日程创建，完成
     * schedule_event   日程事件完成
     * annual_report    年度报告，完成N项任务，N项事件
     */
    private String category;

    /**
     * 日程名称
     */
    private String scheduleTitle;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
