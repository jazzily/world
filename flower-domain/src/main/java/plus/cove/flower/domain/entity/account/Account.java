package plus.cove.flower.domain.entity.account;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.dromara.hutool.core.util.RandomUtil;
import org.dromara.hutool.crypto.digest.DigestUtil;
import plus.cove.infrastructure.component.BaseEntity;
import plus.cove.infrastructure.helper.AssertHelper;
import plus.cove.infrastructure.helper.StringHelper;

import java.time.LocalDateTime;

/**
 * 账号实体类
 * <p>
 * 包括账号信息，密码等
 * 与业务没有关系
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Account extends BaseEntity {
    /**
     * 名称
     */
    private String name;

    /**
     * 状态
     */
    private AccountState state;

    /**
     * 加盐
     */
    private String salt;

    /**
     * 密码
     * 加密后存储
     */
    private String secret;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 过期时间
     * 锁定状态，过期时间指锁定是否过期
     */
    private LocalDateTime expiredTime;

    /**
     * 创建密码
     * 同时赋值salt和secret
     */
    public void createPassword(String password) {
        this.salt = RandomUtil.randomString(32);
        this.secret = buildPassword(password);
    }

    /**
     * 生成密码
     */
    public String buildPassword(String password) {
        AssertHelper.assertNotEmpty(password, "password is empty");
        return DigestUtil.sha256Hex(StringHelper.join(password, this.salt));
    }

    /**
     * 验证密码
     */
    public boolean verifyPassword(String password) {
        String md5Password = this.buildPassword(password);
        return md5Password.equals(this.secret);
    }

    /**
     * 验证状态
     */
    public boolean verifyState() {
        // 废止状态
        if (this.state == AccountState.DISABLED) {
            return false;
        }

        // 锁定状态
        if (this.state == AccountState.LOCKED) {
            return this.expiredTime.isBefore(LocalDateTime.now());
        }

        return true;
    }

    /**
     * 初始化
     */
    public static Account init() {
        Account entity = new Account();
        entity.updateTime = LocalDateTime.now();
        return entity;
    }

    /**
     * 创建
     * <p>
     * 包括id
     */
    public static Account create() {
        Account entity = new Account();
        entity.valueOf();
        entity.state = AccountState.ACTIVE;
        entity.createTime = LocalDateTime.now();
        entity.expiredTime = LocalDateTime.now();
        return entity;
    }
}
