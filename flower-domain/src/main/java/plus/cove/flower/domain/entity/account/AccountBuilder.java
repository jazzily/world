package plus.cove.flower.domain.entity.account;

/**
 * 账号建造器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public final class AccountBuilder {
    private Account entity = null;

    private AccountBuilder(){}

    public static AccountBuilder init() {
        AccountBuilder builder = new AccountBuilder();
        builder.entity = Account.create();
        return builder;
    }

    public AccountBuilder name(String name) {
        this.entity.setName(name);
        return this;
    }

    public AccountBuilder user(Long userId) {
        this.entity.setUserId(userId);
        return this;
    }

    public AccountBuilder password(String password) {
        // 生成salt
        entity.createPassword(password);

        return this;
    }

    public Account build() {
        return this.entity;
    }
}
