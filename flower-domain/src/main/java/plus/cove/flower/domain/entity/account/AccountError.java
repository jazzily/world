package plus.cove.flower.domain.entity.account;

import plus.cove.infrastructure.exception.BusinessError;

/**
 * 账号错误
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum AccountError implements BusinessError {
    ACCOUNT_HAD_EXISTED("A1000", "该账号已存在"),
    ACCOUNT_NAME_ABSENT("A1100", "该账号不存在"),
    ACCOUNT_LOGIN_FAILURE("A1200", "账号或密码错误"),
    ACCOUNT_VALIDATE_FAILURE("A1210", "账号验证码错误"),
    ACCOUNT_LOGIN_LOCKED("A1300", "账号被锁定，稍后再试"),
    ACCOUNT_LOGIN_DISABLED("A1301","账号被禁用，无法使用"),
    EXCEED_PASSWORD_LIMIT("A1400", "超过密码错误上限"),
    EXCEED_LOGIN_LIMIT("A1500", "超过登陆错误上限");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     */
    AccountError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
