package plus.cove.flower.domain.entity.account;

import plus.cove.infrastructure.exception.BusinessException;

import java.time.LocalDateTime;

/**
 * 账号规则
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public final class AccountRuler {
    private static final int SUSPECT_COUNT = 3;
    private static final int LOCK_COUNT = 6;

    // 验证过的
    private boolean validated;
    // 初始值
    private int initial;
    // 返回信息
    private String message;

    private AccountRuler() {
        this.validated = true;
    }

    public static AccountRuler init(Integer initial) {
        AccountRuler builder = new AccountRuler();
        builder.initial = (initial == null ? 0 : initial);
        return builder;
    }

    /**
     * 验证密码
     */
    public AccountRuler validPassword(boolean valid) {
        if (!validated) {
            return this;
        }

        if (!valid) {
            this.validated = false;
            this.initial++;
            this.message = AccountError.ACCOUNT_LOGIN_FAILURE.message();
        }
        return this;
    }

    /**
     * 验证验证码
     */
    public AccountRuler validValidation(boolean valid) {
        if (!validated) {
            return this;
        }

        if (!valid) {
            this.validated = false;
            this.initial++;
            this.message = AccountError.ACCOUNT_LOGIN_FAILURE.message();
        }
        return this;
    }

    public void validAccount(Account account) {
        if (!this.validated) {
            if (this.initial >= SUSPECT_COUNT) {
                account.setState(AccountState.SUSPECT);
            }
            if (this.initial >= LOCK_COUNT) {
                account.setState(AccountState.LOCKED);
                account.setExpiredTime(LocalDateTime.now().plusHours(24L));
            }
        } else {
            account.setState(AccountState.ACTIVE);
        }
    }

    public void validException() {
        if (!this.validated) {
            if (this.initial >= LOCK_COUNT) {
                throw new BusinessException(AccountError.EXCEED_LOGIN_LIMIT, this.message);
            }
            if (this.initial >= SUSPECT_COUNT) {
                throw new BusinessException(AccountError.EXCEED_PASSWORD_LIMIT, this.message);
            }
            throw new BusinessException(AccountError.ACCOUNT_LOGIN_FAILURE);
        }
    }
}
