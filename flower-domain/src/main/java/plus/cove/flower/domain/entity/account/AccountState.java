package plus.cove.flower.domain.entity.account;

import plus.cove.infrastructure.component.BaseEnum;

/**
 * 账号状态
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum AccountState implements BaseEnum {
    ACTIVE(1, "激活的"),
    SUSPECT(2, "可疑的"),
    LOCKED(3, "锁定的"),
    DISABLED(4, "禁用的");

    /**
     * 枚举值
     */
    private final int value;

    /**
     * 枚举描述
     */
    private final String description;

    /**
     * 构造函数
     */
    AccountState(final int value, final String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
