package plus.cove.flower.domain.entity.author;

import org.dromara.hutool.core.text.CharSequenceUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseTimeEntity;

import jakarta.persistence.Entity;

/**
 * 用户信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Author extends BaseTimeEntity {
    /**
     * 名称
     */
    private String name;

    /**
     * 电话
     */
    private String email;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态
     */
    private String status;

    public static Author init(Long id) {
        Author author = new Author();
        author.setId(id);
        return author;
    }

    /**
     * 创建
     *
     * @param name  名称
     * @param email 邮箱
     * @return 作者
     */
    public static Author create(String name, String email) {
        Author author = new Author();
        author.valueOf();
        author.name = name;
        author.email = email;
        author.status = CharSequenceUtil.EMPTY;
        author.avatar = CharSequenceUtil.EMPTY;
        return author;
    }
}
