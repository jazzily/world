package plus.cove.flower.domain.entity.author;

import plus.cove.infrastructure.exception.BusinessError;

/**
 * 作者错误
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum AuthorError implements BusinessError {
    ACCOUNT_NAME_ABSENCE("B1000", "该账号不存在"),
    ACCOUNT_NAME_EXIST("B1001", "账号已存在"),
    ACCOUNT_PASSWORD_ERROR("B1002", "密码错误"),
    ACCOUNT_STATUS_INVALID("B1003", "该用户没有使用权限");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     */
    AuthorError(final String code, final String message) {
        this.code =  code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
