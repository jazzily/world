package plus.cove.flower.domain.entity.global;

import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.flower.domain.entity.schedule.ScheduleWeek;
import plus.cove.infrastructure.component.BaseEntity;

import jakarta.persistence.Entity;
import java.time.LocalDate;

/**
 * 日历
 * <p>
 * 用于日历显示
 * 农历数据
 * 节假日
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class CalendarData extends BaseEntity {
    /**
     * 名称
     * 日期或节气
     */
    private String name;

    /**
     * 标记
     * 日历表中特殊标记
     * 例如：班，休，气
     */
    private String sign;

    /**
     * 标题
     * 农历全程
     * 比如：冬月廿四
     */
    private String title;

    /**
     * 备注
     */
    private String remark;

    /**
     * 所在日期
     */
    private LocalDate onDate;

    /**
     * 所在星期
     */
    private ScheduleWeek onWeek;
}
