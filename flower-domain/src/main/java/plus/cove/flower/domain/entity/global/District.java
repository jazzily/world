package plus.cove.flower.domain.entity.global;

import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;

import jakarta.persistence.Entity;

/**
 * 区域
 * <p>
 * 包括省市区
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class District extends BaseEntity {
    /**
     * 行政编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 全称
     */
    private String title;

    /**
     * 类型
     */
    private DistrictType type;

    public static District from(DistrictType type) {
        District entity = new District();
        entity.type = type;
        return entity;
    }
}
