package plus.cove.flower.domain.entity.global;

import org.dromara.hutool.core.text.CharSequenceUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;

import jakarta.persistence.Entity;

/**
 * 数据
 * <p>
 * 通用数据
 * 保存非结构化数据，采用json格式保存
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class GeneralData extends BaseEntity {
    /**
     * 编码
     * 保证唯一集合，建议有意义的英文
     */
    private String code;

    /**
     * 名称
     * 该条数据的说明
     */
    private String name;

    /**
     * 类别
     */
    private String type;

    /**
     * 数据
     * 仅支持json格式
     */
    private String data;

    /**
     * 创建
     */
    public static GeneralData create(String code, String name, String data) {
        GeneralData entity = new GeneralData();
        entity.code = code;
        entity.name = name;
        entity.data = data;

        return entity;
    }

    /**
     * 格式化
     */
    public String format(Object... arguments) {
        return CharSequenceUtil.indexedFormat(this.data, arguments);
    }
}
