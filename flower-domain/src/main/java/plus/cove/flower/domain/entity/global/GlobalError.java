package plus.cove.flower.domain.entity.global;

import plus.cove.infrastructure.exception.BusinessError;

/**
 * 全局错误
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum GlobalError implements BusinessError {
    GENERAL_NO_EXIST("G1000", "G1000:配置不存在");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     */
    GlobalError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
