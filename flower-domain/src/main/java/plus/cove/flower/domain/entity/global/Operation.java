package plus.cove.flower.domain.entity.global;

import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;

import jakarta.persistence.Entity;
import java.time.LocalDateTime;

/**
 * 操作日志
 * <p>
 * 记录登录失败情况
 * 登录成功后，失败的记录转为无用的记录
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Operation extends BaseEntity {
    /**
     * 行为
     * LOGIN
     */
    private String action;

    /**
     * 结果
     * Y=成功 SUCCESS
     * N=失败 FAILURE
     */
    private String result;

    /**
     * 目标编码
     */
    private String targetId;

    /**
     * 用户
     */
    private Long actionId;

    /**
     * 行为时间
     */
    private LocalDateTime actionTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    public void withUser(Long userId) {
        this.actionId = userId;
    }

    public static Operation init(Long actionId, String action, String result) {
        Operation entity = new Operation();
        entity.setAction(action);
        entity.setActionId(actionId);
        entity.setResult(result);

        LocalDateTime now = LocalDateTime.now();
        entity.actionTime = now;
        entity.updateTime = now;
        return entity;
    }

    public static Operation init(Long actionId, OperationAction action, String result) {
        return init(actionId, action.getAction(), result);
    }
}
