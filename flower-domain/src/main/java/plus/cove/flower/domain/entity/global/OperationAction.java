package plus.cove.flower.domain.entity.global;

import plus.cove.infrastructure.component.BaseEnum;

/**
 * 操作行为
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum OperationAction implements BaseEnum {
    USER_LOGIN(10, "USER_LOGIN", "用户登录"),
    USER_LOGOUT(11,"USER_LOGOUT","用户注销"),
    EVENT_DELETE(20, "EVENT_DELETE", "事件删除");

    /**
     * 枚举值
     */
    private final int value;

    /**
     * 行为
     */
    private final String action;

    /**
     * 枚举描述
     */
    private final String description;

    /**
     * 构造函数
     */
    OperationAction(final int value, final String action, final String description) {
        this.value = value;
        this.action = action;
        this.description = description;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getAction() {
        return action;
    }

    @Override
    public String getDescription() {
        return description;
    }
}

