package plus.cove.flower.domain.entity.global;

import org.dromara.hutool.crypto.digest.DigestUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;

import jakarta.persistence.Entity;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 验证码
 * <p>
 * 可以是手机验证码
 * <p>
 * 一次性使用，有过期时间
 * hash处理
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Validation extends BaseEntity {
    /**
     * 目标
     * 可以是手机号，邮箱等
     */
    private String target;

    /**
     * 验证码
     */
    private String authCode;

    /**
     * 类别
     * validationCategory
     */
    private String authCategory;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 过期时间
     */
    private LocalDateTime expiredTime;

    /**
     * 创建验证码
     *
     */
    public static Validation create(String target, String authCode, String authCategory, long expSeconds) {
        Validation entity = new Validation();

        entity.target = target;
        entity.createTime = LocalDateTime.now();;
        entity.expiredTime = entity.createTime.plusSeconds(expSeconds);;

        // authCode需要hash
        entity.authCode = DigestUtil.md5Hex(authCode);
        entity.authCategory = authCategory;
        return entity;
    }

    /**
     * 是否有效
     */
    public static boolean valid(Validation entity, String authCode) {
        if (entity == null || entity.getExpiredTime().isBefore(LocalDateTime.now())) {
            return false;
        }

        String md5Auth = DigestUtil.md5Hex(authCode);
        return Objects.equals(entity.getAuthCode(), md5Auth);
    }
}
