package plus.cove.flower.domain.entity.global;

import org.dromara.hutool.core.util.RandomUtil;

/**
 * 验证码生成器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public final class ValidationBuilder {
    private static final String RANDOM_EASY_WORD = "aAbBcCdDeEfFgGhHjJkKLmMnNpPqQrRsStTuUvVwWxXyY3456789";

    private String target;
    private String authCode;
    private ValidationCategory authCategory;

    private ValidationBuilder() {
    }

    public static ValidationBuilder init() {
        return new ValidationBuilder();
    }

    /**
     * 对象
     */
    public void target(String target) {
        this.target = target;
    }

    /**
     * 创建验证码
     */
    public void authCode(ValidationCategory category) {
        this.authCategory = category;

        switch (category) {
            case LOGIN_EMAIL, RESET_EMAIL:
                authCode = RandomUtil.randomNumbers(8);
                break;
            case LOGIN_PHONE:
                authCode = RandomUtil.randomNumbers(6);
                break;
            default:
                authCode = RandomUtil.randomString(6);
                break;
        }
    }

    /**
     * 查看验证码
     * 未经加密的
     */
    public String authCode() {
        return this.authCode;
    }

    /**
     * 创建
     */
    public Validation build() {
        return Validation.create(this.target, this.authCode, this.authCategory.name(), this.authCategory.getExpire());
    }
}
