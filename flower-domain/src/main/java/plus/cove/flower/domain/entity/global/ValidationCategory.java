package plus.cove.flower.domain.entity.global;

import plus.cove.infrastructure.component.BaseEnum;

/**
 * 验证码类别
 * <p>
 * 类型不同，有效期不同
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ValidationCategory implements BaseEnum {
    // 登录，有效期10分钟
    LOGIN_EMAIL(10, 600, "LOGIN_EMAIL_VALIDATION", "用户登录验证码发送邮件"),
    RESET_EMAIL(11, 600, "RESET_EMAIL_VALIDATION", "用户找回密码发送邮件"),
    CHANGE_EMAIL(12, 600, "CHANGE_EMAIL_VALIDATION", "用户修改账号发送邮件"),
    LOGOUT_EMAIL(13, 600, "LOGOUT_EMAIL_VALIDATION", "用户注销账号发送邮件"),

    LOGIN_PHONE(20, 600, "LOGIN_PHONE_VALIDATION", "用户登录验证码发送短信");

    /**
     * 枚举值
     */
    private final int value;

    /**
     * 有效期
     * 单位秒
     */
    private final int expire;

    /**
     * 配置
     * 统一配置
     */
    private final String config;

    /**
     * 枚举描述
     */
    private final String description;

    /**
     * 构造函数
     */
    ValidationCategory(final int value, final int expire, final String config, final String description) {
        this.value = value;
        this.expire = expire;
        this.config = config;
        this.description = description;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public Integer getExpire() {
        return expire;
    }

    public String getConfig() {
        return config;
    }

    @Override
    public String getDescription() {
        return description;
    }
}

