package plus.cove.flower.domain.entity.global.builder;

import plus.cove.flower.domain.entity.global.Operation;
import plus.cove.flower.domain.entity.global.OperationAction;

/**
 * 操作建造器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class OperationBuilder {
    protected Long userId;
    protected String target;
    protected String action;
    protected String result;

    public static OperationBuilder init(OperationAction action) {
        OperationBuilder builder = new OperationBuilder();
        builder.action = action.getAction();
        return builder;
    }

    public OperationBuilder action(Long userId) {
        this.userId = userId;
        return this;
    }

    public OperationBuilder target(String target) {
        this.target = target;
        return this;
    }

    public OperationBuilder success() {
        return this.result("Y");
    }

    public OperationBuilder failure() {
        return this.result("N");
    }

    public OperationBuilder result(String result) {
        this.result = result;
        return this;
    }

    /**
     * 创建操作
     */
    public Operation build() {
        Operation entity = Operation.init(this.userId, this.action, this.result);
        entity.setTargetId(this.target);
        // 默认值
        if (entity.getResult() == null) {
            entity.setResult("Y");
        }
        return entity;
    }
}
