package plus.cove.flower.domain.entity.issue;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;
import java.time.LocalDateTime;

/**
 * 反馈意见
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Issue extends BaseEntity {
    /**
     * 标题
     */
    private String title;

    /**
     * 联系方式
     */
    private String contact;

    /**
     * 内容
     */
    private String content;

    /**
     * 所属用户
     */
    private Long userId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    public static Issue create() {
        Issue entity = new Issue();
        entity.createTime = LocalDateTime.now();
        return entity;
    }
}
