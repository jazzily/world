package plus.cove.flower.domain.entity.label;

import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;

import jakarta.persistence.Entity;

/**
 * 标签
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Label extends BaseEntity {
    /**
     * 名称
     */
    private String name;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 使用数
     * 多少个日程使用
     */
    private Integer usingCount;

    public static Label init(Long id) {
        Label entity = new Label();
        entity.setId(id);
        return entity;
    }

    public static Label create() {
        Label entity = new Label();
        entity.valueOf();
        entity.usingCount = 0;
        return entity;
    }

    /**
     * 创建
     */
    public static Label from(Long userId, String name) {
        Label entity = new Label();
        entity.name = name;
        entity.userId = userId;
        return entity;
    }
}
