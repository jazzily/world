package plus.cove.flower.domain.entity.label;

import plus.cove.infrastructure.exception.BusinessError;

/**
 * 标签错误
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum LabelError implements BusinessError {
    LABEL_HAD_EXIST("L1000", "该标签已存在"),
    LABEL_NOT_EXIST("L1001", "该标签不存在"),
    LABEL_IN_USING("L1002", "该标签正在使用中");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     */
    LabelError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
