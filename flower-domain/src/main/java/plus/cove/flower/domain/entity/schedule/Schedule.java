package plus.cove.flower.domain.entity.schedule;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.flower.domain.principal.PrincipalPermit;
import plus.cove.infrastructure.component.BaseTimeEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 日程
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Schedule extends BaseTimeEntity
        implements PrincipalPermit {
    /**
     * 执行状态
     * 表示计划，完成，超期
     */
    private ScheduleState state;

    /**
     * 标题
     */
    private String title;

    /**
     * 地点
     */
    private String place;

    /**
     * 备注
     */
    private String remark;

    /**
     * 重复类型
     */
    private ScheduleRepeatType repeatType;

    /**
     * 重复项目
     */
    private String repeatItem;

    /**
     * 重复提示
     */
    private String repeatPrompt;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    private LocalDateTime finishTime;

    /**
     * 完成比率
     */
    private Integer completeRate;

    /**
     * 完成时间
     */
    private LocalDateTime completeTime;

    /**
     * 重要性
     * 越大越重要1-5
     */
    private Byte importance;

    /**
     * 作者
     */
    private Long userId;

    /**
     * 日程事件
     */
    private List<ScheduleEvent> events;

    public boolean completed() {
        return this.state == ScheduleState.FINISHED;
    }

    /**
     * 完成
     */
    public void complete() {
        this.complete(100);
    }

    /**
     * 完成率
     */
    public void complete(Integer rate) {
        this.completeRate = rate;
        this.completeTime = LocalDateTime.now();

        // 完成率100，则修改状态
        if (rate != null && rate >= 100) {
            this.state = ScheduleState.FINISHED;
        }
    }

    /**
     * 添加事件
     */
    public void addEvent(ScheduleEvent event) {
        if (this.events == null) {
            this.events = new ArrayList<>();
        }
        this.events.add(event);
    }

    /**
     * 初始化
     */
    public static Schedule init(Long scheduleId) {
        Schedule entity = new Schedule();
        entity.setId(scheduleId);
        return entity;
    }

    /**
     * 创建
     */
    public static Schedule create() {
        Schedule entity = new Schedule();
        entity.valueOf();
        entity.state = ScheduleState.PLANNING;
        entity.completeRate = 0;
        entity.importance = 0;
        return entity;
    }

    @Override
    public Long getPermitUser() {
        return this.userId;
    }
}
