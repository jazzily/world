package plus.cove.flower.domain.entity.schedule;

import plus.cove.infrastructure.exception.BusinessError;

/**
 * 日程错误
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ScheduleError implements BusinessError {
    NOT_SUPPORT_TYPE("S1000", "不支持的类型"),
    LABEL_HAD_EXIST("S2000", "该标签已存在");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     */
    ScheduleError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
