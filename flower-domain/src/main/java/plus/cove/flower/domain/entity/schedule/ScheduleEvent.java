package plus.cove.flower.domain.entity.schedule;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.flower.domain.principal.PrincipalPermit;
import plus.cove.infrastructure.component.BaseTimeEntity;

import java.time.LocalDateTime;

/**
 * 日程事件
 * <p>
 * 根据repeatType和repeatDate计算所得
 * 农历repeat换算为公历时间
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class ScheduleEvent extends BaseTimeEntity
        implements PrincipalPermit {
    /**
     * 事件名称
     */
    private String title;

    /**
     * 状态
     */
    private ScheduleEventState state;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    private LocalDateTime finishTime;

    /**
     * 结束备注
     */
    private String remark;

    /**
     * 完成时间
     */
    private LocalDateTime completeTime;

    /**
     * 作者
     */
    private Long userId;

    /**
     * 日程id
     */
    private Long scheduleId;

    /**
     * 相关日程
     */
    private Schedule schedule;

    /**
     * 完成
     */
    public void complete() {
        complete(100);
    }

    /**
     * 完成率
     */
    public void complete(Integer rate) {
        this.completeTime = LocalDateTime.now();

        // 完成率100，则修改状态
        if (rate != null && rate >= 100) {
            this.state = ScheduleEventState.FINISHED;
        }
    }

    /**
     * 计划
     */
    public void plan(LocalDateTime start, LocalDateTime finish) {
        this.startTime = start;
        this.finishTime = finish;
    }

    /**
     * 初始化
     */
    public static ScheduleEvent init(Long eventId) {
        ScheduleEvent entity = new ScheduleEvent();
        entity.setId(eventId);
        return entity;
    }

    /**
     * 创建
     */
    public static ScheduleEvent create(Long scheduleId) {
        ScheduleEvent entity = new ScheduleEvent();
        entity.valueOf();
        entity.state = ScheduleEventState.RUNNING;
        entity.scheduleId = scheduleId;
        return entity;
    }

    /**
     * 创建
     */
    public static ScheduleEvent create(Long scheduleId, String title, String remark, Long userId) {
        ScheduleEvent entity = create(scheduleId);
        entity.title = title;
        entity.remark = remark;
        entity.userId = userId;
        return entity;
    }

    @Override
    public Long getPermitUser() {
        return this.userId;
    }
}
