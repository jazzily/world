package plus.cove.flower.domain.entity.schedule;

import plus.cove.infrastructure.component.BaseEnum;

/**
 * 日程时间状态
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ScheduleEventState implements BaseEnum {
    RUNNING(10, "执行中"),
    FINISHED(20, "已完成"),
    DESERTED(30, "未完成");

    /**
     * 枚举值
     */
    private final int value;

    /**
     * 枚举描述
     */
    private final String description;

    /**
     * 构造函数
     */
    ScheduleEventState(final int value, final String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
