package plus.cove.flower.domain.entity.schedule;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseTimeEntity;

/**
 * 日程标签
 * <p>
 * <p>
 * 一个日程支持一个标签（暂）
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class ScheduleLabel extends BaseTimeEntity {
    /**
     * 标签id
     */
    private Long labelId;

    /**
     * 标签名称
     */
    private String labelName;

    /**
     * 日程id
     */
    private Long scheduleId;

    public static ScheduleLabel init(Long id) {
        ScheduleLabel entity = new ScheduleLabel();
        entity.setId(id);
        return entity;
    }

    public static ScheduleLabel from(Long scheduleId, Long labelId) {
        ScheduleLabel entity = new ScheduleLabel();
        entity.scheduleId = scheduleId;
        entity.labelId = labelId;
        return entity;
    }
}
