package plus.cove.flower.domain.entity.schedule;

import plus.cove.infrastructure.component.BaseEnum;

/**
 * 日程重复类型
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ScheduleRepeatType implements BaseEnum {
    ONE_TIME(10, "一次"),
    EVERY_DAY(20, "每天"),
    EVERY_WEEK(30, "每周"),
    EVERY_MONTH(40, "每月"),
    EVERY_YEAR(50, "每年");


    /**
     * 枚举值
     */
    private final int value;

    /**
     * 枚举描述
     */
    private final String description;

    /**
     * 构造函数
     */
    ScheduleRepeatType(final int value, final String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
