package plus.cove.flower.domain.entity.schedule;

import plus.cove.infrastructure.component.BaseEnum;

/**
 * 日程执行状态
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ScheduleState implements BaseEnum {
    PLANNING(10, "计划中"),
    DELAYED(11, "延迟的"),
    FINISHED(20, "已完成"),
    DESERTED(30, "未完成的");

    /**
     * 枚举值
     */
    private final int value;

    /**
     * 枚举描述
     */
    private final String description;

    /**
     * 构造函数
     */
    ScheduleState(final int value, final String description) {
        this.value = value;
        this.description = description;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
