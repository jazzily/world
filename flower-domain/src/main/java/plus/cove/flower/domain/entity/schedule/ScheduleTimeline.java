package plus.cove.flower.domain.entity.schedule;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plus.cove.infrastructure.component.BaseEntity;

import java.time.LocalDateTime;

/**
 * 日程时间线
 * <p>
 * 日程生成和完成
 * 事件完成
 * <p>
 * 删除后相应的数据同步删除
 * 完成后记录，修改未完成后，同步删除
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class ScheduleTimeline extends BaseEntity {
    /**
     * 标题
     */
    private String title;

    /**
     * 阶段
     * start    创建
     * finish   完成
     */
    private String phase;

    /**
     * 类型
     * schedule         日程创建，完成
     * schedule_event   日程事件完成
     * annual_report    年度报告，完成N项任务，N项事件
     */
    private String category;

    /**
     * 相关目标
     */
    private Long targetId;

    /**
     * 日程编码
     */
    private Long scheduleId;

    /**
     * 日程名称
     */
    private String scheduleTitle;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    public ScheduleTimeline start(){
        this.phase = "start";
        return this;
    }

    public ScheduleTimeline finish(){
        this.phase = "finish";
        return this;
    }

    public static ScheduleTimeline schedule(Long scheduleId) {
        ScheduleTimeline entity = new ScheduleTimeline();
        entity.scheduleId = scheduleId;
        return entity;
    }

    public static ScheduleTimeline event(Long eventId) {
        ScheduleTimeline entity = new ScheduleTimeline();
        entity.targetId = eventId;
        return entity;
    }

    public static ScheduleTimeline schedule(Schedule model) {
        ScheduleTimeline entity = new ScheduleTimeline();
        entity.title = model.getTitle();
        entity.category = "schedule";
        entity.scheduleId = model.getId();
        entity.targetId = model.getId();
        entity.userId = model.getUserId();
        entity.createTime = LocalDateTime.now();
        return entity;
    }

    public static ScheduleTimeline event(ScheduleEvent model) {
        ScheduleTimeline entity = new ScheduleTimeline();
        entity.title = model.getTitle();
        entity.category = "schedule_event";
        entity.scheduleId = model.getScheduleId();
        entity.targetId = model.getId();
        entity.userId = model.getUserId();
        entity.createTime = LocalDateTime.now();
        return entity;
    }

    public static ScheduleTimeline report(Long userId, String year) {
        ScheduleTimeline entity = new ScheduleTimeline();
        entity.title = year;
        entity.category = "annual_report";
        entity.scheduleId = 0L;
        entity.targetId = 0L;
        entity.userId = userId;
        entity.createTime = LocalDateTime.now();
        return entity;
    }
}
