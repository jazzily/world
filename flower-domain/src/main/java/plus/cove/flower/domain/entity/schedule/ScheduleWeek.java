package plus.cove.flower.domain.entity.schedule;

import lombok.Getter;
import plus.cove.infrastructure.component.BaseEnum;

/**
 * 日程日期
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ScheduleWeek implements BaseEnum {
    MONDAY(1, "MON", "星期一"),
    TUESDAY(2, "TUE", "星期二"),
    WEDNESDAY(3, "WED", "星期三"),
    THURSDAY(4, "THU", "星期四"),
    FRIDAY(5, "FRI", "星期五"),
    SATURDAY(6, "SAT", "星期六"),
    SUNDAY(7, "SUN", "星期日");

    /**
     * 枚举值
     */
    private final int value;

    /**
     * 简称
     */
    @Getter
    private final String title;

    /**
     * 枚举描述
     */
    private final String description;

    /**
     * 构造函数
     */
    ScheduleWeek(final int value, final String title, final String description) {
        this.value = value;
        this.title = title;
        this.description = description;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
