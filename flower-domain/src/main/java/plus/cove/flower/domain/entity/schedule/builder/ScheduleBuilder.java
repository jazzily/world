package plus.cove.flower.domain.entity.schedule.builder;

import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleRepeatType;
import plus.cove.infrastructure.helper.StringHelper;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 日程建造器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public abstract class ScheduleBuilder {
    protected static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy年MM月dd日");

    protected Long userId;
    protected String title;
    protected String place;
    protected String remark;

    protected ScheduleRepeatType repeatType;
    protected String repeatItem;

    protected LocalDateTime startTime;
    protected LocalDateTime finishTime;

    public static ScheduleBuilder init(ScheduleRepeatType repeatType, String repeatItem) {
        ScheduleBuilder builder = switch (repeatType) {
            case EVERY_DAY -> new ScheduleEveryDayBuilder();
            case EVERY_WEEK -> new ScheduleEveryWeekBuilder();
            case EVERY_MONTH -> new ScheduleEveryMonthBuilder();
            case EVERY_YEAR -> new ScheduleEveryYearBuilder();
            default -> new ScheduleOneTimeBuilder();
        };

        builder.repeatType = repeatType;
        builder.repeatItem = repeatItem;
        return builder;
    }

    public ScheduleBuilder title(String title) {
        this.title = title;
        return this;
    }

    public ScheduleBuilder place(String place) {
        this.place = place;
        return this;
    }

    public ScheduleBuilder remark(String remark) {
        this.remark = remark;
        return this;
    }

    public ScheduleBuilder userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public ScheduleBuilder plan(LocalDateTime startTime, LocalDateTime finishTime) {
        this.startTime = startTime;
        this.finishTime = finishTime;
        return this;
    }

    /**
     * 创建日程信息
     * <p>
     * 创建基本信息
     * 适用于所有日程
     */
    protected Schedule buildInfo() {
        Schedule entity = Schedule.create();
        entity.setTitle(this.title);
        entity.setPlace(this.place);
        entity.setRemark(this.remark);
        entity.setStartTime(this.startTime);
        entity.setFinishTime(this.finishTime);
        entity.setRepeatItem(this.repeatItem);
        entity.setUserId(this.userId);
        entity.setRepeatType(this.repeatType);

        String proStart = this.startTime.format(DATE_FORMATTER);
        String proFinish = this.finishTime.format(DATE_FORMATTER);
        String prompt = StringHelper.join("从", proStart, "到", proFinish, "的", entity.getRepeatType().getDescription(), "活动");
        entity.setRepeatPrompt(prompt);
        return entity;
    }

    /**
     * 计算天数
     */
    protected long buildDays(){
        Duration duration = Duration.between(this.startTime, this.finishTime);
        return duration.toDays();
    }

    /**
     * 创建日程事件
     */
    protected abstract void buildEvent(Schedule entity);

    /**
     * 创建日程
     */
    public Schedule build() {
        Schedule entity = this.buildInfo();
        this.buildEvent(entity);
        return entity;
    }
}
