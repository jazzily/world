package plus.cove.flower.domain.entity.schedule.builder;

import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 日程每天构造器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class ScheduleEveryDayBuilder extends ScheduleBuilder {
    @Override
    protected void buildEvent(Schedule entity) {
        // 各种时间
        LocalDate startDate = this.startTime.toLocalDate();
        LocalTime startTime = this.startTime.toLocalTime();
        LocalTime finishTime = this.finishTime.toLocalTime();

        // 按天生成活动
        long days = buildDays();
        List<ScheduleEvent> events = new ArrayList<>((int) days);
        for (long i = 0; i <= days; i++) {
            LocalDateTime start = LocalDateTime.of(startDate.plusDays(i), startTime);
            LocalDateTime finish = LocalDateTime.of(startDate.plusDays(i), finishTime);

            ScheduleEvent event = ScheduleEvent.create(entity.getId(), entity.getTitle(), entity.getRemark(), entity.getUserId());
            event.plan(start, finish);
            event.setUserId(entity.getUserId());
            events.add(event);
        }

        entity.setEvents(events);
    }
}
