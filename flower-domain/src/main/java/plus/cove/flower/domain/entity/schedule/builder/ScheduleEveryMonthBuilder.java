package plus.cove.flower.domain.entity.schedule.builder;

import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

/**
 * 日程每月构造器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class ScheduleEveryMonthBuilder extends ScheduleBuilder {
    @Override
    protected void buildEvent(Schedule entity) {
        // 各种时间
        LocalDate startDate = this.startTime.toLocalDate();
        LocalTime startTime = this.startTime.toLocalTime();
        LocalTime finishTime = this.finishTime.toLocalTime();

        // 按月生成活动
        long days = buildDays();
        List<ScheduleEvent> events = new ArrayList<>((int) days);
        for (long i = 0; i <= days; i++) {
            LocalDateTime start = LocalDateTime.of(startDate.plusDays(i), startTime);

            String month = String.format("%02d", start.getDayOfMonth());
            // 是否符合重复项
            // 第一种情况 repeatItem[01-31]包含本日
            boolean hasItem = entity.getRepeatItem().contains(month);

            // 第二种情况 本日是月末且repeatItem包含32（32表示月末）
            if (!hasItem) {
                hasItem = (start.with(lastDayOfMonth()).getDayOfMonth() == start.getDayOfMonth() &&
                        entity.getRepeatItem().contains("32"));
            }

            if (hasItem) {
                LocalDateTime finish = LocalDateTime.of(startDate.plusDays(i), finishTime);

                ScheduleEvent event = ScheduleEvent.create(entity.getId(), entity.getTitle(), entity.getRemark(), entity.getUserId());
                event.plan(start, finish);
                events.add(event);
            }
        }

        entity.setEvents(events);
    }
}
