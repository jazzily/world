package plus.cove.flower.domain.entity.schedule.builder;

import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 日程每年构造器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class ScheduleEveryYearBuilder extends ScheduleBuilder {
    @Override
    protected void buildEvent(Schedule entity) {
        // 各种时间
        LocalDate startDate = this.startTime.toLocalDate();
        LocalTime startTime = this.startTime.toLocalTime();
        LocalTime finishTime = this.finishTime.toLocalTime();

        // 按年生成活动
        long days = buildDays();
        List<ScheduleEvent> events = new ArrayList<>((int) days);
        for (long i = 0; i <= days; i++) {
            LocalDateTime start = LocalDateTime.of(startDate.plusDays(i), startTime);
            // 是否符合重复项
            // repeatItem 0101,1231: startTime yyyy-MM-dd
            String date = String.format("%02d%02d", start.getMonthValue(), start.getDayOfMonth());
            if (entity.getRepeatItem().contains(date)) {
                LocalDateTime finish = LocalDateTime.of(startDate.plusDays(i), finishTime);

                ScheduleEvent event = ScheduleEvent.create(entity.getId(), entity.getTitle(), entity.getRemark(), entity.getUserId());
                event.plan(start, finish);
                events.add(event);
            }
        }
        entity.setEvents(events);
    }
}
