package plus.cove.flower.domain.entity.schedule.builder;

import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * 日程一次建造器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class ScheduleOneTimeBuilder extends ScheduleBuilder {
    @Override
    protected void buildEvent(Schedule entity) {
        List<ScheduleEvent> events = new ArrayList<>(1);

        ScheduleEvent event = ScheduleEvent.create(entity.getId(), entity.getTitle(), entity.getRemark(), entity.getUserId());
        event.plan(this.startTime, this.finishTime);
        events.add(event);

        entity.setEvents(events);
    }
}
