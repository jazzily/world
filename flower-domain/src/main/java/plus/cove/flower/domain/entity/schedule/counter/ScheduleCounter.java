package plus.cove.flower.domain.entity.schedule.counter;

import lombok.Data;
import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEventState;

import java.util.List;

/**
 * 日程计算器
 * <p>
 * 用于计算完成率
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class ScheduleCounter {
    /**
     * 状态
     */
    private ScheduleEventState state;

    /**
     * 个数
     */
    private Integer count;

    /**
     * 计算
     */
    public static int calculate(List<ScheduleCounter> counters) {
        int total = 0;
        int close = 0;
        for (ScheduleCounter counter : counters) {
            total += counter.getCount();
            if (counter.getState() == ScheduleEventState.FINISHED) {
                close += counter.getCount();
            }
        }

        if (total == 0 || close == 0) {
            return 0;
        }
        return close * 100 / total;
    }

    /**
     * 计算
     */
    public static Schedule calculate(List<ScheduleCounter> counters, Long scheduleId) {
        Schedule entity = Schedule.init(scheduleId);

        int ratio = calculate(counters);
        entity.complete(ratio);
        return entity;
    }
}
