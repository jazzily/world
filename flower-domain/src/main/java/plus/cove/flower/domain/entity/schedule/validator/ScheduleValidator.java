package plus.cove.flower.domain.entity.schedule.validator;

import org.dromara.hutool.core.text.StrUtil;
import plus.cove.flower.domain.entity.schedule.ScheduleRepeatType;
import plus.cove.infrastructure.helper.AssertHelper;
import plus.cove.infrastructure.helper.StringHelper;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * 日程验证器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class ScheduleValidator {
    // 按周算，最多10年，即520
    private static final int MAX_TASK_COUNT = 52 * 10 + 1;

    // 按周范围[1-7]
    private static final String[] WEEK_RANGE = new String[]{"1", "2", "3", "4", "5", "6", "7", ","};

    // 按月范围[1-32] 32表示月末
    private static final String[] MONTH_RANGE = new String[]{
            "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
            "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
            "31", "32", ","};

    // 按年范围[0101-1231]
    private static final String[] YEAR_RANGE = new String[]{
            "0101", "0102", "0103", "0104", "0105", "0106", "0107", "0108", "0109", "0110", "0111", "0112", "0113", "0114", "0115", "0116", "0117", "0118", "0119", "0120", "0121", "0122", "0123", "0124", "0125", "0126", "0127", "0128", "0129", "0130", "0131",
            "0201", "0202", "0203", "0204", "0205", "0206", "0207", "0208", "0209", "0210", "0211", "0212", "0213", "0214", "0215", "0216", "0217", "0218", "0219", "0220", "0221", "0222", "0223", "0224", "0225", "0226", "0227", "0228", "0229",
            "0301", "0302", "0303", "0304", "0305", "0306", "0307", "0308", "0309", "0310", "0311", "0312", "0313", "0314", "0315", "0316", "0317", "0318", "0319", "0320", "0321", "0322", "0323", "0324", "0325", "0326", "0327", "0328", "0329", "0330", "0331",
            "0401", "0402", "0403", "0404", "0405", "0406", "0407", "0408", "0409", "0410", "0411", "0412", "0413", "0414", "0415", "0416", "0417", "0418", "0419", "0420", "0421", "0422", "0423", "0424", "0425", "0426", "0427", "0428", "0429", "0430",
            "0501", "0502", "0503", "0504", "0505", "0506", "0507", "0508", "0509", "0510", "0511", "0512", "0513", "0514", "0515", "0516", "0517", "0518", "0519", "0520", "0521", "0522", "0523", "0524", "0525", "0526", "0527", "0528", "0529", "0530", "0531",
            "0601", "0602", "0603", "0604", "0605", "0606", "0607", "0608", "0609", "0610", "0611", "0612", "0613", "0614", "0615", "0616", "0617", "0618", "0619", "0620", "0621", "0622", "0623", "0624", "0625", "0626", "0627", "0628", "0629", "0630",
            "0701", "0702", "0703", "0704", "0705", "0706", "0707", "0708", "0709", "0710", "0711", "0712", "0713", "0714", "0715", "0716", "0717", "0718", "0719", "0720", "0721", "0722", "0723", "0724", "0725", "0726", "0727", "0728", "0729", "0730", "0731",
            "0801", "0802", "0803", "0804", "0805", "0806", "0807", "0808", "0809", "0810", "0811", "0812", "0813", "0814", "0815", "0816", "0817", "0818", "0819", "0820", "0821", "0822", "0823", "0824", "0825", "0826", "0827", "0828", "0829", "0830", "0831",
            "0901", "0902", "0903", "0904", "0905", "0906", "0907", "0908", "0909", "0910", "0911", "0912", "0913", "0914", "0915", "0916", "0917", "0918", "0919", "0920", "0921", "0922", "0923", "0924", "0925", "0926", "0927", "0928", "0929", "0930",
            "1001", "1002", "1003", "1004", "1005", "1006", "1007", "1008", "1009", "1010", "1011", "1012", "1013", "1014", "1015", "1016", "1017", "1018", "1019", "1020", "1021", "1022", "1023", "1024", "1025", "1026", "1027", "1028", "1029", "1030", "1031",
            "1101", "1102", "1103", "1104", "1105", "1106", "1107", "1108", "1109", "1110", "1111", "1112", "1113", "1114", "1115", "1116", "1117", "1118", "1119", "1120", "1121", "1122", "1123", "1124", "1125", "1126", "1127", "1128", "1129", "1130",
            "1201", "1202", "1203", "1204", "1205", "1206", "1207", "1208", "1209", "1210", "1211", "1212", "1213", "1214", "1215", "1216", "1217", "1218", "1219", "1220", "1221", "1222", "1223", "1224", "1225", "1226", "1227", "1228", "1229", "1230", "1231",
            ","};

    /**
     * 重复类型
     */
    private ScheduleRepeatType repeatType;

    /**
     * 重复项目
     * <p>
     * 重复类型为按周，按月，按年则必填
     * 按周使用周几：[1-7]；按月使用日，32代表月末：[01-32]；按年使用月日：[0101-1231]
     * 多项以,分割
     */
    private String repeatItem;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    private LocalDateTime finishTime;

    /**
     * 个性验证
     */
    public long valid() {
        // 日程时间校验
        AssertHelper.assertFalse(this.finishTime.isBefore(this.startTime), "结束时间必须大于开始时间");

        // 重复项目-空检验
        int repeatCount = 0;
        if (this.repeatType == ScheduleRepeatType.EVERY_WEEK ||
                this.repeatType == ScheduleRepeatType.EVERY_MONTH ||
                this.repeatType == ScheduleRepeatType.EVERY_YEAR) {
            AssertHelper.assertNotEmpty(this.repeatItem, "重复项目不能为空");
            // 计算重复次数备用
            repeatCount = StrUtil.count(this.repeatItem, ',');
        }

        // 重复项目校验
        // 日程个数校验-总条数不得大约1000条
        boolean repeat = true;
        long days = Duration.between(this.startTime, this.finishTime).toDays();
        switch (this.repeatType) {
            case EVERY_WEEK:
                repeat = StringHelper.containsAny(this.repeatItem, WEEK_RANGE);
                days = days * repeatCount / 7;
                break;
            case EVERY_MONTH:
                repeat = StringHelper.containsAny(this.repeatItem, MONTH_RANGE);
                days = days * repeatCount / 31;
                break;
            case EVERY_YEAR:
                repeat = StringHelper.containsAny(this.repeatItem, YEAR_RANGE);
                days = days * repeatCount / 365;
                break;
            default:
                break;
        }

        AssertHelper.assertTrue(repeat, "无效的重复项目");
        AssertHelper.assertTrue(days < MAX_TASK_COUNT, "超过日程最大支持数");
        return days;
    }

    /**
     * 生成日程验证器
     */
    public static ScheduleValidator of(LocalDateTime startTime, LocalDateTime finishTime,
                                       ScheduleRepeatType repeatType, String repeatItem) {
        ScheduleValidator model = new ScheduleValidator();
        model.startTime = startTime;
        model.finishTime = finishTime;
        model.repeatType = repeatType;

        // 重复项
        model.repeatItem = StringHelper.trim(repeatItem, ",").concat(",");
        return model;
    }
}
