package plus.cove.flower.domain.principal;

import plus.cove.infrastructure.exception.BusinessError;

/**
 * 作者错误
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum PrincipalError implements BusinessError {
    PRINCIPAL_NO_EXIST("1000", "账号不存在"),
    PRINCIPAL_NO_PERMIT("1100", "账号无权限");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     */
    PrincipalError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
