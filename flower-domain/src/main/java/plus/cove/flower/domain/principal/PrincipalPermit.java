package plus.cove.flower.domain.principal;

/**
 * 用户权限
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface PrincipalPermit {
    /**
     * 用户id
     */
    Long getPermitUser();
}
