package plus.cove.flower.domain.principal;


import lombok.Data;

/**
 * 用户凭证
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Data
public class PrincipalUser {
    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户姓名
     */
    private String userName;

    public static PrincipalUser from(Long userId, String userName) {
        PrincipalUser user = new PrincipalUser();
        user.setUserId(userId);
        user.setUserName(userName);
        return user;
    }
}
