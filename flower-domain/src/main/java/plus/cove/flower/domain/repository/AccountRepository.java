package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.account.Account;

import jakarta.enterprise.context.ApplicationScoped;


/**
 * 账号仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface AccountRepository {
    /**
     * 获取账号
     *
     * @param name 用户名
     * @return 用户名对应的账号，没有记录返回 null
     */
    Account selectByName(String name);

    /**
     * 获取账号
     */
    Account selectByUser(Long userId);

    /**
     * 插入账号
     *
     * @param entity 实体
     */
    void insertOne(Account entity);

    /**
     * 更新状态
     *
     * @param entity 实体
     */
    void updateState(Account entity);

    /**
     * 更新
     */
    void updateOne(Account entity);

    /**
     * 根据用户更新
     */
    void updateUser(Account entity);
}
