package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.author.Author;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * 作者仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface AuthorRepository {
    /**
     * 获取作者
     *
     * @param id id
     */
    Author selectById(Long id);

    /**
     * 插入作者
     *
     * @param entity 实体
     */
    void insertOne(Author entity);

    /**
     * 更新作者
     */
    void updateOne(Author entity);
}
