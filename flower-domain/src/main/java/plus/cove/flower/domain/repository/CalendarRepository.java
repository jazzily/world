package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.global.CalendarData;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;

/**
 * 日历数据仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface CalendarRepository {
    /**
     * 按日期获取
     *
     */
    List<CalendarData> selectByDate(String date);
}
