package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.global.District;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;

/**
 * 账号仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface DistrictRepository {
    /**
     * 获取列表
     *
     * @param entity 实体
     */
    List<District> selectList(District entity);
}

