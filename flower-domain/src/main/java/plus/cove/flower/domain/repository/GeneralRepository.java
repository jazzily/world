package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.global.GeneralData;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * 通用数据仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface GeneralRepository {
    /**
     * 按编码获取
     *
     */
    GeneralData selectByCode(String code);

    /**
     * 插入一条
     *
     */
    void insertOne(GeneralData entity);
}
