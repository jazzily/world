package plus.cove.flower.domain.repository;

import jakarta.enterprise.context.ApplicationScoped;
import plus.cove.flower.domain.entity.issue.Issue;

/**
 * 需求仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface IssueRepository {
    /**
     * 插入
     */
    void insertOne(Issue entity);
}
