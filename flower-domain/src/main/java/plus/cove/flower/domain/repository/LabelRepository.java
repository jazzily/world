package plus.cove.flower.domain.repository;

import jakarta.enterprise.context.ApplicationScoped;
import plus.cove.flower.domain.entity.label.Label;

import java.util.List;

/**
 * 标签仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface LabelRepository {
    /**
     * 根据id查找
     */
    Label selectById(Long id);

    /**
     * 按用户查找
     */
    List<Label> selectByUser(Long user);

    /**
     * 按名称查找
     */
    Label selectOne(Label entity);

    /**
     * 插入
     */
    void insertOne(Label entity);

    /**
     * 更新
     */
    void updateOne(Label entity);

    /**
     * 删除
     */
    void deleteOne(Label entity);

    /**
     * 增加使用数
     */
    void increaseOne(Long labelId);

    /**
     * 减少使用数
     */
    void decreaseOne(Long labelId);
}
