package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.global.Operation;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * 操作仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface OperationRepository {
    /**
     * 插入一条
     */
    void insertOne(Operation entity);

    /**
     * 清空对应实体
     */
    void updateOne(Operation entity);

    /**
     * 获取条数
     */
    Integer selectCount(Operation entity);
}
