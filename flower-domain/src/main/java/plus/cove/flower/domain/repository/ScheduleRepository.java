package plus.cove.flower.domain.repository;

import jakarta.enterprise.context.ApplicationScoped;
import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;
import plus.cove.flower.domain.entity.schedule.ScheduleLabel;
import plus.cove.flower.domain.entity.schedule.ScheduleTimeline;
import plus.cove.flower.domain.entity.schedule.counter.ScheduleCounter;
import plus.cove.flower.domain.repository.schedule.*;

import java.util.List;


/**
 * 日程仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface ScheduleRepository {
    /**
     * 插入日程
     *
     * @param entity 实体
     */
    void insertSchedule(Schedule entity);

    /**
     * 更新日程
     * 按id更新
     */
    void updateSchedule(Schedule entity);

    /**
     * 删除日程
     * 按id删除
     */
    void deleteSchedule(Long scheduleId);

    /**
     * 插入事件
     */
    void insertEvent(Schedule entity);

    /**
     * 更新事件
     *
     * @param event 事件
     */
    void updateEvent(ScheduleEvent event);

    /**
     * 更新
     * 按日程更新事件
     */
    void updateEventBySchedule(ScheduleEvent event);

    /**
     * 删除事件
     */
    void deleteEvent(Long eventId);

    /**
     * 更新标签
     */
    void insertLabel(ScheduleLabel entity);

    /**
     * 删除标签
     */
    void deleteLabel(ScheduleLabel entity);

    /**
     * 获取一个标签
     */
    ScheduleLabel selectOneLabel(ScheduleLabel entity);

    /**
     * 更新标签名称
     */
    void updateLabelName(ScheduleLabel entity);

    /**
     * 获取列表
     */
    List<ScheduleListExport> selectListSchedule(ScheduleListImport model);

    /**
     * 获取日程
     */
    Schedule selectOneSchedule(Long scheduleId);

    /**
     * 获取日程标签
     */
    List<ScheduleLabel> selectScheduleLabel(Long scheduleId);

    /**
     * 获取日程事件
     */
    List<ScheduleEvent> selectScheduleEvent(Long scheduleId);

    /**
     * 获取事件
     */
    List<EventListExport> selectListEvent(EventListImport model);

    /**
     * 获取事件
     */
    ScheduleEvent selectOneEvent(Long eventId);

    /**
     * 获取计算器
     */
    List<ScheduleCounter> selectCounter(Long scheduleId);

    /**
     * 按年获取时间线
     */
    List<ScheduleTimeline> selectTimeline(TimelineListImport model);

    /**
     * 插入时间线
     */
    void insertTimeline(ScheduleTimeline entity);

    /**
     * 删除时间线
     */
    void deleteTimeline(ScheduleTimeline entity);
}
