package plus.cove.flower.domain.repository;

import plus.cove.flower.domain.entity.global.Validation;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * 验证码仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public interface ValidationRepository {
    /**
     * 插入一条
     *
     * @param entity 实体
     */
    void insertOne(Validation entity);

    /**
     * 获取一条
     *
     * @param target   目标
     * @param category 类别
     * @return 对应验证码
     */
    Validation selectOne(String target, String category);
}
