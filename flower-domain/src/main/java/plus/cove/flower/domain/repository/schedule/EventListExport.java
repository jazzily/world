package plus.cove.flower.domain.repository.schedule;

import lombok.Data;
import plus.cove.flower.domain.entity.schedule.ScheduleEventState;

import java.time.LocalDateTime;

@Data
public class EventListExport {
    private Long id;
    private String title;
    private ScheduleEventState state;
    private LocalDateTime startTime;
    private LocalDateTime finishTime;
    private Long scheduleId;
    private String scheduleTitle;
}
