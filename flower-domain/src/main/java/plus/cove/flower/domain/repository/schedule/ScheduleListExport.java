package plus.cove.flower.domain.repository.schedule;

import lombok.Data;
import plus.cove.flower.domain.entity.schedule.ScheduleRepeatType;
import plus.cove.flower.domain.entity.schedule.ScheduleState;

import java.time.LocalDateTime;

@Data
public class ScheduleListExport {
    private Long id;
    private ScheduleState state;
    private String title;
    private ScheduleRepeatType repeatType;
    private LocalDateTime finishTime;
    private Integer completeRate;
    private Byte importance;

    // 标签
    private String labels;
}
