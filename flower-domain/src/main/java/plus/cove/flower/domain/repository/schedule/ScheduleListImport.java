package plus.cove.flower.domain.repository.schedule;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class ScheduleListImport {
    private LocalDate start;
    private LocalDate finish;

    private List<String> labels;
    private List<Integer> states;

    private String sortBy;
    private Long userId;
}
