package plus.cove.flower.domain.repository.schedule;

import lombok.Data;

import java.util.List;

@Data
public class TimelineListImport {
    /**
     * 所属年份
     */
    private String year;

    /**
     * 类别
     */
    private List<String> category;

    private Long userId;
}
