package plus.cove.flower.repository;

import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.account.Account;
import plus.cove.flower.domain.repository.AccountRepository;
import plus.cove.flower.repository.mapper.AccountMapper;

import java.time.LocalDateTime;

@ApplicationScoped
public class AccountRepositoryImpl implements AccountRepository {
    private static final Logger LOG = Logger.getLogger(AccountRepositoryImpl.class);

    private final AccountMapper accountMapper;

    public AccountRepositoryImpl(final AccountMapper accountMapper) {
        this.accountMapper = accountMapper;
    }

    @Override
    public Account selectByName(String name) {
        return accountMapper.selectByName(name);
    }

    @Override
    public Account selectByUser(Long userId) {
        return accountMapper.selectByUser(userId);
    }

    @Override
    public void insertOne(Account account) {
        accountMapper.insert(account);
    }

    @Override
    public void updateState(Account entity) {
        Account model = new Account();
        model.setId(entity.getId());
        model.setState(entity.getState());
        model.setExpiredTime(entity.getExpiredTime());
        model.setUpdateTime(LocalDateTime.now());

        accountMapper.updateById(model);
    }

    @Override
    public void updateOne(Account entity) {
        accountMapper.updateById(entity);
    }

    @Override
    public void updateUser(Account entity){
        accountMapper.updateByUser(entity);
    }
}
