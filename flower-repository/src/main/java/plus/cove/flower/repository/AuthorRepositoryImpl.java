package plus.cove.flower.repository;

import org.jboss.logging.Logger;

import plus.cove.flower.domain.entity.author.Author;
import plus.cove.flower.domain.repository.AuthorRepository;
import plus.cove.flower.repository.mapper.AuthorMapper;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AuthorRepositoryImpl implements AuthorRepository {
    private static final Logger LOG = Logger.getLogger(AuthorRepositoryImpl.class);

    private final AuthorMapper authorMapper;

    public AuthorRepositoryImpl(final AuthorMapper authorMapper) {
        this.authorMapper = authorMapper;
    }

    @Override
    public Author selectById(Long id) {
        return this.authorMapper.selectById(id);
    }

    @Override
    public void insertOne(Author entity) {
        this.authorMapper.insert(entity);
    }

    @Override
    public void updateOne(Author entity) {
        this.authorMapper.updateById(entity);
    }
}
