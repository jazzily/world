package plus.cove.flower.repository;

import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.global.CalendarData;
import plus.cove.flower.domain.repository.CalendarRepository;
import plus.cove.flower.repository.mapper.CalendarMapper;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class CalendarRepositoryImpl implements CalendarRepository {
    private static final Logger LOG = Logger.getLogger(CalendarRepositoryImpl.class);

    private final CalendarMapper calendarMapper;

    public CalendarRepositoryImpl(final CalendarMapper calendarMapper) {
        this.calendarMapper = calendarMapper;
    }

    @Override
    public List<CalendarData> selectByDate(String date) {
        return calendarMapper.selectByDate(date);
    }
}
