package plus.cove.flower.repository;

import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.global.District;
import plus.cove.flower.domain.repository.DistrictRepository;
import plus.cove.flower.repository.mapper.DistrictMapper;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class DistrictRepositoryImpl implements DistrictRepository {
    private static final Logger LOG = Logger.getLogger(DistrictRepositoryImpl.class);

    private final DistrictMapper districtMapper;

    public DistrictRepositoryImpl(final DistrictMapper districtMapper) {
        this.districtMapper = districtMapper;
    }

    @Override
    public List<District> selectList(District entity) {
       return districtMapper.selectByEntity(entity);
    }
}
