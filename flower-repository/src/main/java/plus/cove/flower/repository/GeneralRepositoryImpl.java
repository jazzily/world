package plus.cove.flower.repository;

import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.global.GeneralData;
import plus.cove.flower.domain.repository.GeneralRepository;
import plus.cove.flower.repository.mapper.GeneralMapper;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GeneralRepositoryImpl implements GeneralRepository {
    private static final Logger LOG = Logger.getLogger(GeneralRepositoryImpl.class);

    private final GeneralMapper generalMapper;

    public GeneralRepositoryImpl(final GeneralMapper generalMapper) {
        this.generalMapper = generalMapper;
    }

    @Override
    public GeneralData selectByCode(String code){
        return generalMapper.selectByCode(code);
    }

    @Override
    public void insertOne(GeneralData entity) {
        generalMapper.insert(entity);
    }
}
