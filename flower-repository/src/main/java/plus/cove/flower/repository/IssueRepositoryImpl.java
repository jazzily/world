package plus.cove.flower.repository;

import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.issue.Issue;
import plus.cove.flower.domain.repository.IssueRepository;
import plus.cove.flower.repository.mapper.IssueMapper;

@ApplicationScoped
public class IssueRepositoryImpl implements IssueRepository {
    private static final Logger LOG = Logger.getLogger(IssueRepositoryImpl.class);

    private final IssueMapper issueMapper;

    public IssueRepositoryImpl(
            final IssueMapper issueMapper) {
        this.issueMapper = issueMapper;
    }

    @Override
    public void insertOne(Issue entity) {
        this.issueMapper.insert(entity);
    }
}
