package plus.cove.flower.repository;

import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.label.Label;
import plus.cove.flower.domain.repository.LabelRepository;
import plus.cove.flower.repository.mapper.LabelMapper;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class LabelRepositoryImpl implements LabelRepository {
    private static final Logger LOG = Logger.getLogger(LabelRepositoryImpl.class);

    private final LabelMapper labelMapper;

    public LabelRepositoryImpl(
            final LabelMapper labelMapper) {
        this.labelMapper = labelMapper;
    }

    @Override
    public Label selectById(Long id) {
        return this.labelMapper.selectById(id);
    }

    @Override
    public List<Label> selectByUser(Long userId) {
        return this.labelMapper.selectByUser(userId);
    }

    @Override
    public Label selectOne(Label entity) {
        return this.labelMapper.selectByEntity(entity);
    }

    @Override
    public void insertOne(Label entity) {
        this.labelMapper.insert(entity);
    }

    @Override
    public void updateOne(Label entity) {
        this.labelMapper.updateById(entity);
    }

    @Override
    public void deleteOne(Label entity) {
        this.labelMapper.deleteByEntity(entity);
    }

    @Override
    public void increaseOne(Long labelId) {
        this.labelMapper.increase(labelId);
    }

    @Override
    public void decreaseOne(Long labelId) {
        this.labelMapper.decrease(labelId);
    }
}
