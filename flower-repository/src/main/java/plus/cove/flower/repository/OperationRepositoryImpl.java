package plus.cove.flower.repository;

import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.global.Operation;
import plus.cove.flower.domain.repository.OperationRepository;
import plus.cove.flower.repository.mapper.OperationMapper;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class OperationRepositoryImpl implements OperationRepository {
    private static final Logger LOG = Logger.getLogger(OperationRepositoryImpl.class);

    private final OperationMapper operationMapper;

    public OperationRepositoryImpl(final OperationMapper operationMapper) {
        this.operationMapper = operationMapper;
    }

    @Override
    public void insertOne(Operation entity) {
        operationMapper.insert(entity);
    }

    @Override
    public void updateOne(Operation entity) {
        operationMapper.update(entity);
    }

    @Override
    public Integer selectCount(Operation entity) {
        return operationMapper.selectCount(entity);
    }
}
