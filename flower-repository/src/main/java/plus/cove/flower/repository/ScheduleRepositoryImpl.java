package plus.cove.flower.repository;

import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;
import plus.cove.flower.domain.entity.schedule.ScheduleLabel;
import plus.cove.flower.domain.entity.schedule.ScheduleTimeline;
import plus.cove.flower.domain.entity.schedule.counter.ScheduleCounter;
import plus.cove.flower.domain.repository.ScheduleRepository;
import plus.cove.flower.domain.repository.schedule.*;
import plus.cove.flower.repository.mapper.ScheduleEventMapper;
import plus.cove.flower.repository.mapper.ScheduleLabelMapper;
import plus.cove.flower.repository.mapper.ScheduleMapper;
import plus.cove.flower.repository.mapper.ScheduleTimelineMapper;

import java.time.LocalDateTime;
import java.util.List;

@ApplicationScoped
public class ScheduleRepositoryImpl implements ScheduleRepository {
    private static final Logger LOG = Logger.getLogger(ScheduleRepositoryImpl.class);

    private final ScheduleMapper scheduleMapper;
    private final ScheduleEventMapper scheduleEventMapper;
    private final ScheduleLabelMapper scheduleLabelMapper;
    private final ScheduleTimelineMapper scheduleTimelineMapper;

    public ScheduleRepositoryImpl(
            final ScheduleMapper scheduleMapper,
            final ScheduleEventMapper scheduleEventMapper,
            final ScheduleLabelMapper scheduleLabelMapper,
            final ScheduleTimelineMapper scheduleTimelineMapper
    ) {
        this.scheduleMapper = scheduleMapper;
        this.scheduleEventMapper = scheduleEventMapper;
        this.scheduleLabelMapper = scheduleLabelMapper;
        this.scheduleTimelineMapper = scheduleTimelineMapper;
    }

    @Override
    public void insertSchedule(Schedule entity) {
        scheduleMapper.insert(entity);
    }

    @Override
    public void updateSchedule(Schedule entity) {
        entity.setUpdateTime(LocalDateTime.now());
        scheduleMapper.updateById(entity);
    }

    @Override
    public void deleteSchedule(Long scheduleId) {
        scheduleMapper.deleteById(scheduleId);
        scheduleEventMapper.deleteBySchedule(scheduleId);
    }

    @Override
    public void insertEvent(Schedule entity) {
        this.scheduleEventMapper.insertList(entity.getEvents());
    }

    @Override
    public void updateEvent(ScheduleEvent event) {
        event.setUpdateTime(LocalDateTime.now());
        this.scheduleEventMapper.updateById(event);
    }

    @Override
    public void deleteEvent(Long eventId) {
        this.scheduleEventMapper.deleteById(eventId);
    }

    @Override
    public void updateEventBySchedule(ScheduleEvent event){
        event.setUpdateTime(LocalDateTime.now());
        this.scheduleEventMapper.updateBySchedule(event);
    }

    @Override
    public void insertLabel(ScheduleLabel entity) {
        this.scheduleLabelMapper.insert(entity);
    }

    @Override
    public void deleteLabel(ScheduleLabel entity) {
        this.scheduleLabelMapper.deleteByEntity(entity);
    }

    @Override
    public ScheduleLabel selectOneLabel(ScheduleLabel entity) {
        return this.scheduleLabelMapper.selectByEntity(entity);
    }

    @Override
    public void updateLabelName(ScheduleLabel entity) {
        this.scheduleLabelMapper.updateLabelName(entity);
    }

    @Override
    public List<ScheduleListExport> selectListSchedule(ScheduleListImport model) {
        return this.scheduleMapper.selectList(model);
    }

    @Override
    public Schedule selectOneSchedule(Long scheduleId) {
        return this.scheduleMapper.selectById(scheduleId);
    }

    @Override
    public List<ScheduleLabel> selectScheduleLabel(Long scheduleId) {
        return this.scheduleLabelMapper.selectBySchedule(scheduleId);
    }

    @Override
    public List<ScheduleEvent> selectScheduleEvent(Long scheduleId) {
        return this.scheduleEventMapper.selectBySchedule(scheduleId);
    }

    @Override
    public List<EventListExport> selectListEvent(EventListImport model) {
        return this.scheduleEventMapper.selectList(model);
    }

    @Override
    public ScheduleEvent selectOneEvent(Long eventId) {
        return this.scheduleEventMapper.selectById(eventId);
    }

    @Override
    public List<ScheduleCounter> selectCounter(Long scheduleId) {
        return this.scheduleMapper.selectCounter(scheduleId);
    }

    @Override
    public List<ScheduleTimeline> selectTimeline(TimelineListImport model){
        return scheduleTimelineMapper.selectList(model);
    }

    @Override
    public void insertTimeline(ScheduleTimeline entity){
        scheduleTimelineMapper.insert(entity);
    }

    @Override
    public void deleteTimeline(ScheduleTimeline entity){
        scheduleTimelineMapper.deleteByEntity(entity);
    }
}
