package plus.cove.flower.repository;

import org.jboss.logging.Logger;
import plus.cove.flower.domain.entity.global.Validation;
import plus.cove.flower.domain.repository.ValidationRepository;
import plus.cove.flower.repository.mapper.ValidationMapper;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ValidationRepositoryImpl implements ValidationRepository {
    private static final Logger LOG = Logger.getLogger(ValidationRepositoryImpl.class);

    private final ValidationMapper validationMapper;

    public ValidationRepositoryImpl(final ValidationMapper validationMapper) {
        this.validationMapper = validationMapper;
    }

    @Override
    public void insertOne(Validation entity) {
        validationMapper.insert(entity);
    }

    @Override
    public Validation selectOne(String target, String category) {
        return validationMapper.selectOne(target, category);
    }
}
