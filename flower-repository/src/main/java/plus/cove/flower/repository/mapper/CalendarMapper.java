package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import plus.cove.flower.domain.entity.global.CalendarData;

import java.util.List;

/**
 * 日历数据仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface CalendarMapper {
    List<CalendarData> selectByDate(String date);
}
