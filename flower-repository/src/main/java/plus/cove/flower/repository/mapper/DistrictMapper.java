package plus.cove.flower.repository.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.global.District;

import java.util.List;

/**
 * 区域仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface DistrictMapper {
    List<District> selectByEntity(@Param("et") District entity);
}
