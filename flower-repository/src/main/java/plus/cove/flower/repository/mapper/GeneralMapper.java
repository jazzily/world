package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.global.GeneralData;

/**
 * 通用数据仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface GeneralMapper {
    GeneralData selectByCode(String code);

    void insert(@Param("et") GeneralData entity);
}
