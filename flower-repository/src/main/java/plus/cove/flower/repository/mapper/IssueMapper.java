package plus.cove.flower.repository.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.issue.Issue;

/**
 * 需求仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface IssueMapper {
    void insert(@Param("et") Issue entity);
}
