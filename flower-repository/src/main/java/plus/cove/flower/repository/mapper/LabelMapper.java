package plus.cove.flower.repository.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.label.Label;

import java.util.List;

/**
 * 标签仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface LabelMapper {
    Label selectById(Long id);

    Label selectByEntity(@Param("et") Label entity);

    List<Label> selectByUser(Long userId);

    void insert(@Param("et") Label entity);

    void updateById(@Param("et") Label entity);

    void deleteByEntity(@Param("et") Label entity);

    void increase(Long id);

    void decrease(Long id);
}
