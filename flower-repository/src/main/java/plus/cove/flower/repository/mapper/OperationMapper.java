package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.global.Operation;

/**
 * 操作仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface OperationMapper {
    void insert(@Param("et") Operation entity);

    void update(@Param("et") Operation entity);

    Integer selectCount(@Param("et") Operation entity);
}
