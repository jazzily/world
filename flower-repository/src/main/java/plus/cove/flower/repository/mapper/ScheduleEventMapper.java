package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.schedule.ScheduleEvent;
import plus.cove.flower.domain.repository.schedule.EventListExport;
import plus.cove.flower.domain.repository.schedule.EventListImport;

import java.util.List;

/**
 * 日程活动仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface ScheduleEventMapper {
    void insertOne(@Param("et") ScheduleEvent event);

    void insertList(@Param("ets") List<ScheduleEvent> events);

    void updateById(@Param("et") ScheduleEvent event);

    void updateBySchedule(@Param("et") ScheduleEvent event);

    void deleteById(Long id);

    void deleteBySchedule(Long id);

    ScheduleEvent selectById(Long id);

    List<EventListExport> selectList(@Param("ip") EventListImport model);

    List<ScheduleEvent> selectBySchedule(Long scheduleId);
}
