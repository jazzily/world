package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.schedule.ScheduleLabel;

import java.util.List;

/**
 * 日程标签仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface ScheduleLabelMapper {
    void insert(@Param("et") ScheduleLabel entity);

    void deleteById(Long id);

    void deleteByEntity(@Param("et") ScheduleLabel entity);

    void updateLabelName(@Param("et") ScheduleLabel entity);

    ScheduleLabel selectByEntity(@Param("et") ScheduleLabel entity);

    List<ScheduleLabel> selectBySchedule(Long scheduleId);
}
