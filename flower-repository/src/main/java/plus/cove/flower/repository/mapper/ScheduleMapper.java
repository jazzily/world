package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.schedule.Schedule;
import plus.cove.flower.domain.entity.schedule.counter.ScheduleCounter;
import plus.cove.flower.domain.repository.schedule.ScheduleListExport;
import plus.cove.flower.domain.repository.schedule.ScheduleListImport;

import java.util.List;

/**
 * 日程仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface ScheduleMapper {
    void insert(@Param("et") Schedule entity);

    void updateById(@Param("et") Schedule entity);

    void deleteById(Long id);

    Schedule selectById(Long id);

    List<ScheduleListExport> selectList(@Param("ip") ScheduleListImport model);

    List<ScheduleCounter> selectCounter(Long id);
}
