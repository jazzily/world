package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.schedule.ScheduleTimeline;
import plus.cove.flower.domain.repository.schedule.TimelineListImport;

import java.util.List;

/**
 * 日程时间线仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface ScheduleTimelineMapper {
    List<ScheduleTimeline> selectList(@Param("et") TimelineListImport input);

    void insert(@Param("et") ScheduleTimeline entity);

    void deleteByEntity(@Param("et") ScheduleTimeline entity);
}
