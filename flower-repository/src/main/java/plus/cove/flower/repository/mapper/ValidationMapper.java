package plus.cove.flower.repository.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import plus.cove.flower.domain.entity.global.Validation;

/**
 * 验证仓储
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Mapper
public interface ValidationMapper {
    void insert(@Param("et") Validation entity);

    Validation selectOne(@Param("tar") String target, @Param("cat") String category);
}
