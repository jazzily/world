package plus.cove.infrastructure.component;

import lombok.Getter;
import plus.cove.infrastructure.exception.BusinessError;
import plus.cove.infrastructure.exception.BusinessException;

/**
 * 操作结果
 * <p>
 * 业务的返回，包括
 * 1 非正常输入
 * 2 业务逻辑错误
 * 3 其他系统捕获错误
 * <p>
 * 针对调用成功返回，但结果未知的情况，code=0，业务数据各自返回
 * <p>
 * 前台获取后，根据code进行判断是否成功
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Getter
public class ActionResult<T> {
    private static final String SUCCESS_CODE = "0";
    private static final String SUCCESS_MESSAGE = "ok";

    private String code;

    private String message;

    private T data;

    /**
     * 默认构造函数
     */
    private ActionResult() {
    }

    /**
     * 全参构造函数
     *
     * @param code    编码
     * @param message 提示
     */
    private ActionResult(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 默认的静态函数
     */
    public static ActionResult result() {
        return new ActionResult<>();
    }

    /**
     * 成功的静态方法
     */
    public static ActionResult success() {
        return new ActionResult<>(SUCCESS_CODE, SUCCESS_MESSAGE);
    }

    /**
     * 失败的静态方法
     */
    public static ActionResult failure(BusinessError error) {
        return new ActionResult<>(error.code(), error.message());
    }

    /**
     * 成功
     *
     * @param data 成功返回数据
     */
    public ActionResult<T> succeed(T data) {
        this.code = SUCCESS_CODE;
        this.message = SUCCESS_MESSAGE;
        this.data = data;
        return this;
    }

    /**
     * 失败
     *
     * @param error 错误枚举
     */
    public ActionResult<?> fail(BusinessError error) {
        this.code = error.code();
        this.message = error.message();
        return this;
    }

    /**
     * 失败
     *
     * @param except 异常
     */
    public ActionResult<?> fail(BusinessException except) {
        this.code = except.getCode();
        this.message = except.getMessage();
        return this;
    }

    /**
     * 失败
     *
     * @param error 错误枚举
     * @param data  错误数据
     */
    public ActionResult<T> fail(BusinessError error, T data) {
        this.fail(error);
        this.data = data;
        return this;
    }

    /**
     * 消息
     */
    public ActionResult message(String message) {
        this.message = message;
        return this;
    }

    /**
     * 输出简单json格式
     */
    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{");
        json.append("\"code\":");
        json.append(code);
        json.append(",\"message\":");

        if (message == null) {
            json.append("null");
        } else {
            json.append("\"");
            json.append(message);
            json.append("\"");
        }
        json.append("}");
        return json.toString();
    }

    @Override
    public String toString() {
        return this.toJson();
    }

    /**
     * 是成功
     */
    public boolean beSuccess() {
        return this.code.equals(SUCCESS_CODE);
    }

    /**
     * 否成功
     *
     */
    public boolean noSuccess() {
        return !beSuccess();
    }
}
