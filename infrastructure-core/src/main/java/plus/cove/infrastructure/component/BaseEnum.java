package plus.cove.infrastructure.component;

/**
 * 通用枚举
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public interface BaseEnum {
    /**
     * 获取枚举值
     */
    Integer getValue();

    /**
     * 获取描述
     */
    String getDescription();
}
