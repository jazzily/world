package plus.cove.infrastructure.component;

import lombok.Getter;

import java.time.LocalDateTime;

/**
 * 实体基类
 * <p>
 * 领域类，表示业务对象
 * 1. 提供基本属性
 * 2. 提供基础方法，valueOf用于生成id等基本属性
 * 3. 可进行扩展，带创建属性，带更新属性
 * init表示初始化一个实例，使用系统默认值
 * create表示创建一个新实例，使用实体默认值
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Getter
public abstract class BaseTimeEntity extends BaseEntity {
    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 初始化实体
     * <p>
     * 统一管理id生成
     * 方便更换id生成器
     *
     * @since 1.0
     */
    protected void valueOf() {
        super.valueOf();

        LocalDateTime now = LocalDateTime.now();
        this.createTime = now;
        this.updateTime = now;
    }
}
