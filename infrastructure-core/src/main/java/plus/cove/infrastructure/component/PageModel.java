package plus.cove.infrastructure.component;

import lombok.Getter;

/**
 * 分页信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Getter
public class PageModel {
    public static final PageModel DEFAULT = PageModel.of(1, 10);

    /**
     * 当前页码
     */
    private int page;

    /**
     * 页大小
     */
    private int size;

    private PageModel() {
    }

    public static PageModel of(Integer page, Integer size) {
        PageModel model = new PageModel();
        model.page = (page == null || page < 1 ? 1 : page);
        model.size = (size == null || size < 1 ? 1 : size);
        return model;
    }
}
