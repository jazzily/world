package plus.cove.infrastructure.component;

import java.util.List;

/**
 * 分页信息
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class PageResult<E> {
    /**
     * 当前页码
     */
    private int page;

    /**
     * 页大小
     */
    private int size;

    /**
     * 总页数
     */
    private int pages;

    /**
     * 总记录数
     */
    private int total;

    /**
     * 数据
     */
    private List<E> rows;

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    public int getPages() {
        return pages;
    }

    public int getTotal() {
        return total;
    }

    public List<E> getRows() {
        return rows;
    }

    /**
     * 从PageModel和Rows组装PageResult
     *
     */
    public static <E> PageResult<E> from(int page, int size, int total, List<E> rows) {
        PageResult<E> result = new PageResult<>();
        result.page = page;
        result.size = size;
        result.total = total;
        result.rows = rows;

        if (size > 0) {
            int more = (total % size);
            int pages = (total / size);
            result.pages = pages + (more == 0 ? 0 : 1);
        } else {
            result.pages = 1;
        }
        return result;
    }

    public static <E> PageResult<E> from(PageModel page, Integer total, List<E> rows) {
        return from(page.getPage(), page.getSize(), total, rows);
    }
}
