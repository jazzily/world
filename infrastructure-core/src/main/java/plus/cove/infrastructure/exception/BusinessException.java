package plus.cove.infrastructure.exception;

import lombok.Getter;

/**
 * 业务异常
 * <p>
 * 所有业务异常的基类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Getter
public class BusinessException extends RuntimeException {
    /**
     * 编码，唯一编码
     */
    private final String code;

    /**
     * 信息，错误信息
     */
    private final String message;

    public BusinessException(BusinessError error) {
        super(error.message(), null, false, false);
        this.code = error.code();
        this.message = error.message();
    }

    public BusinessException(BusinessError error, Throwable cause) {
        super(error.message(), cause, false, false);
        this.code = error.code();
        this.message = error.message();
    }

    public BusinessException(BusinessError error, String message) {
        super(message, null, false, false);
        this.code = error.code();
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return String.format("%s: code=%s, message=%s",
                this.getClass().getName(),
                this.code,
                this.message == null ? "null" : this.message);
    }

    /**
     * 从错误获取异常信息
     */
    public static BusinessException from(BusinessError error) {
        return new BusinessException(error);
    }
}
