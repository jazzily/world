package plus.cove.infrastructure.exception;

/**
 * 服务错误
 * 20-29
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ServiceError implements BusinessError {
    // 无效请求，用户请求操作，此错误可预知
    BAD_REQUEST("400", "无效请求"),

    // 无效认证，比如：认证失败，过期等，前台返回登录
    BAD_AUTHENTICATION("401", "无效认证"),
    // 无效授权，比如：用户非法操作，前台提示错误
    BAD_AUTHORIZATION("403", "无效授权"),
    // 无效版本，请求版本和接口版本不一致，前台强制刷新
    NOT_VERSION("405", "无效版本"),

    // 其他未知错误，不可预知
    SERVER_ERROR("500", "内部错误");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * 构造函数
     */
    ServiceError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}

