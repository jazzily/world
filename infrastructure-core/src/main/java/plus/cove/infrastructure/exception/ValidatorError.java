package plus.cove.infrastructure.exception;

/**
 * 验证错误
 * code 范围：100-199
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum ValidatorError implements BusinessError {
    INVALID_FORMATTING("100", "无效的格式"),
    INVALID_ARGUMENT("101", "无效的参数"),

    JWT_CREATION_EXCEPTION("110", "生成TOKEN异常"),
    JWT_DECODE_EXCEPTION("111", "解码TOKEN异常"),
    JWT_VERIFY_EXCEPTION("112", "验证TOKEN异常"),
    JWT_INVALID_SECRET("113", "无效的TOKEN秘钥"),

    CONVERTER_INVALID_ORIGIN("120", "无效的来源数据"),
    CONVERTER_INVALID_TARGET("121", "无效的目标数据"),
    CONVERTER_INVALID_TYPE("122", "无效的类型类别"),

    IDEMPOTENT_TOKEN_MISSING("130", "无效的提交参数"),
    IDEMPOTENT_TOKEN_DUPLICATED("131", "重复提交数据"),

    JSON_SERIALIZER_FAILURE("140", "JSON序列化失败"),
    JSON_DESERIALIZER_FAILURE("141", "JSON发序列化失败"),
    JSON_PATH_FAILURE("142", "JSON读取路径失败"),
    JSON_NONSUPPORT_TYPE("143", "不支持的类型");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    ValidatorError(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}

