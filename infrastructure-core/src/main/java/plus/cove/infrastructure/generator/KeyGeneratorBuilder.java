package plus.cove.infrastructure.generator;

/**
 * 主键构造器
 *
 * <p>
 * 用来生成主键，主键保持递增（非连续）
 * 使用方法：KeyGeneratorBuilder.INSTANCE.build()
 * <p>
 * 多机器可以通过在环境变量通过
 * summer.generator.work-id设置work id
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class KeyGeneratorBuilder {
    private static final String PROP_WORK_ID = "summer.generator.worker-id";
    private final KeyGenerator dailyGen;
    private final KeyGenerator uniqueGen;

    /**
     * 单例模式
     */
    public static final KeyGeneratorBuilder INSTANCE = new KeyGeneratorBuilder();

    private KeyGeneratorBuilder() {
        // 获取环境变量
        String prop = System.getProperty(PROP_WORK_ID);
        int workId = (prop == null || prop.isEmpty()) ? 0 : Integer.parseInt(prop);

        this.uniqueGen = new SnowflakeKeyGenerator(workId);
        this.dailyGen = new DailyNumberGenerator(workId);
    }

    /**
     * 创建全局key
     */
    public long buildUniqueKey() {
        return this.uniqueGen.generateKey();
    }

    /**
     * 创建每日key
     */
    public long buildDailyKey() {
        return this.dailyGen.generateKey();
    }
}
