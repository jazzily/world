package plus.cove.infrastructure.helper;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;

/**
 * 集合帮助类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class CollectionHelper {
    private CollectionHelper() {
    }

    /**
     * 是否空
     */
    public static boolean isEmpty(Collection<?> source) {
        return source == null || source.isEmpty();
    }

    /**
     * 不为空
     */
    public static boolean isNotEmpty(Collection<?> source) {
        return !isEmpty(source);
    }

    /**
     * 合并
     */
    public static <T, E> boolean combine(Collection<T> origin, Collection<E> target, BiConsumer<T, E> consumer) {
        if (CollectionHelper.isEmpty(origin) || CollectionHelper.isEmpty(target)) {
            return false;
        }

        // 双循环
        for (T ori : origin) {
            for (E tar : target) {
                consumer.accept(ori, tar);
            }
        }

        return true;
    }

    /**
     * 删除
     * <p>
     * 根据consumer在origin中保留target中存在的
     */
    public static <T, E> boolean retain(List<T> origin, List<E> target, BiPredicate<T, E> consumer) {
        // origin为空，不做处理
        if (CollectionHelper.isEmpty(origin)) {
            return false;
        }
        // target为空，清空origin
        if (CollectionHelper.isEmpty(target)) {
            origin.clear();
        }

        boolean remove = false;
        // 匹配
        Iterator<T> origins = origin.iterator();
        while (origins.hasNext()) {
            T ori = origins.next();
            boolean has = false;
            for (E tar : target) {
                if (consumer.test(ori, tar)) {
                    has = true;
                    break;
                }
            }

            // 没有则删除
            if (!has) {
                origins.remove();
                remove = true;
            }
        }

        return remove;
    }
}
