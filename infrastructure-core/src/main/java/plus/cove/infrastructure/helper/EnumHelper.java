package plus.cove.infrastructure.helper;

import org.dromara.hutool.core.text.CharSequenceUtil;
import plus.cove.infrastructure.component.BaseEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 枚举基类工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class EnumHelper {
    private EnumHelper() {
    }

    /**
     * 根据枚举值获取枚举
     */
    public static <E extends Enum<?> & BaseEnum> E valueOf(Class<E> enumClass, int value) {
        if (!enumClass.isEnum()) {
            return null;
        }

        // 获取所有枚举
        E[] enums = enumClass.getEnumConstants();
        for (E e : enums) {
            if (e.getValue() == value) {
                return e;
            }
        }

        return null;
    }

    /**
     * 根据枚举名称获取枚举值
     *
     * @param names 枚举名称，忽略大小写，逗号分割多个
     */
    public static <E extends Enum<?> & BaseEnum> List<E> nameOf(Class<E> enumClass, String names) {
        if (!enumClass.isEnum() || CharSequenceUtil.isEmpty(names)) {
            return Collections.emptyList();
        }

        List<E> list = new ArrayList<>();

        // 获取所有枚举
        E[] enums = enumClass.getEnumConstants();
        String[] values = names.split(",");
        for (E e : enums) {
            for (String v : values) {
                if (e.name().equalsIgnoreCase(v)) {
                    list.add(e);
                }
            }
        }

        return list;
    }

    /**
     * 根据枚举名称获取枚举值
     *
     * @param names 枚举名称，忽略大小写，逗号分割多个
     */
    public static <E extends Enum<?> & BaseEnum> List<Integer> toValues(Class<E> enumClass, String names) {
        if (!enumClass.isEnum() || CharSequenceUtil.isEmpty(names)) {
            return Collections.emptyList();
        }

        List<Integer> list = new ArrayList<>();

        // 获取所有枚举
        E[] enums = enumClass.getEnumConstants();
        String[] values = names.split(",");
        for (E e : enums) {
            for (String v : values) {
                if (e.name().equalsIgnoreCase(v)) {
                    list.add(e.getValue());
                }
            }
        }

        return list;
    }

    /**
     * 是否在范围内
     */
    public static boolean betweenIn(BaseEnum origin, BaseEnum... sources) {
        boolean has = false;
        for (BaseEnum source : sources) {
            if (origin == source) {
                has = true;
                break;
            }
        }

        return has;
    }
}
