package plus.cove.infrastructure.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 身份辅助类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class IdentityHelper {
    private IdentityHelper() {
    }

    /**
     * 根据规则拆分地址
     * 将目标字符串拆分成三段
     * 规则
     * 第1截取点 优先级 "县">"区">"市">"旗">"盟" 首次出现位置
     * 第2截取点 优先级 "村">"路">"街 ">"乡" 倒序首次出现位置
     * <p>
     * 设 第一截取点为m,第二截取点为n
     * 三段为 0-m,m-n,n-结尾
     *
     * @param address 身份证地址信息
     * @return String[] 拆分结果
     */
    public static String[] splitAddress(String address) {
        String[] numberArray = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] provinceArray = {"县", "区", "市", "旗", "盟 "};
        String[] countryArray = {"村", "路", "街", "乡"};
        char[] addressArray = address.toCharArray();

        boolean termination = false;
        // 对截取点m赋值
        int m = 0;
        for (String province : provinceArray) {
            if (termination) {
                break;
            }
            for (int i = 0; i < addressArray.length; i++) {
                boolean equals = province.equals(String.valueOf(addressArray[i]));
                if (equals) {
                    termination = true;
                    m = i;
                    break;
                }
            }
        }
        termination = false;

        // 判断是否包含数字信息
        boolean number = StringHelper.containsAny(address, numberArray);
        // 对截取点n赋值
        int n = addressArray.length;
        if (number) {
            String regEx = "\\D";
            Pattern p = Pattern.compile(regEx);
            Matcher mb = p.matcher(address);
            String result = mb.replaceAll("").trim();
            char ch = result.charAt(0);
            n = address.indexOf(ch);
        } else {
            for (String country : countryArray) {
                if (termination) {
                    break;
                }
                for (int i = addressArray.length - 1; i > m + 1; i--) {
                    boolean equals = country.equals(String.valueOf(addressArray[i]));
                    if (equals) {
                        termination = true;
                        n = i + 1;
                        break;
                    }
                }
            }
        }
        // 截取字符串
        // 县、区、市段 0-m+1
        String addressCity = address.substring(0, m + 1);
        String streetAddress = "";
        String particularAddress = "";

        // 街道地址==详细地址
        if (n == addressArray.length) {
            streetAddress = address.substring(m + 1, n);
            particularAddress = address.substring(m + 1, n);

        } else {
            // 街道地址段 m + 1, n
            streetAddress = address.substring(m + 1, n);
            // 详细地址段 n至结尾
            particularAddress = address.substring(n);
        }
        return new String[]{addressCity, streetAddress, particularAddress};
    }
}
