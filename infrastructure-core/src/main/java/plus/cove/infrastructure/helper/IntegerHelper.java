package plus.cove.infrastructure.helper;

/**
 * 整型工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class IntegerHelper {
    /**
     * 私有构造器
     */
    private IntegerHelper() {
    }

    /**
     * 判断是null或者zero
     *
     * @param origin 原值
     */
    public static boolean isEmpty(Integer origin) {
        return origin == null || origin == 0;
    }

    /**
     * 判断是否相等
     * <p>
     * 其一为null，返回false
     *
     * @param origin 原始值
     * @param value  比较值
     */
    public static boolean equals(Integer origin, Integer value) {
        if (origin == null || value == null) {
            return false;
        }
        return origin.intValue() == value.intValue();
    }

    /**
     * 判断是否不等
     * <p>
     * 其一为null，返回true
     *
     * @param origin 原始值
     * @param target 比较值
     */
    public static boolean notEquals(Integer origin, Integer target) {
        return !equals(origin, target);
    }

    /**
     * 进行加法运算
     *
     * @param origin 原值为null，则记为0
     */
    public static int plus(Integer origin, int value) {
        if (origin == null) {
            return value;
        }
        return origin + value;
    }

    /**
     * 判断是否大于某值
     * null返回false
     *
     * @param origin 原始值
     * @param value  比较值
     * @return 比较结果
     */
    public static boolean greaterThan(Integer origin, int value) {
        return origin != null && origin > value;
    }

    /**
     * 判断是否小于某值
     * null返回false
     *
     * @param origin 原始值
     * @param value  比较值
     * @return 比较结果
     */
    public static boolean lessThan(Integer origin, int value) {
        return origin != null && origin < value;
    }
}
