package plus.cove.infrastructure.helper;

/**
 * 长整型工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class LongHelper {
    /**
     * 私有构造器
     */
    private LongHelper() {
    }

    /**
     * 判断是null或者zero
     *
     * @param origin 原值
     */
    public static boolean isEmpty(Long origin) {
        return origin == null || origin == 0;
    }

    /**
     * 判断是否相等
     * <p>
     * 其一为null，返回true
     *
     * @param origin 原始值
     * @param target 比较值
     */
    public static boolean equals(Long origin, Long target) {
        if (origin == null || target == null) {
            return false;
        }
        return origin.longValue() == target.longValue();
    }

    /**
     * 判断是否不等
     * <p>
     * 其一为null，返回true
     *
     * @param origin 原始值
     * @param target 比较值
     */
    public static boolean notEquals(Long origin, Long target) {
        return !equals(origin, target);
    }

    /**
     * 进行加法运算
     *
     * @param origin 原值为null，则记为0
     */
    public static long plus(Long origin, long value) {
        if (origin == null) {
            return value;
        }
        return origin + value;
    }

    /**
     * 判断是否大于某值
     * null返回false
     *
     * @param origin 原始值
     * @param value  比较值
     * @return 比较结果
     */
    public static boolean greaterThan(Long origin, long value) {
        return origin != null && origin > value;
    }

    /**
     * 判断是否小于某值
     * null返回false
     *
     * @param origin 原始值
     * @param value  比较值
     * @return 比较结果
     */
    public static boolean lessThan(Long origin, long value) {
        return origin != null && origin < value;
    }
}
