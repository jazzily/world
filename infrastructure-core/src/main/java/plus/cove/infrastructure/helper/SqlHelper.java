package plus.cove.infrastructure.helper;

/**
 * sql工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class SqlHelper {
    private static final char SECTION_DIVIDER = '.';
    private static final char GROUP_DIVIDER = ',';

    /**
     * 私有构造器
     */
    private SqlHelper() {
    }

    /**
     * 转换sql语句
     *
     * @param source 原值, 比如create.asc,update.desc
     * @return 有效的sql语句, 比如：create_time asc, update_time desc
     */
    public static String orderBy(String source) {
        StringBuilder sqlResult = new StringBuilder(16);
        char[] srcArray = source.toCharArray();

        // <field>.<order>
        int index = 0;
        int length = srcArray.length;
        boolean field = false;

        for (int i = 0; i < length; i++) {
            char src = srcArray[i];
            // 遇到. 匹配field
            if (src == SECTION_DIVIDER) {
                field = (i > index);
                if (field) {
                    String one = new String(srcArray, index, i - index);
                    sqlResult.append(one).append(" ");
                }
                i++;
                index = i;
            }

            // 最后
            boolean last = (i == length - 1);
            // 遇到, 匹配order
            if (src == GROUP_DIVIDER || last) {
                // 有order
                boolean order = (i > index);
                if (order) {
                    if (field) {
                        String two = new String(srcArray, index, i - index);
                        two = two.equalsIgnoreCase("desc") ? "desc" : "asc";
                        sqlResult.append(two);
                    } else {
                        String one = new String(srcArray, index, i - index);
                        sqlResult.append(one);
                    }

                    if (last) {
                        sqlResult.append(src);
                    } else {
                        sqlResult.append(GROUP_DIVIDER);
                    }
                }
                field = false;
                i++;
                index = i;
            }
        }

        return sqlResult.toString();
    }

    /**
     * 转换sql语句
     *
     * @param source  原值, 比如create.asc,update.desc
     * @param mapping 字段映射 比如create.create_time,update.update_time
     * @return 有效的sql语句, 比如：create_time asc, update_time desc
     */
    public static String orderBy(String source, String mapping) {
        StringBuilder sqlResult = new StringBuilder(16);

        // 分割
        String[] mapArray = StringHelper.split(mapping, GROUP_DIVIDER);
        String[] srcArray = StringHelper.split(source, GROUP_DIVIDER);

        // 匹配
        String matcher = String.valueOf(SECTION_DIVIDER);
        int index = 0;
        for (String src : srcArray) {
            String field = src;
            String order = "";
            if (src.contains(matcher)) {
                String[] sections = StringHelper.split(src, SECTION_DIVIDER);
                field = sections[0];
                order = sections[1];
            }

            // 字段匹配
            for (String map : mapArray) {
                if (map.startsWith(field.concat(matcher))) {
                    field = map.substring(field.length() + 1);
                    break;
                }
            }

            // order判断
            if ("desc".equalsIgnoreCase(order)) {
                order = "DESC";
            } else {
                order = "ASC";
            }

            if (index > 0) {
                sqlResult.append(", ");
            }
            index++;
            sqlResult.append(field).append(" ").append(order);
        }

        return sqlResult.toString();
    }
}
