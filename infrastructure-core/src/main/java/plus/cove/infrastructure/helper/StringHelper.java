package plus.cove.infrastructure.helper;

/**
 * 字符串帮助类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public final class StringHelper {
    /**
     * 私有构造器
     */
    private StringHelper() {
    }

    /**
     * base64格式
     */
    public static final String BASE64 = "base64,";

    /**
     * 空字符串
     */
    public static final String EMPTY = "";

    /**
     * 是否为空
     */
    public static boolean isEmpty(CharSequence source) {
        return source == null || source.isEmpty();
    }

    /**
     * 不为空
     */
    public static boolean isNotEmpty(CharSequence source) {
        return !isEmpty(source);
    }

    /**
     * 是否相等
     *
     * @param origin 原始文件
     * @param target 目标文件
     */
    public static boolean equals(CharSequence origin, CharSequence target) {
        if (origin == null || target == null) {
            return false;
        }

        return origin.equals(target);
    }

    /**
     * 是否不等
     *
     * @param origin 原始文件
     * @param target 目标文件
     */
    public static boolean notEquals(CharSequence origin, CharSequence target) {
        return !equals(origin, target);
    }

    /**
     * 包含任意一个
     * <p>
     * source包含targets中的某一个
     *
     * @param source  源值
     * @param targets 目标值，被包含
     */
    public static boolean containsAny(String source, CharSequence... targets) {
        if (source == null || source.isEmpty()) {
            return false;
        }

        boolean has = false;
        for (CharSequence tar : targets) {
            if (source.contains(tar)) {
                has = true;
                break;
            }
        }
        return has;
    }

    /**
     * 相等唯一一个
     * <p>
     * source与targets中的任一个相等即可
     *
     * @param origin    源字符串
     * @param testChars 目标字符
     */
    public static boolean equalsAny(String origin, CharSequence... testChars) {
        boolean has = false;

        if (isNotEmpty(origin)) {
            for (CharSequence tc : testChars) {
                if (origin.contentEquals(tc)) {
                    has = true;
                    break;
                }
            }
        }

        return has;
    }

    /**
     * 用特定字符在原字符的特定位置代替原字符
     *
     * @param source     原始字符串
     * @param beginIndex 起始位置
     * @param length     结束位置
     * @param mask       替换字符串
     */
    public static String mask(String source, int beginIndex, int length, char mask) {
        if (beginIndex >= source.length() - 1) {
            return source;
        }

        char[] target = source.toCharArray();
        for (int i = beginIndex; i < source.length() && length > 0; i++, length--) {
            target[i] = mask;
        }
        return new String(target);
    }

    /**
     * 分组
     * 分组后最后一位增加
     */
    public static String group(String source, String separator, int length, int increment) {
        if (source == null || source.isEmpty() || separator == null || separator.isEmpty()) {
            return source;
        }

        String target;

        int lastNum = 0;
        int lastIdx = source.lastIndexOf(separator);
        if (lastIdx == -1) {
            target = source + separator;
            lastNum = increment;
        } else {
            target = source.substring(0, lastIdx + 1);
            lastNum = Integer.parseInt(source.substring(lastIdx + 1)) + 1;
        }

        // 前补0
        StringBuilder sb = new StringBuilder(length);
        sb.append(lastNum);
        while (sb.length() < length) {
            sb.insert(0, "0");
        }

        return target + sb.toString();
    }

    /**
     * 合并
     * 两个字符串，使用concat
     * 多个字符串，使用StringBuilder
     */
    public static String join(String origin, String... targets) {
        // 一个字符串
        if (targets.length == 0) {
            return origin;
        }

        // 两个字符串拼接，推荐使用concat
        if (targets.length == 1) {
            return origin.concat(targets[0]);
        }

        // 多个字符串拼接，推荐使用StringBuilder
        StringBuilder sb = new StringBuilder(targets.length + 1);
        sb.append(origin);
        for (String tar : targets) {
            sb.append(tar);
        }
        return sb.toString();
    }

    /**
     * 截取
     */
    public static String trim(String origin, String str) {
        if (isEmpty(origin) || isEmpty(str)) {
            return EMPTY;
        }

        String actual = origin;
        if (actual.startsWith(str)) {
            actual = actual.substring(str.length());
        }

        if (actual.endsWith(str)) {
            actual = actual.substring(0, actual.length() - str.length());
        }

        return actual;
    }

    /**
     * 分割字符串
     * <p>
     * 适合较短字符串分割，不支持正则
     */
    public static String[] split(String origin, Character separator) {
        char[] orgArray = origin.toCharArray();

        // 计算数组个数
        int length = 0;
        for (char oa : orgArray) {
            if (oa == separator) {
                length++;
            }
        }

        // 返回数组
        String[] result = new String[length + 1];

        // 数组字符串
        int index = 0;
        int start = 0;
        for (int j = 0; j < orgArray.length; j++) {
            if (orgArray[j] == separator) {
                result[index] = new String(orgArray, start, j - start);
                index++;
                start = ++j;
            }
        }

        // 最后一个
        result[index] = new String(orgArray, start, orgArray.length - start);

        return result;
    }
}
