package plus.cove.infrastructure.validator;

import plus.cove.infrastructure.helper.StringHelper;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * 时间校验器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class DateRangeValidator implements ConstraintValidator<DateRange, Object> {
    private DateRange range;

    @Override
    public void initialize(DateRange range) {
        this.range = range;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        // 转换日期
        LocalDate dt = tryParseValue(value, this.range.pattern());
        if (dt == null) {
            return false;
        }

        // 验证日期
        ChronoUnit unit = range.unit();
        int min = range.min();
        int max = range.max();

        LocalDate now = LocalDate.now();
        if (min > Integer.MIN_VALUE && dt.isBefore(now.plus(min, unit))) {
            return false;
        }
        if (max < Integer.MAX_VALUE && dt.isAfter(now.plus(max, unit))) {
            return false;
        }

        return true;
    }

    /**
     * 尝试转换值
     */
    private LocalDate tryParseValue(Object value, String[] pattern) {
        // 日期模式
        if (value instanceof LocalDate val) {
            return val;
        }

        // 字符串模式
        if (value instanceof String str) {
            // 是否匹配
            return tryParseDate(str, pattern);
        }

        return null;
    }

    /**
     * 尝试转化
     * <p>
     * 空字符串返回null
     * 转化失败返回null
     *
     * @param value    转换的值
     * @param patterns 转换模式，比如yyyy-MM-dd
     */
    private static LocalDate tryParseDate(String value, String... patterns) {
        // 是否为空
        if (value == null || value.isEmpty()) {
            return null;
        }

        // 是否匹配
        LocalDate actual = null;
        for (String pattern : patterns) {
            // 匹配
            String delimiter = pattern.replaceAll("[\\sYyMmd]", "");
            if (!delimiter.isEmpty() && (!StringHelper.containsAny(value, delimiter))) {
                continue;
            }

            // 强转，测试
            try {
                actual = LocalDate.parse(value, DateTimeFormatter.ofPattern(pattern));
            } catch (Exception e) {
                // nothing
            }

            // 正常，返回
            if (actual != null) {
                break;
            }
        }

        return actual;
    }
}