package plus.cove.infrastructure.validator;

import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.ValidatorError;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import jakarta.validation.groups.Default;
import java.util.Collection;
import java.util.Set;

/**
 * 验证工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class ValidatorUtils {
    private ValidatorUtils() {
    }

    /**
     * 验证数据
     * 使用默认分组
     */
    public static <T, C> ActionResult<C> valid(T input) {
        return valid(input, Default.class);
    }

    /**
     * 验证数据
     *
     * @param input 需要验证的数据
     */
    public static <T, C> ActionResult<C> valid(T input, Class<?>... groups) {
        ActionResult<C> result = ActionResult.success();
        if (input == null) {
            result.fail(ValidatorError.INVALID_ARGUMENT);
            return result;
        }

        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<T>> validates = validator.validate(input, groups);

        if (validates != null && !validates.isEmpty()) {
            ConstraintViolation<T> validate = validates.stream()
                    .findFirst()
                    .orElseThrow(IllegalArgumentException::new);

            result.fail(ValidatorError.INVALID_ARGUMENT)
                    .message(validate.getMessage());
            return result;
        }

        return result;
    }


    /**
     * 校验集合
     * 使用默认分组
     */
    public static <E, C> ActionResult<C> validCollection(Collection<E> inputs) {
        return validCollection(inputs, Default.class);
    }

    /**
     * 校验集合
     *
     * @param inputs 校验的集合
     */
    public static <E, C> ActionResult<C> validCollection(Collection<E> inputs, Class<?>... groups) {
        ActionResult<C> result = ActionResult.success();

        if (inputs == null || inputs.isEmpty()) {
            result.fail(ValidatorError.INVALID_ARGUMENT);
            return result;
        }

        for (E input : inputs) {
            ActionResult<C> validated = valid(input, groups);
            if (validated.noSuccess()) {
                result = validated;
                break;
            }
        }

        return result;
    }
}
