package plus.cove.infrastructure.extension.authorizer;

import io.smallrye.jwt.auth.principal.JWTCallerPrincipal;
import lombok.Getter;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * jwt认证凭证
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Getter
public class JwtAuthorizer extends JWTCallerPrincipal {
    private final JwtClaim claim;

    public JwtAuthorizer(String rawToken, JwtClaim claim) {
        super(rawToken, "JWT");
        this.claim = claim;
    }

    protected Collection<String> doGetClaimNames() {
        return List.of(claim.getClaim());
    }

    protected Object getClaimValue(String claimName) {
        return claim.getClaim();
    }

    @Override
    public Set<String> getGroups() {
        return Set.of(claim.getActors().split(","));
    }

}