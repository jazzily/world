package plus.cove.infrastructure.extension.authorizer;

import lombok.Getter;

/**
 * jwt输出结果
 * 包括是否成功和身份
 *
 * @author jimmy.zhang
 * since 1.0
 */
@Getter
public class JwtClaim {
    /**
     * 声明
     */
    private String claim;

    /**
     * 附加
     */
    private String extra;

    /**
     * 角色
     */
    private String actors;

    public static JwtClaim of(String claim, String extra, String actors) {
        JwtClaim jwt = new JwtClaim();
        jwt.claim = claim;
        jwt.extra = extra;
        jwt.actors = actors;
        return jwt;
    }
}
