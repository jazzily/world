package plus.cove.infrastructure.extension.authorizer;

/**
 * jwt输入结果
 * 包括token和过期时间
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class JwtResult {
    /**
     * token值
     */
    private String token;

    /**
     * 过期时间
     * 单位秒
     */
    private Integer expire;

    private JwtResult() {
    }

    public String getToken() {
        return token;
    }

    public Integer getExpire() {
        return expire;
    }

    public static JwtResult of(String token, Integer expire) {
        JwtResult result = new JwtResult();
        result.expire = expire;
        result.token = token;
        return result;
    }
}
