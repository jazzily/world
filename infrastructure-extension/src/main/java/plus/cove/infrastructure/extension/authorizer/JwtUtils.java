package plus.cove.infrastructure.extension.authorizer;


import jakarta.enterprise.context.ApplicationScoped;
import org.dromara.hutool.core.text.CharSequenceUtil;
import org.dromara.hutool.core.util.RandomUtil;
import org.dromara.hutool.json.jwt.JWT;
import org.dromara.hutool.json.jwt.JWTException;
import org.dromara.hutool.json.jwt.signers.JWTSigner;
import org.dromara.hutool.json.jwt.signers.JWTSignerUtil;
import plus.cove.infrastructure.exception.BusinessException;
import plus.cove.infrastructure.exception.ValidatorError;

import java.time.Instant;

/**
 * jwt工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class JwtUtils {
    private final JwtConfig jwtConfig;

    public JwtUtils(
            final JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    /**
     * 创建token
     */
    public JwtResult encode(String claim, String extra, String... actor) {
        String secret = jwtConfig.secret();
        if (CharSequenceUtil.isEmpty(secret)) {
            throw new BusinessException(ValidatorError.JWT_INVALID_SECRET);
        }

        String jwtId = RandomUtil.randomString(10);
        long now = Instant.now().getEpochSecond();
        long exp = now + jwtConfig.expired().longValue();

        try {
            JWTSigner signer = JWTSignerUtil.hs512(secret.getBytes());
            String token = JWT.of()
                    .setPayload("sub", jwtConfig.subject())
                    .setPayload("iss", jwtConfig.issuer())
                    .setPayload("jti", jwtId)
                    .setPayload("iat", String.valueOf(now))
                    .setPayload("exp", String.valueOf(exp))
                    .setPayload(jwtConfig.claim(), claim)
                    .setPayload(jwtConfig.extra(), extra)
                    .setPayload(jwtConfig.actor(), actor)
                    .setSigner(signer)
                    .sign();
            return JwtResult.of(token, jwtConfig.expired());
        } catch (JWTException ex) {
            throw new BusinessException(ValidatorError.JWT_CREATION_EXCEPTION, ex);
        }
    }

    /**
     * 创建token
     */
    public JwtResult encode(String claim) {
        return encode(claim, "extra", jwtConfig.actor());
    }

    /**
     * 解密token
     */
    public JwtClaim decode(String token) {
        String secret = jwtConfig.secret();

        JWTSigner signer = JWTSignerUtil.hs512(secret.getBytes());
        JWT jwt = JWT.of(token)
                .setSigner(signer);

        // 验证算法与过期时间
        boolean verify = jwt.validate(5);
        if (!verify) {
            throw new BusinessException(ValidatorError.JWT_VERIFY_EXCEPTION);
        }

        String claim = jwt.getPayload(jwtConfig.claim()).toString();
        String extra = jwt.getPayload(jwtConfig.extra()).toString();
        String actor = jwt.getPayload(jwtConfig.actor()).toString();
        if (actor == null || actor.isEmpty()) {
            actor = jwtConfig.actor();
        }

        return JwtClaim.of(claim, actor, extra);
    }
}
