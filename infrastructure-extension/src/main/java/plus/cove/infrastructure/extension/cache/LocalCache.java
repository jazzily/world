package plus.cove.infrastructure.extension.cache;

import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheName;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.function.Function;

/**
 * 本地缓存
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class LocalCache {
    private static final String CACHE_NAME = "cove";

    @Inject
    @CacheName(CACHE_NAME)
    Cache cache;

    /**
     * 获取值
     */
    public <V> V get(String name, String key, Function<String, V> loader) {
        String keyed = name.concat(key);
        return cache.get(keyed, loader).await().indefinitely();
    }

    public void del(String name, String key) {
        String keyed = name.concat(key);
        cache.invalidate(keyed).await().indefinitely();
    }
}
