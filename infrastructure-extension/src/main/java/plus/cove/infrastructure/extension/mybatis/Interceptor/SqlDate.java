package plus.cove.infrastructure.extension.mybatis.Interceptor;

import java.lang.annotation.*;
import java.time.temporal.ChronoUnit;

/**
 * sql date处理
 * 针对LocalDate增加一天处理
 *
 * @author jimmy.zhang
 * @since 2.0
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SqlDate {
    /**
     * 增加
     */
    int increment() default 1;

    /**
     * 单位
     */
    ChronoUnit unit() default ChronoUnit.DAYS;
}
