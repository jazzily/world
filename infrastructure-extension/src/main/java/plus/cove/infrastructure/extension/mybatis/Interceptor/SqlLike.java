package plus.cove.infrastructure.extension.mybatis.Interceptor;

import java.lang.annotation.*;

/**
 * sql like处理
 * 通配符转义，支持%_[
 * 默认前后增加%%
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SqlLike {
    /**
     * 前缀
     */
    String prefix() default "";

    /**
     * 后缀
     */
    String suffix() default "%";
}
