package plus.cove.infrastructure.extension.mybatis.Interceptor;

import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.lang.reflect.Field;
import java.time.LocalDate;

/**
 * like通配符拦截器
 * 处理like注解中还有通配符的情况
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Intercepts({@Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
public class SqlStatementInterceptor implements Interceptor {
    private static final String PARAM_NAME_PREFIX = "param";
    private static final String LIKE_ESCAPE_CHARACTERS = "%_[";
    private static final char LIKE_ESCAPE = '\\';

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Object intercept(Invocation invocation) throws Throwable {
        Object[] args = invocation.getArgs();
        MappedStatement ms = (MappedStatement) args[0];
        if (SqlCommandType.SELECT != ms.getSqlCommandType()) {
            return invocation.proceed();
        }

        Object param = args[1];
        if (param instanceof MapperMethod.ParamMap map) {
            if (map.isEmpty()) {
                return invocation.proceed();
            }

            for (Object mKey : map.keySet()) {
                Object origin = map.get(mKey);
                // 存在默认参数的对象，存在自己命名的参数就显示两个value，所以只查找一个即可
                if (origin == null || !mKey.toString().startsWith(PARAM_NAME_PREFIX)) {
                    continue;
                }

                // 查找SqlStatement注解
                SqlStatement statement = origin.getClass().getAnnotation(SqlStatement.class);
                if (statement == null) {
                    continue;
                }

                // 遍历参数类的字段
                Field[] fields = origin.getClass().getDeclaredFields();
                for (Field field : fields) {
                    // 查找SqlLike注解，只支持String类型的参数
                    SqlLike sqlLike = field.getClass().getAnnotation(SqlLike.class);
                    if (sqlLike != null && field.getType().equals(String.class)) {
                        field.setAccessible(true);
                        // 设置前的值
                        String before = (String) field.get(origin);
                        if (before == null || before.isEmpty()) {
                            continue;
                        }

                        // 转义通配符
                        String escape = buildEscape(before, sqlLike);

                        // 设置处理后的值
                        field.set(origin, escape);
                        continue;
                    }

                    // 查找SqlDate注解，只支持LocalDate类型的参数
                    SqlDate sqlDate = field.getClass().getAnnotation(SqlDate.class);
                    if (sqlDate != null && field.getType().equals(LocalDate.class)) {
                        field.setAccessible(true);
                        // 转义通配符
                        LocalDate before = (LocalDate) field.get(origin);
                        if (before == null) {
                            continue;
                        }
                        field.set(origin, before.plus(sqlDate.increment(), sqlDate.unit()));
                    }
                }
            }
        }

        return invocation.proceed();
    }

    private static String buildEscape(String before, SqlLike sqlLike) {
        StringBuilder sbEscape = new StringBuilder(before.length() + LIKE_ESCAPE_CHARACTERS.length());
        sbEscape.append(sqlLike.prefix());
        for (int k = 0; k < before.length(); k++) {
            char c = before.charAt(k);
            // 是否存在需要转移的字符
            for (int l = 0; l < LIKE_ESCAPE_CHARACTERS.length(); l++) {
                if (LIKE_ESCAPE_CHARACTERS.charAt(l) == c) {
                    sbEscape.append(LIKE_ESCAPE);
                    break;
                }
            }
            sbEscape.append(c);
        }
        sbEscape.append(sqlLike.suffix());
        return sbEscape.toString();
    }

    @Override
    public Object plugin(Object target) {
        if (target instanceof Executor) {
            return Plugin.wrap(target, this);
        }
        return target;
    }
}
