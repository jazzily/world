package plus.cove.infrastructure.extension.provider;

import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.BusinessException;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * 自定义异常
 * <p>
 * 自定义业务异常
 * http status code     400
 * http result code     具体错误异常码
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Provider
public class BusinessExceptionProvider implements ExceptionMapper<BusinessException> {
    @Override
    public Response toResponse(BusinessException exception) {
        ActionResult result = ActionResult.result();
        result.fail(exception);

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
