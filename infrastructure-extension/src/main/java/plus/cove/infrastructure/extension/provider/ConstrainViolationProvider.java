package plus.cove.infrastructure.extension.provider;

import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.ValidatorError;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * 校验异常
 * <p>
 * 自定义验证异常
 * http status code 400
 * http result code 101
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Provider
public class ConstrainViolationProvider implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(ConstraintViolationException exception) {
        StringBuilder sbError = new StringBuilder();
        for (ConstraintViolation<?> violation : exception.getConstraintViolations()) {
            sbError.append(violation.getPropertyPath().toString()).append(":");
            sbError.append(violation.getMessage()).append(",");
        }

        ActionResult result = ActionResult.failure(ValidatorError.INVALID_ARGUMENT)
                .message(sbError.toString());

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
