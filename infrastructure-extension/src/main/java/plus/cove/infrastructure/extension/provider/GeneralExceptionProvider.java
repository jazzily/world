package plus.cove.infrastructure.extension.provider;

import org.jboss.logging.Logger;
import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.ServiceError;
import plus.cove.infrastructure.extension.serializer.JsonUtils;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * 通用异常错误
 * <p>
 * 其他未知错误
 * http status code    500
 * http result         500或无
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Provider
public class GeneralExceptionProvider implements ExceptionMapper<Exception> {
    private static final Logger LOG = Logger.getLogger(JsonUtils.class);

    @Override
    public Response toResponse(Exception exception) {
        // 异常异常
        LOG.errorf(exception, "general-exception");

        ActionResult result = ActionResult.failure(ServiceError.SERVER_ERROR)
                .message(exception.getMessage());

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
