package plus.cove.infrastructure.extension.provider;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.ValidatorError;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * json格式化异常
 * <p>
 * http status code    400
 * http result         500或无
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Provider
public class JacksonExceptionProvider implements ExceptionMapper<MismatchedInputException> {
    @Override
    public Response toResponse(MismatchedInputException exception) {
        ActionResult result = ActionResult.failure(ValidatorError.INVALID_FORMATTING)
                .message(exception.getMessage());

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
