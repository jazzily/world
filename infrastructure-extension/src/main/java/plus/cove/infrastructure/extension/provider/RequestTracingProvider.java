package plus.cove.infrastructure.extension.provider;

import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.ext.Provider;
import org.dromara.hutool.core.io.IoUtil;
import org.dromara.hutool.core.util.RandomUtil;
import org.jboss.logging.Logger;
import plus.cove.infrastructure.extension.security.SecurityConfig;
import plus.cove.infrastructure.helper.StringHelper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 请求追踪
 * <p>
 * 提供统一的编码以便后续查找
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@Provider
public class RequestTracingProvider implements
        ContainerRequestFilter, ContainerResponseFilter {
    private static final Logger LOG = Logger.getLogger(RequestTracingProvider.class);

    private static final String EMPTY_BODY = "null";

    @Inject
    SecurityConfig config;

    @Context
    HttpServerRequest request;
    @Context
    HttpServerResponse response;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String requestCode = RandomUtil.randomString(10);
        requestContext.setProperty("Product-Code", requestCode);
        response.putHeader("X-Product-Trace", requestCode);

        // 请求体
        String body = EMPTY_BODY;
        InputStream origin = requestContext.getEntityStream();
        if (origin != null && origin.available() < config.requestTracing()) {
            byte[] byteOrigin = IoUtil.readBytes(origin);

            // String输出
            body = new String(byteOrigin);
            // Stream输出
            InputStream stream = new ByteArrayInputStream(byteOrigin);
            requestContext.setEntityStream(stream);
        }

        // 请求路由
        String requestRoute = StringHelper.join(request.method().toString(), request.path());
        response.putHeader("X-Product-Router", requestRoute);

        // 日志
        LOG.debugf("request, code: %s, path: %s, query: %s, body: %s",
                requestCode, requestRoute, request.query(), body);
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
    }
}
