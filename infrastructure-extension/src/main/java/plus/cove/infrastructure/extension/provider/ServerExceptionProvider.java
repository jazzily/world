
package plus.cove.infrastructure.extension.provider;

import io.quarkus.security.AuthenticationFailedException;
import io.smallrye.mutiny.Uni;
import io.vertx.ext.web.RoutingContext;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;
import plus.cove.infrastructure.component.ActionResult;
import plus.cove.infrastructure.exception.ServiceError;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * 响应式异常
 * <p>
 * <a href="https://github.com/michalvavrik/quarkus-reproducer/blob/master/src/main/java/org/acme/ExceptionMapper.java">quarkus-reproducer</a>
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class ServerExceptionProvider {
    @ServerExceptionMapper(value = AuthenticationFailedException.class, priority = Priorities.USER)
    public Uni<Response> handleAuthentication(RoutingContext routingContext) {
        ActionResult result = ActionResult.failure(ServiceError.BAD_AUTHENTICATION)
                .message(routingContext.failure().getMessage());

        Response response = Response.status(Response.Status.BAD_REQUEST)
                .entity(result)
                .type(MediaType.APPLICATION_JSON)
                .build();
        return Uni.createFrom().item(response);
    }
}
