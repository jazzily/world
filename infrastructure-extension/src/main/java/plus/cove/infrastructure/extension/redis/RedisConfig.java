package plus.cove.infrastructure.extension.redis;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * redis配置
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ConfigMapping(prefix = "summer.redis-config")
public interface RedisConfig {
    /**
     * 过期时间
     * 单位，秒
     * 默认32天
     */
    @WithDefault("2764800")
    Long expireTime();

    /**
     * 分组分隔符
     * 默认分号
     */
    @WithDefault(":")
    String groupLabel();

    /**
     * 锁前缀
     */
    @WithDefault("LOCK")
    String lockPrefix();

    /**
     * 锁过期时间
     * 单位：秒
     */
    @WithDefault("30")
    Long lockExpired();

    /**
     * 锁等待时间
     * 单位：毫秒
     */
    @WithDefault("5")
    Long lockWaiting();
}

