package plus.cove.infrastructure.extension.redis;

import org.dromara.hutool.core.thread.ThreadUtil;
import io.vertx.mutiny.redis.client.RedisAPI;
import io.vertx.mutiny.redis.client.Response;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;

/**
 * Redis实现分布式锁
 * 参考https://github.com/pjmike/redis-distributed-lock/
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class RedisLock {
    // 等待时间-单位毫秒
    private static final long SLEEP_TIME = 1L;
    private static final String RETURN_OK = "OK";

    final RedisConfig redisConfig;
    final RedisAPI redisApi;

    public RedisLock(
            final RedisConfig redisConfig,
            final RedisAPI redisApi) {
        this.redisConfig = redisConfig;
        this.redisApi = redisApi;

    }

    private String buildKey(String name) {
        return String.join(redisConfig.groupLabel(), redisConfig.lockPrefix(), name);
    }

    private boolean returnValue(Response response, String value) {
        return response != null && response.toString().equals(value);
    }

    /**
     * 加锁
     * <p>
     * Redis的2.6.12及以后，使用 set key value [NX] [EX] 命令
     *
     * @param name    名称
     * @param value   值
     * @param expired 过期时间
     * @return 是否锁定
     */
    public boolean lock(String name, String value, long expired) {
        String key = buildKey(name);
        List<String> args = List.of(key, value, "NX", "EX", String.valueOf(expired));
        Response response = redisApi.set(args).await().indefinitely();
        return returnValue(response, RETURN_OK);
    }

    /**
     * 加锁
     * <p>
     * Redis的2.6.12及以后，使用 set key value [NX] [EX] 命令
     *
     * @param name  名称
     * @param value 值
     * @return 是否锁定
     */
    public boolean lock(String name, String value) {
        long seconds = redisConfig.lockExpired();
        String key = buildKey(name);
        List<String> args = List.of(key, value, "NX", "EX", String.valueOf(seconds));
        Response response = redisApi.set(args).await().indefinitely();
        return returnValue(response, RETURN_OK);
    }

    /**
     * 解锁
     * <p>
     * 简单版本：直接删除
     *
     * @param key 键
     * @return 是否解锁
     */
    public boolean unlock(String key) {
        List<String> args = List.of(key);
        Response response = redisApi.del(args).await().indefinitely();

        return returnValue(response, RETURN_OK);
    }

    /**
     * 等待枷锁
     * <p>
     * 等待解锁，需要设置等待时间，防止死锁
     *
     * @param key   键
     * @param value 值
     * @return 是否锁定
     */
    public boolean tryLock(String key, String value) {
        long millis = redisConfig.lockWaiting();
        while (millis >= 0) {
            boolean lock = lock(key, value);
            if (lock) {
                return true;
            }
            millis -= SLEEP_TIME;
            ThreadUtil.sleep(SLEEP_TIME);
        }
        return false;
    }

    /**
     * 等待解锁
     * <p>
     * 解锁时需要验证value值
     *
     * @param key   键
     * @param value 值
     * @return 是否解锁
     */
    public boolean tryUnlock(String key, String value) {
        if (value == null) {
            throw new IllegalArgumentException("value must not be null");
        }

        Response getRes = redisApi.get(key).await().indefinitely();
        if (returnValue(getRes, value)) {
            Response delRes = redisApi.del(List.of(key)).await().indefinitely();
            return returnValue(delRes, RETURN_OK);
        }
        return false;
    }
}
