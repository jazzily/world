package plus.cove.infrastructure.extension.redis;

import org.dromara.hutool.core.util.RandomUtil;
import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.keys.KeyCommands;
import io.quarkus.redis.datasource.value.SetArgs;
import io.quarkus.redis.datasource.value.ValueCommands;
import plus.cove.infrastructure.extension.serializer.JsonUtils;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.List;

/**
 * redis客户端工具
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class RedisUtils {
    // 过期时间随机数,5分钟
    private static final long EXPIRE_RANDOM = 300;

    final RedisConfig redisConfig;
    final JsonUtils jsonUtils;

    final KeyCommands<String> keyCommand;
    final ValueCommands<String, String> valCommand;

    public RedisUtils(
            final RedisConfig redisConfig,
            final JsonUtils jsonUtils,
            RedisDataSource ds) {
        this.redisConfig = redisConfig;
        this.jsonUtils = jsonUtils;

        keyCommand = ds.key(String.class);
        valCommand = ds.value(String.class, String.class);
    }

    /**
     * 生成key
     * 根据分组和名称生成key
     */
    private String buildKey(String group, String name) {
        return String.join(redisConfig.groupLabel(), group, name);
    }

    /**
     * 生成过期时间
     * 过期时间随机增加秒数，5分钟以内
     */
    private long buildExp(long expire) {
        if (expire > EXPIRE_RANDOM) {
            return RandomUtil.randomLong(EXPIRE_RANDOM) + expire;
        }

        return expire;
    }

    /**
     * 锁定
     *
     * @param group  分组
     * @param name   名称
     * @param value  值
     * @param expire 过期时间，单位秒
     */
    public String lock(String group, String name, String value, Long expire) {
        String key = this.buildKey(group, name);

        SetArgs args = new SetArgs();
        args.ex(expire);
        args.nx();
        return valCommand.setGet(key, value, args);
    }

    /**
     * 获取字符串
     */
    public String getString(String group, String name) {
        String key = buildKey(group, name);
        return valCommand.get(key);
    }

    /**
     * 获取对象
     */
    public <T> T getObject(String group, String name, Class<T> clazz) {
        String key = this.buildKey(group, name);
        String value = valCommand.get(key);
        return jsonUtils.fromObject(value, clazz);
    }

    /**
     * 获取集合
     */
    public <T> List<T> getList(String group, String name, Class<T> clazz) {
        String key = this.buildKey(group, name);
        String value = valCommand.get(key);
        return jsonUtils.fromArray(value, clazz);
    }

    /**
     * 获取值
     */
    public <T> Object get(String group, String name, Class<T> clazz) {
        String key = this.buildKey(group, name);
        String value = valCommand.get(key);
        return jsonUtils.fromJson(value, clazz);
    }

    /**
     * 设置值
     */
    public void set(String group, String name, Object value) {
        set(group, name, value, redisConfig.expireTime());
    }

    /**
     * 设置值
     */
    public void set(String group, String name, Object value, Long expire) {
        String key = this.buildKey(group, name);
        String json = jsonUtils.toJson(value);

        SetArgs args = new SetArgs();
        long ex = (expire == null ? redisConfig.expireTime() : expire.longValue());
        args.ex(buildExp(ex));
        valCommand.set(key, json, args);
    }

    /**
     * 清除值
     */
    public void del(String group, String name) {
        String key = this.buildKey(group, name);
        keyCommand.del(key);
    }
}
