package plus.cove.infrastructure.extension.security;

import io.quarkus.runtime.annotations.StaticInitSafe;
import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * 安全配置
 * 设置安全相关参数
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@StaticInitSafe
@ConfigMapping(prefix = "summer.security-config")
public interface SecurityConfig {
    /**
     * 监控地址
     */
    @WithDefault("metrics")
    String metricsUrl();

    /**
     * 监控角色
     */
    @WithDefault("METRICS")
    String metricsRole();

    /**
     * 幂等有效时间
     * 单位，秒
     */
    @WithDefault("10")
    Integer idempotentTime();

    /**
     * 幂等token名称
     */
    @WithDefault("api-idempotent")
    String idempotentToken();

    /**
     * 执行模式
     */
    @WithDefault("DEVELOP")
    SecurityMode executionMode();

    /**
     * 默认验证码
     */
    @WithDefault("123456")
    String validationCode();

    /**
     * request跟踪日志
     * <p>
     * 设置request body的最大长度，超过此长度则不记录
     * 默认0，不记录
     */
    @WithDefault("0")
    Integer requestTracing();
}
