package plus.cove.infrastructure.extension.security;

/**
 * 安全模式
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public enum SecurityMode {
    /**
     * 开发环境
     * 开发人员使用，版本变化很大
     *
     */
    DEVELOP,

    /**
     * 生产环境
     * 面对外部环境，版本相当稳定
     */
    PRODUCT;
}
