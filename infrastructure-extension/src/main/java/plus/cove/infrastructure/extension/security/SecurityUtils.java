package plus.cove.infrastructure.extension.security;

import org.dromara.hutool.crypto.digest.DigestUtil;
import io.vertx.core.http.HttpServerRequest;
import plus.cove.infrastructure.exception.BusinessException;
import plus.cove.infrastructure.exception.ValidatorError;
import plus.cove.infrastructure.extension.redis.RedisLock;
import plus.cove.infrastructure.helper.StringHelper;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * 安全工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class SecurityUtils {
    private static final String IDE_TOKEN = "TOKEN";

    final SecurityConfig securityConfig;
    final RedisLock redisLock;

    public SecurityUtils(
            final SecurityConfig securityConfig,
            final RedisLock redisLock) {
        this.securityConfig = securityConfig;
        this.redisLock = redisLock;
    }

    /**
     * 检查是否幂等
     */
    public void apiIdempotent(HttpServerRequest request) {
        String token = request.getParam(securityConfig.idempotentToken());
        if (StringHelper.isNotEmpty(token)) {
            boolean lock = redisLock.lock(token, IDE_TOKEN, securityConfig.idempotentTime().longValue());
            if (!lock) {
                throw new BusinessException(ValidatorError.IDEMPOTENT_TOKEN_DUPLICATED);
            }
        }
    }

    /**
     * 生成验证码
     * <p>
     * 根据执行环境进行判断
     * 开始环境，统一使用设置
     */
    public String validationCode(String origin) {
        if (securityConfig.executionMode() == SecurityMode.DEVELOP) {
            return DigestUtil.md5Hex(securityConfig.validationCode());
        }
        return origin;
    }
}
