package plus.cove.infrastructure.extension.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import plus.cove.infrastructure.component.BaseEnum;

import java.io.IOException;
import java.lang.reflect.Field;

/**
 * 枚举类型转换器
 *
 * @author jimmy.zhang
 * @since 1.0
 */
public class EnumDeserializer extends StdDeserializer<BaseEnum> {
    public static final EnumDeserializer INSTANCE = new EnumDeserializer();

    protected EnumDeserializer() {
        super(BaseEnum.class);
    }

    @Override
    public BaseEnum getEmptyValue(DeserializationContext context) {
        return null;
    }

    @Override
    public BaseEnum deserialize(JsonParser jp, DeserializationContext context) throws IOException {
        String currentName = jp.currentName();
        Object currentValue = jp.getCurrentValue();

        BaseEnum one = null;
        int enumValue = jp.getValueAsInt();

        Field[] fields = currentValue.getClass().getDeclaredFields();
        // 默认有field
        for (Field field : fields) {
            if (field.getName().equals(currentName)) {
                Object[] enums = field.getType().getEnumConstants();
                // 默认有enum
                for (Object anEnum : enums) {
                    BaseEnum origin = (BaseEnum) anEnum;
                    if (origin.getValue() == enumValue) {
                        one = origin;
                        break;
                    }
                }
                break;
            }
        }

        return one;
    }

    @Override
    public BaseEnum deserializeWithType(JsonParser p, DeserializationContext context, TypeDeserializer typeDeserializer) throws IOException {
        return this.deserialize(p, context);
    }
}
