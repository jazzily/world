package plus.cove.infrastructure.extension.serializer;

/**
 * json生成器
 *
 * @author jimmy.zhang
 * @since 2.0
 */
public class JsonBuilder {
    private static final String COMMA = ",";
    private final StringBuilder builder;

    public JsonBuilder(int capacity) {
        builder = new StringBuilder(capacity * 7);
        builder.append("{");
    }

    /**
     * 追加
     *
     */
    public JsonBuilder append(String key, String value) {
        if (key == null || key.isEmpty()) {
            return this;
        }

        builder.append("\"");
        builder.append(key);
        builder.append("\":");
        if (value == null) {
            builder.append("null");
        } else {
            builder.append("\"");
            builder.append(value);
            builder.append("\"");
        }

        appendComma();
        return this;
    }

    /**
     * 追加分号
     *
     */
    protected JsonBuilder appendComma() {
        builder.append(COMMA);
        return this;
    }

    @Override
    public String toString() {
        // 最后一个是comma
        if (builder.lastIndexOf(COMMA) == builder.length() - 1) {
            builder.deleteCharAt(builder.length() - 1);
        }
        builder.append("}");
        return builder.toString();
    }
}
