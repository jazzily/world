package plus.cove.infrastructure.extension.serializer;

import io.quarkus.runtime.annotations.StaticInitSafe;
import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * json配置
 * 包括时间格式，Long类型转为String
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@StaticInitSafe
@ConfigMapping(prefix = "summer.json-config")
public interface JsonConfig {
    /**
     * 日期格式化
     */
    @WithDefault("yyyy-MM-dd")
    String dateFormat();

    /**
     * 时间格式
     */
    @WithDefault("HH:mm:ss")
    String timeFormat();

    /**
     * 日期时间格式
     */
    @WithDefault("yyyy-MM-dd'T'HH:mm:ss")
    String datetimeFormat();
}

