package plus.cove.infrastructure.extension.serializer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * json配置
 * 包括时间格式，Long类型转为String
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class JsonRegister {
    private static final Logger LOG = Logger.getLogger(JsonRegister.class);

    final JsonConfig jsonConfig;

    public JsonRegister(final JsonConfig jsonConfig) {
        this.jsonConfig = jsonConfig;
    }

    private JsonMapper jsonMapper;

    public JsonMapper jsonMapper() {
        return this.jsonMapper;
    }

    @PostConstruct
    void initJsonMapper() {
        jsonMapper = JsonMapper.builder()
                // 忽略重复注册
                .configure(MapperFeature.IGNORE_DUPLICATE_MODULE_REGISTRATIONS, false)
                // 反序列化忽略不存在的属性
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                // 使用ISO-8601格式化日期和时间
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .build();

        SimpleModule jsonModule = new SimpleModule();
        // Long类型
        jsonModule.addSerializer(Long.class, ToStringSerializer.instance);
        jsonModule.addSerializer(Long.TYPE, ToStringSerializer.instance);

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(jsonConfig.dateFormat());
        jsonModule.addSerializer(LocalDate.class, new LocalDateSerializer(dateFormatter));
        jsonModule.addDeserializer(LocalDate.class, new LocalDateDeserializer(dateFormatter));

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(jsonConfig.timeFormat());
        jsonModule.addSerializer(LocalTime.class, new LocalTimeSerializer(timeFormatter));
        jsonModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer(timeFormatter));

        DateTimeFormatter datetimeFormatter = DateTimeFormatter.ofPattern(jsonConfig.datetimeFormat());
        jsonModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(datetimeFormatter));
        jsonModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(datetimeFormatter));

        jsonMapper.registerModule(jsonModule);
    }
}

