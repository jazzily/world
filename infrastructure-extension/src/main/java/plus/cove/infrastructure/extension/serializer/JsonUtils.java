package plus.cove.infrastructure.extension.serializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.jboss.logging.Logger;
import plus.cove.infrastructure.exception.BusinessException;
import plus.cove.infrastructure.exception.ValidatorError;

import jakarta.enterprise.context.ApplicationScoped;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * json工具类
 *
 * @author jimmy.zhang
 * @since 1.0
 */
@ApplicationScoped
public class JsonUtils {
    private static final Logger LOG = Logger.getLogger(JsonUtils.class);

    // 集合开始
    private static final String ARRAY_START = "[";

    final JsonRegister jsonRegister;

    public JsonUtils(final JsonRegister jsonRegister) {
        this.jsonRegister = jsonRegister;
    }

    /**
     * 反序列化
     *
     * @param json json字符串
     * @param path json路径
     * @return json值
     */
    public String fromPath(String json, String path) {
        if (json == null || json.isEmpty()) {
            return null;
        }

        String[] paths = path.split("\\.");
        try {
            JsonNode node = jsonRegister.jsonMapper().readTree(json);
            for (String pt : paths) {
                node = node.path(pt);
            }
            return node.asText();
        } catch (Exception e) {
            throw new BusinessException(ValidatorError.JSON_PATH_FAILURE);
        }
    }

    /**
     * 反序列化对象
     *
     * @param json  json字符串
     * @param clazz 类型
     * @return 序列化后的对象
     */
    public <T> T fromObject(String json, Class<T> clazz) {
        if (json == null || json.isEmpty() || clazz == null) {
            return null;
        }

        if (clazz.equals(String.class)) {
            throw new BusinessException(ValidatorError.JSON_NONSUPPORT_TYPE);
        }

        try {
            return jsonRegister.jsonMapper().readValue(json, clazz);
        } catch (Exception e) {
            throw new BusinessException(ValidatorError.JSON_DESERIALIZER_FAILURE);
        }
    }

    /**
     * 反序列化集合
     *
     * @param json  json字符串
     * @param clazz 类型
     * @return 反序列化集合
     */
    public <T> List<T> fromArray(String json, Class<T> clazz) {
        // 空集合
        if (json == null || json.isEmpty() || clazz == null) {
            return Collections.emptyList();
        }

        try {
            ObjectMapper mapper = jsonRegister.jsonMapper();
            CollectionType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, clazz);
            return mapper.readValue(json, listType);
        } catch (Exception ex) {
            throw new BusinessException(ValidatorError.JSON_DESERIALIZER_FAILURE, ex);
        }
    }

    /**
     * 反序列化
     * 支持对象和数组
     *
     * @param json  json字符串
     * @param clazz 类型
     * @return 返回对象
     */
    public <T> Object fromJson(String json, Class<T> clazz) {
        if (json == null || json.isEmpty() || clazz == null) {
            return null;
        }

        // 集合类型
        if (json.startsWith(ARRAY_START)) {
            return fromArray(json, clazz);
        }

        return fromObject(json, clazz);
    }

    /**
     * 序列化
     *
     * @param obj 对象实体
     * @return json
     */
    public String toJson(Object obj) {
        if (obj == null) {
            return null;
        }

        try {
            return jsonRegister.jsonMapper().writeValueAsString(obj);
        } catch (Exception ex) {
            throw new BusinessException(ValidatorError.JSON_SERIALIZER_FAILURE, ex);
        }
    }
}
