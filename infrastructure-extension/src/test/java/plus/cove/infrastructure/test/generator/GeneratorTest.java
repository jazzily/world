package plus.cove.infrastructure.test.generator;


import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import plus.cove.infrastructure.generator.KeyGeneratorBuilder;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

@QuarkusTest
public class GeneratorTest {
    @Test
    public void buildUniqueKey() {
        long id = KeyGeneratorBuilder.INSTANCE.buildUniqueKey();
        System.out.println(id);
        Assertions.assertTrue(id > 0, "id测试");
    }

    @Test
    public void buildManyUniqueKey() {
        Set<Long> keys = new HashSet<>(100);
        // 生成多个应该均匀分布，不会偶数多，奇数少
        IntStream.rangeClosed(1, 10).parallel().forEach(is -> {
            for (int i = 0; i < 10; i++) {
                long id = KeyGeneratorBuilder.INSTANCE.buildUniqueKey();
                keys.add(id);
                System.out.println(String.format("%d:%d", is, id));
            }
        });
        Assertions.assertTrue(keys.size() > 0, "id测试");
    }

    @Test
    public void buildDailyKey() throws InterruptedException {
        long id = KeyGeneratorBuilder.INSTANCE.buildDailyKey();
        System.out.println(id);
        Assertions.assertTrue(id > 0, "id测试");
    }
}
