package plus.cove.infrastructure.test.helper;


import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import plus.cove.infrastructure.helper.SqlHelper;
import plus.cove.infrastructure.helper.StringHelper;

@QuarkusTest
public class SqlHelperTest {
    @Test
    public void orderBy() {
        String sql1 = SqlHelper.orderBy(
                "create.asc,update.desc,name,id",
                "create.create_time,update.update_time");
        System.out.println(sql1);

        Assertions.assertNotNull(sql1, "order-by");
    }

    @Test
    public void split(){
        String[] result = StringHelper.split("create.create_time,update.update_time",',');
        Assertions.assertEquals(2, result.length, "split");
    }
}
