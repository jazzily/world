package plus.cove.infrastructure.test.helper;


import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import plus.cove.infrastructure.helper.StringHelper;

@QuarkusTest
public class StringHelperTest {
    @Test
    public void group() {
        String g1 = StringHelper.group("100-001-001", "-", 3, 1);
        System.out.println(g1);

        String g2 = StringHelper.group("100", "-", 3, 1);
        System.out.println(g2);

        String g3 = StringHelper.group("100-001", "-", 3, 1);
        System.out.println(g3);

        Assertions.assertNotNull(g3, "字符串分组");
    }

    @Test
    public void mask() {
        String g1 = StringHelper.mask("13911006493", 3, 4, '*');
        System.out.println(g1);

        String g11 = StringHelper.mask("张旭", 1, 10, '*');
        System.out.println(g11);

        String g2 = StringHelper.mask("120226202101011478", 10, 4, '*');
        System.out.println(g2);

        String g3 = StringHelper.mask("李坚强", 1, 10, '*');
        System.out.println(g3);

        Assertions.assertNotNull(g3, "字符串分组");
    }

    @Test
    public void join() {
        String expect0 = "1";
        String actual0 = StringHelper.join("-", "1");
        System.out.println(actual0);
        Assertions.assertEquals(expect0, actual0);

        String expect1 = "1-2";
        String actual1 = StringHelper.join("-", "1", "2");
        System.out.println(actual1);
        Assertions.assertEquals(expect1, actual1);

        String expect2 = "1-2-3";
        String actual2 = StringHelper.join("-", "1", "2", "3");
        System.out.println(actual2);
        Assertions.assertEquals(expect2, actual2);

        String expect3 = "123";
        String actual3 = StringHelper.join("", "1", "2", "3");
        System.out.println(actual3);
        Assertions.assertEquals(expect3, actual3);
    }

    @Test
    public void trim() {
        String origin = "123ABCDEDF123";
        String expected = "ABCDEDF";
        String actual = StringHelper.trim(origin, "123");

        System.out.println(actual);
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void format() {
        String origin = "002";
        String actual = String.format("%03d", 2);
        System.out.println(actual);
        Assertions.assertEquals(origin, actual);
    }

    @Test
    public void containAny() {
        // 包含判断
        String origin = "第134号";
        String[] source = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        boolean has = StringHelper.containsAny(origin, source);
        Assertions.assertTrue(has);
    }

    @Test
    public void equalsAny() {
        // 相同判断
        String origin = "05,";
        String[] source1 = new String[]{"01,", "02,", "03,"};
        boolean has1 = StringHelper.equalsAny(origin, source1);
        Assertions.assertFalse(has1);

        String[] source2 = new String[]{"01,", "02,", "03,", "04,", "05,"};
        boolean has2 = StringHelper.equalsAny(origin, source2);
        Assertions.assertTrue(has2);
    }
}
