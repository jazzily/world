package plus.cove.infrastructure.test.hutool;

import io.quarkus.test.junit.QuarkusTest;
import org.dromara.hutool.core.text.StrUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class StrUtilTest {
    @Test
    public void sub() {
        // 内容
        String content = "123456789";
        String result = StrUtil.sub(content, 0, 10);
        System.out.println(result);
        Assertions.assertEquals(content, result);
    }

    @Test
    public void format() {
        String expect = "ABC-202212220123456789";
        String actual1 = String.format("A%s-%d%02d%02d%010d",
                "BC", 2022, 12, 22,
                123456789);
        System.out.println(actual1);

        String actual2 = StrUtil.indexedFormat("A{0}-{1}{2}{3}{4}",
                "BC", "2022", "12", "22",
                StrUtil.padPre("123456789", 10, "0"));
        System.out.println(actual2);
        Assertions.assertEquals(expect, actual1, "format1");
        Assertions.assertEquals(expect, actual2, "format2");
    }

    @Test
    public void containOnly() {
        char[] weeks = new char[]{'1', '2', '3', '4', '5', '6', '7', ','};
        String success = "1,2,3";
        String failure = "0,8";
        boolean yes = StrUtil.containsOnly(success, weeks);
        System.out.println(yes);
        Assertions.assertTrue(yes);
        boolean no = StrUtil.containsOnly(failure, weeks);
        System.out.println(no);
        Assertions.assertFalse(no);
    }
}
