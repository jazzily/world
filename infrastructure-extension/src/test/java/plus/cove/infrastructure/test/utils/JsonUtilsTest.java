package plus.cove.infrastructure.test.utils;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import plus.cove.infrastructure.component.BaseEnum;
import plus.cove.infrastructure.extension.serializer.JsonUtils;

import jakarta.inject.Inject;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@QuarkusTest
public class JsonUtilsTest {
    private String json_object = "{\"name\":\"jimmy\",\"age\":40,\"gender\":\"MALE\",\"good\":true,\"birth\":\"1980-01-01\",\"createDate\":\"2023-04-04T13:19:09\"}";
    private String json_list = "[{\"name\":\"jimmy\",\"age\":40,\"gender\":\"MALE\",\"good\":true,\"birth\":\"1980-01-01\"}]";

    @Inject
    JsonUtils jsonUtils;

    @Test
    public void fromPath() {
        String name = jsonUtils.fromPath(json_object, "name");
        System.out.println(name);
        String age = jsonUtils.fromPath(json_object, "age");
        System.out.println(age);
        String good = jsonUtils.fromPath(json_object, "good");
        System.out.println(good);

        Assertions.assertNotNull(name);
    }

    @Test
    public void fromObject() {
        JsonModel model = jsonUtils.fromObject(json_object, JsonModel.class);
        System.out.println(model);

        Assertions.assertNotNull(model);
    }

    @Test
    public void fromArray() {
        List<JsonModel> models = jsonUtils.fromArray(json_list, JsonModel.class);
        for (JsonModel model : models) {
            System.out.println(model);
        }

        Assertions.assertTrue(models.size() == 1);
    }

    @Test
    public void fromJson() {
        JsonModel model = (JsonModel) jsonUtils.fromJson(json_object, JsonModel.class);
        System.out.println(model);
        Assertions.assertNotNull(model);

        List<JsonModel> models = (List) jsonUtils.fromJson(json_list, JsonModel.class);
        for (JsonModel mod : models) {
            System.out.println(model);
        }
        Assertions.assertTrue(models.size() == 1);
    }

    @Test
    public void toJson() {
        JsonModel model = new JsonModel();
        model.setName("jimmy");
        model.setAge(40);
        model.setGender(JsonGender.FEMALE);
        model.setGood(false);
        model.setBirth(LocalDate.of(1980, 1, 1));
        model.setCreateDate(LocalDateTime.now());
        model.setCreateTime(LocalTime.now());

        String json = jsonUtils.toJson(model);

        System.out.println(json);
        Assertions.assertNotNull(json);
    }

    public static class JsonModel {
        private String name;
        private Integer age;
        private JsonGender gender;
        private Boolean good;
        private LocalDate birth;
        private LocalDateTime createDate;
        private LocalTime createTime;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public JsonGender getGender() {
            return gender;
        }

        public void setGender(JsonGender gender) {
            this.gender = gender;
        }

        public Boolean getGood() {
            return good;
        }

        public void setGood(Boolean good) {
            this.good = good;
        }

        public LocalDate getBirth() {
            return birth;
        }

        public void setBirth(LocalDate birth) {
            this.birth = birth;
        }

        public LocalDateTime getCreateDate() {
            return createDate;
        }

        public void setCreateDate(LocalDateTime createDate) {
            this.createDate = createDate;
        }

        public LocalTime getCreateTime() {
            return createTime;
        }

        public void setCreateTime(LocalTime createTime) {
            this.createTime = createTime;
        }

        @Override
        public String toString() {
            return String.format("name=%s,age=%s,gender=%s,good=%s,birth=%s,createDate=%s",
                    this.name, this.age, this.good, this.gender.getDescription(), this.birth.toString(),this.createDate.toString());
        }
    }

    public enum JsonGender implements BaseEnum {
        MALE(0, "男"),
        FEMALE(1, "女");

        /**
         * 枚举值
         */
        private int value;

        /**
         * 枚举描述
         */
        private String desc;

        /**
         * 构造函数
         *
         * @param
         * @return
         */
        JsonGender(final int value, final String desc) {
            this.value = value;
            this.desc = desc;
        }

        @Override
        public Integer getValue() {
            return value;
        }

        @Override
        public String getDescription() {
            return desc;
        }
    }

}
