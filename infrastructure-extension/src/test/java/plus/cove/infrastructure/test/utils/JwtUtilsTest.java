package plus.cove.infrastructure.test.utils;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import plus.cove.infrastructure.extension.authorizer.JwtResult;
import plus.cove.infrastructure.extension.authorizer.JwtUtils;

import jakarta.inject.Inject;

@QuarkusTest
public class JwtUtilsTest {
    @Inject
    JwtUtils jwtUtils;

    @Test
    public void build() {
        JwtResult result = jwtUtils.encode("123456", "extra", "admin");
        System.out.print(result.getToken());

        Assertions.assertNotNull(result.getToken());
    }
}
