package plus.cove.infrastructure.test.utils;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import plus.cove.infrastructure.extension.redis.RedisLock;

import jakarta.inject.Inject;
import java.util.UUID;

@QuarkusTest
public class RedisLockTest {
    @Inject
    RedisLock redisLock;

    @Test
    public void tryLock() {
        String value = UUID.randomUUID().toString();
        boolean lock1 = redisLock.tryLock("lock1", value);
        System.out.println(lock1);
        Assertions.assertTrue(lock1);

        boolean lock2 = redisLock.tryLock("lock1", value);
        System.out.println(lock2);
        Assertions.assertFalse(lock2);
    }

    @Test
    public void tryUnlock() {
        String value1 = UUID.randomUUID().toString();
        boolean lock = redisLock.tryLock("lock", value1);
        System.out.println(lock);
        Assertions.assertTrue(lock);

        String value2 = UUID.randomUUID().toString();
        boolean unlock = redisLock.tryUnlock("lock", value2);
        Assertions.assertFalse(unlock);
    }
}
