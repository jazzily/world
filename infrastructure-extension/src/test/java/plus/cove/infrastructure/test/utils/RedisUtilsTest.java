package plus.cove.infrastructure.test.utils;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import plus.cove.infrastructure.extension.redis.RedisUtils;

import jakarta.inject.Inject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@QuarkusTest
public class RedisUtilsTest {
    @Inject
    RedisUtils redisUtils;

    @Test
    public void setObject() {
        JsonModel model = new JsonModel();
        model.setName("jimmy");
        model.setAge(40);
        model.setGood(false);
        model.setBirth(LocalDate.of(1980, 1, 1));
        redisUtils.set("test", "object", model);
    }

    @Test
    public void setArray() {
        JsonModel model = new JsonModel();
        model.setName("jimmy");
        model.setAge(40);
        model.setGood(false);
        model.setBirth(LocalDate.of(1980, 1, 1));

        List<JsonModel> models = new ArrayList<>();
        models.add(model);
        redisUtils.set("test", "array", models);
    }

    @Test
    public void del(){
        redisUtils.del("test","object");
        redisUtils.del("test","array");
    }

    @Test
    public void get() {
        JsonModel model = (JsonModel) redisUtils.get("test", "name", JsonModel.class);
        System.out.println(model);
    }

    @Test
    public void getObject() {
        JsonModel model = redisUtils.getObject("test", "object", JsonModel.class);
        System.out.println(model);
    }

    @Test
    public void getArray() {
        List<JsonModel> models = redisUtils.getList("test", "array", JsonModel.class);
        for (JsonModel model : models) {
            System.out.println(model);
        }
    }

    public static class JsonModel {
        private String name;
        private Integer age;
        private Boolean good;
        private LocalDate birth;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public Boolean getGood() {
            return good;
        }

        public void setGood(Boolean good) {
            this.good = good;
        }

        public LocalDate getBirth() {
            return birth;
        }

        public void setBirth(LocalDate birth) {
            this.birth = birth;
        }

        @Override
        public String toString() {
            return String.format("name=%s,age=%s,good=%s,birth=%s",
                    this.name, this.age, this.good, this.birth.toString());
        }
    }
}
