#! /bin/bash
# ./deploy.sh [-p|--profile] [-b|--build] [-r|--remote]
# ./deploy.sh -p develop
set -e

build_prof="develop"
build_root="../.."
build_path="flower-api"
build_file="webapi-runner.jar"
remote_host="root@101.200.53.244"
remote_path="webapi-9000"

echo "[DEPLOY] ==build params=="
while [ $# -gt 0 ];
do
   case $1 in
   -p|--profile)
       build_prof=$2
       shift 2
       ;;
   -b|--build)
       build_path=$2
       shift 2
       ;;
   -r|--remote)
       remote_path=$2
       shift 2
       ;;
   esac
done

remote_path="/usr/local/${remote_path}"
echo "[DEPLOY] build_profile=${build_prof}"
echo "[DEPLOY] package_build_path=${build_path}"
echo "[DEPLOY] package_remote_path=${remote_path}"

echo "[DEPLOY] ==build package=="
echo "[DEPLOY] install start..."
mvn clean install -Dmaven.test.skip=true -Dquarkus.profile=${build_prof} -f ${build_root}/pom.xml
echo "[DEPLOY] package start....."
mvn clean package -Dmaven.test.skip=true -Dquarkus.profile=${build_prof} -Dquarkus.package.type=uber-jar -f ${build_root}/${build_path}/pom.xml

echo "[DEPLOY] copy jar start..."
scp ${build_root}/${build_path}/target/${build_file} ${remote_host}:${remote_path}/app.jar

echo "[DEPLOY] ==deploy package=="
ssh -t -t ${remote_host} << EOF
    echo "[DEPLOY-REMOTE] remote deploy path....."
    cd ${remote_path}

    echo "[DEPLOY-REMOTE] shutdown jar application..."
    ps -ef | grep 'app.jar' | grep -v grep | awk '{print $2}' | xargs kill -9

    echo "[DEPLOY-REMOTE] start jar application..."
    /usr/local/share/jdk-17.0.1/bin/java -jar app.jar&

    exit
EOF

echo "[DEPLOY] ==deploy [${build_prof}] to [${remote_host}] finish=="