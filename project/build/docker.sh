#! /bin/bash
# ./docker.sh [-p|--profile] [-v|--version] [-b|--build] [-t|--type=jvm|native]
# ./docker.sh -p develop -v 1.0.0 --type native
set -e

build_prof="develop"
build_pack="1.0.0"
build_root="../.."
build_path="flower-api"
build_type="jvm"

package_type="jvm"
package_file="webapi-runner.jar"
package_args=""

echo "[docker] ==build params=="
while [ $# -gt 0 ];
do
   case $1 in
   -p|--profile)
       build_prof=$2
       shift 2
       ;;
   -v|--version)
       build_pack=$2
       shift 2
       ;;
   -b|--build)
       build_path=$2
       shift 2
       ;;
   -t|--type)
       build_type=$2
       shift 2
       ;;
   esac
done

case ${build_type} in
jvm)
  package_type="uber-jar"
  package_file="webapi-runner.jar"
  package_args=""
  ;;
native)
  package_type="native"
  package_file="webapi-runner"
  package_args=""
  echo "[docker] build reflection config"

  cd ../docker
  sh reflection-build.sh
  cd ../build
  ;;
esac

echo "[docker] build_profile=${build_prof}"
echo "[docker] build_version=${build_pack}"
echo "[docker] build_path=${build_path}"
echo "[docker] build_type=${build_type}"
echo "[docker] package_type=${package_type}"
echo "[docker] package_file=${package_file}"

echo "[docker] ==build package=="
echo "[docker] install start..."
mvn clean install -Dmaven.test.skip=true -Dquarkus.profile=${build_prof} -f ${build_root}/pom.xml
echo "[docker] package start..."
mvn clean package -Dmaven.test.skip=true -Dquarkus.profile=${build_prof} -Dquarkus.package.type=${package_type} ${package_args} -f ${build_root}/${build_path}/pom.xml

echo "[docker] ==docker build=="
cd ../docker/

echo "[docker] copy docker jar..."
cp ${build_root}/${build_path}/target/${package_file} ${package_file}

echo "[docker] build docker image"
docker build -t flower/webapi:${build_pack} -f Dockerfile.${build_type} .

echo "[docker] remove docker jar"
rm ${package_file}

echo "[docker] ==docker image=="
# save docker image
# docker save ${image_name}:${build_numb} > ${image_file}.tar

# copy docker image
# scp ${image_file}.tar root@${build_node}:/usr/local/webapi

# run image
# docker load -i [file.tar]
# docker run -id --name [name] -p 8080:9000 [-e "JAVA_OPTS=-Dsummer.generator.worker-id=0"] -d flower/webapi:{version}
