#! /bin/bash
# reflection-build.sh
set -e

proj_path=$(cd ../../$(dirname $0); pwd)
work_path="${proj_path}/flower-api/src/main/resources/"
work_file="${proj_path}/flower-api/src/main/resources/reflection-config.json"

echo "[native] copy origin config file"
cp -rf reflection-config.json ${work_path}

echo "[native] init config file"
sed -i '$d' ${work_file}
sed -i '$d' ${work_file}
echo "}," >> ${work_file}

echo "[native] entity path"
find_path="${proj_path}/flower-domain/src/main/java/"
find_sign="@Data"
cd ${find_path}

echo "[native] entity file"
for file in `find $1 -type f | grep .java | cut -c 3-`
do
  echo "[native] entity file ${find_path}${file}"
  if [ `grep -c "$find_sign" "${find_path}${file}"` -ne '0' ]
  then
    echo "{"                                                                                >> ${work_file}
    echo "  \"name\" : \"${file%.java}\"," | sed 's/\//./g'                                 >> ${work_file}
    echo "  \"allDeclaredConstructors\" : true,"                                            >> ${work_file}
    echo "  \"allPublicConstructors\" : true,"                                              >> ${work_file}
    echo "  \"allDeclaredMethods\" : true,"                                                 >> ${work_file}
    echo "  \"allPublicMethods\" : true,"                                                   >> ${work_file}
    echo "  \"allDeclaredFields\" : true,"                                                  >> ${work_file}
    echo "  \"allPublicFields\" : true"                                                     >> ${work_file}
    echo "},"                                                                               >> ${work_file}
  fi
done

echo "[native] application path"
find_path="${proj_path}/flower-application/src/main/java/"
find_sign="@Data"
cd ${find_path}

echo "[native] application file"
for file in `find $1 -type f | grep .java | cut -c 3-`
do
  echo "[native] application file ${find_path}${file}"
  if [ `grep -c "$find_sign" "${find_path}${file}"` -ne '0' ]
  then
    echo "{"                                                                                >> ${work_file}
    echo "  \"name\" : \"${file%.java}\"," | sed 's/\//./g'                                 >> ${work_file}
    echo "  \"allDeclaredConstructors\" : true,"                                            >> ${work_file}
    echo "  \"allPublicConstructors\" : true,"                                              >> ${work_file}
    echo "  \"allDeclaredMethods\" : true,"                                                 >> ${work_file}
    echo "  \"allPublicMethods\" : true,"                                                   >> ${work_file}
    echo "  \"allDeclaredFields\" : true,"                                                  >> ${work_file}
    echo "  \"allPublicFields\" : true"                                                     >> ${work_file}
    echo "},"                                                                               >> ${work_file}
  fi
done

echo "[native] remove last comma"
sed -i '$s/.$//' ${work_file}

echo "]"                                                                                    >> ${work_file}