-- --------------------------------------------------------
-- Host:                         101.200.53.244
-- Server version:               5.7.28 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for flower
CREATE DATABASE IF NOT EXISTS `flower` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `flower`;

-- Dumping structure for table flower.et_account
CREATE TABLE IF NOT EXISTS `et_account` (
  `id` bigint(20) NOT NULL,
  `name` varchar(64) NOT NULL COMMENT '用户名',
  `state` tinyint(1) unsigned NOT NULL COMMENT '状态',
  `salt` varchar(256) NOT NULL COMMENT '加盐',
  `secret` varchar(256) NOT NULL COMMENT '加密',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `expired_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='账号';

-- Data exporting was unselected.

-- Dumping structure for table flower.et_author
CREATE TABLE IF NOT EXISTS `et_author` (
  `id` bigint(20) NOT NULL,
  `name` varchar(32) NOT NULL COMMENT '名称',
  `email` varchar(64) NOT NULL COMMENT '手机',
  `avatar` varchar(256) DEFAULT NULL COMMENT '头像',
  `status` varchar(32) DEFAULT NULL COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='作者';

-- Data exporting was unselected.

-- Dumping structure for table flower.et_issue
CREATE TABLE IF NOT EXISTS `et_issue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) DEFAULT NULL COMMENT '标题',
  `contact` varchar(32) DEFAULT NULL COMMENT '联系方式',
  `content` varchar(256) DEFAULT NULL COMMENT '反馈内容',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='反馈建议';

-- Data exporting was unselected.

-- Dumping structure for table flower.et_label
CREATE TABLE IF NOT EXISTS `et_label` (
  `id` bigint(20) NOT NULL,
  `name` varchar(32) NOT NULL COMMENT '名称',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `using_count` int(11) NOT NULL DEFAULT '0' COMMENT '使用数',
  PRIMARY KEY (`id`),
  KEY `IDX_USER` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='标签';

-- Data exporting was unselected.

-- Dumping structure for table flower.et_schedule
CREATE TABLE IF NOT EXISTS `et_schedule` (
  `id` bigint(20) NOT NULL,
  `state` int(11) NOT NULL COMMENT '状态',
  `title` varchar(32) NOT NULL COMMENT '标题',
  `place` varchar(64) DEFAULT NULL COMMENT '位置',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `repeat_type` int(11) DEFAULT NULL COMMENT '重复类型',
  `repeat_item` varchar(4096) DEFAULT NULL COMMENT '重复项目',
  `repeat_prompt` varchar(128) DEFAULT NULL COMMENT '重复提示',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `finish_time` datetime DEFAULT NULL COMMENT '结束时间',
  `complete_rate` int(11) DEFAULT '0' COMMENT '完成比率',
  `complete_time` datetime DEFAULT NULL COMMENT '完成时间',
  `importance` tinyint(4) DEFAULT '0' COMMENT '重要度',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='日程';

-- Data exporting was unselected.

-- Dumping structure for table flower.et_schedule_event
CREATE TABLE IF NOT EXISTS `et_schedule_event` (
  `id` bigint(20) NOT NULL,
  `title` varchar(32) NOT NULL COMMENT '标题',
  `state` int(11) NOT NULL COMMENT '状态',
  `remark` varchar(2048) DEFAULT NULL COMMENT '备注',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `finish_time` datetime NOT NULL COMMENT '结束时间',
  `complete_time` datetime DEFAULT NULL COMMENT '完成时间',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `schedule_id` bigint(20) NOT NULL COMMENT '日程id',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `IDX_SCHEDULE` (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='日程事件';

-- Data exporting was unselected.

-- Dumping structure for table flower.et_schedule_label
CREATE TABLE IF NOT EXISTS `et_schedule_label` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label_id` bigint(20) NOT NULL COMMENT '标签id',
  `label_name` varchar(32) NOT NULL COMMENT '标签名称',
  `schedule_id` bigint(20) NOT NULL COMMENT '日程id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COMMENT='日程标签';

-- Data exporting was unselected.

-- Dumping structure for table flower.et_schedule_timeline
CREATE TABLE IF NOT EXISTS `et_schedule_timeline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL COMMENT '标题',
  `phase` varchar(32) NOT NULL COMMENT '阶段',
  `category` varchar(32) NOT NULL COMMENT '类型',
  `target_id` bigint(20) NOT NULL COMMENT '目标',
  `user_id` bigint(20) NOT NULL COMMENT '用户',
  `schedule_id` bigint(20) NOT NULL COMMENT '日程',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COMMENT='时间线';

-- Data exporting was unselected.

-- Dumping structure for table flower.gb_calendar
CREATE TABLE IF NOT EXISTS `gb_calendar` (
  `id` bigint(20) NOT NULL,
  `name` varchar(8) NOT NULL COMMENT '名称：日期或节气',
  `sign` varchar(4) NOT NULL COMMENT '标记：休 班 气',
  `title` varchar(32) NOT NULL COMMENT '标题：农历全程',
  `remark` varchar(64) DEFAULT NULL COMMENT '备注',
  `on_date` date NOT NULL COMMENT '所在日期',
  `on_week` int(11) NOT NULL COMMENT '所在星期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统日历';

-- Data exporting was unselected.

-- Dumping structure for table flower.gb_district
CREATE TABLE IF NOT EXISTS `gb_district` (
  `id` bigint(20) NOT NULL,
  `code` varchar(8) NOT NULL COMMENT '编码',
  `name` varchar(36) NOT NULL COMMENT '名称',
  `title` varchar(128) NOT NULL COMMENT '全称',
  `type` tinyint(1) unsigned NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='行政区域';

-- Data exporting was unselected.

-- Dumping structure for table flower.gb_general
CREATE TABLE IF NOT EXISTS `gb_general` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL COMMENT '编码',
  `name` varchar(32) NOT NULL COMMENT '名称',
  `type` varchar(32) NOT NULL COMMENT '类型',
  `data` varchar(1024) NOT NULL COMMENT '数据',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10004 DEFAULT CHARSET=utf8mb4 COMMENT='通用数据';

-- Data exporting was unselected.

-- Dumping structure for table flower.gb_operation
CREATE TABLE IF NOT EXISTS `gb_operation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(64) NOT NULL COMMENT '行为',
  `result` char(1) NOT NULL COMMENT '结果',
  `target_id` varchar(32) DEFAULT NULL COMMENT '目标编码',
  `action_id` bigint(20) NOT NULL COMMENT '行为用户',
  `action_time` datetime NOT NULL COMMENT '行为时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8mb4 COMMENT='操作日志';

-- Data exporting was unselected.

-- Dumping structure for table flower.gb_validation
CREATE TABLE IF NOT EXISTS `gb_validation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `target` varchar(256) NOT NULL COMMENT '用户码',
  `auth_code` varchar(256) NOT NULL COMMENT '认证码',
  `auth_category` varchar(32) NOT NULL COMMENT '认证类别',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `expired_time` datetime NOT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COMMENT='验证码';

-- Data exporting was unselected.

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
