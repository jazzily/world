DELETE FROM et_account;
DELETE FROM et_author;
DELETE FROM et_issue;
DELETE FROM et_label;
DELETE FROM et_schedule;
DELETE FROM et_schedule_event;
DELETE FROM et_schedule_label;

DELETE FROM et_schedule_timeline;
DELETE FROM gb_operation;
DELETE FROM gb_validation;